# Installation and Running
To run the application in a unix-like environment, run the following command

    ./pic config.cfg
where `config.cfg` is the name of the configuration file that sets up the simulation. Note that the file extension is irrelevant, the only thing that matters is that the file is plain text.

# Configuration Options
The configuration file describes the set of options that are required to run a simulation. Each of the options are
described and possible values are presented and explained.

## Syntax Notes

The configuration options can come from three separate sources -- listed below in the order of priority (if the same option is specified from multiple sources, the value from the source that comes first on this list will be the one that is used)
1. Command line arguments (TODO: Example)
2. Plain text configuration file which gets passed into the application (as a filePath) as the first command line argument./
3. Default configuration options that come from a plain text file called `default.cfg` in the same directory as the application.

In general, options are NOT case sensitive, the only exception being file paths.

When an option takes multiple arguments, the arguments are comma-delimited. If an argument is a vector, then the vector
is enclosed in parentheses and separated with semi-colons. See the following example

        setParticleBC=right, open, (1;0)
The `setParticleBC` is passed 3 arguments: `right`, `open`, and the 2d-vector `(1,0)`

The next section outlines each option available to the configuration file. The following notes are important to
understanding how the options are outlined.

When an option is written as two terms separated by a period, it means that the option (the second term) must be in a
section labeled with the first term. For example, `InteriorProperties.bField` would look like the following in a
configuration file:

    [InteriorProperties]
        bField=tests/csvs/fieldTest.csv
Although the label and the option will be included for each option below, the label only needs to be included once, before the options that utilize it.

Note that if an option is marked "(Optional)" then it means that this option can be excluded from the config file and
a default value will be used instead. The default value is outlined in the option description.

## Timing
This section outlines the timing control settings for the simulation.
#### Simulation Timestep - `dt`
`dt` is the timestep of the simulation. It should be less than one tenth the smallest plasma period of your simulation
in order to capture all important particle motion. The option takes a single floating-point number such as

    dt = .01
#### Simulation End Time - `T`
`T` is the end time of the simulation. The simulation always starts at time=0 and progresses by an amount `dt` at
each timestep until it reaches or exceeds the time `T`. The option takes a single floating-point number such as

    T = 10

## Grid Characteristics
This section outlines the properties associated with the background grid of the simulation. The background grid stores
the values of the electric and magnetic fields, particle densities and mediates interpolation to and from the simulation
particles.
#### Type of Background Grid (Optional) - `gridType`
`gridType` specifies the type of grid to be used. There are two possible values for a grid


Type           | Description
---            | ---
`Basic`        | Basic Cartesian background grid
`Neutralizing` | Background grid that has a fixed background charge density that is the negative of the starting charge density of the grid. TODO: Common usage

If not included in the configuration file, the default value of `gridType` is `Basic`. Example usage:

    gridType = Neutralizing

#### Mesh File Path - `meshFile`
`meshFile` specifies the file path of the GMSH mesh that describes the Cartesian grid to be used for the background grid of the simulation. The file path can be absolute or relative to the directory from which the program is launched (TODO - Check this out). Example usage:

    meshFile = tests/meshes/2Dsingleblockmesh.msh

#### Domain Characteristics - `InteriorProperties.setDomain`
`InteriorProperties.setDomain` allows you to define interior domain characteristics. This option can be called more then once, and should have an entry for each domain specified by the mesh file. The `setDomain` option takes three arguments

    setDomain = Label Name, Particles Permitted?, Dielectric Constant

The arguments are explained in the following table
| Index | Argument | Description | Possible Values |
| --- |--- | --- | --- |
| 0 | Label Name | The label name of the region (needs to match the mesh element labels) | Any string |
| 1 | Particles Permitted? | Are Particles allowed in this region of the domain | `Yes` or `No` |
| 2 | Dielectric Constant | The dielectric constant of this region of the domain. Units are up to the user | Non-zero positive float |
Example usage:

    [InteriorProperties]
        setDomain = interior, Yes, 1

#### Setting Background Magnetic Field (Optional) - `InteriorProperties.bField`
`InteriorProperties.bField` allows the user to set a background magnetic field for the simulation that is constant in time but may vary in space. The user may supply the path of a *.csv file which specifies a magnetic field vector at each node point of the grid, or they may specify a single vector to be used at each node.

Example usage for a file path (spatially varying magnetic field):

    [InteriorProperties]
        bField = tests/csvs/fieldTest.csv

Example usage for a spatially-constant magnetic field vector:

    [InteriorProperties]
        bField = (1;0.5;-1)

Note that regardless of the dimension of the simulation, a 3D magnetic field is expected. If a magnetic field of lower dimension is supplied (from the constant vector or the *.csv file) then the remaining dimensions are assumed to be 0. For example, if the B-Field is specified as `bField = (1;0)`, then this will be interpreted as the vector `(1,0,0)`.

#### Field Boundary Conditions - `BoundaryConditions.setFieldBC`
`BoundaryConditions.setFieldBC` allows the user to define grid boundary conditions (BCs) for the solution to Poission's equation for the electric potential in the domain. The `setFieldBC` option takes 3+ arguments defined as follows

    setFieldBC = Label Name, BC Type, Boundary Normal, Parameters...

Each argument is explained in the following table:
| Index | Argument | Description | Possible Values |
| --- |--- | --- | --- |
| 0 | Label Name | The label name of the region (needs to match the mesh element labels) | Any string |
| 1 |  BC Type | The type of boundary condition to apply to this boundary of the grid | `Dirichlet`, `Time Dependent Dirichlet`, `Neumann`, `Zero Field`, `Plasma Dielectric Interface`, `Periodic`|
| 2 | Boundary Normal | The outward pointing normal of the boundary. | A vector specifying a principle Cartesian direction |
| 3 | Parameters...  | Additional Parameters needed by the boundary condtion specified | Depends on BC type, see table below  |

The table below outlines how each boundary condition should be used and which parameters are needed.
|BC Type | Description | Parameters |
|--- | --- | --- |
| `Dirichlet` | Set the boundary at a fixed electric potential | 1 float -- Defines the electric potential at the boundary |
| `Time Dependent Dirichlet` | Set the boundary to a time-varying electric potential  | 1 string -- File path of a *.csv file which includes a floating point value for each timestep in the simulation. At each timestep the boundary will be fixed to the corresponding value from the file.  |
| `Neumann` | Set the derivative of the electric potential normal to the wall to a fixed value  | 1 float -- Defines the derivative of the electric potential normal to the wall |
| `Zero Field` | Forces the  electric field to be zero at the boundary  | N/A |
| `Plasma Dielectric Interface` | Defines a boundary that is an interface between a dielectric material and a plasma. Note that the outward normal of this condition should be pointing away from the plasma  | 2 floats -   The dielectric constant of the plasma followed by the dielectric constant of the  dielectric (comma-separated) |
| `Periodic` | Set this boundary to be periodic in the potential. Should be used with another periodic condition on the opposite boundary  | N/A |

Example usage:

    [BoundaryConditions]
        setFieldBC = top, zero field, (0;1)
        setFieldBC = bottom, neumann, (0;-1), 1
        setFieldBC = right, time dependent dirichlet, (1;0), tests/csvs/1dTest.csv
        setFieldBC = left, dirichlet, (-1;0), 0

#### Particle Boundary Conditions - `BoundaryConditions.setParticleBC`

`BoundaryConditions.setParticleBC` allows the user to define grid boundary conditions (BCs) for particles that come into contact with the boundaries. The `setParticleBC` option takes 3+ arguments defined as follows

    setParticleBC = Label Name, BC Type, Boundary Normal, Parameters...

Each argument is explained in the following table:
| Index | Argument | Description | Possible Values |
| --- |--- | --- | --- |
| 0 | Label Name | The label name of the region (needs to match the mesh element labels) | Any string |
| 1 |  BC Type | The type of boundary condition to apply to this boundary of the grid | `Open`, `Reflecting`, `Scattering`, `Absorbing`, `Periodic`, `Solid`|
| 2 | Boundary Normal | The outward pointing normal of the boundary. | A vector specifying a principle Cartesian direction |
| 3 | Parameters...  | Additional Parameters needed by the boundary condtion specified | Depends on BC type, see table below  |

The table below outlines how each boundary condition should be used and which parameters are needed.
|BC Type | Description | Parameters |
|--- | --- | --- |
| `Open` | Allows particles to be lost from the simulation once they cross this boundary | N/A |
| `Reflecting` | Specular reflection of the particle off the boundary  | 1 float -- Coefficient of restitution |
| `Scattering` | Non-Specular reflection of the particle off the boundary | 1 float -- Coefficient of restitution |
| `Absorbing` | Absorb the particle to the wall, contributes to surface charge density | N/A |
| `Periodic` | Makes the particle reappear on the opposite of the domain with the same velocity. Best used if the opposite wall is also labeled `Periodic`  | N/A |
| `Solid` | Condition that combines `Absorbing` with the emission of secondary electrons and ions  | 12 floats - Electron roughness factor (0-2), Electron gamma at zero energy Emax normal incidence (eV), Electron Energy at gamma max normal, Electron gammaMax normal direction, Electron emission threshold (electron affinity)(eV), Electron crossover energy (eV), Ion Emax normal incidence (eV), Ion gammaMax normal direction, Ion emission threshold (eV), Ion power law coefficient, Electron mass (for secondary electrons), Electron charge (for secondary electrons)|

Example usage:

    [BoundaryConditions]
        setParticleBC = right, open, (1;0)
        setParticleBC = left, reflecting, (-1;0), 1
        setParticleBC = top, scattering, (0;1), 0.5
        setParticleBC = bottom, absorbing, (0;-1)

#### Type of Grid Interpolation (Optional) - `interpolatorType`
`interpolatorType` specifies the type of grid interpolation to be used. There is currently only one option for the interpolator although more options may come in the future.


Type           | Description
---            | ---
`Linear`        | Linear interpolation scheme (TODO: Expression for interpolation?)

If not included in the configuration file, the default value of `interpolatorType` is `Linear`. Example usage:

    interpolatorType = Linear

#### Method of Solving Poisson's Equation (Optional) - `fieldSolverType`
`fieldSolverType` specifies the type of grid to be used. There are two possible values for a grid


Type           | Description
---            | ---
`Direct`        | Direct solver using Eigen's (TODO: Link) built-in matrix solver. This is the fastest choice for 1D and 2D applications
`Multigrid` | Algebraic Multigrid solver based on amcgl (TODO: Link). Best choice for 3D problems

If not included in the configuration file, the default value of `fieldSolverType` is `Direct`. Example usage:

    fieldSolverType = Multigrid

## Particle Characteristics

#### Type of Particle Integrator (Optional) - `particleMoverType`
`particleMoverType` specifies the type of integrator to move particles in the simulation. There is currently only one option for the particle mover although more options may come in the future.


Type           | Description
---            | ---
`Leap Frog`        | 2nd order Leap-Frog integration scheme (TODO: Link to wiki page?)

If not included in the configuration file, the default value of `particleMoverType` is `Leap Frog`. Example usage:

    particleMoverType = Leap Frog

#### Add Particle Species Description - `Particles.addParticle`
`Particles.addParticle` allows the user to define a new type of particle used in the simulation. The option takes three arguments, a string for the particle name, and two floats for the mass and charge of the particle

    addParticle = Name, Mass, Charge

An example usage is

    [Particles]
        addParticle=electron, 1E-31, -1E-19
#### Species Initialization Settings - `Particles.initialize`
`Particles.initialize` allows the user to define how the particles of a particular species are to initialized in the simulation. The `initialize` option takes 2+ arguments depending on the type of initialization

    initialize = Species Name, Initialization Type, Parameters...

The name defines which particle is being initialized, the type is the method of initialization to be used and the parameters depend on the the initialization type. There are currently three types of initialization, as described in the following table

|Init Type | Description | Parameters |
|--- | --- | --- |
| `Total` | Specifies the total number of particles to be intialized in the domain. The particles are uniformly distributed in the permissible regions of the domain | 1 Int -- The total number of particles to be initialized |
| `Density`   | Specifies the density of particles to initialized in the domain. This has units of [# sim particles] / [volume]  |  1 float -- The particle density in the domain |
| `Quiet Start` | Creates Gaussian distributions of particles that are placed in a regular subgrid. This reduces the starting potential energy of the simulation  | 2 N-D Vecotors (where N is the dimension of the grid) -- The first specifies the number of spatial samples per cell in each dimension. The second specifies the number of velocity samples in each dimension. The total number of particles will be the product of the spatial samples vector multiplied by the product of the velocity samples vector multiplied by the number of permissible grid cells |

Some example usages are

    [Particles]
        initialize = electron, total, 100
        initialize = ion1, density, 10.5
        initialize = ion2, quiet start, (10;11), (10;20)

#### Species Velocity Distribution `Particles.velocity_distribution`
`Particles.velocity_distribution` allows the user to define which underlying velocity distribution a particular particle species has. The distribution is sampled when initializing the particles. The `velocity_distribution` option takes 3+ arguments depending on the type distribution selected.

    velocity_distribution = Species Name, Dimensions, Distribution Type, Parameters...

The `Species Name` defines which species the distribution is defined for. `Dimensions` is a string that contains any combination of `x`, `y`, and `z` and defines which dimension(s) this velocity distribution applies to. There are curently 2 supported types of Distributions and they are defined in the following table:

|Distribution Type | Description | Parameters |
|--- | --- | --- |
| `Uniform` | Specifies a uniform velocity distribution | 2 floats -- minimum velocity followed by maximum velocity |
| `Normal` | Specifies a Gaussian velocity distribution | 2 floats -- The mean followed by the standard deviation  |

Some example usages are

    [Particles]
        velocity_distribution=electron, xy, normal, 0.1, .3
        velocity_distribution=ion, xy, normal, -0.5, .2
        velocity_distribution=ion2, x, normal, -0.5, .2
        velocity_distribution=ion2, y, uniform, -0.1, .1


## Physics
TODO

## Outputs

#### Output Interval (Optional) - `Output.interval`
`Output.interval` defines the default write interval of the output files in terms of iterations. If a data dump is desired every other iteration then set `iteration = 2`. If no interval is specified, the default value is `1`. An example usage is

    [Output]
        interval = 13

This option will be overridden by the property specified in an individual output entry.

#### Output Start Time (Optional) - `Output.start time`
`Output.start time` defines the default start time of the data output. Data will not be written to disk for times less than this value. If no start time is specified a default value of `0` is used. An example usage is

    [Output]
        start time = 1

This option will be overridden by the property specified in an individual output entry.

#### Output End Time (Optional) - `Output.end time`
`Output.end time` defines the default end time of the data output. Data will not be written to disk for times greater than this value. If no end time is specified a default value of `Infinity` is used. An example usage is

    [Output]
        end time = 100

This option will be overridden by the property specified in an individual output entry.

#### Output Directory (Optional) - `Output.directory`
`Output.directory` defines the default output directory in which any output files are stored. The path can be relative (to the directory that the application is launched from) or it can be a complete path. If a path is not specified then the output directory is the relative path `./output/`. An example usage is

    [Output]
        directory = ../test_output/

This option will be overridden by the property specified in an individual output entry.

#### Output to Timestamped Folder (Optional) - `Output.timestamp folder`
`Output.timestamp folder` is a boolean that specifies whether the output data will be placed in a subfolder of the output directory that is labeled with the timestamp of the simulation. If not specified this is turned on by default.

    [Output]
        timestamp folder = false

This option will be overridden by the property specified in an individual output entry.

#### Output File Extension (Optional) - `Output.file extension`
`Output.file extension` defines the default file extension of the output files. When specifying this property, use the file extension string including the '.' (e.g. use ".csv" for *.csv files). If no file extension is specified then ".csv" is used. An example usage for plain text files would be

    [Output]
        file extension = .txt

If no file extension is desired, leave the RHS of the equals sign blank like:

    [Output]
        file extension =
This option will be overridden by the property specified in an individual output entry.

#### Output Filename Suffix (Optional) - `Output.suffix`
`Output.suffix` defines the default type of suffix used when naming output files. There are currently 2 output options
Type           | Description
---            | ---
`Time`        | Uses the current simulation time as the file suffix
`Iteration` | Uses the currect simulation iteration count as the file suffix

If no suffix is specified then the suffix `Iteration` is used. An example usage is

    [Output]
        suffix = Time

This option will be overridden by the property specified in an individual output entry.

#### Output Precision (Optional) - `Output.precision`
`Output.precision` defines the default floating point precision of the output data. The precision is an integer that defines the number of significant digits in the output. See std::setprecision documentation for a more detailed explanation of how the precision integer is used. If no precision is set then the default value is the maximum precision available to the floating point type. An example usage is

    [Output]
        precision = 6

This option will be overridden by the property specified in an individual output entry.

#### Output Data to Write (Optional) - `Output.write`
`Output.write` defines a single output configuration for a specified target. The user can define as many such configurations as desired (effective duplicates will be ignored). The format of this option is

    write = target, option1=value1, option2=value2, ...

where `target` specifies the data to be output can have the following values
Target           | Description
---            | ---
`Position` | Particle Positions (One row for each particle, one file per timestep)
`Velocity` | Particle Velocity (One row for each particle, one file per timestep)
`Species_Traits` | Characteristics of each species (One row for each type of particle, one file output for full sim)
`Species_Count`  | The total number of particles of each species at the given timestep (One row for each timestep)
`Electric_Field` | Electric Field On Grid (One row for each grid point, one file per timestep)
`Magnetic_Field` | Magnetic Field On Grid (One row for each grid point, one file per timestep)
`Potential` | Electric Potential On Grid (One row for each grid point, one file per timestep)
`Charge_Density` | Charge Density on Grid (One row for each grid point, one file per timestep)
`Surface_Charge_Density` | Surface charge density wherever it is stored (One row for each stored grid point, one file per timestep)
`Kinetic_Energy` | Total Kinetic Energy in the simulation (One row for each timestep)
`Potential_Energy` | Total Potential Energy in the simulation (One row for each timestep)
`Total_Energy` | Total Energy in the simulation (One row for each timestep)


The `option=value` pairs are additional options for the output setup. The order in which they are listed does not matter. The user has access to the following options
Option           | Description | Value Type | Default |
---            | --- | ---- | ---- |
`interval`  | The write interval of the output (i.e. how many simulation iterations between writes)  | Integer | `1` |
| `prefix`  | The prefix of the output file  | String  | The full name of the target (e.g. `Electric_Field` for the `Electric_Field` target)  |
| `start time`   | Start time of the output. No output will be recorded before the simulation reaches this time  | Floating Point Number  | `0`  |
| `end time`  | End time of the output. No output will be recorded after the simulation reaches this time  | Floating Point Number  | `Infinity` |
| `indices`  | The indices of the nodes or particles to write. If empty, it is assumed that all indices should be used  | Vector of Integers  | `()` (Empty Vector)  |
| `directory`   | The output directory for this entry  | String  | `./output/`  |
| `timestamp folder`   | Whether or not to nest the output files in a folder marked with a timestamp (within the directory specified)  | Boolean  | `True`  |
| `file extension`   | The file extension for this entry  | String  | `.csv`  |
| `suffix`  | The suffix style for this entry (either `Time` or `Iteration`)  | String  | `Iteration`  |
| `precision`  | The level of precision used when writing floating point numbers  | Integer  | Maximum Precision (system dependent)  |


Example usages are below

    write = Position, indices=(1;10;100)
    write = Velocity, prefix="vel", interval=13, start time=1, end time=10
    write = Species_Traits, directory = ../tests/, file extension = .txt
    write = Electric_Field, prefix = "efield", interval=2
    write = magnetic_field, precision=6, suffix=time, timestamp folder=false
#### Automated Analysis - `Output.analysis`
Whether or not the default analysis suite should be used to automatically analyze the results of the simulation at the end of the simulation
An example usage is given below, by default the value is `false`

    analysis = true

#### Comments for the simulation - `Output.comment`
A set of strings compiled from all input sources that are stored and written to the log file. These comments are only used to help organize simulation runs and have no purpose in the application. An example would be

    comment = This simulation is a test of neutral collisions

Note that the user may enter as many separate comments as desired and all will be recorded.
#### Log file destination - `Output.logFile`
The location of a log file where the settings of each simulation are recorded for easy search-ability and future reference. An example would be

    logFile = /usr/anthony/Drobox/Sims/log.csv

