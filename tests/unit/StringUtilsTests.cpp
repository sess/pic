#include "../catch.hpp"
#include "IOUtils.h"

using namespace picard;

TEST_CASE("Lower Case", "[string_utils]") {
    REQUIRE(to_lower("aABb_Cc") == "aabb_cc");
    REQUIRE(to_lower("!@#$%^&*()_+{}<?>,/']['") == "!@#$%^&*()_+{}<?>,/']['");
    REQUIRE(lowerEq("abC", "abc"));
    REQUIRE(!lowerEq("abs", "abc"));
}

TEST_CASE("Add Slash", "[string_utils]"){
    REQUIRE(addSlash("abc") == "abc/");
    REQUIRE(addSlash("abc/") == "abc/");
}

TEST_CASE("Character Dimension", "[string_utils]") {
    REQUIRE(dimToChar(0) == 'x');
    REQUIRE(dimToChar(1) == 'y');
    REQUIRE(dimToChar(2) == 'z');
    REQUIRE(dimToChar(3) == '?');

    REQUIRE(charToDim('x') == 0);
    REQUIRE(charToDim('y') == 1);
    REQUIRE(charToDim('z') == 2);
    REQUIRE(charToDim('o') == -1);

}

TEST_CASE("Contains character", "[string_utils]") {
    std::string s1 = "ABC";
    std::string s2 = "abc";

    REQUIRE(contains(s1, 'A'));
    REQUIRE(contains(s2, 'A'));
    REQUIRE(!contains(s2, 'z'));
}

TEST_CASE("Tokenize string", "[string_utils]") {
    std::string str = " 123 ,(abc);;,(1;3;4),   0 \t";
    auto res1 = tokenize(str, ",", " \t");
    auto res2 = tokenize(str, ";", "()");
    auto res3 = tokenize(str, " ", "\t");
    auto res4 = tokenize(str, "|", " \t");
    auto res5 = tokenize(str, "|", "");
    auto res6 = tokenize(str, "$", "");

    REQUIRE(res1 == std::vector<std::string>({"123", "(abc);;", "(1;3;4)", "0"}));
    REQUIRE(res2 == std::vector<std::string>({" 123 ,(abc", ",(1", "3", "4),   0 \t"}));
    REQUIRE(res3 == std::vector<std::string>({"123", ",(abc);;,(1;3;4),", "0"}));
    REQUIRE(res4 == std::vector<std::string>({"123 ,(abc);;,(1;3;4),   0"}));
    REQUIRE(res5 == std::vector<std::string>({str}));
    REQUIRE(res6 == std::vector<std::string>({" 123 ,(abc);;,(1;3;4),   0 \t"}));
    REQUIRE(tokenize("") == std::vector<std::string>({}));

    REQUIRE(tokenizeVector<std::string>("abc") == std::vector<std::string>({"abc"}));
    REQUIRE(tokenizeVector<std::string>("abc;def") == std::vector<std::string>({"abc","def"}));
    REQUIRE(tokenizeVector<std::string>("(abc;def)") == std::vector<std::string>({"abc","def"}));
}

TEST_CASE("Convert strings to reals", "[string_utils]") {
    std::vector<std::string> numbers = {"1.1", "2.2", "3.3", "0", "-1", "1.32423"};
    auto res1 = vcast<Real>(numbers);
    std::vector<std::string> bad_numbers = {"a", "4,4", "tor"};
    REQUIRE_THROWS(vcast<Real>(bad_numbers));
    REQUIRE(feq(res1, std::vector<Real>({1.1, 2.2, 3.3, 0, -1, 1.32423})));
}
