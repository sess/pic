#include "IOUtils.h"
#include "../catch.hpp"
#include "Output.h"
#include "Simulation.h"

using namespace picard;
using namespace std;

TEST_CASE("OutputEntry should write", "[output]"){
    OutputEntry pos = {POSITION, 3, "pos", 1, 10.5};

    REQUIRE(pos.shouldWrite(0, 1) == true);
    REQUIRE(pos.shouldWrite(0, 0) == false);
    REQUIRE(pos.shouldWrite(1, 1) == false);
    REQUIRE(pos.shouldWrite(3, 6) == true);
    REQUIRE(pos.shouldWrite(3, 10.5) == true);
    REQUIRE(pos.shouldWrite(3, 11) == false);
    REQUIRE(pos.shouldWrite(4, 11) == false);
}

TEST_CASE("OutputEntry use suffix", "[output]"){
    REQUIRE(OutputEntry(POSITION).useSuffix() == true);
    REQUIRE(OutputEntry(VELOCITY).useSuffix() == true);
    REQUIRE(OutputEntry(SPECIES_COUNT).useSuffix() == false);
    REQUIRE(OutputEntry(ELECTRIC_FIELD).useSuffix() == true);
    REQUIRE(OutputEntry(MAGNETIC_FIELD).useSuffix() == true);
    REQUIRE(OutputEntry(POTENTIAL).useSuffix() == true);
    REQUIRE(OutputEntry(CHARGE_DENSITY).useSuffix() == true);
    REQUIRE(OutputEntry(SURFACE_CHARGE_DENSITY).useSuffix() == true);
    REQUIRE(OutputEntry(KINETIC_ENERGY).useSuffix() == false);
    REQUIRE(OutputEntry(POTENTIAL_ENERGY).useSuffix() == false);
    REQUIRE(OutputEntry(TOTAL_ENERGY).useSuffix() == false);
    REQUIRE(OutputEntry(SPECIES_TEMPERATURE).useSuffix() == false);
    REQUIRE(OutputEntry(SPECIES_VELOCITY).useSuffix() == false);
}

TEST_CASE("OutputEntry repeat check", "[output]"){
    Output o;
    o.add(OutputEntry(POSITION, 10, "pos", 1, 10, {1,5,9}, "dir", ".b", TIME, 13, false));
    o.add(OutputEntry(VELOCITY));
    o.add(OutputEntry(SPECIES_COUNT));

    REQUIRE(o.isRepeat(OutputEntry(POSITION)) == false);
    REQUIRE(o.isRepeat(OutputEntry(VELOCITY)) == true);
    REQUIRE(o.isRepeat(OutputEntry(SPECIES_COUNT)) == true);
    REQUIRE(o.isRepeat(OutputEntry(POSITION, 10, "pos", 1, 10, {1,9})) == false);
    REQUIRE(o.isRepeat(OutputEntry(POSITION, 10, "position", 1, 10, {1,5,9})) == false);
    REQUIRE(o.isRepeat(OutputEntry(POSITION, 10, "pos", 1, 10, {1,5,9}, "dir", ".b", TIME, 13, false)) == true);
    REQUIRE(o.isRepeat(OutputEntry(POSITION, 10, "pos", 1, 11, {1,5,9}, "dir", ".b", TIME, 13, true)) == false);
    REQUIRE(o.isRepeat(OutputEntry(POSITION, 10, "pos", 1, 11, {1,5,9}, "dir", ".b", TIME, 13, false)) == true);
    REQUIRE(o.isRepeat(OutputEntry(POSITION, 10, "pos", 0, 10, {1,5,9}, "dir", ".b", TIME, 13, false)) == true);
    REQUIRE(o.isRepeat(OutputEntry(SPECIES_COUNT, 10, "pos", 0, 10, {1,5,9}, "dir", ".b", TIME, 13, false)) == false);
}

TEST_CASE("OutputEntry filePath settings", "[output]"){
    OutputEntry o(POSITION);
    o.prefix = "pos";
    o.directory = "csvs/";
    o.fileExtension = ".txt";
    o.precision = 3;
    o.suffix = TIME;

    Options opt;
    opt.t = 13.5432;
    opt.iteration = 33;
    opt.verbose = false;

    Simulation s(opt);

    REQUIRE(o.filePath(s) == "csvs/data/pos_t13.543200.txt");
    REQUIRE(o.filePath() == "csvs/data/pos.txt");

    o.suffix = ITERATION;
    REQUIRE(o.filePath(s) == "csvs/data/pos_i33.txt");

}

TEST_CASE("Species Count", "[output]"){
    // TODO
}

TEST_CASE("Output positions", "[output]"){
    Options o("tests/config/two_particle.cfg");
    Simulation s(o);

    // Get the starting positions to compare later
    vector2d<string> startingPositions;
    for(auto& species : s.particles()){
        for(auto& p : species.second){
            auto new_row = concatenate(vector<string>({cast<string>(p.speciesId())}), vcast<string>(p.r));
            push_back(startingPositions, new_row);
        }
    }

    s.setVerbose(false);
    s.run();

    // Get ending positions
    vector2d<string> endingPositions;
    for(auto& species : s.particles()){
        for(auto& p : species.second){
            auto new_row = concatenate(vector<string>({cast<string>(p.speciesId())}), vcast<string>(p.r));
            push_back(endingPositions, new_row);
        }
    }

    //Check that all the relevant files were produces
    for(int i=0; i<10; i+=5){
        std::string fileName = "./output/data/rank0_Position_i" + to_string(i) + ".csv";
        // Check that the relevant file exists
        REQUIRE(exists(fileName));

        // file has the right number of particles
        auto vec = parse2DVector<string>(fileName, true);
        REQUIRE(vec.rows() == 2);
        REQUIRE(vec.cols() == 3);
    }

    //Check that the starting and ending positions match up
    auto startMatch = (startingPositions == parse2DVector<string>("./output/data/rank0_Position_i0.csv", true));
    REQUIRE(startMatch);
    auto endMatch = (endingPositions == parse2DVector<string>("./output/data/rank0_Position_i10.csv", true));
    REQUIRE(endMatch);

    s.cleanupOutput();
}

TEST_CASE("Output velocities", "[output]"){
    Options o("tests/config/two_particle.cfg");
    Simulation s(o);

    // Get the starting positions to compare later
    vector2d<string> startingVelocities;
    for(auto& species : s.particles()){
        for(auto& p : species.second){
            auto new_row = concatenate(vector<string>({cast<string>(p.speciesId())}), vcast<string>(p.v));
            push_back(startingVelocities, new_row);
        }
    }

    s.setVerbose(false);
    s.run();

    // Get ending positions
    vector2d<string> endingVelocities;
    for(auto& species : s.particles()){
        for(auto& p : species.second){
            auto new_row = concatenate(vector<string>({cast<string>(p.speciesId())}), vcast<string>(p.v));
            push_back(endingVelocities, new_row);
        }
    }

    // Check that all the relevant files were produces
    for(int i=0; i<10; i++){
        std::string fileName = "./output/data/rank0_vel_i" + to_string(i) + ".csv";
        // Check that the relevant file exists
        REQUIRE(exists(fileName));

        // file has the right number of particles
        auto vec = parse2DVector<string>(fileName, true);
        REQUIRE(vec.rows() == 2);
        REQUIRE(vec.cols() == 3);
    }

    // Check that the starting and ending positions match up
    auto startMatch = (startingVelocities == parse2DVector<string>("./output/data/rank0_vel_i0.csv", true));
    auto endMatch = (endingVelocities == parse2DVector<string>("./output/data/rank0_vel_i10.csv", true));
    REQUIRE(startMatch);
    REQUIRE(endMatch);

    s.cleanupOutput();
}

TEST_CASE("Output species traits", "[output]"){
 // TODO
}

TEST_CASE("Output electric field", "[output]"){
    Simulation s("tests/config/two_particle.cfg");
    s.setVerbose(false);
    s.run();

    // Check that all the relevant files were produces
    for(int i=0; i<10; i++){
        std::string fileName = "./output/data/Electric_Field_t" + to_string(s.timestep()*i) + ".csv";
        // Check that the relevant file exists
        REQUIRE(exists(fileName));

        // file has the right number of particles
        auto vec = parse2DVector(fileName, true);
        REQUIRE(vec.rows() == 100);
        REQUIRE(vec.cols() == 2);
    }

    s.cleanupOutput();
}

TEST_CASE("Output magnetic field", "[output]"){
    Simulation s("tests/config/two_particle.cfg");
    s.setVerbose(false);
    s.run();

    // Check that all the relevant files were produces
    for(int i=0; i<10; i++){
        std::string fileName = "./output/data/Magnetic_Field_i" + to_string(i) + ".csv";
        if(i >= 5) {
            // Check that the relevant file exists
            REQUIRE(exists(fileName));

            // file has the right number of particles
            auto vec = parse2DVector(fileName, true);
            REQUIRE(vec.rows() == 100);
            REQUIRE(vec.cols() == 3);
        } else{
            REQUIRE(!exists(fileName));
        }
    }

    s.cleanupOutput();
}

TEST_CASE("Output potential", "[output]"){
    Simulation s("tests/config/two_particle.cfg");
    s.setVerbose(false);
    s.run();

    // Check that all the relevant files were produces
    for(int i=0; i<10; i++){
        std::string fileName = "./output/data/Potential_i" + to_string(i) + ".csv";
        if(i <= 5) {
            // Check that the relevant file exists
            REQUIRE(exists(fileName));

            // file has the right number of particles
            auto vec = parse2DVector(fileName, true);
            REQUIRE(vec.rows() == 100);
        } else{
            REQUIRE(!exists(fileName));
        }
    }
    s.cleanupOutput();
}

TEST_CASE("Output charge density", "[output]"){
    Simulation s("tests/config/two_particle.cfg");
    s.setVerbose(false);
    s.run();

    std::vector<int> indices = {3,6,9};
    // Check that all the relevant files were produces
    for(auto i : indices){
        std::string fileName = "./output/data/Electric_Field_t" + to_string(s.timestep()*i) + ".csv";
        // Check that the relevant file exists
        REQUIRE(exists(fileName));

        // file has the right number of particles
        auto vec = parse2DVector(fileName, true);
        REQUIRE(vec.cols() == 2);
        REQUIRE(vec.rows() == 100);
    }

    s.cleanupOutput();
}

TEST_CASE("Output surface charge density", "[output]"){
 // TODO
}

TEST_CASE("Output energy", "[output]"){
    Options o("tests/config/two_particle.cfg");

    Simulation s(o);
    s.setVerbose(false);
    s.run();

    std::vector<std::string> fileNames = {
            "./output/data/Kinetic_Energy.csv",
            "./output/data/Potential_Energy.csv",
            "./output/data/Total_Energy.csv"
    };
    for(auto fileName : fileNames) {
        // Check that the relevant file exists
        REQUIRE(exists(fileName));

        // file has the right number of particles
        auto vec = parse2DVector(fileName, true);
        REQUIRE(vec.rows() == 11);
        REQUIRE(vec.cols() == 4);
    }

    s.cleanupOutput();
}

