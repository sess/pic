#include "../catch.hpp"
#include "Domain.h"
#include "Distributions.h"

using namespace picard;

TEST_CASE("Domain Size", "[domain]") {
    Domain d(3, {0, 0, 0}, {1, 2, 3});

    REQUIRE(d.length(0) == 1);
    REQUIRE(d.length(1) == 2);
    REQUIRE(d.length(2) == 3);
    REQUIRE(feq(d.volume(), 6.));
}

TEST_CASE("Domain Equality","[domain]"){
    Domain d1(2, {1,1}, {2,2});

    REQUIRE(d1 != Domain(3, {1,1}, {2,2}));
    REQUIRE(d1 != Domain(2, {1,1.5}, {2,2}));
    REQUIRE(d1 != Domain(2, {1,1}, {2,2.1}));
    REQUIRE(d1 == Domain(2, {1,1}, {2,2}));

    Domain d2(2, false, 1);
    REQUIRE(d2 != Domain(2, true, 1));
    REQUIRE(d2 != Domain(2, false, 2));
    REQUIRE(d2 == Domain(2, false, 1));
}

TEST_CASE("Domain Normal From Id", "[domain]") {
    for(int i=1; i<=3; i++){
        auto d = Domain::Random(i);
        for(int id=0; id<2*d.nDim(); id++){
            auto def = defaultNormal((BoundaryId)id);
            def.resize(d.nDim());
            def = def*((d.particlesPermitted()) ? 1 : -1);
            REQUIRE(d.normal((BoundaryId)id) == def);
        }
    }
}

void testDomainAttributes(const Domain& d, int nDim, vec min, vec max, bool permitted, int id){
    REQUIRE(d.nDim() == nDim);
    REQUIRE(d.min() == min);
    REQUIRE(d.max() == max);
    REQUIRE(d.particlesPermitted() == permitted);
    REQUIRE(d.id() == id);
    REQUIRE(d.size() == max - min);
    REQUIRE(d.bounds(X_MIN) == min[0]);
    REQUIRE(d.bounds(X_MAX) == max[0]);

    if(nDim >= 2) {
        REQUIRE(d.bounds(Y_MIN) == min[1]);
        REQUIRE(d.bounds(Y_MAX) == max[1]);
    } else{
        REQUIRE_THROWS(d.bounds(Y_MIN));
        REQUIRE_THROWS(d.bounds(Y_MAX));
        REQUIRE_THROWS(d.bounds(NONE));
        REQUIRE_THROWS(d.bounds(INTERIOR));
    }
    if(nDim >= 3) {
        REQUIRE(d.bounds(Z_MIN) == min[2]);
        REQUIRE(d.bounds(Z_MAX) == max[2]);
    } else{
        REQUIRE_THROWS(d.bounds(Z_MIN));
        REQUIRE_THROWS(d.bounds(Z_MAX));
    }
}

TEST_CASE("Set Min, Max, Dim, Particles Permitted, id", "[domain]") {
    Domain d(2, {0,0,0}, {1,1,1}, true, 10);
    testDomainAttributes(d, 2, {0,0}, {1,1}, true, 10);
    d.setId(11);
    testDomainAttributes(d, 2, {0,0}, {1,1}, true, 11);
    d.setParticlesPermitted(false);
    testDomainAttributes(d, 2, {0,0}, {1,1}, false, 11);
    d.setDimension(3, {-1,-1,-1}, {2,2,2});
    testDomainAttributes(d, 3, {-1,-1,-1}, {2,2,2}, false, 11);
    d.setMin({0,0,0});
    testDomainAttributes(d, 3, {0,0,0}, {2,2,2}, false, 11);
    d.setMax({10,10,10});
    testDomainAttributes(d, 3, {0,0,0}, {10,10,10}, false, 11);
    d.setMin(2, -2);
    testDomainAttributes(d, 3, {0,0,-2}, {10,10,10}, false, 11);
    d.setMax(0, 4);
    testDomainAttributes(d, 3, {0,0,-2}, {4,10,10}, false, 11);
}

TEST_CASE("Domain Expansion", "[domain]") {
    auto dims = {1, 2, 3};
    for (auto dim : dims) {
        Domain d(dim);

        int pts = 100;
        vec currMin(dim, INFINITY);
        vec currMax(dim, -INFINITY);
        for (auto i = 0; i < pts; i++) {
            auto v = uniform(vec(dim, -5), vec(dim, 5));
            d.expandToInclude(v);
            currMin = c_min(currMin, v);
            currMax = c_max(currMax, v);
        }

        for (auto i = 0; i < dim; i++) {
            REQUIRE(d.min(i) == currMin[i]);
            REQUIRE(d.max(i) == currMax[i]);
        }
    }
}

TEST_CASE("Inside/Outside/Boundary Domain", "[domain]") {
    Domain d3(3, {0, 0, 0}, {1, 2, 3});
    REQUIRE(d3.isOutside({-1, .5, .5}));
    REQUIRE(d3.isOutside({2, .5, .5}));
    REQUIRE(d3.isOutside({.5, -1, .5}));
    REQUIRE(d3.isOutside({.5, 3, .5}));
    REQUIRE(d3.isOutside({.5, .5, -1}));
    REQUIRE(d3.isOutside({.5, .5, 4}));
    REQUIRE(!d3.isOutside({.5, .5, .5}));
    REQUIRE(!d3.isOutside({0, 0, 0}));
    REQUIRE(!d3.isOutside({1, 0, 0}));
    REQUIRE(!d3.isOutside({1, 2, 0}));
    REQUIRE(!d3.isOutside({1, 2, 3}));

    REQUIRE(!d3.isOutside({1, 2, 0}, X_MIN));
    REQUIRE(d3.isOutside({.5, -1, .5}, Y_MIN));
    REQUIRE(!d3.isOutside({.5, -1, .5}, Y_MAX));

    REQUIRE(!d3.isInside({-1, .5, .5}));
    REQUIRE(d3.isInside({.5, .5, .5}));
    REQUIRE(d3.isInside({1, 2, 3}));

    REQUIRE(d3.isOnBoundary({1, 2, 3}));
    REQUIRE(d3.isOnBoundary({0, .5, .5}));
    REQUIRE(!d3.isOnBoundary({0, .5, 5}));

    REQUIRE(!d3.isOnBoundary({1, 2, 3}, X_MIN));
    REQUIRE(d3.isOnBoundary({1, 2, 3}, Z_MAX));
    REQUIRE(d3.isOnBoundary({0, .5, .5}, X_MIN));
    REQUIRE(!d3.isOnBoundary({0, .5, .5}, Y_MIN));
    REQUIRE(!d3.isOnBoundary({0, .5, 5}, X_MIN));

    REQUIRE(d3.boundaryIds({1, 0, 0}) == std::vector<BoundaryId>({X_MAX, Y_MIN, Z_MIN}));
    REQUIRE(d3.boundaryIds({1, 2, 3}) == std::vector<BoundaryId>({X_MAX, Y_MAX, Z_MAX}));
    REQUIRE(d3.boundaryIds({0, .5, .5}) == std::vector<BoundaryId>({X_MIN}));
    REQUIRE(d3.boundaryIds({0, .5, 5}) == std::vector<BoundaryId>());


    Domain d2(2, {0, 0}, {1, 2});
    REQUIRE(d2.isOutside({-1, .5}));
    REQUIRE(!d2.isOutside({1, 2}));
    REQUIRE(d2.isOutside({1, 2.01}));
    REQUIRE(!d2.isOutside({1, 1.99}));

    REQUIRE(d2.isOutside({-1, .5}, X_MIN));
    REQUIRE(!d2.isOutside({-1, .5}, X_MAX));
    REQUIRE(!d2.isOutside({-1, .5}, Y_MIN));
    REQUIRE(!d2.isOutside({1, 2}, X_MAX));
    REQUIRE(d2.isOutside({1, 2.01}, Y_MAX));
    REQUIRE_THROWS(d2.isOutside({1, 2.01}, Z_MIN));

    REQUIRE(d2.isInside({.99, 1.99}));
    REQUIRE(d2.isInside({0, 0}));
    REQUIRE(d2.isInside({0, 2}));
    REQUIRE(d2.isInside({1, 0}));
    REQUIRE(d2.isInside({.5, .5}));
    REQUIRE(!d2.isInside({-1, .5}));
    REQUIRE(!d2.isInside({1, 2.01}));

    REQUIRE(d2.isOnBoundary({1, 1.99}));
    REQUIRE(!d2.isOnBoundary({1, 2.01}));
    REQUIRE(!d2.isOnBoundary({.99, 1.99}));
    REQUIRE(d2.isOnBoundary({0, 0}));

    REQUIRE(d2.isOnBoundary({1, 1.99}, X_MAX));
    REQUIRE(!d2.isOnBoundary({1, 2.01}, X_MAX));
    REQUIRE(!d2.isOnBoundary({.99, 1.99}, X_MIN));
    REQUIRE(d2.isOnBoundary({0, 0}, X_MIN));
    REQUIRE(!d2.isOnBoundary({0, 0}, X_MAX));
    REQUIRE_THROWS(d2.isOnBoundary({1, 2.01}, Z_MIN));

    REQUIRE(d2.boundaryIds({1, 1.99}) == std::vector<BoundaryId>({X_MAX}));
    REQUIRE(d2.boundaryIds({1, 2.01}) == std::vector<BoundaryId>());
    REQUIRE(d2.boundaryIds({.99, 1.99}) == std::vector<BoundaryId>());
    REQUIRE(d2.boundaryIds({0, 0}) == std::vector<BoundaryId>({X_MIN, Y_MIN}));

    Domain d1(1, vec({0}), vec({1}));
    REQUIRE(d1.isOutside({-1}));
    REQUIRE(d1.isOutside({2}));
    REQUIRE(!d1.isOutside({1}));
    REQUIRE(!d1.isOutside({0}));
    REQUIRE(!d1.isOutside({0.5}));

    REQUIRE(d1.isOutside({2}, X_MAX));
    REQUIRE(!d1.isOutside({2}, X_MIN));
    REQUIRE(!d1.isOutside({1}, X_MIN));
    REQUIRE(d1.isOutside({-.1}, X_MIN));
    REQUIRE_THROWS(d1.isOutside({0.5}, Z_MIN));

    REQUIRE(!d1.isInside({2}));
    REQUIRE(!d1.isInside({-1}));
    REQUIRE(d1.isInside({1}));
    REQUIRE(d1.isInside({0}));
    REQUIRE(d1.isInside({0.5}));

    REQUIRE(d1.boundaryIds({2}) == std::vector<BoundaryId>());
    REQUIRE(d1.boundaryIds({1}) == std::vector<BoundaryId>({X_MAX}));
    REQUIRE(d1.boundaryIds({0}) == std::vector<BoundaryId>({X_MIN}));
    REQUIRE(d1.boundaryIds({0.5}) == std::vector<BoundaryId>());

    REQUIRE(d1.isOnBoundary({1}));
    REQUIRE(d1.isOnBoundary({0}));
    REQUIRE(!d1.isOnBoundary({2}));
    REQUIRE(!d1.isOnBoundary({0.5}));

    REQUIRE(!d1.isOnBoundary({1}, X_MIN));
    REQUIRE(d1.isOnBoundary({1}, X_MAX));
    REQUIRE(d1.isOnBoundary({0}, X_MIN));
    REQUIRE(!d1.isOnBoundary({2}, X_MIN));
    REQUIRE_THROWS(d1.isOnBoundary({0.5}, Y_MIN));
}


TEST_CASE("Intersection 1D -- particle reentering the domain", "[domain]") {
    const int nDim = 1;
    Domain d(nDim, vec({-0.5}), vec({0.5}));

    // Particle re-entering the domain
    std::vector<Real> ray = {-0.75, -0.25};
    auto intersection = d.intersects(ray);
    REQUIRE(intersection.count == 0);

    // Particle in the interior of the domain
    std::vector<Real> ray2 = {-0.35, -0.25};
    auto intersection2 = d.intersects(ray2);
    REQUIRE(intersection2.count == 0);

    // Particle leaving the domain
    std::vector<Real> ray3 = {-0.25, -0.75};
    auto intersection3 = d.intersects(ray3);
    REQUIRE(intersection3.count == 1);
    REQUIRE(feq(intersection3.point[0], -0.5));
    REQUIRE(intersection3.dim == 0);
    REQUIRE(feq(intersection3.normal[0], -1.));

    // Particle hitting exactly one edge of the domain
    std::vector<Real> ray4 = {0.49, 0.5};
    auto intersection4 = d.intersects(ray4);
    REQUIRE(intersection4.count == 1);
    REQUIRE(feq(intersection4.point[0], 0.5));
    REQUIRE(intersection4.dim == 0);
    REQUIRE(feq(intersection4.normal[0], 1.));
}

std::vector<Real> getPos(Domain& d, int location, int& nBoundary) {
    switch (location) {
        case 0 :
            return d.randomPositionOutside();
        case 1 :
            return d.randomBoundaryPosition(&nBoundary);
        case 2 :
            return d.randomPositionInside();
        default:
            throw std::runtime_error("Error in Domain Tests: getPos: location code not recognized");
    }
}

int intersectsVal(int start, int nBoundaryStart, int end, int nBoundaryEnd) {
    if (start > end || (start == 1)) {
        return (nBoundaryStart > 1 || nBoundaryEnd > 1) ? 2 : 1;
    }

    return 0;
}

TEST_CASE("Intersections -- Many particles, random trajectories, 1D, 2D, 3D", "[domain]") {
    int trials = 1000;
    srand((unsigned int) time(NULL));

    std::vector<int> possibleLocations = {
            0, // Outside Domain
            1, // Boundary Domain
            2 // Inside Domain
    };
    for (int nDim = 1; nDim <= 3; nDim++) {
        for (int trial = 0; trial < trials; trial++) {
            // Start with a random domain
            Domain d = Domain::Random(nDim);

            // Loop through all combinations of start and end positions
            for (auto startLocation : possibleLocations) {
                if (startLocation == 1)
                    continue;
                int nBoundariesStart = 0;
                auto startPos = getPos(d, startLocation, nBoundariesStart);
                for (auto endLocation : possibleLocations) {
                    if (startLocation == 1 && endLocation == 1)
                        continue;

                    int nBoundariesEnd = 0;
                    auto endPos = getPos(d, endLocation, nBoundariesEnd);

                    auto ray = concatenate(startPos, endPos);

                    auto intersection = d.intersects(ray);

                    // Make sure the intersectsDomain function is returning as expected and print some things if not
                    if (intersection.count !=
                        intersectsVal(startLocation, nBoundariesStart, endLocation, nBoundariesEnd)) {
                        std::cout << "start: " << startLocation << " end: " << endLocation << std::endl;
                        std::cout << d << std::endl;
                        std::cout << "ray: " << ray << std::endl;
                        std::cout << "intersection: " << intersection.point << std::endl;
                        std::cout << "normal: " << intersection.normal << std::endl;
                        std::cout << "Face dim: " << intersection.dim << std::endl;
                        std::cout << "end pos outside domain?" << d.isOutside(endPos) << std::endl;
                    }

                    REQUIRE(intersection.count ==
                            intersectsVal(startLocation, nBoundariesStart, endLocation, nBoundariesEnd));

                    // Make sure the face that is intersected is set properly
                    if (intersection) {
                        bool faceDimCorrect =
                                feq(intersection.point[intersection.dim], d.min(intersection.dim)) ||
                                feq(intersection.point[intersection.dim], d.max(intersection.dim));
                        REQUIRE(faceDimCorrect);
                    }
                }
            }
        }
    }
}

TEST_CASE("Boundary Name/Id", "[domain]") {
    REQUIRE(toString(NONE) == "unknown");
    REQUIRE(toString(X_MIN) == "xmin");
    REQUIRE(toString(X_MAX) == "xmax");
    REQUIRE(toString(Y_MIN) == "ymin");
    REQUIRE(toString(Y_MAX) == "ymax");
    REQUIRE(toString(Z_MIN) == "zmin");
    REQUIRE(toString(Z_MAX) == "zmax");
    REQUIRE(toString(INTERIOR) == "interior");
    REQUIRE(toString((BoundaryId)7) == "unknown");

    REQUIRE(defaultNormal(NONE) == vec({}));
    REQUIRE(defaultNormal(X_MIN) == vec({-1, 0, 0}));
    REQUIRE(defaultNormal(X_MAX) == vec({1, 0, 0}));
    REQUIRE(defaultNormal(Y_MIN) == vec({0, -1, 0}));
    REQUIRE(defaultNormal(Y_MAX) == vec({0, 1, 0}));
    REQUIRE(defaultNormal(Z_MIN) == vec({0, 0, -1}));
    REQUIRE(defaultNormal(Z_MAX) == vec({0, 0, 1}));
    REQUIRE(defaultNormal(INTERIOR) == vec({}));
    REQUIRE(defaultNormal((BoundaryId)78) == vec({}));
}

TEST_CASE("Random Positions", "[domain]"){
    int trials = 100;
    for(int t=0; t<trials; t++){
        auto d = Domain::Random(uniform_int(1,3));
        auto maxId = 2*d.nDim() - 1;

        REQUIRE(!d.isOutside(d.randomPositionInside()));
        REQUIRE(d.isInside(d.randomPositionInside()));

        REQUIRE(d.isOutside(d.randomPositionOutside()));
        REQUIRE(!d.isInside(d.randomPositionOutside()));

        int bounds;
        auto boundaryPos = d.randomBoundaryPosition(&bounds);
        REQUIRE(d.isOnBoundary(boundaryPos));
        REQUIRE(d.boundaryIds(boundaryPos).size() == bounds);

        auto id = (BoundaryId)uniform_int(0,maxId);
        boundaryPos = d.randomBoundaryPosition(id);
        REQUIRE(d.isOnBoundary(boundaryPos));
        REQUIRE(d.isOnBoundary(boundaryPos, id));
        REQUIRE(!d.isOnBoundary(boundaryPos, (BoundaryId)((id+1)%(maxId+1))));
        REQUIRE(d.boundaryIds(boundaryPos) == std::vector<BoundaryId>({id}));

        auto outsideBoundaryPos = d.randomPositionOutside(id);
        REQUIRE(d.isOutside(outsideBoundaryPos));
        REQUIRE(d.isOutside(outsideBoundaryPos, id));
        REQUIRE(!d.isOutside(outsideBoundaryPos, (BoundaryId)((id+1)%(maxId+1))));
    }
}


