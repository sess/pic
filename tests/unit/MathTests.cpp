#include "../catch.hpp"
#include "Definitions.h"
#include "MathUtils.h"
#include "IOUtils.h"

using namespace picard;
using namespace std;

TEST_CASE("Exponential Interpolation", "[math_utils]") {

    std::map<Real,Real> samples = {{0.1,1}, {1, 2}, {10, .1}, {100, .0001}, {1000, 1}};
    auto coeffs = getExpCoefficients(samples);

    // Check extrapolation
    REQUIRE(feq(expInterpolate(0.001, samples, coeffs), 1));
    REQUIRE(feq(expInterpolate(10000, samples, coeffs), 1));

    // Check matchup of sample points
    for(auto& entry : samples)
        REQUIRE((feq(expInterpolate(entry.first, samples, coeffs), entry.second)));

    // Create and write some interpolation data for plotting
//    auto Npts = 20000;
//    vector2d<Real> test_data(Npts, 2);
//    for(auto i=0; i<Npts; i++){
//        auto x = 0.1*Real(i);
//        test_data(i,0) = x;
//        test_data(i,1) = expInterpolate(x, samples, coeffs);
//    }
//
//    write2DVector(test_data, "exp_interp.csv");

}