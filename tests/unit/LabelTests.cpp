#include "../catch.hpp"
#include "Label.h"
#include "Distributions.h"
#include "CastUtils.h"

using namespace picard;

/*******************************************************
 *                   Field Labels                      *
 *******************************************************/

TEST_CASE("Dirichlet Label Creation", "[labels]") {
    std::string name = "asldkjdfs";
    auto priority = uniform_int() % 55;
    auto voltage = uniform() * 550;
    auto normal = uniform(vec(3, -5), vec(3, 5));
    auto dirichlet = makeDirichlet(name, priority, normal, voltage);
    REQUIRE(dirichlet->name == name);
    REQUIRE(dirichlet->priority == priority);
    REQUIRE(dirichlet->normal == normal);
    REQUIRE(tsfs(dirichlet->type) == "Dirichlet");
    REQUIRE(dirichlet->numberOfRequiredArguments() == dirichlet->properties.size());
    REQUIRE(dirichlet->numberOfRequiredArguments() == 1);
    REQUIRE(dirichlet->type == FieldLabel::DIRICHLET);
    REQUIRE(dirichlet->properties[0] == voltage);

    auto p = makeDirichlet(X_MIN, priority, voltage);
    dirichlet = p.second;
    REQUIRE(p.first == 0);
    REQUIRE(dirichlet->name == toString(X_MIN));
    REQUIRE(dirichlet->normal == defaultNormal(X_MIN));
    REQUIRE(dirichlet->priority == priority);
    REQUIRE(dirichlet->numberOfRequiredArguments() == dirichlet->properties.size());
    REQUIRE(dirichlet->numberOfRequiredArguments() == 1);
    REQUIRE(dirichlet->type == FieldLabel::DIRICHLET);
    REQUIRE(dirichlet->properties[0] == voltage);
}

TEST_CASE("Time-Dependent Dirichlet Label Creation", "[labels]") {
    std::string name = "asldkjdfs";
    auto priority = uniform_int() % 55;
    std::vector<Real> voltage = samples<Real>(300, std::uniform_real_distribution<Real>(-5., 5.));
    auto normal = uniform(vec(3, -5), vec(3, 5));
    auto td_dirichlet = makeTimeDependentDirichlet(name, priority, normal, voltage);
    REQUIRE(td_dirichlet->name == name);
    REQUIRE(td_dirichlet->priority == priority);
    REQUIRE(td_dirichlet->normal == normal);
    REQUIRE(tsfs(td_dirichlet->type) == "Time Dependent Dirichlet");
    REQUIRE(td_dirichlet->numberOfRequiredArguments() == 1);
    REQUIRE(td_dirichlet->type == FieldLabel::TIME_DEPENDENT_DIRICHLET);
    REQUIRE(td_dirichlet->properties == voltage);

    auto p = makeTimeDependentDirichlet(X_MAX, priority, voltage);
    td_dirichlet = p.second;
    REQUIRE(td_dirichlet->name == toString(X_MAX));
    REQUIRE(td_dirichlet->priority == priority);
    REQUIRE(td_dirichlet->normal == defaultNormal(X_MAX));
    REQUIRE(tsfs(td_dirichlet->type) == "Time Dependent Dirichlet");
    REQUIRE(td_dirichlet->numberOfRequiredArguments() == 1);
    REQUIRE(td_dirichlet->type == FieldLabel::TIME_DEPENDENT_DIRICHLET);
    REQUIRE(td_dirichlet->properties == voltage);
}

TEST_CASE("Step Label Creation", "[labels]") {
    std::string name = "asldkjdfs";
    auto priority = uniform_int() % 55;
    Real baseVoltage = 1;
    Real stepVoltage = -100;
    Real startTime = 1;
    Real endTime = 2;
    auto normal = uniform(vec(3, -5), vec(3, 5));
    auto step_dirichlet = makeStep(name, priority, normal, baseVoltage, stepVoltage, startTime, endTime);
    REQUIRE(step_dirichlet->name == name);
    REQUIRE(step_dirichlet->priority == priority);
    REQUIRE(step_dirichlet->normal == normal);
    REQUIRE(tsfs(step_dirichlet->type) == "Step");
    REQUIRE(step_dirichlet->numberOfRequiredArguments() == 4);
    REQUIRE(step_dirichlet->type == FieldLabel::STEP);
    std::vector<Real> properties = {baseVoltage, stepVoltage, startTime, endTime};
    REQUIRE(step_dirichlet->properties == properties);

    auto p = makeStep(X_MAX, priority, baseVoltage, stepVoltage, startTime, endTime);
    step_dirichlet = p.second;
    REQUIRE(step_dirichlet->name == toString(X_MAX));
    REQUIRE(step_dirichlet->priority == priority);
    REQUIRE(step_dirichlet->normal == defaultNormal(X_MAX));
    REQUIRE(tsfs(step_dirichlet->type) == "Step");
    REQUIRE(step_dirichlet->numberOfRequiredArguments() == 4);
    REQUIRE(step_dirichlet->type == FieldLabel::STEP);
    REQUIRE(step_dirichlet->properties == properties);
}

TEST_CASE("Neumann Label Creation", "[labels]") {
    std::string name = "asldkjdfs";
    auto priority = uniform_int() % 55;
    auto voltage = uniform() * 550;
    auto normal = uniform(vec(3, -5), vec(3, 5));
    auto neumann = makeNeumann(name, priority, normal, voltage);
    REQUIRE(neumann->name == name);
    REQUIRE(neumann->priority == priority);
    REQUIRE(neumann->normal == normal);
    REQUIRE(tsfs(neumann->type) == "Neumann");
    REQUIRE(neumann->numberOfRequiredArguments() == neumann->properties.size());
    REQUIRE(neumann->numberOfRequiredArguments() == 1);
    REQUIRE(neumann->type == FieldLabel::NEUMANN);
    REQUIRE(neumann->properties[0] == voltage);

    auto p = makeNeumann(Y_MIN, priority, voltage);
    neumann = p.second;
    REQUIRE(neumann->name == toString(Y_MIN));
    REQUIRE(neumann->priority == priority);
    REQUIRE(neumann->normal == defaultNormal(Y_MIN));
    REQUIRE(tsfs(neumann->type) == "Neumann");
    REQUIRE(neumann->numberOfRequiredArguments() == neumann->properties.size());
    REQUIRE(neumann->numberOfRequiredArguments() == 1);
    REQUIRE(neumann->type == FieldLabel::NEUMANN);
    REQUIRE(neumann->properties[0] == voltage);
}

TEST_CASE("Zero Field Label Creation", "[labels]") {
    std::string name = "asldkjdfs";
    auto priority = uniform_int() % 55;
    auto normal = uniform(vec(3, -5), vec(3, 5));
    auto zeroField = makeZeroField(name, priority, normal);
    REQUIRE(zeroField->name == name);
    REQUIRE(zeroField->priority == priority);
    REQUIRE(zeroField->normal == normal);
    REQUIRE(tsfs(zeroField->type) == "Zero Field");
    REQUIRE(zeroField->numberOfRequiredArguments() == zeroField->properties.size());
    REQUIRE(zeroField->numberOfRequiredArguments() == 0);
    REQUIRE(zeroField->type == FieldLabel::ZERO_FIELD);

    auto p = makeZeroField(Y_MAX, priority);
    zeroField = p.second;
    REQUIRE(zeroField->name == toString(Y_MAX));
    REQUIRE(zeroField->priority == priority);
    REQUIRE(zeroField->normal == defaultNormal(Y_MAX));
    REQUIRE(tsfs(zeroField->type) == "Zero Field");
    REQUIRE(zeroField->numberOfRequiredArguments() == zeroField->properties.size());
    REQUIRE(zeroField->numberOfRequiredArguments() == 0);
    REQUIRE(zeroField->type == FieldLabel::ZERO_FIELD);
}

TEST_CASE("Plasma-Dielectric Interface Label Creation", "[labels]") {
    std::string name = "asldkjdfs";
    auto priority = uniform_int() % 55;
    auto e1 = uniform() * 20;
    auto e2 = uniform() * 20;
    auto normal = uniform(vec(3, -5), vec(3, 5));
    auto pdInterace = makePlasmaDielectricInterface(name, priority, normal, e1, e2);
    REQUIRE(pdInterace->name == name);
    REQUIRE(pdInterace->priority == priority);
    REQUIRE(pdInterace->normal == normal);
    REQUIRE(tsfs(pdInterace->type) == "Plasma Dielectric Interface");
    REQUIRE(pdInterace->numberOfRequiredArguments() == pdInterace->properties.size());
    REQUIRE(pdInterace->numberOfRequiredArguments() == 2);
    REQUIRE(pdInterace->type == FieldLabel::PLASMA_DIELECTRIC_INTERFACE);
    REQUIRE(pdInterace->properties[0] == e1);
    REQUIRE(pdInterace->properties[1] == e2);

    auto p = makePlasmaDielectricInterface(Z_MIN, priority, e1, e2);
    pdInterace = p.second;
    REQUIRE(pdInterace->name == toString(Z_MIN));
    REQUIRE(pdInterace->priority == priority);
    REQUIRE(pdInterace->normal == defaultNormal(Z_MIN));
    REQUIRE(tsfs(pdInterace->type) == "Plasma Dielectric Interface");
    REQUIRE(pdInterace->numberOfRequiredArguments() == pdInterace->properties.size());
    REQUIRE(pdInterace->numberOfRequiredArguments() == 2);
    REQUIRE(pdInterace->type == FieldLabel::PLASMA_DIELECTRIC_INTERFACE);
    REQUIRE(pdInterace->properties[0] == e1);
    REQUIRE(pdInterace->properties[1] == e2);
}

TEST_CASE("Periodic Field Label Creation", "[labels]") {
    std::string name = "asldkjdfs";
    auto priority = uniform_int() % 55;
    auto normal = uniform(vec(3, -5), vec(3, 5));
    auto periodic = makePeriodicField(name, priority, normal);
    REQUIRE(periodic->name == name);
    REQUIRE(periodic->priority == priority);
    REQUIRE(periodic->normal == normal);
    REQUIRE(tsfs(periodic->type) == "Periodic");
    REQUIRE(periodic->numberOfRequiredArguments() == periodic->properties.size());
    REQUIRE(periodic->numberOfRequiredArguments() == 0);
    REQUIRE(periodic->type == FieldLabel::PERIODIC);

    auto p = makePeriodicField(Z_MAX, priority);
    periodic = p.second;
    REQUIRE(periodic->name == toString(Z_MAX));
    REQUIRE(periodic->priority == priority);
    REQUIRE(periodic->normal == defaultNormal(Z_MAX));
    REQUIRE(tsfs(periodic->type) == "Periodic");
    REQUIRE(periodic->numberOfRequiredArguments() == periodic->properties.size());
    REQUIRE(periodic->numberOfRequiredArguments() == 0);
    REQUIRE(periodic->type == FieldLabel::PERIODIC);
}

TEST_CASE("Interior Label Creation", "[labels]") {
    std::string name = "asldkjdfs";
    auto priority = uniform_int() % 55;
    auto dielectric_constant = uniform() * 550;
    bool permitted = uniform() < 0.5;
    auto interior = makeInterior(name, priority, dielectric_constant, permitted);
    REQUIRE(interior->name == name);
    REQUIRE(interior->priority == priority);
    REQUIRE(interior->normal == vec());
    REQUIRE(tsfs(interior->type) == "Interior");
    REQUIRE(interior->numberOfRequiredArguments() == interior->properties.size());
    REQUIRE(interior->numberOfRequiredArguments() == 2);
    REQUIRE(interior->type == FieldLabel::INTERIOR);
    REQUIRE(interior->properties[0] == dielectric_constant);
    if (permitted)
        REQUIRE(interior->properties[1] > 0);
    else
        REQUIRE(interior->properties[1] < 0);

    auto p = makeInterior(priority, dielectric_constant, permitted);
    interior = p.second;
    REQUIRE(interior->name == toString(INTERIOR));
    REQUIRE(interior->priority == priority);
    REQUIRE(interior->normal == vec());
    REQUIRE(tsfs(interior->type) == "Interior");
    REQUIRE(interior->numberOfRequiredArguments() == interior->properties.size());
    REQUIRE(interior->numberOfRequiredArguments() == 2);
    REQUIRE(interior->type == FieldLabel::INTERIOR);
    REQUIRE(interior->properties[0] == dielectric_constant);
    if (permitted)
        REQUIRE(interior->properties[1] > 0);
    else
        REQUIRE(interior->properties[1] < 0);
}

/*******************************************************
 *                Particle BC Labels                   *
 *******************************************************/

TEST_CASE("Open Particle BC Label Creation", "[labels]") {
    std::string name = "asldkjdfs";
    auto normal = uniform(vec(3, -5), vec(3, 5));
    auto priority = uniform_int() % 55;
    auto open = makeOpen(name, priority, normal);
    REQUIRE(open->name == name);
    REQUIRE(open->normal == normal);
    REQUIRE(tsfs(open->type) == "Open");
    REQUIRE(open->numberOfRequiredArguments() == open->properties.size());
    REQUIRE(open->numberOfRequiredArguments() == 0);
    REQUIRE(open->type == ParticleBoundaryLabel::OPEN);

    auto p = makeOpen(X_MIN, priority);
    open = p.second;
    REQUIRE(open->name == toString(X_MIN));
    REQUIRE(open->normal == defaultNormal(X_MIN));
    REQUIRE(tsfs(open->type) == "Open");
    REQUIRE(open->numberOfRequiredArguments() == open->properties.size());
    REQUIRE(open->numberOfRequiredArguments() == 0);
    REQUIRE(open->type == ParticleBoundaryLabel::OPEN);
}

TEST_CASE("Reflecting Particle BC Label Creation", "[labels]") {
    std::string name = "asldkjdfs";
    auto coefRest = uniform();
    auto priority = uniform_int() % 55;
    auto normal = uniform(vec(3, -5), vec(3, 5));
    auto reflecting = makeReflecting(name, priority, normal, coefRest);
    REQUIRE(reflecting->name == name);
    REQUIRE(reflecting->normal == normal);
    REQUIRE(tsfs(reflecting->type) == "Reflecting");
    REQUIRE(reflecting->numberOfRequiredArguments() == reflecting->properties.size());
    REQUIRE(reflecting->numberOfRequiredArguments() == 1);
    REQUIRE(reflecting->type == ParticleBoundaryLabel::REFLECTING);
    REQUIRE(reflecting->properties[0] == coefRest);

    auto p = makeReflecting(X_MAX, priority, coefRest);
    reflecting = p.second;
    REQUIRE(reflecting->name == toString(X_MAX));
    REQUIRE(reflecting->normal == defaultNormal(X_MAX));
    REQUIRE(tsfs(reflecting->type) == "Reflecting");
    REQUIRE(reflecting->numberOfRequiredArguments() == reflecting->properties.size());
    REQUIRE(reflecting->numberOfRequiredArguments() == 1);
    REQUIRE(reflecting->type == ParticleBoundaryLabel::REFLECTING);
    REQUIRE(reflecting->properties[0] == coefRest);
}

TEST_CASE("Scattering Particle BC Label Creation", "[labels]") {
    std::string name = "asldkjdfs";
    auto coefRest = uniform();
    auto priority = uniform_int() % 55;
    auto normal = uniform(vec(3, -5), vec(3, 5));
    auto scattering = makeScattering(name, priority, normal, coefRest);
    REQUIRE(scattering->name == name);
    REQUIRE(scattering->normal == normal);
    REQUIRE(tsfs(scattering->type) == "Scattering");
    REQUIRE(scattering->numberOfRequiredArguments() == scattering->properties.size());
    REQUIRE(scattering->numberOfRequiredArguments() == 1);
    REQUIRE(scattering->type == ParticleBoundaryLabel::SCATTERING);
    REQUIRE(scattering->properties[0] == coefRest);

    auto p = makeScattering(Y_MIN, priority, coefRest);
    scattering = p.second;
    REQUIRE(scattering->name == toString(Y_MIN));
    REQUIRE(scattering->normal == defaultNormal(Y_MIN));
    REQUIRE(tsfs(scattering->type) == "Scattering");
    REQUIRE(scattering->numberOfRequiredArguments() == scattering->properties.size());
    REQUIRE(scattering->numberOfRequiredArguments() == 1);
    REQUIRE(scattering->type == ParticleBoundaryLabel::SCATTERING);
    REQUIRE(scattering->properties[0] == coefRest);
}

TEST_CASE("Absorbing Particle BC Label Creation", "[labels]") {
    std::string name = "asldkjdfs";
    auto normal = uniform(vec(3, -5), vec(3, 5));
    auto priority = uniform_int() % 55;
    auto absorbing = makeAbsorbing(name, priority, normal);
    REQUIRE(absorbing->name == name);
    REQUIRE(absorbing->normal == normal);
    REQUIRE(tsfs(absorbing->type) == "Absorbing");
    REQUIRE(absorbing->numberOfRequiredArguments() == absorbing->properties.size());
    REQUIRE(absorbing->numberOfRequiredArguments() == 0);
    REQUIRE(absorbing->type == ParticleBoundaryLabel::ABSORBING);

    auto p = makeAbsorbing(Y_MAX, priority);
    absorbing = p.second;
    REQUIRE(absorbing->name == toString(Y_MAX));
    REQUIRE(absorbing->normal == defaultNormal(Y_MAX));
    REQUIRE(tsfs(absorbing->type) == "Absorbing");
    REQUIRE(absorbing->numberOfRequiredArguments() == absorbing->properties.size());
    REQUIRE(absorbing->numberOfRequiredArguments() == 0);
    REQUIRE(absorbing->type == ParticleBoundaryLabel::ABSORBING);
}

TEST_CASE("Periodic Particle BC Label Creation", "[labels]") {
    std::string name = "asldkjdfs";
    auto normal = uniform(vec(3, -5), vec(3, 5));
    auto priority = uniform_int() % 55;
    auto periodic = makePeriodicParticle(name, priority, normal);
    REQUIRE(periodic->name == name);
    REQUIRE(periodic->normal == normal);
    REQUIRE(tsfs(periodic->type) == "Periodic");
    REQUIRE(periodic->numberOfRequiredArguments() == periodic->properties.size());
    REQUIRE(periodic->numberOfRequiredArguments() == 0);
    REQUIRE(periodic->type == ParticleBoundaryLabel::PERIODIC);

    auto p = makePeriodicParticle(Z_MIN, priority);
    periodic = p.second;
    REQUIRE(periodic->name == toString(Z_MIN));
    REQUIRE(periodic->normal == defaultNormal(Z_MIN));
    REQUIRE(tsfs(periodic->type) == "Periodic");
    REQUIRE(periodic->numberOfRequiredArguments() == periodic->properties.size());
    REQUIRE(periodic->numberOfRequiredArguments() == 0);
    REQUIRE(periodic->type == ParticleBoundaryLabel::PERIODIC);
}

TEST_CASE("Solid Particle BC Label Creation", "[labels]") {
    std::string name = "asldkjdfs";
    auto a0 = uniform();
    auto a1 = uniform();
    auto a2 = uniform();
    auto a3 = uniform();
    auto a4 = uniform();
    auto a5 = uniform();
    auto a6 = uniform();
    auto a7 = uniform();
    auto a8 = uniform();
    auto a9 = uniform();
    auto m = uniform();
    auto q = uniform();
    auto normal = uniform(vec(3, -5), vec(3, 5));
    auto priority = uniform_int() % 55;
    auto solid = makeSolid(name, priority, normal, a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, m, q);
    REQUIRE(solid->name == name);
    REQUIRE(solid->normal == normal);
    REQUIRE(tsfs(solid->type) == "Solid");
    REQUIRE(solid->numberOfRequiredArguments() == solid->properties.size());
    REQUIRE(solid->numberOfRequiredArguments() == 12);
    REQUIRE(solid->type == ParticleBoundaryLabel::SOLID);
    REQUIRE(solid->properties == vec({a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, m, q}));

    auto p = makeSolid(Z_MAX, priority, a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, m, q);
    solid = p.second;
    REQUIRE(solid->name == toString(Z_MAX));
    REQUIRE(solid->normal == defaultNormal(Z_MAX));
    REQUIRE(tsfs(solid->type) == "Solid");
    REQUIRE(solid->numberOfRequiredArguments() == solid->properties.size());
    REQUIRE(solid->numberOfRequiredArguments() == 12);
    REQUIRE(solid->type == ParticleBoundaryLabel::SOLID);
    REQUIRE(solid->properties == vec({a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, m, q}));
}
