#include <CastUtils.h>
#include "../catch.hpp"
#include "Interpolator.h"
#include "Grid.h"
#include "Particle.h"
#include "LeapFrog.h"

using namespace picard;
using namespace std;

void checkWeightingSums(vec spacing){
    Interpolator interp;
    auto N = spacing.size();
    vec minPt = uniform(vec(N,-10), vec(N, 10));
    vector<vec> cell;
    int numNodes = pow(2,N);
    for(int i=0; i<numNodes; i++){
        auto disp = vcast<Real>(displacement(i));
        disp.resize(N);
        cell.push_back(minPt + disp*spacing);
    }
    vec maxPt = cell.back();
    vec inPt = uniform(minPt, maxPt);
    
    Real weights = 0;
    for(auto pt : cell){
        weights += interp.W(pt, inPt, spacing);
    }
    REQUIRE(feq(weights, 1.));
}

TEST_CASE("Weighting Sums to one (1D, 2D, 3D)", "[interpolator]"){
    int trials = 100;
    for(int i=0; i<trials; i++){
        for(int d=1; d<= 3; d++){
            checkWeightingSums(uniform(vec(d,0.01), vec(d,10)));
        }
    }
}

TEST_CASE("Specific weighting calculations", "[interpolator]"){
    Interpolator i;
    auto g1 = Grid::Periodic(Domain(1, vec({0}), vec({10})),ivec({11}));
    REQUIRE(i.W(g1->position(9), {9.25}, {1.}) == .75);
    REQUIRE(i.W(g1->position(10), {9.25}, {1.}) == .25);
    
    REQUIRE(i.W(g1->position(9), {9.}, {1.}) == 1.);
    REQUIRE(i.W(g1->position(10), {9.}, {1.}) == 0.);
    
    auto g2 = Grid::Periodic(Domain(2, {0,0}, {100,100}),{101,101});
    vec p2 = {0.25, 0.75};
    REQUIRE(feq(i.W(g2->position(0), p2, g2->spacing()), 0.1875));
    REQUIRE(feq(i.W(g2->position(1), p2, g2->spacing()), 0.0625));
    REQUIRE(feq(i.W(g2->position(101), p2, g2->spacing()), 0.5625));
    REQUIRE(feq(i.W(g2->position(102), p2, g2->spacing()), 0.1875));
    
    auto g3 = Grid::Periodic(Domain(3, {0,0,0}, {10,10,10}),{11,11,11});
    vec p3 = {1.25, 0.75, 0.25};
    REQUIRE(i.W(g3->position(1), p3, g3->spacing()) == 0.1875*.75);
    REQUIRE(feq(i.W(g3->position(1), p3, g3->spacing()), 0.1875*.75));
    REQUIRE(feq(i.W(g3->position(2), p3, g3->spacing()), 0.0625*.75));
    REQUIRE(feq(i.W(g3->position(12), p3, g3->spacing()), 0.5625*.75));
    REQUIRE(feq(i.W(g3->position(13), p3, g3->spacing()), 0.1875*.75));
    REQUIRE(feq(i.W(g3->position(122), p3, g3->spacing()), 0.1875*.25));
    REQUIRE(feq(i.W(g3->position(123), p3, g3->spacing()), 0.0625*.25));
    REQUIRE(feq(i.W(g3->position(133), p3, g3->spacing()), 0.5625*.25));
    REQUIRE(feq(i.W(g3->position(134), p3, g3->spacing()), 0.1875*.25));
}

TEST_CASE("Interpolate Particle to Grid", "[interpolator]"){
    Interpolator interp;
    int trials = 100;
    for(int trial=0; trial<trials; trial++){
        for(int d=1; d<= 3; d++){
            auto grid = Grid::Periodic(Domain::Random(d), picard::uniform_int(ivec(d,2), ivec(d, 50)));
            auto nSpecies = picard::uniform_int(1,10);
            auto iSpecies = picard::uniform_int(0, nSpecies-1);
            grid->allocateGridData(nSpecies);
            vec pos;
            if(trial % 10 == 0)
                pos = grid->randomBoundaryPosition();
            else
                pos = grid->randomPositionInside();

            auto species = make_shared<Species>("test", uniform(0,10), uniform(-10,10));
            Particle part(pos, pos,species.get());
            double rho = part.q() / grid->cellVolume();
            interp.particleToGrid(part, iSpecies, *grid);
            auto cell = grid->getEnclosingCell(part.r);

            for(auto i : cell){
                auto delta = grid->spacing() - vabs(part.r - grid->position(i));
                double dV = abs(product(delta));
                double W = dV / (grid->cellVolume() / pow(2.0, grid->numBoundaries(i)));
                REQUIRE(feq(W*rho, grid->chargeDensity(i, iSpecies)));
            }
        }
    }
}

double hyperplane_val(const vec& pos, const vec& slopes, Real offset) {
    return offset + dot(slopes, pos);
}

TEST_CASE("Interpolate scalar from grid - linear test", "[interpolator]"){
    // Linear interpolation should be able to reproduce a linear function exactly
    Interpolator interp;
    int grids = 10;
    int trials = 10;
    for(int g=0; g<grids; g++){
        for(int d=1; d<= 3; d++){
            auto grid = Grid::Periodic(Domain::Random(d), picard::uniform_int(ivec(d,2), ivec(d, 50)));
            grid->allocateGridData(1);
            
            // Fill the potential with a randomly generated linear function
            auto offset = uniform(-10, 10);
            auto slopes = uniform(vec(d,-10), vec(d,10));
            for(int i=0; i<grid->totalPoints(); i++)
                grid->potential[i] = hyperplane_val(grid->position(i), slopes, offset);
            
            for(int trial = 0; trial < trials; trial++){
                vec pos = grid->randomPositionInside();
                if(!feq(hyperplane_val(pos, slopes, offset), interp.scalarFieldValue(pos, grid->potential, *grid))){
                    cerr << "delta: "
                         << hyperplane_val(pos, slopes, offset) - interp.scalarFieldValue(pos, grid->potential, *grid)
                         << endl;
                }
                REQUIRE(feq(hyperplane_val(pos, slopes, offset), interp.scalarFieldValue(pos, grid->potential, *grid)));
            }
        }
    }
}

TEST_CASE("Interpolate scalar from grid - random data test", "[interpolator]"){
    Interpolator interp;
    int grids = 10;
    int trials = 50;
    for(int g=0; g<grids; g++){
        for(int d=1; d<= 3; d++){
            auto grid = Grid::Periodic(Domain::Random(d), picard::uniform_int(ivec(d,2), ivec(d, 50)));
            grid->allocateGridData(1);
            
            // Fill the potential with a randomly generated data
            for(int i=0; i<grid->totalPoints(); i++)
                grid->potential[i] = uniform();
            
            for(int trial = 0; trial < trials; trial++){
                vec pos;
                if(trial % 10 == 0)
                    pos = grid->randomBoundaryPosition();
                else
                    pos = grid->randomPositionInside();
                
                auto cell = grid->getEnclosingCell(pos);
                double interpedVal = 0;
                for(auto i : cell){
                    auto delta = grid->spacing() - vabs(pos - grid->position(i));
                    double dV = abs(product(delta));
                    double W = dV / grid->cellVolume();
                    interpedVal += W*grid->potential[i];
                }
                REQUIRE(feq(interpedVal, interp.scalarFieldValue(pos, grid->potential, *grid)));
            }
        }
    }
}

TEST_CASE("Interpolate E,B field from grid to Particle - linear test", "[interpolator]"){
    // Linear interpolation should be able to reproduce a linear function exactly
    Interpolator interp;
    LeapFrog pm;
    
    int grids = 10;
    int trials = 10;
    for(int g=0; g<grids; g++){
        for(int d=1; d<= 3; d++){
            auto grid = Grid::Periodic(Domain::Random(d), picard::uniform_int(ivec(d,2), ivec(d, 50)));
            grid->allocateGridData(picard::uniform_int(1, 5));
            grid->bField = vector2d<Real>::Constant(grid->totalPoints(), d, 0.0);
            
            // Fill the E and B field with with a randomly generated linear function
            auto offset = uniform(-10, 10);
            auto slopes = uniform(vec(d,-10), vec(d,10));
            for(int i=0; i<grid->totalPoints(); i++){
                auto val = hyperplane_val(grid->position(i), slopes, offset);
                for(int j = 0; j<d; j++){
                    grid->eField(i, j) = val;
                    grid->bField(i, j) = val;
                }
            }
            
            for(int trial = 0; trial < trials; trial++){
                bool bField = (uniform() > 0.5);
                
                vec pos = grid->randomPositionInside();
                auto species = make_shared<Species>("test", uniform(0,10), uniform(-10,10));
                Particle part(pos, pos, species.get());
                
                interp.gridToParticle(part, *grid, pm, bField);
                
                vec comp(d, hyperplane_val(pos, slopes, offset));
                comp.resize(3, 0);
                
                REQUIRE(feq(comp, pm.EField()));
                if(bField)
                    REQUIRE(feq(comp, pm.BField()));
                else
                    REQUIRE(pm.BField() == vec(3,0));
            }
        }
    }
}

TEST_CASE("Interpolate E,B field from grid to Particle - random data test", "[interpolator]"){
    Interpolator interp;
    LeapFrog pm;
    
    int grids = 10;
    int trials = 10;
    for(int g=0; g<grids; g++){
        for(int d=1; d<= 3; d++){
            auto grid = Grid::Periodic(Domain::Random(d), picard::uniform_int(ivec(d,2), ivec(d, 50)));
            grid->allocateGridData(picard::uniform_int(1, 5));
            grid->bField = vector2d<Real>::Constant(grid->totalPoints(), d, 0.0);
            
            // Fill the E and B field with with a randomly generated linear function
            for(int i=0; i<grid->totalPoints(); i++){
                for(int j = 0; j<d; j++){
                    grid->eField(i, j) = uniform();
                    grid->bField(i, j) = uniform();
                }
            }
            
            for(int trial = 0; trial < trials; trial++){
                bool bField = (uniform() > 0.5);
                
                vec pos = grid->randomPositionInside();
                auto species = make_shared<Species>("test", uniform(0,10), uniform(-10,10));

                Particle part(pos, pos, species.get());
                
                interp.gridToParticle(part, *grid, pm, bField);
                
                // Recompute separately for comparison
                auto cell = grid->getEnclosingCell(pos);
                vec interpEVal(d, 0);
                vec interpBVal(d, 0);
                for(auto i : cell){
                    auto delta = grid->spacing() - vabs(pos - grid->position(i));
                    double dV = abs(product(delta));
                    double W = dV / grid->cellVolume();
                    interpEVal = interpEVal + W*getRow(grid->eField, i);
                    interpBVal = interpBVal + W*getRow(grid->bField, i);
                }
                interpEVal.resize(3, 0);
                interpBVal.resize(3, 0);
                
                REQUIRE(feq(interpEVal, pm.EField()));
                if(bField)
                    REQUIRE(feq(interpBVal, pm.BField()));
                else
                    REQUIRE(pm.BField() == vec(3,0));
            }
        }
    }

}
