#include "../catch.hpp"
#include "VectorUtils.h"
#include "CastUtils.h"
#include "Distributions.h"

using namespace picard;

TEST_CASE("addition", "[vector]") {
    vec v1({7.3, 1.0, -1.6});
    vec v2({4.0, -6.2, 1.1});

    REQUIRE(feq(v1 + v2, vec({11.3, -5.2, -0.5})));
    REQUIRE(feq(v2 + v1, vec({11.3, -5.2, -0.5})));
    REQUIRE(feq(v1 + 7, vec({14.3, 8.0, 5.4})));
    REQUIRE(feq(v1 + -9.1, vec({-1.8, -8.1, -10.7})));
    REQUIRE(feq(v1 + 4.1f, vec({7.3 + 4.1f, 1.0 + 4.1f, -1.6 + 4.1f})));
    REQUIRE(feq(7 + v1, vec({7.3 + 7, 1.0 + 7, -1.6 + 7})));
    REQUIRE(feq(-9.1 + v1, vec({-1.8, -8.1, -10.7})));
    REQUIRE(feq(4.1f + v1, vec({7.3 + 4.1f, 1.0 + 4.1f, -1.6 + 4.1f})));
    
    vec v3({2});
    vec v4({3,1});
    REQUIRE_THROWS(v4 + v3);
    REQUIRE_THROWS(v3 + v4);
}

TEST_CASE("subtraction", "[vector]") {
    vec v1({-7.8, 5.7, -2.9});
    vec v2({7.9, 0.0, 8.1});

    REQUIRE(feq(v1 - v2, vec({-15.7, 5.7, -11.0})));
    REQUIRE(feq(v2 - v1, vec({15.7, -5.7, 11.0})));
    REQUIRE(feq(v1 - 8, vec({-15.8, -2.3, -10.9})));
    REQUIRE(feq(v1 - 3.1, vec({-10.9, 2.6, -6.0})));
    REQUIRE(feq(v1 - -4.7f, vec({-7.8 - -4.7f, 5.7 - -4.7f, -2.9 - -4.7f})));
    REQUIRE(feq(8 - v1, vec({15.8, 2.3, 10.9})));
    REQUIRE(feq(3.1 - v1, vec({10.9, -2.6, 6.0})));
    REQUIRE(feq(-4.7f - v1, vec({-4.7f - -7.8, -4.7f - 5.7, -4.7f - -2.9})));
    vec v3({2});
    vec v4({3,1});
    REQUIRE_THROWS(v4 - v3);
    REQUIRE_THROWS(v3 - v4);
}

TEST_CASE("negation", "[vector]") {
    vec v1({-7.8, 5.7, -2.9});
    vec v2({7.9, 0.0, 8.1});

    REQUIRE(-v1 == vec({7.8, -5.7, 2.9}));
    REQUIRE(-v2 == vec({-7.9, -0.0, -8.1}));
}

TEST_CASE("multiplication", "[vector]") {
    vec v1({4.6, 6.6, -2.2});
    vec v2({2.0, 3.3, 2.0});

    REQUIRE(feq(v1 * v2, vec({9.2, 21.78, -4.4})));
    REQUIRE(feq(v2 * v1, vec({9.2, 21.78, -4.4})));
    REQUIRE(feq(v1 * 5, vec({23., 33., -11})));
    REQUIRE(feq(v1 * 4.6, vec({21.16, 30.36, -10.12})));
    REQUIRE(feq(v1 * -8.3f, vec({4.6 * -8.3f, 6.6 * -8.3f, -2.2 * -8.3f})));
    REQUIRE(feq(5 * v1, vec({23., 33., -11})));
    REQUIRE(feq(4.6 * v1, vec({21.16, 30.36, -10.12})));
    REQUIRE(feq(-8.3f * v1, vec({4.6 * -8.3f, 6.6 * -8.3f, -2.2 * -8.3f})));
    
    vec v3({2});
    vec v4({3,1});
    REQUIRE_THROWS(v4 * v3);
    REQUIRE_THROWS(v3 * v4);
}

TEST_CASE("division", "[vector]") {
    vec v1({4.6, 6.6, -2.2});
    vec v2({2.0, 3.3, 2.0});

    REQUIRE(feq(v1 / v2, vec({2.3, 2.0, -1.1})));
    REQUIRE(feq(v1 / 1, vec({4.6, 6.6, -2.2})));
    REQUIRE(feq(v1 / 1.1, vec({4.6 / 1.1, 6.6 / 1.1, -2.2 / 1.1})));
    REQUIRE(feq(v1 / -1.1f, vec({-4.6 / 1.1f, -6.6 / 1.1f, 2.2 / 1.1f})));
    REQUIRE(feq(1 / v1, vec({1. / 4.6, 1. / 6.6, 1. / -2.2})));
    REQUIRE(feq(1.1 / v1, vec({1.1 / 4.6, 1.1 / 6.6, 1.1 / -2.2})));
    REQUIRE(feq(-1.1f / v1, vec({-1.1f / 4.6, -1.1f / 6.6, -1.1f / -2.2})));
    
    vec v3({2});
    vec v4({3,1});
    REQUIRE_THROWS(v4 / v3);
    REQUIRE_THROWS(v3 / v4);
}

TEST_CASE("c_min", "[vector]") {
    vec v1({4.6, 6.6, -2.2});
    vec v2({2.0, 3.3, 2.0});

    REQUIRE(c_min(v1, v2) == vec({2.0, 3.3, -2.2}));
    REQUIRE(c_min(v2, v1) == vec({2.0, 3.3, -2.2}));
    REQUIRE(c_min(v2, 2.0) == vec({2.0, 2.0, 2.0}));
    REQUIRE(c_min(v2, 3.1) == vec({2.0, 3.1, 2.0}));
    REQUIRE(c_min(v2, 1.1) == vec({1.1, 1.1, 1.1}));
    REQUIRE(c_min(v2, 5.1) == vec({2.0, 3.3, 2.0}));
    REQUIRE(c_min(2.0, v2) == vec({2.0, 2.0, 2.0}));
    REQUIRE(c_min(3.1, v2) == vec({2.0, 3.1, 2.0}));
    REQUIRE(c_min(1.1, v2) == vec({1.1, 1.1, 1.1}));
    REQUIRE(c_min(5.1, v2) == vec({2.0, 3.3, 2.0}));
    
    vec v3({2});
    vec v4({3,1});
    REQUIRE_THROWS(c_min(v4, v3));
    REQUIRE_THROWS(c_min(v3, v4));
}

TEST_CASE("c_max", "[vector]") {
    vec v1({4.6, 6.6, -2.2});
    vec v2({2.0, 3.3, 2.0});

    REQUIRE(c_max(v1, v2) == vec({4.6, 6.6, 2.0}));
    REQUIRE(c_max(v2, v1) == vec({4.6, 6.6, 2.0}));
    REQUIRE(c_max(v2, 2.0) == vec({2.0, 3.3, 2.0}));
    REQUIRE(c_max(v2, 3.1) == vec({3.1, 3.3, 3.1}));
    REQUIRE(c_max(v2, 1.1) == vec({2.0, 3.3, 2.0}));
    REQUIRE(c_max(v2, 5.1) == vec({5.1, 5.1, 5.1}));
    REQUIRE(c_max(2.0, v2) == vec({2.0, 3.3, 2.0}));
    REQUIRE(c_max(3.1, v2) == vec({3.1, 3.3, 3.1}));
    REQUIRE(c_max(1.1, v2) == vec({2.0, 3.3, 2.0}));
    REQUIRE(c_max(5.1, v2) == vec({5.1, 5.1, 5.1}));
    
    vec v3({2});
    vec v4({3,1});
    REQUIRE_THROWS(c_max(v4, v3));
    REQUIRE_THROWS(c_max(v3, v4));
}

TEST_CASE("vector2d", "[vector]") {
    vector2d<Real> emptyVec;
    std::vector<Real> vecToAdd = {1, 2, 3};
    std::vector<Real> col = {1};
    push_back(emptyVec, vecToAdd);
    vector2d<Real> c1(1, 3);
    c1 << 1, 2, 3;

    REQUIRE(emptyVec.rows() == 1);
    REQUIRE(emptyVec.cols() == 3);
    REQUIRE(emptyVec == c1);
    REQUIRE(getRow(emptyVec, 0) == vecToAdd);
    REQUIRE(getCol(emptyVec, 0) == col);
    REQUIRE_THROWS(push_back(emptyVec, std::vector<Real>({1, 2})));

    vector2d<Real> bigVec = vector2d<Real>::Constant(10, 2, 11);
    vector2d<Real> c2 = vector2d<Real>::Constant(11, 2, 11);
    c2.row(10) << 1, 2;
    std::vector<Real> vecToAdd2 = {1, 2};
    std::vector<Real> col2 = {11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 1};
    push_back(bigVec, vecToAdd2);
    REQUIRE_THROWS(push_back(bigVec, std::vector<Real>({1, 2, 3, 4, 5, 6, 7, 8, 9, 0})));
    REQUIRE(bigVec.rows() == 11);
    REQUIRE(bigVec.cols() == 2);
    REQUIRE(bigVec == c2);
    REQUIRE(getRow(bigVec, 0) == std::vector<Real>({11, 11}));
    REQUIRE(getRow(bigVec, bigVec.rows() - 1L) == vecToAdd2);
    REQUIRE(getCol(bigVec, 0) == col2);
}

TEST_CASE("Fast Sort 4", "[vector]") {
    std::array<int, 4> element1 = {{1, 2, 3, 4}};
    std::array<int, 4> compare = {{4, 3, 2, 1}};
    fastSort4(element1, std::greater<int>());
    REQUIRE(element1 == compare);

    std::array<int, 4> element2 = {{3, 4, -1, -1}};
    std::array<int, 4> compare2 = {{4, 3, -1, -1}};
    fastSort4(element2, std::greater<int>());
    REQUIRE(element2 == compare2);

    std::array<int, 4> element3 = {{3, 4, 100, -1}};
    std::array<int, 4> compare3 = {{100, 4, 3, -1}};
    fastSort4(element3, std::greater<int>());
    REQUIRE(element3 == compare3);
}

TEST_CASE("concatenate", "[vector]") {
    vec v1({7.3, 1.0, -1.6});
    vec v2({4.0, -6.2, 1.1});

    REQUIRE(concatenate(v1, v2) == std::vector<Real>({7.3, 1.0, -1.6, 4.0, -6.2, 1.1}));
}

TEST_CASE("sum", "[vector]") {
    vec v1({7.3, 1.0, -1.6});
    ivec v2({1, 2, 3});

    REQUIRE(sum(v1) == 7.3 + 1.0 - 1.6);
    REQUIRE(sum(v2) == 6);
}

TEST_CASE("product", "[vector]") {
    vec v1({7.3, 1.0, -1.6});
    ivec v2({1, 2, 3});

    REQUIRE(product(v1) == 7.3 * 1.0 * -1.6);
    REQUIRE(product(v2) == 6);
}

TEST_CASE("dot", "[vector]") {
    REQUIRE(feq(dot(vec({1, 2, 3}), vec({2, -1, 3})), 9));
    REQUIRE(feq(dot(vec({1, 0, 0}), vec({0, 0, 1})), 0));
    REQUIRE(feq(dot(vec({1, 2}), vec({2, -1})), 0));
    REQUIRE(feq(dot(vec({1, 1}), vec({-2, -1})), -3));

    vec v1 = {uniform(), uniform(), uniform()};
    auto mag_v1 = norm(v1);
    REQUIRE(feq(dot(v1, v1), mag_v1 * mag_v1));
}

TEST_CASE("cross product", "[vector]") {
    // 3d cross products
    REQUIRE(feq(cross3d(vec({1, 2, 3}), vec({2, -1, 3})), {9, 3, -5}));
    REQUIRE(feq(cross3d(vec({1, 0, 0}), vec({0, -1, 0})), {0, 0, -1}));
    REQUIRE(feq(cross3d(vec({1, 2, 3}), vec({-1, -2, -3})), {0, 0, 0}));

    // 2d cross products
    REQUIRE(feq(cross2d(vec({1, 2}), vec({2, -1})), -5));
    REQUIRE(feq(cross2d(vec({1, 2}), vec({2, 0})), -4));
    REQUIRE(feq(cross2d(vec({1, 1}), vec({2, 2})), 0));

    // Cross product Identity
    for (int i = 0; i < 10; i++) {
        srand((unsigned int) time(NULL));

        vec v1 = {uniform(), uniform(), uniform()};
        vec v2 = {uniform(), uniform(), uniform()};
        vec v3 = cross3d(v1, v2);
        REQUIRE(feq(dot(v1, v3), 0));
        REQUIRE(feq(dot(v2, v3), 0));
    }
}

TEST_CASE("resize", "[vector]") {
    REQUIRE(resize(vec({1, 2, 3})) == vec({1, 2, 3}));
    REQUIRE(resize(vec({1, 2})) == vec({1, 2, 0}));
    REQUIRE(resize(vec({1})) == vec({1, 0, 0}));
    REQUIRE(resize(vec()) == vec({0, 0, 0}));
}

TEST_CASE("norm", "[vector]") {
    // 3D
    REQUIRE(feq(norm(vec({1, 2, 3})), sqrt(14)));
    REQUIRE(feq(norm(vec({1, 0, 3})), sqrt(10)));
    REQUIRE(feq(norm(vec({-1, -2, -3})), sqrt(14)));
    REQUIRE(feq(norm(vec({0, 0, 0})), 0));
    REQUIRE(feq(norm(vec({1, 0, 0})), 1.));

    vec v1 = {uniform(), uniform(), uniform()};
    auto norm_v1 = norm(v1);
    REQUIRE(feq(norm(v1 / norm_v1), 1.));

    // 2D
    REQUIRE(feq(norm(vec({1, 2})), sqrt(5.)));
    REQUIRE(feq(norm(vec({1, 0})), sqrt(1.)));
    REQUIRE(feq(norm(vec({-1, -2})), sqrt(5.)));
    REQUIRE(feq(norm(vec({0, 0})), 0));
    REQUIRE(feq(norm(vec({1, 0})), 1.));

    vec v2 = {uniform(), uniform()};
    auto norm_v2 = norm(v2);
    REQUIRE(feq(norm(v2 / norm_v2), 1.));

    // 1D
    for (int i = 0; i < 10; i++) {
        Real val = rand() % rand();
        vec v = {val};
        REQUIRE(feq(norm(v), val));
    }

    vec v3 = {uniform()};
    auto norm_v3 = norm(v3);
    REQUIRE(feq(norm(v3 / norm_v3), 1.));

}

TEST_CASE("to_reals", "[vector]") {
    ivec v2({1, 2, 3});
    REQUIRE(vcast<Real>(v2) == vec({1., 2., 3.}));
}

TEST_CASE("nonZeroDim", "[vector]") {
    REQUIRE(nonZeroDim(vec({0, 1.1, 0})) == 1);
    REQUIRE(nonZeroDim(vec({0, 0, 0})) == 3);
    REQUIRE(nonZeroDim(vec({1.1, 0, 0})) == 0);
    REQUIRE(nonZeroDim(ivec({2, 3, 4})) == 0);
    REQUIRE(nonZeroDim(ivec({0, 0, 4})) == 2);
    REQUIRE(nonZeroDim(ivec({2, 0, 0})) == 0);
}

TEST_CASE("zeroDims", "[vector]") {
    REQUIRE(zeroDims(vec({0, 1.1, 0})) == ivec({0, 2}));
    REQUIRE(zeroDims(vec({0, 0, 0})) == ivec({0, 1, 2}));
    REQUIRE(zeroDims(vec({1.1, 0, 0})) == ivec({1, 2}));
    REQUIRE(zeroDims(ivec({2, 3, 4})) == ivec());
    REQUIRE(zeroDims(ivec({2, 0, 4})) == ivec({1}));
    REQUIRE(zeroDims(ivec({2, 0, 0})) == ivec({1, 2}));
}

TEST_CASE("sign", "[vector]") {
    REQUIRE(sign(vec({0, 1.1, 0})) == ivec({0, 1, 0}));
    REQUIRE(sign(vec({0, 0, 0})) == ivec({0, 0, 0}));
    REQUIRE(sign(vec({-1.1, 0, 0})) == ivec({-1, 0, 0}));
    REQUIRE(sign(ivec({-2, 3, 4})) == ivec({-1, 1, 1}));
    REQUIRE(sign(ivec({2, 0, -4})) == ivec({1, 0, -1}));
    REQUIRE(sign(ivec({2, 0, 0})) == ivec({1, 0, 0}));
}

TEST_CASE("vabs", "[vector]") {
    REQUIRE(vabs(vec({0, 1.1, 0})) == vec({0, 1.1, 0}));
    REQUIRE(vabs(vec({0, 0, 0})) == vec({0, 0, 0}));
    REQUIRE(vabs(vec({-1.1, 0, 0})) == vec({1.1, 0, 0}));
    REQUIRE(vabs(vec({-2, 3, 4})) == vec({2, 3, 4}));
    REQUIRE(vabs(vec({-2, -0, -4})) == vec({2, 0, 4}));
}

TEST_CASE("ijk", "[vector]") {
    ivec numpts = {10, 10, 10};
    REQUIRE(ijk(0, numpts, 3) == ivec({0, 0, 0}));
    REQUIRE(ijk(1, numpts, 3) == ivec({1, 0, 0}));
    REQUIRE(ijk(10, numpts, 3) == ivec({0, 1, 0}));
    REQUIRE(ijk(100, numpts, 3) == ivec({0, 0, 1}));
    REQUIRE(ijk(999, numpts, 3) == ivec({9, 9, 9}));
}

TEST_CASE("disp", "[vector]"){
    REQUIRE(displacement(0) == ivec({0,0,0}));
    REQUIRE(displacement(4) == ivec({0,0,1}));
    REQUIRE(displacement(2) == ivec({0,1,0}));
    REQUIRE(displacement(6) == ivec({0,1,1}));
    REQUIRE(displacement(1) == ivec({1,0,0}));
    REQUIRE(displacement(5) == ivec({1,0,1}));
    REQUIRE(displacement(3) == ivec({1,1,0}));
    REQUIRE(displacement(7) == ivec({1,1,1}));
    REQUIRE(displacement(8) == ivec({0,0,0}));
}
