#include "../catch.hpp"
#include "Options.h"
#include "Grid.h"
#include "IOUtils.h"

using namespace picard;
using namespace std;

TEST_CASE("Config file parsing -- config 1", "[config_parsing]") {
    Options o("tests/config/test_config_2D.cfg");

    REQUIRE(o.dt == 2.);
    REQUIRE(o.T == 18.);
    REQUIRE(o.meshFile == "tests/meshes/2Dsingleblockmesh.msh");
    
    REQUIRE(o.referenceParams->simPlasmaFrequency() == 10);
    REQUIRE(o.referenceParams->simVacuumPermittivity() == 11);
    REQUIRE(o.referenceParams->simChargeMassRatio() == 12);

    // Check grid characteristics
    auto grid = o.makeGrid();
    REQUIRE(grid->subDomains.size() == 1);
    REQUIRE(grid->bField.cols() == 3);
    REQUIRE(grid->bField.rows() == 100);

    int priority = 0;
    vector<shared_ptr<FieldLabel>> fieldLabels = {
            makeInterior("interior", priority++, 11, true),
            makeZeroField("top", priority++, {0, 1}),
            makeNeumann("bottom", priority++, {0, -1}, 1),
            makeTimeDependentDirichlet("right", priority++, {1, 0}, parse1DVector("tests/csvs/1dTest.csv")),
            makeDirichlet("left", priority++, {-1, 0}, 0),
            makePeriodicField("unused1", priority++, {1.0/std::sqrt(2.0), 1.0/std::sqrt(2.0)}),
            makePlasmaDielectricInterface("unused2", priority++, {1, 0}, 1, 10),
            makeStep("unused3", priority++, {1,0}, 0,100,10,100)
    };

    priority = 0;
    vector<shared_ptr<ParticleBoundaryLabel>> particleBoundaryLabels = {
            makeOpen("right", priority++, {1, 0}),
            makeReflecting("left", priority++, {-1, 0}, 1),
            makeScattering("top", priority++, {0, 1}, 0.5),
            makeAbsorbing("bottom", priority++, {0, -1}),
            makePeriodicParticle("unused1", priority++, {-1, 0}),
            makeSolid("unused2", priority++, {1.0/std::sqrt(2.0), 1.0/std::sqrt(2.0)}, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1E-31, -1E-19)
    };

    // Check labels
    REQUIRE(o.fieldLabels.size() == fieldLabels.size());
    for (uint i = 0; i < o.fieldLabels.size(); i++)
        REQUIRE(*o.fieldLabels[i] == *fieldLabels[i]);

    REQUIRE(o.particleBoundaryLabels.size() == particleBoundaryLabels.size());
    for (uint i = 0; i < o.particleBoundaryLabels.size(); i++)
        REQUIRE(*o.particleBoundaryLabels[i] == *particleBoundaryLabels[i]);

    // Check particles
    REQUIRE(o.species.size() == 4);


    REQUIRE(o.referenceSpecies == "ion");

    auto& electron = *o.species["electron"];
    auto& electron_init = o.populationInit[o.species["electron"]];
    REQUIRE(electron.id == 0);
    REQUIRE(electron.m == 1E-31);
    REQUIRE(electron.q == -1E-19);
    REQUIRE(electron_init.type == TOTAL_COUNT);
    REQUIRE(electron_init.count == 100);
    REQUIRE(electron.velocity_distribution.size() == 2);
    REQUIRE(electron.velocity_distribution[0]->params() == vec({0.1, 0.3}));
    REQUIRE(electron.velocity_distribution[1]->params() == vec({0.1, 0.3}));
    REQUIRE(electron.velocity_distribution[0]->name() == "maxwellian");
    REQUIRE(electron.velocity_distribution[1]->name() == "maxwellian");

    auto& ion = *o.species["ion"];
    auto& ion_init = o.populationInit[o.species["ion"]];
    REQUIRE(ion.id == 1);
    REQUIRE(ion.m == 1E-27);
    REQUIRE(ion.q == 1E-19);
    REQUIRE(ion_init.type == TOTAL_COUNT);
    REQUIRE(ion_init.count == 100);
    REQUIRE(ion.velocity_distribution.size() == 2);
    REQUIRE(ion.velocity_distribution[0]->params() == vec({-0.5, .2}));
    REQUIRE(ion.velocity_distribution[1]->params() == vec({-0.5, .2}));
    REQUIRE(ion.velocity_distribution[0]->name() == "maxwellian");
    REQUIRE(ion.velocity_distribution[1]->name() == "maxwellian");

    auto& ion2 = *o.species["ion2"];
    auto& ion2_init = o.populationInit[o.species["ion2"]];
    REQUIRE(ion2.id == 2);
    REQUIRE(ion2.m == 5E-27);
    REQUIRE(ion2.q == 1E-19);
    REQUIRE(ion2_init.type == PARTICLE_DENSITY);
    REQUIRE(ion2_init.density == 10.5);
    REQUIRE(ion2.velocity_distribution.size() == 2);
    REQUIRE(ion2.velocity_distribution[0]->params() == vec({-0.5, .2}));
    REQUIRE(ion2.velocity_distribution[1]->params() == vec({-0.1, .1}));
    REQUIRE(ion2.velocity_distribution[0]->name() == "maxwellian");
    REQUIRE(ion2.velocity_distribution[1]->name() == "uniform");

    auto& ion3 = *o.species["ion3"];
    auto& ion3_init = o.populationInit[o.species["ion3"]];
    REQUIRE(ion3.id == 3);
    REQUIRE(ion3.m == 6E-27);
    REQUIRE(ion3.q == 1E-19);
    REQUIRE(ion3_init.type == QUIET_START);
    REQUIRE(ion3_init.spatialSamples == ivec({10, 11}));
    REQUIRE(ion3_init.velocitySamples == ivec({10, 20}));
    REQUIRE(ion3.velocity_distribution.size() == 2);
    REQUIRE(ion3.velocity_distribution[0]->params() == vec({-0.5, .2}));
    REQUIRE(ion3.velocity_distribution[1]->params() == vec({-0.1, .1}));
    REQUIRE(ion3.velocity_distribution[0]->name() == "maxwellian");
    REQUIRE(ion3.velocity_distribution[1]->name() == "maxwellian");

    // Physics


    // Output Settings
    Output out;
    OutputEntry defaults(UNKNOWN_TARGET, 3, "", .5, 11.5, {}, "../test_output/", "", TIME, 6, false, {0,10,11,15});
    out.add(defaults, {POSITION, VELOCITY, ELECTRIC_FIELD, MAGNETIC_FIELD, POTENTIAL, CHARGE_DENSITY,
                       SURFACE_CHARGE_DENSITY, KINETIC_ENERGY, POTENTIAL_ENERGY, TOTAL_ENERGY, SPECIES_TEMPERATURE,
    SPECIES_VELOCITY});

    // Position
    out.entries()[0].indices = {1,10,100};

    // Velocity
    out.entries()[1].prefix = "vel";
    out.entries()[1].interval = 13;
    out.entries()[1].startTime = 1;

    // Electric Field
    out.entries()[2].prefix = "efield";
    out.entries()[2].interval = 2;

    // Magnetic field
    out.entries()[3].setBaseDirectory("./out/");
    out.entries()[3].fileExtension = ".txt";
    out.entries()[3].precision = 9;

    // Potential
    out.entries()[4].suffix = TIME;

    // Charge Density
    out.entries()[5].endTime = 10;
    out.entries()[5].startTime = 10;

    // Surface charge density
    out.entries()[6].timestampFolder = true;
    out.entries()[6].skip = false;

    // Energy
    out.entries()[7].iterations = {3,6,9};
    out.entries()[7].skip = true;

    for (uint i = 0; i < out.entries().size(); i++)
        REQUIRE(o.output.entries()[i] == out.entries()[i]);

    std::vector<std::string> expectedFiles = {"tests/analysis/test1.jl", "tests/analysis/test2.jl", "tests/analysis/test3.jl"};
    REQUIRE(o.output.additionalJuliaFiles() == expectedFiles);

    REQUIRE(!o.output.automatedAnalysis());
}
