#include "../catch.hpp"
#include "LeapFrog.h"

using namespace picard;
using namespace std;

TEST_CASE("Linear Acceleration", "[particle_mover]"){
    LeapFrog lf;
    for(int d=1; d<=3; d++){
        // Setup the initial particle state
        vec r0 = uniform(vec(d,-10), vec(d,10));
        vec v0 = uniform(vec(d, -1), vec(d, 1));
        Real mass = uniform(0,10);
        Real charge = uniform(-10,10);
        auto species = make_shared<Species>("test", mass, charge);
        Particle particle(r0, v0, species.get());
        
        // Setup the time integration
        vec EField = uniform(vec(d,-10), vec(d,10));
        vec a0 = EField*charge / mass;
        lf.setEField(EField);
        Real t = 0;
        Real dt = 0.01;
        
        while(t < 1){
            vec compPos = r0 + v0*t + 0.5*a0*t*t;
            REQUIRE(feq(compPos, particle.r));
            lf.step(particle, dt, t, d, false);
            t += dt;
        }
    }
}

TEST_CASE("Oscillations", "[particle_mover]"){
    LeapFrog lf;
    for(int d=1; d<=3; d++){
        // Setup the initial particle state
        vec r0 = uniform(vec(d,-10), vec(d,10));
        vec v0 = uniform(vec(d, -1), vec(d, 1));
        Real w2 = uniform(1,20);
        Real w = sqrt(w2);
        Real m = uniform(0,10);
        Real q = uniform(-10,10);
        auto species = make_shared<Species>("test", m, q);
        Particle particle(r0, v0, species.get());

        // Setup the time integration
        Real t = 0;
        Real dt = .001/w;

        while(t < 1){
            vec compPos = (v0/w)*sin(w*t) + r0*cos(w*t); // Sinusoid
            REQUIRE(norm(compPos - particle.r) < 1e-5);

            // Set the electric field based on particle position
            vec EField = -(w2*m/q)*particle.r;
            lf.setEField(EField);

            // Integrate
            lf.step(particle, dt, t, d, false);
            t += dt;
        }
    }
}

TEST_CASE("Magnetic Field Cyclotron Motion", "[particle_mover]") {
    LeapFrog lf;
    int d = 2;// This motion is easiest to get in 2D

    // Setup the initial particle state
    vec r0 = {-1, 0};
    vec v0 = {0, -1};
    Real m = 1;
    Real q = 1;
    auto species = make_shared<Species>("test", m, q);
    Particle particle(r0, v0, species.get());

    // Setup the time integration
    double Bz = 1;
    lf.setBField({0, 0, Bz});
    lf.setEField({0, 0, 0});
    double w = q*Bz/m;
    Real t = 0;
    Real dt = .01/w;
    Real T = 2*pi/w;

    while (t < T) {
        vec compPos = norm(r0)*vec({-cos(w*t), -sin(w*t)});
        REQUIRE(norm(compPos - particle.r) < 1e-5);
        lf.step(particle, dt, t, d, true);
        t += dt;
    }

}
