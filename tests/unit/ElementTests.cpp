#include "../catch.hpp"
#include "Definitions.h"
#include "VectorUtils.h"
#include <iostream>

using namespace picard;
using namespace std;

TEST_CASE("Element Construction", "[elements]") {
    Element e1 = {1};
    REQUIRE(e1.dimension() == 0);
    REQUIRE(e1.size() == 1);
    std::array<int,4> ec1 = {1,-1,-1,-1};
    REQUIRE(e1.storage() == ec1);

    Element e2 = {2, 3};
    REQUIRE(e2.dimension() == 1);
    REQUIRE(e2.size() == 2);
    std::array<int, 4> ec2 = {2,3,-1,-1};
    REQUIRE(e2.storage() == ec2);

    Element e3 = {3, 4, 5, 6};
    REQUIRE(e3.dimension() == 2);
    REQUIRE(e3.size() == 4);
    std::array<int, 4> ec3 = {3,4,5,6};
    REQUIRE(e3.storage() == ec3);

    Cell c1({1, 2});
    Cell c2(2);
    c2[0] = 1;
    c2[1] = 2;
    c2[2] = 3;
    c2[3] = 4;
    Cell c3 = {1, 2, 4, 3, 3, 4, 3, 3};

    REQUIRE(c1.dimension() == 1);
    REQUIRE(c1.size() == 2);
    std::array<int,8> cc1 = {1,2,-1,-1,-1,-1,-1,-1};
    REQUIRE(c1.storage() == cc1);

    REQUIRE(c2.dimension() == 2);
    REQUIRE(c2.size() == 4);
    std::array<int,8> cc2 = {1,2,3,4,-1,-1,-1,-1};
    REQUIRE(c2.storage() == cc2);

    REQUIRE(c3.dimension() == 3);
    REQUIRE(c3.size() == 8);
    std::array<int,8> cc3 = {1, 2, 4, 3, 3, 4, 3, 3};
    REQUIRE(c3.storage() == cc3);
}

TEST_CASE("Element Iteration", "[elements]"){
    Element e = {3, 4};
    int index = 0;
    for(auto i : e){
        REQUIRE(e[index++] == i);
    }

    REQUIRE(index == e.size());
}


TEST_CASE("Element Sorting", "[elements]"){
    Element element1 = {1, 2, 3, 4};
    Element compare = {4, 3, 2, 1};
    fastSort4(element1, std::greater<int>());
    REQUIRE(element1 == compare);

    Element element2 = {3, 4, -1, -1};
    Element compare2 = {4, 3, -1, -1};
    fastSort4(element2, std::greater<int>());
    REQUIRE(element2 == compare2);

    Element element3 = {3, 4, 100, -1};
    Element compare3 = {100, 4, 3, -1};
    fastSort4(element3, std::greater<int>());
    REQUIRE(element3 == compare3);
}
