#include "../catch.hpp"
#include "Label.h"
#include "Grid.h"
#include "BoundaryCondition.h"
#include "Interpolator.h"

using namespace picard;
using namespace std;

TEST_CASE("Open Boundary Condition", "[boundary_conditions]"){
    std::map<int, std::shared_ptr<ParticleBoundaryLabel>> particleLabels = {
            makeReflecting(X_MIN, 0, 1.),
            makeOpen(X_MAX, 0),
            makeReflecting(Y_MIN, 0, 1.),
            makeReflecting(Y_MAX, 0, 1.)
    };
    
    auto grid = Grid::SingleDomain(Domain(2, {-1, -1}, {1, 1}), {101, 201}, {}, particleLabels);
    grid->allocateGridData(1);
    vec end = {2,0};
    vec start = {0,0};
    vec v = {2,0};
    Particle p(end, v);
    p.r_last = start;
    auto intersection = grid->intersectsBoundary(p.ray());
    REQUIRE(intersection.label->type == ParticleBoundaryLabel::OPEN);
    REQUIRE(intersection.id == X_MAX);

    auto bcResult = BoundaryConditions::apply(intersection, p, *grid, Interpolator());

    REQUIRE(bcResult.particlesToAdd.size() == 0);
    REQUIRE(bcResult.status == REMOVE_PARTICLE);
    REQUIRE(p.r == end);
    REQUIRE(p.r_last == start);
    REQUIRE(p.v == v);
}

TEST_CASE("Periodic Boundary Condition", "[boundary_conditions]"){
    std::map<int, std::shared_ptr<ParticleBoundaryLabel>> particleLabels = {
            makeOpen(X_MIN, 0),
            makePeriodicParticle(X_MAX, 0),
            makeOpen(Y_MIN, 0),
            makeOpen(Y_MAX, 0)
    };

    auto grid = Grid::SingleDomain(Domain(2, {-1, -1}, {1, 1}), {101, 201}, {}, particleLabels);
    grid->allocateGridData(1);
    vec end = {2,0};
    vec start = {0,0};
    vec v = {2,0};
    Particle p(end, v);
    p.r_last = start;
    auto intersection = grid->intersectsBoundary(p.ray());
    REQUIRE(intersection.label->type == ParticleBoundaryLabel::PERIODIC);
    REQUIRE(intersection.id == X_MAX);

    auto bcResult = BoundaryConditions::apply(intersection, p, *grid, Interpolator());

    REQUIRE(bcResult.particlesToAdd.size() == 0);
    REQUIRE(bcResult.status == PARTICLE_UPDATED);
    REQUIRE(p.r == start);
    REQUIRE(p.r_last == start);
    REQUIRE(p.v == v);
}

TEST_CASE("Reflecting Boundary Condition", "[boundary_conditions]"){
    std::map<int, std::shared_ptr<ParticleBoundaryLabel>> particleLabels = {
            makeOpen(X_MIN, 0),
            makeReflecting(X_MAX, 0, 0.5),
            makeOpen(Y_MIN, 0),
            makeOpen(Y_MAX, 0)
    };

    auto grid = Grid::SingleDomain(Domain(2, {-1, -1}, {1, 1}), {101, 201}, {}, particleLabels);
    grid->allocateGridData(1);
    vec end = {2,0};
    vec start = {0,0};
    vec v = {2,0};
    Particle p(end, v);
    p.r_last = start;
    auto intersection = grid->intersectsBoundary(p.ray());
    REQUIRE(intersection.label->type == ParticleBoundaryLabel::REFLECTING);
    REQUIRE(intersection.id == X_MAX);

    auto bcResult = BoundaryConditions::apply(intersection, p, *grid, Interpolator());

    REQUIRE(bcResult.particlesToAdd.size() == 0);
    REQUIRE(bcResult.status == PARTICLE_UPDATED);
    REQUIRE(p.r == start);
    REQUIRE(p.r_last == intersection.point);
    REQUIRE(p.v == -0.5*v);
}

TEST_CASE("Scattering Boundary Condition", "[boundary_conditions]"){
    std::map<int, std::shared_ptr<ParticleBoundaryLabel>> particleLabels = {
            makeOpen(X_MIN, 0),
            makeScattering(X_MAX, 0, 1.0),
            makeOpen(Y_MIN, 0),
            makeOpen(Y_MAX, 0)
    };

    auto grid = Grid::SingleDomain(Domain(2, {-1, -1}, {1, 1}), {101, 201}, {}, particleLabels);
    grid->allocateGridData(1);
    vec end = {2,0};
    vec start = {0,0};
    vec v = {2,0};
    Particle p(end, v);
    p.r_last = start;
    auto intersection = grid->intersectsBoundary(p.ray());
    REQUIRE(intersection.label->type == ParticleBoundaryLabel::SCATTERING);
    REQUIRE(intersection.id == X_MAX);

    auto bcResult = BoundaryConditions::apply(intersection, p, *grid, Interpolator());

    REQUIRE(bcResult.particlesToAdd.size() == 0);
    REQUIRE(bcResult.status == PARTICLE_UPDATED);
    REQUIRE(grid->isInside(p.r));
    REQUIRE(p.r_last == intersection.point);
    REQUIRE(feq(norm(p.v), norm(v)));
}

TEST_CASE("Absorbing Boundary Condition", "[boundary_conditions]"){
    std::map<int, std::shared_ptr<ParticleBoundaryLabel>> particleLabels = {
            makeOpen(X_MIN, 0),
            makeAbsorbing(X_MAX, 0),
            makeOpen(Y_MIN, 0),
            makeOpen(Y_MAX, 0)
    };

    auto grid = Grid::SingleDomain(Domain(2, {-10, -10}, {10, 10}), {15, 15}, {}, particleLabels);
    grid->allocateGridData(1);
    vec end = {20,0};
    vec start = {0.1,0.1};
    vec v = {20,0};
    auto species = make_shared<Species>("test", 1,1);
    Particle p(end, v, species.get());
    p.r_last = start;
    auto intersection = grid->intersectsBoundary(p.ray());
    REQUIRE(intersection.label->type == ParticleBoundaryLabel::ABSORBING);
    REQUIRE(intersection.id == X_MAX);

    auto bcResult = BoundaryConditions::apply(intersection, p, *grid, Interpolator());

    REQUIRE(bcResult.particlesToAdd.size() == 0);
    REQUIRE(bcResult.status == REMOVE_PARTICLE);
    REQUIRE(p.r == end);
    REQUIRE(p.r_last == start);
    REQUIRE(p.v == v);

    Interpolator interp;
    int lowerLeft = grid->index(intersection.point);
    grid->surfaceChargeGlobal = grid->surfaceChargeLocal;
    auto element = grid->getBoundaryElement(lowerLeft,0);
    for(auto i : element){
        auto weight = interp.W(grid->position(i), intersection.point, grid->spacing());
        auto charge = p.q() * weight * grid->spacing(0) / grid->cellVolume();
        REQUIRE(feq(grid->getSurfaceCharge(i), charge));
    }
}

TEST_CASE("Solid Boundary Condition", "[boundary_conditions]"){

}

