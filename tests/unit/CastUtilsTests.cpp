#include "../catch.hpp"
#include "CastUtils.h"
#include <iostream>

using namespace picard;
using namespace std;

TEST_CASE("casting", "[cast_utils]") {
    // Generic Casting
    int i1 = cast<int>(1.4);
    REQUIRE(i1 == int(1.4));
    
    double i2 = cast<double>(1);
    REQUIRE(i2 == double(1));
    
    // Converting strings to numbers
    int i3 = cast<int>(std::string("1.4"));
    REQUIRE(i3 == stoi("1.4"));
    
    int i4 = cast<int>(std::string("1"));
    REQUIRE(i4 == stoi("1"));
    
    double i5 = cast<double>(std::string("1.4"));
    REQUIRE(i5 == stod("1.4"));
    
    double i6 = cast<double>(std::string("1"));
    REQUIRE(i6 == stod("1"));

    REQUIRE(true == cast<bool>(std::string("true")));
    REQUIRE(true == cast<bool>(std::string("True")));
    REQUIRE(false == cast<bool>(std::string("false")));
    REQUIRE(false == cast<bool>(std::string("False")));
    REQUIRE(false == cast<bool>(std::string("abc")));

    
    // Converting numbers to strings
    std::string s1 = cast<std::string>(1);
    REQUIRE(s1 == "1");
    
    std::string s2 = cast<std::string>(1.2);
    REQUIRE(1.2 == cast<double>(s2));

    std::string s3 = cast<std::string>(1.2, 0);
    REQUIRE(1.0 == cast<double>(s3));

    // Converting vectors to vectors
    std::vector<double> v1 = {1.2, 3.4, 5.6};
    std::vector<int> v1_int = vcast<int>(v1);
    std::vector<int> v1_int2 = cast<std::vector<int>>(v1);
    REQUIRE(v1_int == std::vector<int>({1,3,5}));
    REQUIRE(v1_int2 == std::vector<int>({1,3,5}));

    std::vector<int> v2 = {1, 3, 5};
    std::vector<double> v2_double = vcast<double>(v2);
    std::vector<double> v2_double2 = cast<std::vector<double>>(v2);
    REQUIRE(v2_double == std::vector<double>({1., 3., 5.}));
    REQUIRE(v2_double2 == std::vector<double>({1., 3., 5.}));

    std::vector<double> v3 = {1.2, 3.2, 5.2};
    std::vector<std::string> v3_string = vcast<std::string>(v3);
    std::vector<std::string> v3_string2 = cast<std::vector<std::string>>(v3);
    REQUIRE(v3 == vcast<double>(v3_string));
    REQUIRE(v3 == vcast<double>(v3_string2));

    
    // Converting Scalars to vectors
    std::vector<std::string> v4 = cast<std::vector<std::string>>(1.5);
    REQUIRE(v4 == std::vector<std::string>({cast<std::string>(1.5)}));

    std::vector<std::string> v5 = cast<std::vector<std::string>>(5);
    REQUIRE(v5 == std::vector<std::string>({"5"}));

    std::vector<int> v6 = cast<std::vector<int>>(1.5);
    REQUIRE(v6 == std::vector<int>({1}));

    // Converting string to string
    std::string st1 = "1sdlksdf";
    REQUIRE(st1 == cast<std::string>(st1));
}

TEST_CASE("To String From Stream", "[cast_utils]"){
    REQUIRE(tsfs(1) == "1");
    REQUIRE(tsfs(1.4) == "1.4");
}

TEST_CASE("Pointer and reference conversions", "[cast_utils]"){
    auto a = make_shared<int>(1);
    REQUIRE_NOTHROW(ptr_to_ref(a));
    REQUIRE_NOTHROW(ptr_to_cref(a));

    REQUIRE(ptr_to_ref(a) == *a);
    REQUIRE(ptr_to_cref(a) == *a);

    REQUIRE_THROWS(ptr_to_ref(shared_ptr<int>(nullptr)));
    REQUIRE_THROWS(ptr_to_cref(shared_ptr<int>(nullptr)));
}
