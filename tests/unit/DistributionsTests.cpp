#include <memory>
#include "../catch.hpp"

#include "Distributions.h"

using namespace picard;

TEST_CASE("Multidistribution params", "[distributions]"){
    vec mean = {0.3, 0.3, 0.3};
    vec stddev = {0.2, 0.2, 0.2};
    auto md = normal_multi_distribution(mean, stddev);
    REQUIRE(md.param(0) == mean);
    REQUIRE(md.param(1) == stddev);
}

TEST_CASE("Test normal distribution", "[distributions]") {
    int trials = 5000;
    int num_at_sigma = 0;
    Real sigma = 1.0;
    Real mean = 0;
    distribution<Real> normal = std::normal_distribution<Real>(mean, sigma);

    for (int i = 0; i < trials; i++) {
        if ((std::abs(normal()) - mean) < sigma)
            num_at_sigma++;
    }
    REQUIRE((Real) num_at_sigma / (Real) trials > 0.65);
    REQUIRE((Real) num_at_sigma / (Real) trials < 0.71);
}

TEST_CASE("Test uniform distribution", "[distributions]") {
    int trials = 100;
    Real min = 0;
    Real max = 1.0;
    distribution<Real> uniform = std::uniform_real_distribution<Real>(min, max);

    for (int i = 0; i < trials; i++) {
        auto val = uniform();
        REQUIRE(val >= min);
        REQUIRE(val <= max);
    }
}

TEST_CASE("Test uniform distribution (void parameters)", "[distributions]") {
    int trials = 100;
    for (int i = 0; i < trials; i++) {
        auto val = uniform();
        REQUIRE(val >= 0);
        REQUIRE(val <= 1);
    }
}

TEST_CASE("Test uniform int distribution", "[distributions]") {
    int trials = 100;
    int min = 0;
    int max = std::numeric_limits<int>::max();
    distribution<Real> uniform = std::uniform_real_distribution<Real>(min, max);

    for (int i = 0; i < trials; i++) {
        auto val = uniform();
        REQUIRE(val >= min);
        REQUIRE(val <= max);
    }
}

TEST_CASE("Test uniform int distribution (void parameters)", "[distributions]") {
    int trials = 100;
    for (int i = 0; i < trials; i++) {
        auto val = uniform_int();
        REQUIRE(val >= 0);
        REQUIRE(val <= std::numeric_limits<int>::max());
    }
}

TEST_CASE("Random ivec Generation", "[distributions]") {
    int trials = 100;
    int halfRange = 10;

    for (int i = 0; i < trials; i++) {
        auto size = uniform_int(1, 3);
        auto min = uniform_int(ivec(size, -halfRange), ivec(size, 0));
        auto max = uniform_int(ivec(size, 0), ivec(size, halfRange));

        auto v = uniform_int(min, max);
        REQUIRE(v.size() == size);

        for (uint i = 0; i < v.size(); i++) {
            REQUIRE(v[i] >= min[i]);
            REQUIRE(v[i] <= max[i]);
        }
    }
}

TEST_CASE("Random vec Generation", "[distributions]") {
    int trials = 100;
    Real halfRange = 8.5;

    for (int i = 0; i < trials; i++) {
        auto size = uniform_int(1, 3);
        auto min = uniform(vec(size, -halfRange), vec(size, 0));
        auto max = uniform(vec(size, 0), vec(size, halfRange));

        auto v = uniform(min, max);
        REQUIRE(v.size() == size);

        for (uint i = 0; i < v.size(); i++) {
            REQUIRE(v[i] >= min[i]);
            REQUIRE(v[i] <= max[i]);
        }
    }
}

TEST_CASE("Different Seed Each Session"){
    //TODO
}

TEST_CASE("Different Threads Use Different Generators"){
    //TODO
}
