#include "../catch.hpp"
#include "NeutralCollisions.h"
#include "Options.h"
#include "Grid.h"

using namespace picard;
using namespace std;

TEST_CASE("Electron-Neutral Collisions", "[neutral_collision tests]") {
    Options o("tests/config/collisional_drift_electrons.cfg");

    // Check species
    REQUIRE(o.species.size() == 2);
    auto electron = o.species["electron"];
    auto neutral = o.species["neutral"];

    // Check on grid initialization
    auto& neutral_init = o.populationInit[neutral];

    // Check number of physics
    REQUIRE(o.localPhysics.size() == 1);

    // Check that the type of physics created is the correct type
    auto en_collisions = dynamic_pointer_cast<ElectronNeutralCollisions>(o.localPhysics[0]);
    REQUIRE(en_collisions != shared_ptr<ElectronNeutralCollisions>(nullptr));

    // Check that electron species matches electrons
    REQUIRE(en_collisions->get_charged_species() == electron);

    // Check that neutral species matches neutrals
    REQUIRE(en_collisions->get_neutrals() == neutral);

    // Check that cross section data matches the file
    map<Real, Real> cross_section_samples = {
            {0.001, 8E-20},
            {0.01,  6E-20},
            {0.1,   1E-20},
            {0.2,   1E-21},
            {1,     1E-20},
            {13,    2.3E-19},
            {100,   8E-20}
    };
    REQUIRE(en_collisions->get_sigma_samples().size() == 1);
    REQUIRE(en_collisions->get_coefficients().size() == 1);
    REQUIRE(en_collisions->get_sigma_samples()[0] == cross_section_samples);
    REQUIRE(en_collisions->get_coefficients()[0] == getExpCoefficients(cross_section_samples));

    // Apply physics to a particle with a fixed energy and compare frequency of collisions to probabiliy calculated by hand
    //TODO: probably do this in another test
}
