#include "../catch.hpp"
#include "Label.h"
#include "Grid.h"
#include "DirectSolver.h"
#include "MultiGridSolver.h"

using namespace picard;
using namespace std;

TEST_CASE("Linear Solve tests (1D, 2D and 3D grids) with all solver types", "[field_solver]"){
//    for(int d = 1; d<=3; d++){
//        Domain domain = Domain::Random(d);
//        Real min_phi = uniform(-10, 10);
//        Real max_phi = uniform(-10, 10);
//        Real slope = (max_phi - min_phi) / (domain.bounds(X_MAX) - domain.bounds(X_MIN));
//        Real offset = min_phi - slope*domain.bounds(X_MIN);
//
//        std::map<int, std::shared_ptr<FieldLabel>> fieldLabels;
//        fieldLabels.insert(makeInterior(0, 1, true));
//        fieldLabels.insert(makeDirichlet(X_MIN, 1, min_phi));
//        fieldLabels.insert(makeDirichlet(X_MAX, 1, max_phi));
//
//        for(int id=2; id<2*d; id++)
//            fieldLabels.insert(makeNeumann((BoundaryId)id, 0, 0));
//
//        auto grid = Grid::SingleDomain(domain, uniform_int(ivec(d,3), ivec(d,50)), fieldLabels, {});
//        grid->allocateGridData(1);
//
////        cerr << "d: " << d << " minphi: " << min_phi << " maxphi: " << max_phi << " grid: " << *grid << endl;
//
//        vector<shared_ptr<FieldSolver>> solvers = {make_shared<DirectSolver>(*grid), make_shared<MultiGridSolver>(*grid)};
//        vector<string> solverNames = {"direct", "multigrid"};
//        int count = 0;
//        for(auto solver : solvers){
////            cerr << "solver: " << solverNames[count++] << endl;
//            solver->findPotential(0, 0, *grid);
//            solver->findElectricField(*grid);
//
//            auto negSlope = -slope;
//            for(int i=0; i<grid->totalPoints(); i++){
//                REQUIRE(std::abs(grid->eField(i,0) - negSlope) < 1e-10);
//                for(int j=1; j<d; j++)
//                    REQUIRE(std::abs(grid->eField(i,j)) < 1e-10);
//                REQUIRE(abs(grid->potential[i] - (grid->position(i,0)*slope + offset)) < 1e-10);
//            }
//        }
//    }
}

