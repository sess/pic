#include "../catch.hpp"
#include "Grid.h"
#include "GmshReader.h"
#include "Label.h"
#include <string>
#include <fstream>
#include "Simulation.h"

using namespace picard;
using namespace std;

TEST_CASE("Spatial Comparison Test", "[mesh_parsing]") {
    GmshNode n1, n2;
    n1.r = {0.01, 0, 0};
    n2.r = {0.02, 0, 0};

    SpatialLessThan s(0.000990099, 1);
    REQUIRE(s(n1, n2));
    REQUIRE(!s(n2, n1));
}


// Parsing check
TEST_CASE("Parsing check with 1D mesh", "[mesh_parsing]") {
    string mesh_path = "tests/meshes/1Dmesh.msh";
    GmshReader r(mesh_path);
    r.sort();
    // Test that the parser has worked properly and read the correct number of elements
    REQUIRE(r.nodes().size() == 101);
    REQUIRE(r.lastParsed() == mesh_path);
    REQUIRE(r.elements().size() == 102);
    REQUIRE(r.nDim() == 1);
    REQUIRE(feq(r.getDomain().min(), vec({0})));
    REQUIRE(feq(r.getDomain().max(), vec({1})));
    auto& labels = r.labels();
    REQUIRE(labels.size() == 3);
    REQUIRE(labels.at(1).name == "left");
    REQUIRE(labels.at(2).name == "right");
    REQUIRE(labels.at(3).name == "interior");
    for (auto entry : labels) {
        REQUIRE(entry.first == entry.second.id);
        if (entry.second.name != "interior")
            REQUIRE(entry.second.dim == 0);
        else
            REQUIRE(entry.second.dim == 1);
    }

    int nBoundaryElements = 0;
    int nInteriorElements = 0;
    for (auto& e : r.elements()) {
        if (e.type == POINT)
            nBoundaryElements++;
        else if (e.type == BAR)
            nInteriorElements++;
    }
    REQUIRE(nBoundaryElements == 2);
    REQUIRE(nInteriorElements == 100);
}

TEST_CASE("Parsing check with single-block 2D mesh", "[mesh_parsing]") {
    string mesh_path = "tests/meshes/2Dsingleblockmesh.msh";
    GmshReader r(mesh_path);
    r.sort();
    // Test that the parser has worked properly and read the correct number of elements
    REQUIRE(r.nodes().size() == 100);
    REQUIRE(r.lastParsed() == mesh_path);
    REQUIRE(r.elements().size() == 117);
    REQUIRE(r.nDim() == 2);
    REQUIRE(feq(r.getDomain().min(), vec({0, 0})));
    REQUIRE(feq(r.getDomain().max(), vec({1, 1})));
    auto& labels = r.labels();
    REQUIRE(labels.size() == 5);
    REQUIRE(labels.at(1).name == "bottom");
    REQUIRE(labels.at(2).name == "right");
    REQUIRE(labels.at(3).name == "top");
    REQUIRE(labels.at(4).name == "left");
    REQUIRE(labels.at(5).name == "interior");
    for (auto entry : labels) {
        REQUIRE(entry.first == entry.second.id);
        if (entry.second.name != "interior")
            REQUIRE(entry.second.dim == 1);
        else
            REQUIRE(entry.second.dim == 2);
    }

    int nBoundaryElements = 0;
    int nInteriorElements = 0;
    for (auto& e : r.elements()) {
        if (e.type == BAR)
            nBoundaryElements++;
        else if (e.type == QUAD)
            nInteriorElements++;
    }
    REQUIRE(nBoundaryElements == 36);
    REQUIRE(nInteriorElements == 81);
}

TEST_CASE("Parsing check with single-block 3D mesh", "[mesh_parsing]") {
    string mesh_path = "tests/meshes/3Dsingleblockmesh.msh";
    GmshReader r(mesh_path);
    r.sort();
    // Test that the parser has worked properly and read the correct number of elements
    REQUIRE(r.nodes().size() == 1000);
    REQUIRE(r.lastParsed() == mesh_path);
    REQUIRE(r.elements().size() == 1215);
    REQUIRE(r.nDim() == 3);
    REQUIRE(feq(r.getDomain().min(), vec({0, 0, 0})));
    REQUIRE(feq(r.getDomain().max(), vec({1, 1, 1})));
    auto& labels = r.labels();
    REQUIRE(labels.size() == 7);
    REQUIRE(labels.at(1).name == "interior");
    REQUIRE(labels.at(2).name == "x0");
    REQUIRE(labels.at(3).name == "x1");
    REQUIRE(labels.at(4).name == "y0");
    REQUIRE(labels.at(5).name == "y1");
    REQUIRE(labels.at(6).name == "z0");
    REQUIRE(labels.at(7).name == "z1");
    for (auto entry : labels) {
        REQUIRE(entry.first == entry.second.id);
        if (entry.second.name != "interior")
            REQUIRE(entry.second.dim == 2);
        else
            REQUIRE(entry.second.dim == 3);
    }

    int nBoundaryElements = 0;
    int nInteriorElements = 0;
    for (auto& e : r.elements()) {
        if (e.type == QUAD)
            nBoundaryElements++;
        else if (e.type == HEX)
            nInteriorElements++;
    }
    REQUIRE(nBoundaryElements == 81 * 6);
    REQUIRE(nInteriorElements == 9 * 9 * 9);
}

TEST_CASE("Parsing check with dual-block 2D mesh", "[mesh_parsing]") {
    string mesh_path = "tests/meshes/2Ddualblockmesh.msh";
    GmshReader r(mesh_path);
    r.sort();
    // Test that the parser has worked properly and read the correct number of elements
    REQUIRE(r.nodes().size() == 90751);
    REQUIRE(r.lastParsed() == mesh_path);
    REQUIRE(r.elements().size() == 92100);
    REQUIRE(r.nDim() == 2);
    REQUIRE(feq(r.getDomain().min(), vec({-300, 0})));
    REQUIRE(feq(r.getDomain().max(), vec({300, 150})));
    auto& labels = r.labels();
    REQUIRE(labels.size() == 11);
    REQUIRE(labels.at(1).name == "plasma");
    REQUIRE(labels.at(2).name == "dielectric");
    REQUIRE(labels.at(3).name == "buffer_lower");
    REQUIRE(labels.at(4).name == "buffer_upper");
    REQUIRE(labels.at(5).name == "electrode");
    REQUIRE(labels.at(6).name == "interface");
    REQUIRE(labels.at(7).name == "left");
    REQUIRE(labels.at(8).name == "right");
    REQUIRE(labels.at(9).name == "spacecraft");
    REQUIRE(labels.at(10).name == "spacing");
    REQUIRE(labels.at(11).name == "top");
    for (auto entry : labels) {
        REQUIRE(entry.first == entry.second.id);
        if (entry.second.name != "plasma" && entry.second.name != "dielectric")
            REQUIRE(entry.second.dim == 1);
        else
            REQUIRE(entry.second.dim == 2);
    }

    int nBoundaryElements = 0;
    int nInteriorElements = 0;
    for (auto& e : r.elements()) {
        if (e.type == BAR)
            nBoundaryElements++;
        else if (e.type == QUAD)
            nInteriorElements++;
    }
    REQUIRE(nBoundaryElements == 3 * 600 + 2 * 150);
    REQUIRE(nInteriorElements == 600 * 150);
}


// Sorting and num grid pts calculation
TEST_CASE("Sorting and num grid pts check with 1D mesh", "[mesh_parsing]") {
    string mesh_path = "tests/meshes/1Dmesh.msh";
    GmshReader r(mesh_path);
    r.sort();
    auto& nodes = r.nodes();

    REQUIRE(r.getNumGridPoints() == ivec({101}));

    auto trials = r.nodes().size();
    for (uint i = 0; i < trials; i++) {
        auto i1 = rand() % r.nodes().size();
        auto i2 = rand() % r.nodes().size();
        auto nmin = std::min(i1, i2);
        auto nmax = std::max(i1, i2);

        for (int d = r.nDim() - 1; d >= 0; d--) {
            REQUIRE(fleq(nodes[nmin].r[d], nodes[nmax].r[d]));
            if (!feq(nodes[nmin].r[d], nodes[nmax].r[d]))
                break;
        }
    }
}

TEST_CASE("Sorting and num grid pts check with single-block 2D mesh", "[mesh_parsing]") {
    string mesh_path = "tests/meshes/2Dsingleblockmesh.msh";
    GmshReader r(mesh_path);
    r.sort();
    auto& nodes = r.nodes();

    REQUIRE(r.getNumGridPoints() == ivec({10, 10}));

    auto trials = r.nodes().size();
    for (uint i = 0; i < trials; i++) {
        auto i1 = rand() % r.nodes().size();
        auto i2 = rand() % r.nodes().size();
        auto nmin = std::min(i1, i2);
        auto nmax = std::max(i1, i2);

        for (int d = r.nDim() - 1; d >= 0; d--) {
            REQUIRE(fleq(nodes[nmin].r[d], nodes[nmax].r[d]));
            if (!feq(nodes[nmin].r[d], nodes[nmax].r[d]))
                break;
        }
    }
}

TEST_CASE("Sorting and num grid pts check with single-block 3D mesh", "[mesh_parsing]") {
    string mesh_path = "tests/meshes/3Dsingleblockmesh.msh";
    GmshReader r(mesh_path);
    r.sort();
    auto& nodes = r.nodes();

    REQUIRE(r.getNumGridPoints() == ivec({10, 10, 10}));

    auto trials = r.nodes().size();
    for (uint i = 0; i < trials; i++) {
        auto i1 = rand() % r.nodes().size();
        auto i2 = rand() % r.nodes().size();
        auto nmin = std::min(i1, i2);
        auto nmax = std::max(i1, i2);

        for (int d = r.nDim() - 1; d >= 0; d--) {
            REQUIRE(fleq(nodes[nmin].r[d], nodes[nmax].r[d]));
            if (!feq(nodes[nmin].r[d], nodes[nmax].r[d]))
                break;
        }
    }
}

TEST_CASE("Sorting and num grid pts check with dual-block 2D mesh", "[mesh_parsing]") {
    string mesh_path = "tests/meshes/2Ddualblockmesh.msh";
    GmshReader r(mesh_path);
    r.sort();
    auto& nodes = r.nodes();

    REQUIRE(r.getNumGridPoints() == ivec({601, 151}));

    auto trials = r.nodes().size();
    for (uint i = 0; i < trials; i++) {
        auto i1 = rand() % r.nodes().size();
        auto i2 = rand() % r.nodes().size();
        auto nmin = std::min(i1, i2);
        auto nmax = std::max(i1, i2);

        for (int d = r.nDim() - 1; d >= 0; d--) {
            REQUIRE(fleq(nodes[nmin].r[d], nodes[nmax].r[d]));
            if (!feq(nodes[nmin].r[d], nodes[nmax].r[d]))
                break;
        }
    }
}


// Grid generation
TEST_CASE("Grid generation check with 1D mesh", "[mesh_parsing]") {
    string mesh_path = "tests/meshes/1Dmesh.msh";
    GmshReader r(mesh_path);

    vector<shared_ptr<FieldLabel>> fieldLabels = {
            makeInterior("interior", 1, 1, true),
            makeDirichlet("right", 2, {1}, 0),
            makeDirichlet("left", 3, {-1}, 0),

    };

    vector<shared_ptr<ParticleBoundaryLabel>> particleLabels = {
            makeOpen("right", 1, {1}),
            makeOpen("left", 2, {-1})
    };

    auto grid = r.makeGrid(fieldLabels, particleLabels);

    // Check basic grid attributes
    REQUIRE(grid->particleBoundaryLabels.size() == 2);
    REQUIRE(grid->fieldLabels.size() == 101);
    REQUIRE(grid->nDim() == 1);
    REQUIRE(r.nDim() == 1);
    REQUIRE(feq(r.getDomain().min(), vec({0})));
    REQUIRE(feq(r.getDomain().max(), vec({1})));

    // Check that the nodes are the same as they are in the parser and are what should have been calculated
    for (uint n = 0; n < r.nodes().size(); n++) {
        auto ijk = grid->ijk(n);
        for (auto i = 0; i < r.nDim(); i++) {
            REQUIRE(grid->position(n, i) == r.nodes()[n].r[i]);
            REQUIRE(feq(grid->position(n, i), grid->min(i) + grid->spacing(i) * ijk[i]));
        }
    }

    // Require that edge elements have the right labeling
    for (auto entry : grid->particleBoundaryLabels) {
        auto ijk1 = grid->ijk(entry.first[0]);
        if (ijk1[0] == 0) REQUIRE(entry.second->name == "left");
        if (ijk1[0] == 100) REQUIRE(entry.second->name == "right");
    }

}

TEST_CASE("Grid generation check with single-block 2D mesh", "[mesh_parsing]") {
    string mesh_path = "tests/meshes/2Dsingleblockmesh.msh";
    GmshReader r(mesh_path);

    vector<shared_ptr<FieldLabel>> fieldLabels = {
            makeInterior("interior", 0, 1, true),
            makeDirichlet("top", 1, {0, 1}, 0),
            makeDirichlet("bottom", 2, {0, -1}, 100),
            makeDirichlet("right", 3, {1, 0}, 0),
            makeDirichlet("left", 4, {-1, 0}, 0),

    };

    vector<shared_ptr<ParticleBoundaryLabel>> particleLabels = {
            makeOpen("top", 1, {0, 1}),
            makeReflecting("bottom", 2, {0, -1}, 1),
            makeOpen("right", 3, {1, 0}),
            makeOpen("left", 4, {-1, 0})
    };

    auto grid = r.makeGrid(fieldLabels, particleLabels);

    // Check basic grid attributes
    REQUIRE(grid->particleBoundaryLabels.size() == 36);
    REQUIRE(grid->fieldLabels.size() == 100);
    REQUIRE(grid->nDim() == 2);
    REQUIRE(r.nDim() == 2);
    REQUIRE(feq(r.getDomain().min(), vec({0, 0})));
    REQUIRE(feq(r.getDomain().max(), vec({1, 1})));

    // Check that the nodes are the same as they are in the parser and are what should have been calculated
    for (uint n = 0; n < r.nodes().size(); n++) {
        auto ijk = grid->ijk(n);
        for (auto i = 0; i < r.nDim(); i++) {
            REQUIRE(grid->position(n, i) == r.nodes()[n].r[i]);
            REQUIRE(feq(grid->position(n, i), grid->min(i) + grid->spacing(i) * ijk[i]));
        }
    }

    // Require that edge elements have the right labeling
    for (auto entry : grid->particleBoundaryLabels) {
        auto ijk1 = grid->ijk(entry.first[0]);
        auto ijk2 = grid->ijk(entry.first[1]);
        if (ijk1[0] == 0 && ijk2[0] == 0) REQUIRE(entry.second->name == "left");
        if (ijk1[0] == 9 && ijk2[0] == 9) REQUIRE(entry.second->name == "right");
        if (ijk1[1] == 0 && ijk2[1] == 0) REQUIRE(entry.second->name == "bottom");
        if (ijk1[1] == 9 && ijk2[1] == 9) REQUIRE(entry.second->name == "top");
    }

}

TEST_CASE("Grid generation check with single-block 3D mesh", "[mesh_parsing]") {
    string mesh_path = "tests/meshes/3Dsingleblockmesh.msh";
    GmshReader r(mesh_path);

    vector<shared_ptr<FieldLabel>> fieldLabels = {
            makeInterior("interior", 0, 1, true),
            makeDirichlet("x0", 2, {1, 0, 0}, 0),
            makeDirichlet("x1", 3, {-1, 0, 0}, 0),
            makeDirichlet("y0", 4, {0, 1, 0}, 0),
            makeDirichlet("y1", 5, {0, -1, 0}, 0),
            makeDirichlet("z0", 6, {0, 0, 1}, 0),
            makeDirichlet("z1", 7, {0, 0, -1}, 100),
    };

    vector<shared_ptr<ParticleBoundaryLabel>> particleLabels = {
            makeOpen("x0", 1, {1, 0, 0}),
            makeOpen("x1", 2, {-1, 0, 0}),
            makeOpen("y0", 3, {0, 1, 0}),
            makeOpen("y1", 4, {0, -1, 0}),
            makeOpen("z0", 5, {0, 0, 1}),
            makeReflecting("z1", 6, {0, 0, -1}, 1),
    };

    auto grid = r.makeGrid(fieldLabels, particleLabels);

    // Check basic grid attributes
    REQUIRE(grid->particleBoundaryLabels.size() == 81 * 6);
    REQUIRE(grid->fieldLabels.size() == 1000);
    REQUIRE(grid->nDim() == 3);
    REQUIRE(r.nDim() == 3);
    REQUIRE(feq(r.getDomain().min(), vec({0, 0, 0})));
    REQUIRE(feq(r.getDomain().max(), vec({1, 1, 1})));

    // Check that the nodes are the same as they are in the parser and are what should have been calculated
    for (uint n = 0; n < r.nodes().size(); n++) {
        auto ijk = grid->ijk(n);
        for (auto i = 0; i < r.nDim(); i++) {
            REQUIRE(grid->position(n, i) == r.nodes()[n].r[i]);
            REQUIRE(feq(grid->position(n, i), grid->min(i) + grid->spacing(i) * ijk[i]));
        }
    }

    // Require that edge elements have the right labeling
    for (auto entry : grid->particleBoundaryLabels) {
        auto ijk1 = grid->ijk(entry.first[0]);
        auto ijk2 = grid->ijk(entry.first[1]);
        auto ijk3 = grid->ijk(entry.first[2]);
        auto ijk4 = grid->ijk(entry.first[3]);
        if (ijk1[0] == 0 && ijk2[0] == 0 && ijk3[0] == 0 && ijk4[0] == 0)
            REQUIRE(entry.second->name == "x0");
        if (ijk1[0] == 9 && ijk2[0] == 9 && ijk3[0] == 9 && ijk4[0] == 9)
            REQUIRE(entry.second->name == "x1");

        if (ijk1[1] == 0 && ijk2[1] == 0 && ijk3[1] == 0 && ijk4[1] == 0)
            REQUIRE(entry.second->name == "y0");
        if (ijk1[1] == 9 && ijk2[1] == 9 && ijk3[1] == 9 && ijk4[1] == 9)
            REQUIRE(entry.second->name == "y1");

        if (ijk1[2] == 0 && ijk2[2] == 0 && ijk3[2] == 0 && ijk4[2] == 0)
            REQUIRE(entry.second->name == "z0");
        if (ijk1[2] == 9 && ijk2[2] == 9 && ijk3[2] == 9 && ijk4[2] == 9)
            REQUIRE(entry.second->name == "z1");
    }

}

TEST_CASE("Grid generation check with dual-block 2D mesh", "[mesh_parsing]") {
    string mesh_path = "tests/meshes/2Ddualblockmesh.msh";
    GmshReader r(mesh_path);

    vector<shared_ptr<FieldLabel>> fieldLabels = {
            makeInterior("plasma", 0, 1, true),
            makeInterior("dielectric", 0, 10, false),
            makeDirichlet("buffer_lower", 3, {0, -1}, 0),
            makeDirichlet("buffer_upper", 4, {0, -1}, 0),
            makeDirichlet("electrode", 5, {0, -1}, 0),
            makeDirichlet("interface", 6, {0, -1}, 0),
            makeDirichlet("left", 7, {-1, 0}, 0),
            makeDirichlet("right", 8, {1, 0}, 100),
            makeDirichlet("spacing", 8, {0, -1}, 100),
            makeDirichlet("top", 8, {0, 1}, 100),
            makeDirichlet("spacecraft", 8, {0, 1}, 100),
    };

    vector<shared_ptr<ParticleBoundaryLabel>> particleLabels = {
            makeOpen("buffer_lower", 3, {0, -1}),
            makeReflecting("buffer_upper", 4, {0, -1}, 1),
            makeOpen("electrode", 5, {0, -1}),
            makeReflecting("interface", 6, {0, -1}, 1),
            makeOpen("left", 7, {-1, 0}),
            makeOpen("right", 8, {1, 0}),
            makeOpen("spacing", 8, {0, -1}),
            makeOpen("top", 8, {0, 1}),
            makeOpen("spacecraft", 8, {0, 1}),
    };

    auto grid = r.makeGrid(fieldLabels, particleLabels);

    // Check basic grid attributes
    REQUIRE(grid->particleBoundaryLabels.size() == 3 * 600 + 2 * 150);
    REQUIRE(grid->fieldLabels.size() == 90751);
    REQUIRE(grid->nDim() == 2);
    REQUIRE(r.nDim() == 2);
    REQUIRE(feq(r.getDomain().min(), vec({-300, 0})));
    REQUIRE(feq(r.getDomain().max(), vec({300, 150})));

    // Check subdomains
    REQUIRE(grid->subDomains.size() == 2);
    REQUIRE(grid->subDomains[0].id() == 1);
    REQUIRE(feq(grid->subDomains[0].min(), vec({-300, 13})));
    REQUIRE(feq(grid->subDomains[0].max(), vec({300, 150})));

    REQUIRE(grid->subDomains[1].id() == 2);
    REQUIRE(feq(grid->subDomains[1].min(), vec({-300, 0})));
    REQUIRE(feq(grid->subDomains[1].max(), vec({300, 13})));


    // Check that the nodes are the same as they are in the parser and are what should have been calculated
    for (uint n = 0; n < r.nodes().size(); n++) {
        auto ijk = grid->ijk(n);
        for (auto i = 0; i < r.nDim(); i++) {
            REQUIRE(grid->position(n, i) == r.nodes()[n].r[i]);
            REQUIRE(feq(grid->position(n, i), grid->min(i) + grid->spacing(i) * ijk[i]));
        }
    }

    // Require that edge elements have the right labeling
    for (auto entry : grid->particleBoundaryLabels) {
        auto xy1 = grid->position(entry.first[0]);
        auto xy2 = grid->position(entry.first[1]);

        if (entry.second->name == "buffer_lower") {
            REQUIRE(abs(xy1[0]) >= 105);
            REQUIRE(abs(xy2[0]) >= 105);
            REQUIRE(feq(xy1[1], 0));
            REQUIRE(feq(xy2[1], 0));
        } else if (entry.second->name == "buffer_upper") {
            REQUIRE(abs(xy1[0]) >= 105);
            REQUIRE(abs(xy2[0]) >= 105);
            REQUIRE(feq(xy1[1], 13));
            REQUIRE(feq(xy2[1], 13));
        } else if (entry.second->name == "electrode") {
            REQUIRE(abs(xy1[0]) <= 10);
            REQUIRE(abs(xy2[0]) <= 10);
            REQUIRE(feq(xy1[1], 0));
            REQUIRE(feq(xy2[1], 0));
        } else if (entry.second->name == "interface") {
            REQUIRE(abs(xy1[0]) <= 105);
            REQUIRE(abs(xy2[0]) <= 105);
            REQUIRE(feq(xy1[1], 13));
            REQUIRE(feq(xy2[1], 13));
        } else if (entry.second->name == "left") {
            REQUIRE(feq(xy1[0], -300));
            REQUIRE(feq(xy2[0], -300));
        } else if (entry.second->name == "right") {
            REQUIRE(feq(xy1[0], 300));
            REQUIRE(feq(xy2[0], 300));
        } else if (entry.second->name == "spacecraft") {
            REQUIRE(abs(xy1[0]) <= 105);
            REQUIRE(abs(xy2[0]) <= 105);
            REQUIRE(abs(xy1[0]) >= 20);
            REQUIRE(abs(xy2[0]) >= 20);
            REQUIRE(feq(xy1[1], 0));
            REQUIRE(feq(xy2[1], 0));
        } else if (entry.second->name == "spacing") {
            REQUIRE(abs(xy1[0]) <= 20);
            REQUIRE(abs(xy2[0]) <= 20);
            REQUIRE(abs(xy1[0]) >= 10);
            REQUIRE(abs(xy2[0]) >= 10);
            REQUIRE(feq(xy1[1], 0));
            REQUIRE(feq(xy2[1], 0));
        } else if (entry.second->name == "top") {
            REQUIRE(feq(xy1[1], 150));
            REQUIRE(feq(xy2[1], 150));
        }
    }

}
