#include "../catch.hpp"
#include "CastUtils.h"

#include <thread>
#include "Profiler.hpp"

using namespace picard;
using namespace std;

TEST_CASE("profiling", "[profiling]") {
    Profiler p;
    p.start("sleep test");
    std::this_thread::sleep_for(std::chrono::microseconds(1000));
    p.stop();

    p.start("sleep test");
    std::this_thread::sleep_for(std::chrono::microseconds(1000));
    p.stop();

    p.start("sleep test2");
    std::this_thread::sleep_for(std::chrono::microseconds(500));
    p.stop();

    auto results = p.getIntervals<std::chrono::microseconds>();
    REQUIRE(results.size() == 2);
    REQUIRE(results.begin()->first == "sleep test");
    REQUIRE(results.begin()->second.count() > 1500);
    REQUIRE(results.begin()->second.count() < 2800);

    REQUIRE(results.rbegin()->first == "sleep test2");
    REQUIRE(results.rbegin()->second.count() > 300);
    REQUIRE(results.rbegin()->second.count() < 800);

    p.start("test");
    REQUIRE_THROWS(p.results());

    p.clearAll();
    results = p.getIntervals<std::chrono::microseconds>();
    REQUIRE(results.size() == 0);
    REQUIRE_NOTHROW(p.results());
}

#ifdef ENABLE_PROFILING
TEST_CASE("tic toc", "[profiling]") {
    clearProfiler();

    tic("sleep test");
    std::this_thread::sleep_for(std::chrono::microseconds(1000));
    toc();

    tic("sleep test");
    std::this_thread::sleep_for(std::chrono::microseconds(1000));
    toc();

    tic("sleep test2");
    std::this_thread::sleep_for(std::chrono::microseconds(500));
    toc();

    auto results = getProfilerInstance().getIntervals<std::chrono::microseconds>();
    REQUIRE(results.size() == 2);
    REQUIRE(results.begin()->first == "sleep test");
    REQUIRE(results.begin()->second.count() > 1500);
    REQUIRE(results.begin()->second.count() < 2800);

    REQUIRE(results.rbegin()->first == "sleep test2");
    REQUIRE(results.rbegin()->second.count() > 300);
    REQUIRE(results.rbegin()->second.count() < 800);

    tic("test");
    REQUIRE_THROWS(profileResults());

    clearProfiler();
    results = getProfilerInstance().getIntervals<std::chrono::microseconds>();
    REQUIRE(results.size() == 0);
    REQUIRE_NOTHROW(profileResults());
}
#endif
