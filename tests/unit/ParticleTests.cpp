#include "../catch.hpp"
#include "Grid.h"
#include "Interpolator.h"
#include <unordered_set>
#include "Options.h"

using namespace std;
using namespace picard;

TEST_CASE("Particle Energy Calculations", "[particle]"){
    Grid g(Domain(3, {0,0,0}, {1,1,1}), {10,10,10});
    g.potential.resize(g.totalPoints(), 10);
    auto species = make_shared<Species>("test", 15, -10);
    Particle p({0.5,0.5,0.5}, {1,2,3}, species.get());
    
    REQUIRE(feq(p.kineticEnergy(), 14*15*0.5));
    REQUIRE(feq(p.potentialEnergy(g, Interpolator()), -100));
    REQUIRE(feq(p.totalEnergy(g, Interpolator()), 14*15*0.5 - 100));
}


TEST_CASE("Quiet Start Check", "[particle]") {
    Options o;
    auto grid = Grid::Periodic(Domain(3, {0,0,0}, {1,1,1}), {5,5,5});
    auto traits = make_shared<Species>("test", 1,1);
    Particle p(3,traits.get());

    traits->velocity_distribution = maxwellian_multi_distribution({0.1, 0.2, 0.3}, {0.1, 0.2, 0.3});
    o.species["test"] = traits;

    PopulationInit init;
    init.type = QUIET_START;
    init.spatialSamples = {5, 6, 7};
    init.velocitySamples = {5, 6, 7};
    o.populationInit[traits] = init;
    REQUIRE(feq(init.computeDensity(*grid), 2822400));

    auto particles = o.makeParticleList(traits, *grid);
    REQUIRE(particles.size() == (grid->totalCells()*product(init.spatialSamples)*product(init.velocitySamples)));

    set<vec> velocities;
    for(auto& p : particles)
        velocities.insert(p.v);

    REQUIRE(velocities.size() == product(init.velocitySamples));

}

TEST_CASE("Total Count Initialization", "[particle]"){
    Options o;
    auto grid = Grid::Periodic(Domain(3, {0,0,0}, {1,1,1}), {5,5,5});
    auto traits = make_shared<Species>("test", 1,1);
    vec mu = {0.1, 0.2, 0.3};
    vec sig = {0.1, 0.2, 0.3};
    traits->velocity_distribution = normal_multi_distribution(mu, sig);
    o.species["test"] = traits;

    PopulationInit init;
    init.type = TOTAL_COUNT;
    init.count = 10000;
    o.populationInit[traits] = init;
    REQUIRE(feq(init.computeDensity(*grid), 10000));

    auto particles = o.makeParticleList(traits, *grid);
    REQUIRE(particles.size() == init.count);
    vec avg(3, 0);
    for(auto& p : particles){
        avg = avg + p.v;
    }
    avg = avg / particles.size();
    auto min = mu - sig;
    auto max = mu + sig;
    REQUIRE(avg[0] > min[0]);
    REQUIRE(avg[1] > min[1]);
    REQUIRE(avg[2] > min[2]);
    REQUIRE(avg[0] < max[0]);
    REQUIRE(avg[1] < max[1]);
    REQUIRE(avg[2] < max[2]);

}

TEST_CASE("Density Initialization", "[particle]"){
    Options o;
    auto grid = Grid::Periodic(Domain(3, {0,0,0}, {1,1,1}), {5,5,5});
    auto traits = make_shared<Species>("test", 1,1);
    double xmin = -0.1;    double xmax = 0.1;
    double ymin = -0.2;    double ymax = 0.2;
    double zmin = -0.3;    double zmax = 0.3;
    traits->velocity_distribution = uniform_multi_distribution({xmin, ymin, zmin}, {xmax, ymax, zmax});
    o.species["test"] = traits;

    PopulationInit init;
    init.type = PARTICLE_DENSITY;
    init.density = 100;
    o.populationInit[traits] = init;
    REQUIRE(feq(init.computeDensity(*grid), 100));

    auto particles = o.makeParticleList(traits, *grid);
    REQUIRE(particles.size() == (int)init.density*grid->activeVolume());
    for(auto& p : particles) {
        REQUIRE(p.v[0] >= xmin);
        REQUIRE(p.v[0] <= xmax);
        REQUIRE(p.v[1] >= ymin);
        REQUIRE(p.v[1] <= ymax);
        REQUIRE(p.v[2] >= zmin);
        REQUIRE(p.v[2] <= zmax);
    }
}

