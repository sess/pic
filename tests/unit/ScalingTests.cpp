#include "Simulation.h"
#include "../catch.hpp"
#include "Options.h"
#include "Grid.h"
#include "IOUtils.h"

using namespace picard;
using namespace std;

TEST_CASE("Scaling Tests", "[scaling]") {
    Options o("tests/config/collisional_drift_electrons.cfg");
    o.verbose = false;
    Simulation sim(o); // In order for the densities to be properly updated

    // Check that the options were read in correctly for n and T
    REQUIRE(o.referenceParams->physicalDensity() == 6e17);
    REQUIRE(o.referenceParams->physicalTemperature() == 1);

    // Compare scaling values to those calculated by hand
    auto rho_sim = 5.625e-1;
    REQUIRE(o.referenceParams->simDensity() == rho_sim);
    REQUIRE(feq(o.referenceParams->timeScale(), 2.288193E-11));
    REQUIRE(feq(o.referenceParams->lengthScale(), 9.593711E-06));
    REQUIRE(feq(o.referenceParams->massScale(), 5.124375E-31));
    REQUIRE(feq(o.referenceParams->chargeScale(), 9.011250E-20));
    REQUIRE(feq(o.referenceParams->densityScale(), 6e17 / rho_sim));
    REQUIRE(feq(o.referenceParams->weightScale(), 9.4186491713E+02));
    REQUIRE(feq(o.referenceParams->velocityScale(), 4.1927015149440E+05));
    REQUIRE(feq(o.referenceParams->energyScale(), 9.0080086500000E-20));
    REQUIRE(feq(o.referenceParams->energyScaleEV(), 5.625000E-01));
    REQUIRE(feq(o.referenceParams->temperatureScaleEV(), 5.625000E-01 / rho_sim));
    REQUIRE(feq(o.referenceParams->potentialScale(), 1));
    REQUIRE(feq(o.referenceParams->electricFieldScale(), 1.0423495127244E+05));
    REQUIRE(feq(o.referenceParams->chargeDensityScale(), 9.6120000000000E-02));
}
