#include "../catch.hpp"
#include "IOUtils.h"
#include "Distributions.h"

using namespace picard;
using namespace std;

TEST_CASE("Streaming Options", "[io_utils]"){
    vector<string> v1 = {"a", "b", "c"};

    // Default streaming case
    stringstream s;
    s << v1;
    REQUIRE(s.str() == "a,b,c");

}

TEST_CASE("Value parsing", "[io_utils]"){
    string s = "opt1=1, opt2=abc, opt3=3.3, opt4=(1;2;3), opt5=(1.1;2.2;3.3)";
    auto vm = getValueMap(tokenize(s));
    std::map<std::string, std::string> mapCompare = {
            {"opt1", "1"},
            {"opt2", "abc"},
            {"opt3", "3.3"},
            {"opt4","(1;2;3)"},
            {"opt5", "(1.1;2.2;3.3)"}
    };
    REQUIRE(vm == mapCompare);

    auto opt1 = getValue<int>(vm, "opt1");
    REQUIRE(opt1 == 1);

    auto opt2 = getValue<std::string>(vm, "opt2");
    REQUIRE(opt2 == "abc");

    auto opt3 = getValue<double>(vm, "opt3");
    REQUIRE(opt3 == 3.3);

    auto opt4 = getValue<vector<int>>(vm, "opt4");
    REQUIRE(opt4 == std::vector<int>({1,2,3}));

    auto opt5 = getValue<vector<double>>(vm, "opt5");
    REQUIRE(opt5 == std::vector<double>({1.1, 2.2, 3.3}));

}

TEST_CASE("Parse and Write tests for 1D and 2D", "[io_utils]"){
    string path1D = "tests/csvs/test1D.csv";
    string path2D = "tests/csvs/test2D.csv";

    uint rows = 100;
    uint cols = 3;
    vector<Real> test1D = samples(rows, distribution<Real>(std::uniform_real_distribution<Real>()));
    vector2d<Real> test2D = vector2d<Real>::Random(rows,cols);

    // Write vectors to disk
    write1DVector(test1D, path1D);
    write2DVector(test2D, path2D);

    // Read from disk
    auto readback1D = parse1DVector(path1D);
    auto readback2D = parse2DVector(path2D);

    REQUIRE(readback1D == test1D);
    REQUIRE(readback2D == test2D);

    remove(path1D.c_str());
    remove(path2D.c_str());
}

TEST_CASE("Test file existence, emptiness and clearing", "[io_utils]"){
    string path1D = "tests/csvs/test1D.csv";

    remove(path1D.c_str());
    REQUIRE(!exists(path1D));

    vector<Real> test1D = samples(100, distribution<Real>(std::uniform_real_distribution<Real>()));
    write1DVector(test1D, path1D);

    REQUIRE(exists(path1D));

    REQUIRE(isEmpty(path1D) == false);

    clear(path1D);
    REQUIRE(isEmpty(path1D) == true);
    remove(path1D.c_str());
}

TEST_CASE("Appending to a files", "[io_utils]"){
    string path1D = "tests/csvs/test1D.csv";
    string path2D = "tests/csvs/test2D.csv";

    uint rows = 10;
    uint cols = 3;
    vector<Real> test1D = samples(rows, distribution<Real>(std::uniform_real_distribution<Real>()));
    vector2d<Real> test2D = vector2d<Real>::Random(rows,cols);
    vector2d<Real> col1D(rows,1);

    // Test the 2d vector appending as rows
    clear(path2D);
    for(int i = 0;  i < test2D.rows(); i++){
        append(path2D, getRow(test2D, i));
    }
    auto readback2D = parse2DVector(path2D);
    REQUIRE(readback2D == test2D);


    // Test the 1D vector appended as one big row
    clear(path1D);
    for(uint i=0; i<test1D.size(); i++){
        append(path1D, test1D[i], setEnd(" ") << castToString());
    }
    auto readback1D = parse1DVector(path1D);
    REQUIRE(readback1D == test1D);


    // Test the 1D vector appended as a column and read back in 2D
    clear(path1D);
    for(uint i=0; i<test1D.size(); i++){
        append(path1D, test1D[i]);
        col1D(i,0) = test1D[i];
    }
    auto readbackCol1D = parse2DVector(path1D);
    REQUIRE(readbackCol1D == col1D);

    remove(path1D.c_str());
    remove(path2D.c_str());
}

TEST_CASE("Read 1D csv file", "[io_utils]") {
    auto res = parse1DVector("tests/csvs/1dTest.csv");
    std::vector<Real> comparison = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    REQUIRE(res == comparison);
}

vector2d<Real> fieldTestCompare(){
    vector2d<Real> comparison(100, 3);
    comparison << 350, -350, 0,
            350, -272.22, 0,
            350, -194.44, 0,
            350, -116.67, 0,
            350, -38.889, 0,
            350, 38.889, 0,
            350, 116.67, 0,
            350, 194.44, 0,
            350, 272.22, 0,
            350, 350, 0,
            272.22, -350, 0,
            272.22, -272.22, 0,
            272.22, -194.44, 0,
            272.22, -116.67, 0,
            272.22, -38.889, 0,
            272.22, 38.889, 0,
            272.22, 116.67, 0,
            272.22, 194.44, 0,
            272.22, 272.22, 0,
            272.22, 350, 0,
            194.44, -350, 0,
            194.44, -272.22, 0,
            194.44, -194.44, 0,
            194.44, -116.67, 0,
            194.44, -38.889, 0,
            194.44, 38.889, 0,
            194.44, 116.67, 0,
            194.44, 194.44, 0,
            194.44, 272.22, 0,
            194.44, 350, 0,
            350, -350, 0,
            350, -272.22, 0,
            350, -194.44, 0,
            350, -116.67, 0,
            350, -38.889, 0,
            350, 38.889, 0,
            350, 116.67, 0,
            350, 194.44, 0,
            350, 272.22, 0,
            350, 350, 0,
            272.22, -350, 0,
            272.22, -272.22, 0,
            272.22, -194.44, 0,
            272.22, -116.67, 0,
            272.22, -38.889, 0,
            272.22, 38.889, 0,
            272.22, 116.67, 0,
            272.22, 194.44, 0,
            272.22, 272.22, 0,
            272.22, 350, 0,
            194.44, -350, 0,
            194.44, -272.22, 0,
            194.44, -194.44, 0,
            194.44, -116.67, 0,
            194.44, -38.889, 0,
            194.44, 38.889, 0,
            194.44, 116.67, 0,
            194.44, 194.44, 0,
            194.44, 272.22, 0,
            194.44, 350, 0,
            350, -350, 0,
            350, -272.22, 0,
            350, -194.44, 0,
            350, -116.67, 0,
            350, -38.889, 0,
            350, 38.889, 0,
            350, 116.67, 0,
            350, 194.44, 0,
            350, 272.22, 0,
            350, 350, 0,
            272.22, -350, 0,
            272.22, -272.22, 0,
            272.22, -194.44, 0,
            272.22, -116.67, 0,
            272.22, -38.889, 0,
            272.22, 38.889, 0,
            272.22, 116.67, 0,
            272.22, 194.44, 0,
            272.22, 272.22, 0,
            272.22, 350, 0,
            194.44, -350, 0,
            194.44, -272.22, 0,
            194.44, -194.44, 0,
            194.44, -116.67, 0,
            194.44, -38.889, 0,
            194.44, 38.889, 0,
            194.44, 116.67, 0,
            194.44, 194.44, 0,
            194.44, 272.22, 0,
            194.44, 350, 0,
            194.44, -350, 0,
            194.44, -272.22, 0,
            194.44, -194.44, 0,
            194.44, -116.67, 0,
            194.44, -38.889, 0,
            194.44, 38.889, 0,
            194.44, 116.67, 0,
            194.44, 194.44, 0,
            194.44, 272.22, 0,
            194.44, 350, 0;
    return comparison;
}

TEST_CASE("Read 2D csv file", "[io_utils]") {
    auto res = parse2DVector("tests/csvs/fieldTest.csv");
    REQUIRE(res == fieldTestCompare());
}

TEST_CASE("Lenient Parse Into 2D Vector", "[io_utils]"){
    // Test parse from file
    auto res = lenientParse2DVector("tests/csvs/fieldTest.csv", 100);
    REQUIRE(res == fieldTestCompare());

    // Test parse form singular vector
    auto res2 = lenientParse2DVector("(-1;-1;-1)", 100);
    REQUIRE(res2 == vector2d<Real>::Constant(100,3,-1));

    // Test runtime exceptions
    REQUIRE_THROWS(lenientParse2DVector("tests/csvs/fieldTest.csv", 99));
    REQUIRE_THROWS(lenientParse2DVector("tests/csvs/fieldTest_abc.csv", 100));
    REQUIRE_THROWS(lenientParse2DVector("a1,a,3", 100));
}