#include <CastUtils.h>
#include "../catch.hpp"
#include "GmshReader.h"
#include "Grid.h"

using namespace picard;
using namespace std;

void verifyNodeLocations(const Grid& grid){
    for (auto n = 0; n < grid.totalPoints(); n++) {
        auto ijk = grid.ijk(n);
        for (auto i = 0; i < grid.nDim(); i++)
            REQUIRE(feq(grid.position(n, i), grid.min(i) + grid.spacing(i) * ijk[i]));
    }
}

TEST_CASE("Periodic Grid Creation - 3D", "[grid]") {
    auto grid = Grid::Periodic(Domain(3, {0, 0, 0}, {1, 2, 3}), {10, 11, 12});

    // Verify the single subdomain
    REQUIRE(grid->subDomains.size() == 1);
    REQUIRE(grid->subDomains.begin()->size() == vec({1, 2, 3}));
    REQUIRE(grid->subDomains.begin()->min() == vec({0, 0, 0}));
    REQUIRE(grid->subDomains.begin()->max() == vec({1, 2, 3}));

    // Verify the labels
    int index = 0;
    for (auto l : grid->fieldLabels) {
        auto ids = grid->boundaryIds(index++);
        if (ids.size()) {
            if(index != 1) {
                REQUIRE(l->type == FieldLabel::PERIODIC);
                auto max_id = *max_element(ids.begin(), ids.end());
                REQUIRE(l->name == toString(max_id));
            } else {
                // This is the case that a dirichlet label is added to make the field solve well posed
                // The dirichlet condition is added on node 0 and sets the potential to 0 there.
                REQUIRE(l->type == FieldLabel::DIRICHLET);
                REQUIRE(l->properties == vec(1,0));
                REQUIRE(l->name == "reference");
            }

        }
    }

    auto boundaryElements = grid->boundaryElements();
    REQUIRE(grid->particleBoundaryLabels.size() == boundaryElements.size());
    for (auto l : grid->particleBoundaryLabels)
        REQUIRE(l.second->type == ParticleBoundaryLabel::PERIODIC);

    // Verify the grid dimension and parameters
    REQUIRE(grid->min() == vec({0, 0, 0}));
    REQUIRE(grid->max() == vec({1, 2, 3}));
    REQUIRE(grid->size() == vec({1, 2, 3}));
    REQUIRE(grid->totalPoints() == 1320);
    REQUIRE(grid->totalCells() == 990);
    REQUIRE(grid->numPoints() == ivec({10, 11, 12}));
    REQUIRE(feq(grid->activeVolume(), 6.));
    auto dx = 1. / (10. - 1.);
    auto dy = 2. / (11. - 1.);
    auto dz = 3. / (12. - 1.);
    auto cellVol = dx * dy * dz;
    REQUIRE(grid->spacing() == vec({dx, dy, dz}));
    REQUIRE(grid->cellVolume() == cellVol);

    // Verify node locations
    verifyNodeLocations(*grid);
}

TEST_CASE("Single Domain Grid Creation", "[grid]") {
    int nDim = 2;
    Domain d(nDim, {0, 0}, {1, 2});
    int priority = 0;
    std::map<int, std::shared_ptr<FieldLabel>> fieldLabels;
    fieldLabels.insert(makeInterior(priority, 1, true));
    fieldLabels.insert(makeDirichlet(X_MIN, priority++, 1000));
    fieldLabels.insert(makeDirichlet(X_MAX, priority++, 0));
    fieldLabels.insert(makeZeroField(Y_MIN, priority++));
    fieldLabels.insert(makeZeroField(Y_MAX, priority));

    priority = 0;
    std::map<int, std::shared_ptr<ParticleBoundaryLabel>> particleLabels;
    particleLabels.insert(makeAbsorbing(X_MIN, priority++));
    particleLabels.insert(makeSolid(X_MAX, priority++, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12));
    particleLabels.insert(makeOpen(Y_MIN, priority++));
    particleLabels.insert(makeOpen(Y_MAX, priority));

    auto grid = Grid::SingleDomain(d, {101, 201}, fieldLabels, particleLabels);

    // Verify the single subdomain
    REQUIRE(grid->subDomains.size() == 1);
    REQUIRE(grid->subDomains.begin()->size() == vec({1, 2}));
    REQUIRE(grid->subDomains.begin()->min() == vec({0, 0}));
    REQUIRE(grid->subDomains.begin()->max() == vec({1, 2}));

    // Verify the labels
    int index = 0;
    for (auto l : grid->fieldLabels) {
        auto ids = grid->boundaryIds(index++);
        if (ids.size()){
            auto max_id = *max_element(ids.begin(), ids.end());
            REQUIRE(l->name == toString(max_id));
            if (max_id == X_MIN) {
                REQUIRE(l->type == FieldLabel::DIRICHLET);
            } else if (max_id == X_MAX) {
                REQUIRE(l->type == FieldLabel::DIRICHLET);
            } else if (max_id == Y_MIN) {
                REQUIRE(l->type == FieldLabel::ZERO_FIELD);
            } else if (max_id == Y_MAX) {
                REQUIRE(l->type == FieldLabel::ZERO_FIELD);
            }
        }
    }

    auto boundaryElements = grid->boundaryElements();
    REQUIRE(grid->particleBoundaryLabels.size() == boundaryElements.size());
    for (auto l : grid->particleBoundaryLabels) {
        auto id = grid->boundaryId(l.first);
        REQUIRE(l.second->name == toString(id));
        switch (id) {
            case X_MIN:
                REQUIRE(l.second->type == ParticleBoundaryLabel::ABSORBING);
                break;
            case X_MAX:
                REQUIRE(l.second->type == ParticleBoundaryLabel::SOLID);
                break;
            case Y_MIN:
                REQUIRE(l.second->type == ParticleBoundaryLabel::OPEN);
                break;
            case Y_MAX:
                REQUIRE(l.second->type == ParticleBoundaryLabel::OPEN);
                break;
            default:
                throw runtime_error("Found a boudnary id that didn't belong");
        }
    }

    // Verify the grid dimension and parameters
    REQUIRE(grid->min() == vec({0, 0}));
    REQUIRE(grid->max() == vec({1, 2}));
    REQUIRE(grid->size() == vec({1, 2}));
    REQUIRE(grid->totalPoints() == 20301);
    REQUIRE(grid->totalCells() == 20000);
    REQUIRE(grid->numPoints() == ivec({101, 201}));
    REQUIRE(feq(grid->activeVolume(), 2.));
    auto dx = 1. / (101. - 1.);
    auto dy = 2. / (201. - 1.);
    auto cellVol = dx * dy;
    REQUIRE(grid->spacing() == vec({dx, dy}));
    REQUIRE(grid->cellVolume() == cellVol);

    // Verify node locations
    verifyNodeLocations(*grid);

}

void runSingleDomainGridIntersections(shared_ptr<Grid> grid,
                                      std::map<int, std::shared_ptr<ParticleBoundaryLabel>>& particleLabels){
    int trials = 50;
    for(int t=0; t<trials; t++){
        for(auto& entry : particleLabels){
            auto bId = (BoundaryId)entry.first;
            auto start = grid->randomPermittedLocation();
            auto end = grid->randomPositionOutside((BoundaryId)entry.first);
            auto res = grid->intersectsBoundary(concatenate(start, end));
            REQUIRE(res.domainIndex == 0);
            REQUIRE(bId == res.id);
            REQUIRE(res.label == entry.second);
            REQUIRE(res.count == 1);
            REQUIRE(res.dim == idDimension(bId));
            auto domain = grid->subDomains[res.domainIndex];
            REQUIRE(res.normal == domain.normal(bId));
            REQUIRE(res.point[idDimension(bId)] == domain.bounds(bId));
        }
    }
}

TEST_CASE("Intersects Boundary Results Single-Domain - 1D", "[grid]") {
    std::map<int, std::shared_ptr<ParticleBoundaryLabel>> particleLabels;
    particleLabels.insert(makeAbsorbing(X_MIN, 0));
    particleLabels.insert(makeSolid(X_MAX, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12));


    auto grid = Grid::SingleDomain(Domain(1, vec({0.}), vec({1.})), {101}, {}, particleLabels);
    runSingleDomainGridIntersections(grid, particleLabels);
}

TEST_CASE("Intersects Boundary Results Single-Domain - 2D", "[grid]") {
    std::map<int, std::shared_ptr<ParticleBoundaryLabel>> particleLabels;
    particleLabels.insert(makeAbsorbing(X_MIN, 0));
    particleLabels.insert(makeSolid(X_MAX, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12));
    particleLabels.insert(makeOpen(Y_MIN, 0));
    particleLabels.insert(makeOpen(Y_MAX, 0));

    auto grid = Grid::SingleDomain(Domain(2, {0, 0}, {1, 2}), {101, 201}, {}, particleLabels);
    runSingleDomainGridIntersections(grid, particleLabels);
}

TEST_CASE("Intersects Boundary Results Single-Domain - 3D", "[grid]") {
    std::map<int, std::shared_ptr<ParticleBoundaryLabel>> particleLabels;
    particleLabels.insert(makeAbsorbing(X_MIN, 0));
    particleLabels.insert(makeSolid(X_MAX, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12));
    particleLabels.insert(makeReflecting(Y_MIN, 0,1));
    particleLabels.insert(makeScattering(Y_MAX, 0,1));
    particleLabels.insert(makeOpen(Z_MIN, 0));
    particleLabels.insert(makePeriodicParticle(Z_MAX, 0));

    auto grid = Grid::SingleDomain(Domain(3, {0, 0,0}, {1, 2,2}), {101, 201,201}, {}, particleLabels);
    runSingleDomainGridIntersections(grid, particleLabels);
}

TEST_CASE("Intersects Boundary Results with Interior Domain", "[grid]") {
    string mesh_path = "tests/meshes/2Ddualblockmesh.msh";
    GmshReader r(mesh_path);

    auto top = makeOpen("top", 8, {0, 1});
    auto interface = makeReflecting("interface", 6, {0, -1}, 1);
    auto buffer_upper = makeReflecting("buffer_upper", 4, {0, -1}, 1);
    auto left = makeOpen("left", 7, {-1, 0});
    auto right = makeOpen("right", 8, {1, 0});

    std::map<int, std::shared_ptr<ParticleBoundaryLabel>> particleLabels  {
            {X_MIN, left},
            {X_MAX, right},
            {Y_MIN, nullptr},
            {Y_MAX, top}
    };

    vector<shared_ptr<FieldLabel>> fieldLabels = {
            makeInterior("plasma", 0, 1, true),
            makeInterior("dielectric", 0, 10, false),
            makeDirichlet("buffer_lower", 3, {0, -1}, 0),
            makeDirichlet("buffer_upper", 4, {0, -1}, 0),
            makeDirichlet("electrode", 5, {0, -1}, 0),
            makeDirichlet("interface", 6, {0, -1}, 0),
            makeDirichlet("left", 7, {-1, 0}, 0),
            makeDirichlet("right", 8, {1, 0}, 100),
            makeDirichlet("spacing", 8, {0, -1}, 100),
            makeDirichlet("top", 8, {0, 1}, 100),
            makeDirichlet("spacecraft", 8, {0, 1}, 100),
    };

    vector<shared_ptr<ParticleBoundaryLabel>> pLabels = {
            makeOpen("buffer_lower", 3, {0, -1}),
            buffer_upper,
            makeOpen("electrode", 5, {0, -1}),
            interface,
            left,
            right,
            makeOpen("spacing", 8, {0, -1}),
            top,
            makeOpen("spacecraft", 8, {0, 1}),
    };

    auto grid = r.makeGrid(fieldLabels, pLabels);
    int trials = 50;
    for(int t=0; t<trials; t++){
        for(auto& entry : particleLabels){
            auto bId = (BoundaryId)entry.first;
            vec end;
            auto start = grid->randomPermittedLocation();
            if(bId != Y_MIN)
                end = grid->subDomains[0].randomPositionOutside((BoundaryId)entry.first);
            else
                end = grid->subDomains[1].randomPositionInside();

            auto res = grid->intersectsBoundary(concatenate(start, end));
            auto domain = grid->subDomains[res.domainIndex];
            if(bId != Y_MIN) {
                REQUIRE(bId == res.id);
                REQUIRE(res.label == entry.second);
            } else {
                bId = res.id;
                bool correctLabel = (res.label == interface || res.label == buffer_upper);
                bool correctId = (bId == Y_MIN && domain.id() == 1) || (bId == Y_MAX && domain.id() == 2);
                REQUIRE(correctLabel);
                REQUIRE(correctId);
            }
            REQUIRE(res.count == 1);
            REQUIRE(res.dim == idDimension(bId));

            REQUIRE(res.normal == domain.normal(bId));
            REQUIRE(res.point[idDimension(bId)] == domain.bounds(bId));
        }
    }
}

void testGridAttributesAfterRefresh(const Grid& g, int nDim, vec min, vec max, ivec nPoints){
    REQUIRE(g.nDim() == nDim);
    REQUIRE(g.min() == min);
    REQUIRE(g.max() == max);
    REQUIRE(g.numPoints() == nPoints);
    REQUIRE(g.size() == max - min);
    auto spacing = (max - min)/vcast<Real>(nPoints - 1);
    REQUIRE(feq(g.spacing(), spacing));
    REQUIRE(feq(g.cellVolume(), product(g.spacing())));
    verifyNodeLocations(g);
    REQUIRE(g.fieldLabels.size() == product(nPoints));
    if(nDim >= 1) {
        REQUIRE(g.bounds(X_MIN) == min[0]);
        REQUIRE(g.bounds(X_MAX) == max[0]);
    }
    if(nDim >= 2) {
        REQUIRE(g.bounds(Y_MIN) == min[1]);
        REQUIRE(g.bounds(Y_MAX) == max[1]);
    }
    if(nDim >= 3) {
        REQUIRE(g.bounds(Z_MIN) == min[2]);
        REQUIRE(g.bounds(Z_MAX) == max[2]);
    }
}

TEST_CASE("Set Grid Min, Max, Dim, Particles Permitted, id", "[domain]") {
    auto g = Grid::Periodic(Domain(2, {0,0,0}, {1,1,1}), {10,10,10});
    testGridAttributesAfterRefresh(*g, 2, {0,0}, {1,1}, {10,10});
    g->setDimension(3, {-1,-1,-1}, {2,2,2});
    testGridAttributesAfterRefresh(*g, 3, {-1,-1,-1}, {2,2,2}, {10,10,2});
    g->setMin({0,0,0});
    testGridAttributesAfterRefresh(*g, 3, {0,0,0}, {2,2,2}, {10,10,2});
    g->setMax({10,10,10});
    testGridAttributesAfterRefresh(*g, 3, {0,0,0}, {10,10,10}, {10,10,2});
    g->setMin(2, -2);
    testGridAttributesAfterRefresh(*g, 3, {0,0,-2}, {10,10,10}, {10,10,2});
    g->setMax(0, 4);
    testGridAttributesAfterRefresh(*g, 3, {0,0,-2}, {4,10,10}, {10,10,2});
    g->setNumPoints({3,3,3});
    testGridAttributesAfterRefresh(*g, 3, {0,0,-2}, {4,10,10}, {3,3,3});
}

TEST_CASE("Grid Data Allocation - No Surface Charge", "[grid]") {
    int nDim = 3;
    auto g3 = Grid::Periodic(Domain(nDim, {0, 0, 0}, {1, 2, 3}), {10, 11, 12});
    int N = 1320;
    int nSpecies = 3;
    g3->allocateGridData(nSpecies);
    REQUIRE(g3->eField.rows() == N);
    REQUIRE(g3->eField.cols() == nDim);
    REQUIRE(g3->eField == g3->eField * 0.0);

    REQUIRE(g3->potential.size() == N);

    REQUIRE(g3->chargeDensity.rows() == N);
    REQUIRE(g3->chargeDensity.cols() == nSpecies);
    REQUIRE(g3->chargeDensity == g3->chargeDensity * 0.0);

    REQUIRE(g3->surfaceChargeMap.size() == 0);
}

TEST_CASE("Grid Data Allocation - With Surface Charge", "[grid]") {
    int nDim = 2;
    Domain d(nDim, {0, 0}, {1, 2});
    int priority = 0;
    std::map<int, std::shared_ptr<FieldLabel>> fieldLabels;
    fieldLabels.insert(makeInterior(priority, 1, true));
    fieldLabels.insert(makeDirichlet(X_MIN, priority++, 1000));
    fieldLabels.insert(makeDirichlet(X_MAX, priority++, 0));
    fieldLabels.insert(makeZeroField(Y_MIN, priority++));
    fieldLabels.insert(makeZeroField(Y_MAX, priority));

    priority = 0;
    std::map<int, std::shared_ptr<ParticleBoundaryLabel>> particleLabels;
    particleLabels.insert(makeAbsorbing(X_MIN, priority++));
    particleLabels.insert(makeSolid(X_MAX, priority++, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12));
    particleLabels.insert(makeOpen(Y_MIN, priority++));
    particleLabels.insert(makeOpen(Y_MAX, priority));

    auto g2 = Grid::SingleDomain(d, {101, 201}, fieldLabels, particleLabels);
    int N = 101 * 201;
    int nSpecies = 2;
    g2->allocateGridData(nSpecies);
    REQUIRE(g2->eField.rows() == N);
    REQUIRE(g2->eField.cols() == nDim);
    REQUIRE(g2->eField == g2->eField * 0.0);

    REQUIRE(g2->potential.size() == N);

    REQUIRE(g2->chargeDensity.rows() == N);
    REQUIRE(g2->chargeDensity.cols() == nSpecies);
    REQUIRE(g2->chargeDensity == g2->chargeDensity * 0.0);

    int absorbingNodes = 402; // 201 + 201
    REQUIRE(g2->surfaceChargeLocal.size() == absorbingNodes);
    REQUIRE(g2->surfaceChargeGlobal.size() == absorbingNodes);

    for (int i = 0; i < g2->totalPoints(); i++) {
        auto ids = g2->boundaryIds(i);
        if (ids.size() && (ids[0] == X_MIN || ids[0] == X_MAX)) {
            REQUIRE(g2->surfaceChargeMap.find(i) != g2->surfaceChargeMap.end());
        }
    }
}

TEST_CASE("Grid Data Allocation - NeutralizingGrid Overload",  "[grid]") {
    auto g3 = make_shared<NeutralizingGrid>(*Grid::Periodic(Domain(3, vec({0, 0, 0}), vec({1, 1, 1})), {10, 10, 10}));
    g3->allocateGridData(2);

    int N = 1000;
    int nDim = 3;
    int nSpecies = 2;
    REQUIRE(g3->eField.rows() == N);
    REQUIRE(g3->eField.cols() == nDim);
    REQUIRE(g3->eField == g3->eField * 0.0);

    REQUIRE(g3->potential.size() == N);

    REQUIRE(g3->chargeDensity.rows() == N);
    REQUIRE(g3->chargeDensity.cols() == nSpecies);
    REQUIRE(g3->chargeDensity == g3->chargeDensity * 0.0);

    REQUIRE(g3->surfaceChargeMap.size() == 0);
    REQUIRE(g3->neutralizingBackground == vector<Real>(N,0.0));
}

void check_identity_accessors(std::shared_ptr<Grid> grid){
    int trials = 100;
    for (int i = 0; i < trials; i++) {
        auto pos = grid->randomPositionInside();
        auto index = picard::uniform_int(0, grid->totalPoints() - 1);
        auto ijk = picard::uniform_int(ivec(grid->nDim(), 0), grid->numPoints() - 1);

        // Check index
        REQUIRE(feq(grid->index(grid->ijk(index)), index));
        REQUIRE(feq(grid->index(grid->position(index)), index));
        REQUIRE(feq(grid->index(grid->ijk(grid->position(index))), index));
        REQUIRE(feq(grid->index(grid->position(grid->ijk(index))), index));

        // Check positions
        REQUIRE(feq(grid->position(grid->ijk(pos)), grid->position(grid->index(pos))));

        // Check ijks
        REQUIRE(feq(grid->ijk(grid->position(ijk)), ijk));
        REQUIRE(feq(grid->ijk(grid->index(ijk)), ijk));
        REQUIRE(feq(grid->ijk(grid->index(grid->position(ijk))), ijk));
        REQUIRE(feq(grid->ijk(grid->position(grid->index(ijk))), ijk));
    }
}

TEST_CASE("Grid Accessors: ijk, position, index - 1D", "[grid]") {
    auto g1 = Grid::Periodic(Domain(1, vec({0}), vec({1})), {101});

    REQUIRE(g1->ijk(0) == ivec({0}));
    REQUIRE(g1->ijk(vec({0.5005})) == ivec({50}));
    REQUIRE(feq(g1->position(75), vec({0.75})));
    REQUIRE(feq(g1->position(0, 0), 0.));
    REQUIRE(feq(g1->position(ivec({50})), vec({0.5})));
    REQUIRE(feq(g1->position(ivec({100}), 0), 1.));
    REQUIRE(g1->index(ivec({0})) == 0);
    REQUIRE(g1->index(ivec({50})) == 50);
    REQUIRE(g1->index(vec({0.5005})) == 50);
    REQUIRE(g1->index(vec({1.})) == 100);

    // Check identity properties for a bunch of random points
    check_identity_accessors(g1);
}

TEST_CASE("Grid Accessors: ijk, position, index - 2D", "[grid]") {
    auto g2 = Grid::Periodic(Domain(2, {0, 0}, {1, 1}), {101, 101});

    REQUIRE(g2->ijk(102) == ivec({1, 1}));
    REQUIRE(g2->ijk(vec({0.5005, 0.1})) == ivec({50, 10}));
    REQUIRE(feq(g2->position(75), vec({0.75, 0})));
    REQUIRE(feq(g2->position(75, 0), 0.75));
    REQUIRE(feq(g2->position(75, 1), 0));
    REQUIRE(feq(g2->position({50, 10}), vec({0.5, 0.1})));
    REQUIRE(feq(g2->position({100, 0}, 0), 1.));
    REQUIRE(g2->index(ivec({0, 0})) == 0);
    REQUIRE(g2->index(ivec({50, 50})) == 5100);
    REQUIRE(g2->index(vec({0.5005, 0})) == 50);
    REQUIRE(g2->index(vec({1., 1.})) == 10200);

    // Check identity properties for a bunch of random points
    check_identity_accessors(g2);
}

TEST_CASE("Grid Accessors: ijk, position, index - 3D", "[grid]") {
    auto g3 = Grid::Periodic(Domain(3, {0, 0, 0}, {1, 1, 1}), {101, 101, 101});

    REQUIRE(g3->ijk(102) == ivec({1, 1, 0}));
    REQUIRE(g3->ijk(vec({0.5005, 0.1, 0.1})) == ivec({50, 10, 10}));
    REQUIRE(feq(g3->position(75), vec({0.75, 0, 0})));
    REQUIRE(feq(g3->position(101), vec({0, .01, 0})));
    REQUIRE(feq(g3->position(10201), vec({0, 0, .01})));
    REQUIRE(feq(g3->position(75, 0), 0.75));
    REQUIRE(feq(g3->position(75, 1), 0));
    REQUIRE(feq(g3->position(10201, 2), 0.01));
    REQUIRE(feq(g3->position({50, 10, 75}), vec({0.5, 0.1, 0.75})));
    REQUIRE(feq(g3->position({100, 0, 100}, 2), 1.));
    REQUIRE(g3->index(ivec({0, 0, 0})) == 0);
    REQUIRE(g3->index(ivec({50, 50, 50})) == 515150);
    REQUIRE(g3->index(vec({0.5005, 0, 0})) == 50);
    REQUIRE(g3->index(vec({1., 1., 1.})) == 1030300);

    // Check identity properties for a bunch of random points
    check_identity_accessors(g3);
}

TEST_CASE("Get Surface Charge", "[grid]") {
    int nDim = 2;
    Domain d(nDim, {0, 0}, {1, 2});
    int priority = 0;
    std::map<int, std::shared_ptr<FieldLabel>> fieldLabels;
    fieldLabels.insert(makeInterior(priority, 1, true));
    fieldLabels.insert(makeDirichlet(X_MIN, priority++, 1000));
    fieldLabels.insert(makeDirichlet(X_MAX, priority++, 0));
    fieldLabels.insert(makeZeroField(Y_MIN, priority++));
    fieldLabels.insert(makeZeroField(Y_MAX, priority));

    priority = 0;
    std::map<int, std::shared_ptr<ParticleBoundaryLabel>> particleLabels;
    particleLabels.insert(makeAbsorbing(X_MIN, priority++));
    particleLabels.insert(makeSolid(X_MAX, priority++, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12));
    particleLabels.insert(makeOpen(Y_MIN, priority++));
    particleLabels.insert(makeOpen(Y_MAX, priority));

    auto g2 = Grid::SingleDomain(d, {101, 201}, fieldLabels, particleLabels);
    int N = 101 * 201;
    int nSpecies = 2;
    g2->allocateGridData(nSpecies);
    for (int i = 0; i < g2->totalPoints(); i++) {
        auto ids = g2->boundaryIds(i);
        if (ids.size() && (ids[0] == 0 || ids[0] == 1)) {
            auto val = uniform();
            g2->contributeSurfaceCharge(i, val);
            REQUIRE(g2->getSurfaceCharge(i) == val);
        }else{
            REQUIRE(g2->getSurfaceCharge(i) == 0);
        }
    }
}

TEST_CASE("Get Charge Density (Neutralizing and Basic in 1D, 2D, and 3D)", "[grid]") {
    for (int d = 1; d < 3; d++) {
        auto ng = NeutralizingGrid::Periodic(Domain(d, vec({0, 0, 0}), vec({1, 1, 1})), {10, 10, 10});
        auto g = Grid::Periodic(Domain(d, vec({0, 0, 0}), vec({1, 1, 1})), {10, 10, 10});
        int nSpecies = picard::uniform_int(1, 10);
        int c1 = picard::uniform_int(-1, 1);
        int c2 = picard::uniform_int(-1, 1);
        ng->allocateGridData(nSpecies);
        ng->neutralizingBackground.assign(ng->totalPoints(), c2);
        ng->chargeDensity.array() += c1;

        g->allocateGridData(nSpecies);
        g->chargeDensity.array() += c1;

        for (int i = 0; i < g->totalPoints(); i++) {
            REQUIRE(g->getChargeDensity(i) == nSpecies*c1);
            REQUIRE(ng->getChargeDensity(i) == nSpecies*c1 + c2);
        }
    }
}

TEST_CASE("Fill Nodes (1D, 2D, and 3D)", "[grid]") {
    ivec numPts = {10, 30, 17};
    vec min = {-1, -2, -1};
    vec spacing = {2, 3, 1};
    vec max = min + spacing * vcast<Real>(numPts - 1);

    for (int d = 1; d <= 3; d++) {
        Grid g(Domain(d, min, max), numPts);
        g.fillNodeLocations();

        for (int i = 0; i < g.numPoints(0); i++) {
            for (int j = 0; j < g.numPoints(1); j++) {
                for (int k = 0; k < g.numPoints(2); k++) {
                    ivec ijk = {i, j, k};
                    auto pos = min + spacing * vcast<Real>(ijk);
                    REQUIRE(g.position(ivec({i, j, k})) == vec(pos.begin(), pos.begin() + d));
                }
            }
        }
    }
}

TEST_CASE("getAdjacentNode - 1D", "[grid]"){
    auto g = Grid::Periodic(Domain(1, vec({0}), vec({1})), {11});

    REQUIRE(g->getAdjacentNode({0},{1}) == ivec({1}));
    REQUIRE(g->getAdjacentNode({0},{-1}) == ivec({1}));
    REQUIRE(g->getAdjacentNode({0},{0}) == ivec({0}));
    REQUIRE(g->getAdjacentNode({5},{-1}) == ivec({4}));
    REQUIRE(g->getAdjacentNode({5},{1}) == ivec({6}));
    REQUIRE(g->getAdjacentNode({5},{0}) == ivec({5}));
    REQUIRE(g->getAdjacentNode({10},{1}) == ivec({9}));
    REQUIRE(g->getAdjacentNode({10},{-1}) == ivec({9}));
    REQUIRE(g->getAdjacentNode({10},{0}) == ivec({10}));
}

TEST_CASE("getAdjacentNode - 2D", "[grid]"){
    auto g = Grid::Periodic(Domain(2, vec({0,0}), vec({1,1})), {11,21});

    // Bottom left corner
    REQUIRE(g->getAdjacentNode({0,0},{0,0}) == ivec({0,0}));
    REQUIRE(g->getAdjacentNode({0,0},{0,1}) == ivec({0,1}));
    REQUIRE(g->getAdjacentNode({0,0},{0,-1}) == ivec({0,1}));
    REQUIRE(g->getAdjacentNode({0,0},{1,0}) == ivec({1,0}));
    REQUIRE(g->getAdjacentNode({0,0},{-1,0}) == ivec({1,0}));
    REQUIRE(g->getAdjacentNode({0,0},{-1,-1}) == ivec({1,1}));
    REQUIRE(g->getAdjacentNode({0,0},{1,1}) == ivec({1,1}));

    // Top right corner
    REQUIRE(g->getAdjacentNode({10,20},{0,0}) == ivec({10,20}));
    REQUIRE(g->getAdjacentNode({10,20},{0,1}) == ivec({10,19}));
    REQUIRE(g->getAdjacentNode({10,20},{0,-1}) == ivec({10,19}));
    REQUIRE(g->getAdjacentNode({10,20},{1,0}) == ivec({9,20}));
    REQUIRE(g->getAdjacentNode({10,20},{-1,0}) == ivec({9,20}));
    REQUIRE(g->getAdjacentNode({10,20},{-1,-1}) == ivec({9,19}));
    REQUIRE(g->getAdjacentNode({10,20},{1,1}) == ivec({9,19}));

    // Interior
    REQUIRE(g->getAdjacentNode({5,10},{0,0}) == ivec({5,10}));
    REQUIRE(g->getAdjacentNode({5,10},{0,1}) == ivec({5,11}));
    REQUIRE(g->getAdjacentNode({5,10},{0,-1}) == ivec({5,9}));
    REQUIRE(g->getAdjacentNode({5,10},{1,0}) == ivec({6,10}));
    REQUIRE(g->getAdjacentNode({5,10},{-1,0}) == ivec({4,10}));
    REQUIRE(g->getAdjacentNode({5,10},{-1,-1}) == ivec({4,9}));
    REQUIRE(g->getAdjacentNode({5,10},{1,1}) == ivec({6,11}));

    //Right side
    REQUIRE(g->getAdjacentNode({5,20},{0,0}) == ivec({5,20}));
    REQUIRE(g->getAdjacentNode({5,20},{0,1}) == ivec({5,19}));
    REQUIRE(g->getAdjacentNode({5,20},{0,-1}) == ivec({5,19}));
    REQUIRE(g->getAdjacentNode({5,20},{1,0}) == ivec({6,20}));
    REQUIRE(g->getAdjacentNode({5,20},{-1,0}) == ivec({4,20}));
    REQUIRE(g->getAdjacentNode({5,20},{-1,-1}) == ivec({4,19}));
    REQUIRE(g->getAdjacentNode({5,20},{1,1}) == ivec({6,19}));

}

TEST_CASE("getAdjacentNode - 3D", "[grid]"){
    auto g = Grid::Periodic(Domain(3, vec({0,0,0}), vec({1,1,1})), {11, 21, 31});

    // Corners
    ivec c1 = {0,0,0};
    ivec c2 = {10,0,0};
    ivec c3 = {10,20,0};
    ivec c4 = {10,20,30};

    // Edges
    ivec e1 = {0,7,0};
    ivec e2 = {10,0,21};
    ivec e3 = {4,20,0};

    // Faces
    ivec f1 = {1,7,0};
    ivec f2 = {6,0,21};
    ivec f3 = {4,20,10};

    // interior points
    ivec i1 = {1,7,20};
    ivec i2 = {6,4,21};
    ivec i3 = {4,18,10};

    ivec disp1 = {-1, 0, 1};
    vector<ivec> pts = {c1, c2, c3, c4, e1, e2, e3, f1, f2, f3, i1, i2, i3};

    for(auto i : disp1){
        for (auto j : disp1){
            for (auto k : disp1){
                ivec disp  = {i,j,k};
                for(auto pt : pts){
                    ivec new_pt(pt.size());
                    for(uint dim = 0; dim < pt.size(); dim++){
                        new_pt[dim] = pt[dim] + disp[dim];
                        if(new_pt[dim] < 0 || new_pt[dim] >= g->numPoints(dim))
                            new_pt[dim] = pt[dim] - disp[dim];
                    }
                    REQUIRE(g->getAdjacentNode(pt, disp) == new_pt);
                }
            }
        }
    }
}

TEST_CASE("Make Domain From Element", "[grid]"){
    auto g = Grid::Periodic(Domain(2, vec({0, 0}), vec({1, 1})), {11, 21});
    auto e = g->getBoundaryElement(g->index(ivec({10,5})), 1);

    Domain d(2);
    for(auto i : e){
        d.expandToInclude(g->position(i));
    }

    REQUIRE(g->makeDomain(e) == d);
}

TEST_CASE("getBoundaryElement - 1D", "[grid]"){
    auto grid = Grid::Periodic(Domain(1, vec({0}), vec({1})), {11});

    REQUIRE(grid->getBoundaryElement(0, 0) == Element({0}));
    REQUIRE(grid->getBoundaryElement(10, 0) == Element({10}));
    REQUIRE(grid->getBoundaryElement(8, 0) == Element({8}));
}

TEST_CASE("getBoundaryElement - 2D", "[grid]") {
    auto g = Grid::Periodic(Domain(2, vec({0, 0}), vec({1, 1})), {11, 21});

    // Corner points
    auto i0 = g->index(ivec({10, 20}));
    REQUIRE(g->getBoundaryElement(i0, 0) == Element({i0, i0 + g->index(ivec({0, -1}))}));
    REQUIRE(g->getBoundaryElement(i0, 1) == Element({i0, i0 + g->index(ivec({-1, 0}))}));

    auto i1 = g->index(ivec({0, 0}));
    REQUIRE(g->getBoundaryElement(i1, 0) == Element({i1 + g->index(ivec({0, 1})), i1}));
    REQUIRE(g->getBoundaryElement(i1, 1) == Element({i1 + g->index(ivec({1, 0})), i1}));

    // Interior point
    auto i2 = g->index(ivec({1,1}));
    REQUIRE(g->getBoundaryElement(i2, 0) == Element({i2 + g->index(ivec({0, 1})), i2}));
    REQUIRE(g->getBoundaryElement(i2, 1) == Element({i2 + g->index(ivec({1, 0})), i2}));

    // Edge Point
    auto i3 = g->index(ivec({10,5}));
    REQUIRE(g->getBoundaryElement(i3, 1) == Element({i3 , i3 - g->index(ivec({1, 0}))}));
    REQUIRE(g->getBoundaryElement(i3, 0) == Element({i3 + g->index(ivec({0,1})), i3}));
}

TEST_CASE("getBoundaryElement - 3D", "[grid]"){
    auto g = Grid::Periodic(Domain(3, vec({0, 0, 0}), vec({1, 1, 1})), {11, 21, 31});

    // Corner points
    auto i0 = g->index(ivec({10, 20, 30}));
    REQUIRE(g->getBoundaryElement(i0, 0) == Element({i0, i0 + g->index(ivec({0, -1, 0})),
                                                     i0 + g->index(ivec({0, 0, -1})),
                                                     i0 + g->index(ivec({0, -1, -1}))}));
    REQUIRE(g->getBoundaryElement(i0, 1) == Element({i0, i0 + g->index(ivec({-1, 0, 0})),
                                                     i0 + g->index(ivec({0, 0, -1})),
                                                     i0 + g->index(ivec({-1, 0, -1}))}));
    REQUIRE(g->getBoundaryElement(i0, 2) == Element({i0, i0 + g->index(ivec({-1, 0, 0})),
                                                     i0 + g->index(ivec({0, -1, 0})),
                                                     i0 + g->index(ivec({-1, -1, 0}))}));

    auto i1 = g->index(ivec({0, 0, 0}));
    REQUIRE(g->getBoundaryElement(i1, 0) == Element({i1 + g->index(ivec({0, 1, 1})), i1 + g->index(ivec({0, 0, 1})),
                                                     i1 + g->index(ivec({0, 1, 0})), i1}));
    REQUIRE(g->getBoundaryElement(i1, 1) == Element({i1 + g->index(ivec({1, 0, 1})), i1 + g->index(ivec({0, 0, 1})),
                                                     i1 + g->index(ivec({1, 0, 0})), i1}));
    REQUIRE(g->getBoundaryElement(i1, 2) == Element({i1 + g->index(ivec({1, 1, 0})), i1 + g->index(ivec({0, 1, 0})),
                                                     i1 + g->index(ivec({1, 0, 0})), i1}));

    // Interior point
    auto i2 = g->index(ivec({5, 15, 25}));
    REQUIRE(g->getBoundaryElement(i2, 0) == Element({i2 + g->index(ivec({0, 1, 1})), i2 + g->index(ivec({0, 0, 1})),
                                                     i2 + g->index(ivec({0, 1, 0})), i2}));
    REQUIRE(g->getBoundaryElement(i2, 1) == Element({i2 + g->index(ivec({1, 0, 1})), i2 + g->index(ivec({0, 0, 1})),
                                                     i2 + g->index(ivec({1, 0, 0})), i2}));
    REQUIRE(g->getBoundaryElement(i2, 2) == Element({i2 + g->index(ivec({1, 1, 0})), i2 + g->index(ivec({0, 1, 0})),
                                                     i2 + g->index(ivec({1, 0, 0})), i2}));

    // Edge Point
    auto i3 = g->index(ivec({10,5,0}));
    REQUIRE(g->getBoundaryElement(i3, 0) == Element({i3 + g->index(ivec({0, 1, 1})), i3 + g->index(ivec({0, 0, 1})),
                                                     i3 + g->index(ivec({0, 1, 0})), i3}));
    REQUIRE(g->getBoundaryElement(i3, 2) == Element({i3 + g->index(ivec({0, 1, 0})),
                                                     i3 + g->index(ivec({-1, 1, 0})),
                                                     i3, i3 + g->index(ivec({-1, 0, 0}))}));

    // Face Point
    auto i4 = g->index(ivec({10, 5, 25}));
    REQUIRE(g->getBoundaryElement(i4, 0) == Element({i4 + g->index(ivec({0, 1, 1})), i4 + g->index(ivec({0, 0, 1})),
                                                     i4 + g->index(ivec({0, 1, 0})), i4}));
}

TEST_CASE("getEnclosingCell - 1D", "[grid]") {
    auto g = Grid::Periodic(Domain(1, vec({0}), vec({10})), {11});

    REQUIRE(g->getEnclosingCell({0.1}) == Cell({0, 1}));
    REQUIRE(g->getEnclosingCell({0}) == Cell({0, 1}));
    REQUIRE(g->getEnclosingCell({9.5}) == Cell({9, 10}));
    REQUIRE(g->getEnclosingCell({10}) == Cell({10, 9}));
}

TEST_CASE("getEnclosingCell - 2D", "[grid]") {
    auto g = Grid::Periodic(Domain(2, vec({0, 0}), vec({10, 10})), {11, 11});

    REQUIRE(g->getEnclosingCell({0.1, 0.1}) == Cell({0, 1, 11, 12}));
    REQUIRE(g->getEnclosingCell({0, 0}) == Cell({0, 1, 11, 12}));
    REQUIRE(g->getEnclosingCell({9.9, 9.9}) == Cell({108, 109, 119, 120}));
    REQUIRE(g->getEnclosingCell({10, 10}) == Cell({120, 119, 109, 108}));
    REQUIRE(g->getEnclosingCell({5, 5}) == Cell({60, 61, 71, 72}));
    REQUIRE(g->getEnclosingCell({5.4, 5.1}) == Cell({60, 61, 71, 72}));
    REQUIRE(g->getEnclosingCell({10, 5.1}) == Cell({65, 64, 76, 75}));
}

TEST_CASE("getEnclosingCell - 3D", "[grid]") {
    auto g = Grid::Periodic(Domain(3, vec({0, 0, 0}), vec({10, 10, 10})), {11, 11, 11});

    REQUIRE(g->getEnclosingCell({0.1, 0.1, 0}) == Cell({0, 1, 11, 12, 121, 122, 132, 133}));
    REQUIRE(g->getEnclosingCell({0, 0, 0}) == Cell({0, 1, 11, 12, 121, 122, 132, 133}));
    REQUIRE(g->getEnclosingCell({9.9, 9.9, 0}) == Cell({108, 109, 119, 120, 229, 230, 240, 241}));
    REQUIRE(g->getEnclosingCell({9.9, 9.9, 9.9}) == Cell({1197, 1198, 1208, 1209, 1318, 1319, 1329, 1330}));
    REQUIRE(g->getEnclosingCell({10, 10, 0}) == Cell({120, 119, 109, 108, 241, 240, 230, 229}));
    REQUIRE(g->getEnclosingCell({10, 10, 10}) == Cell({1330, 1329, 1319, 1318, 1209, 1208, 1198, 1197}));
    REQUIRE(g->getEnclosingCell({5, 5, 0}) == Cell({60, 61, 71, 72, 181, 182, 192, 193}));
    REQUIRE(g->getEnclosingCell({5.4, 5.1, 0}) == Cell({60, 61, 71, 72, 181, 182, 192, 193}));
    REQUIRE(g->getEnclosingCell({10, 5.1, 0}) == Cell({65, 64, 76, 75, 186, 185, 197, 196}));

    REQUIRE(g->getEnclosingCell({0.1, 0.1, 5.5}) == Cell({605, 606, 616, 617, 726, 727, 737, 738}));
    REQUIRE(g->getEnclosingCell({0, 0, 5.5}) == Cell({605, 606, 616, 617, 726, 727, 737, 738}));
    REQUIRE(g->getEnclosingCell({9.9, 9.9, 5.5}) == Cell({713, 714, 724, 725, 834, 835, 845, 846}));
    REQUIRE(g->getEnclosingCell({10, 10, 5.5}) == Cell({725, 724, 714, 713, 846, 845, 835, 834}));
    REQUIRE(g->getEnclosingCell({5, 5, 5.5}) == Cell({665, 666, 676, 677, 786, 787, 797, 798}));
    REQUIRE(g->getEnclosingCell({5.4, 5.1, 5.5}) == Cell({665, 666, 676, 677, 786, 787, 797, 798}));
    REQUIRE(g->getEnclosingCell({10, 5.1, 5.5}) == Cell({670, 669, 681, 680, 791, 790, 802, 801}));
}

TEST_CASE("boundaryElements - 1D", "[grid]") {
    auto grid = Grid::Periodic(Domain(1, vec({0}), vec({1})), {11});
    auto bElements = grid->boundaryElements();

    REQUIRE(bElements.size() == 2);
    REQUIRE(bElements.find({0, -1, -1, -1}) != bElements.end());
    REQUIRE(grid->boundaryId({0, -1, -1, -1}) == 0);
    REQUIRE(bElements.find({10, -1, -1, -1}) != bElements.end());
    REQUIRE(grid->boundaryId({10, -1, -1, -1}) == 1);
}

TEST_CASE("boundaryElements - 2D", "[grid]") {
    auto grid = Grid::Periodic(Domain(2, vec({0, 0}), vec({1, 1})), {11, 21});
    auto bElements = grid->boundaryElements();

    REQUIRE(bElements.size() == 2 * sum(grid->numPoints() - 1));
    for (auto i : ivec({0, grid->numPoints(0) - 1})) {
        for (int j = 0; j < grid->numPoints(1) - 1; j++) {
            int ind1 = grid->index(ivec({i, j}));
            int ind2 = grid->index(ivec({i, j + 1}));
            Element e = {ind1, ind2, -1, -1};
            fastSort4(e, std::greater<int>());
            REQUIRE(bElements.find(e) != bElements.end());
            if (i == 0)
                REQUIRE(grid->boundaryId(e) == 0);
            else
                REQUIRE(grid->boundaryId(e) == 1);
        }
    }

    for (auto j : ivec({0, grid->numPoints(1) - 1})) {
        for (int i = 0; i < grid->numPoints(0) - 1; i++) {
            int ind1 = grid->index(ivec({i, j}));
            int ind2 = grid->index(ivec({i + 1, j}));
            Element e = {ind1, ind2, -1, -1};
            fastSort4(e, std::greater<int>());
            REQUIRE(bElements.find(e) != bElements.end());
            if (j == 0)
                REQUIRE(grid->boundaryId(e) == 2);
            else
                REQUIRE(grid->boundaryId(e) == 3);
        }
    }

}

TEST_CASE("boundaryElements - 3D", "[grid]") {
    auto grid = Grid::Periodic(Domain(3, vec({0, 0, 0}), vec({1, 1, 1})), {11, 21, 31});
    auto bElements = grid->boundaryElements();

    REQUIRE(bElements.size() == 2 * (10 * 20 + 10 * 30 + 20 * 30));
    for (auto i : ivec({0, grid->numPoints(0) - 1})) {
        for (int j = 0; j < grid->numPoints(1) - 1; j++) {
            for (int k = 0; k < grid->numPoints(2) - 1; k++) {
                int ind1 = grid->index(ivec({i, j, k}));
                int ind2 = grid->index(ivec({i, j + 1, k}));
                int ind3 = grid->index(ivec({i, j + 1, k + 1}));
                int ind4 = grid->index(ivec({i, j, k + 1}));
                Element e = {ind1, ind2, ind3, ind4};
                fastSort4(e, std::greater<int>());
                REQUIRE(bElements.find(e) != bElements.end());
                if (i == 0)
                    REQUIRE(grid->boundaryId(e) == 0);
                else
                    REQUIRE(grid->boundaryId(e) == 1);
            }
        }
    }

    for (auto j : ivec({0, grid->numPoints(1) - 1})) {
        for (int i = 0; i < grid->numPoints(0) - 1; i++) {
            for (int k = 0; k < grid->numPoints(2) - 1; k++) {
                int ind1 = grid->index(ivec({i, j, k}));
                int ind2 = grid->index(ivec({i + 1, j, k}));
                int ind3 = grid->index(ivec({i + 1, j, k + 1}));
                int ind4 = grid->index(ivec({i, j, k + 1}));
                Element e = {ind1, ind2, ind3, ind4};
                fastSort4(e, std::greater<int>());
                REQUIRE(bElements.find(e) != bElements.end());
                if (j == 0)
                    REQUIRE(grid->boundaryId(e) == 2);
                else
                    REQUIRE(grid->boundaryId(e) == 3);
            }
        }
    }

    for (auto k : ivec({0, grid->numPoints(2) - 1})) {
        for (int j = 0; j < grid->numPoints(1) - 1; j++) {
            for (int i = 0; i < grid->numPoints(0) - 1; i++) {
                int ind1 = grid->index(ivec({i, j, k}));
                int ind2 = grid->index(ivec({i, j + 1, k}));
                int ind3 = grid->index(ivec({i + 1, j + 1, k}));
                int ind4 = grid->index(ivec({i + 1, j, k}));
                Element e = {ind1, ind2, ind3, ind4};
                fastSort4(e, std::greater<int>());
                REQUIRE(bElements.find(e) != bElements.end());
                if (k == 0)
                    REQUIRE(grid->boundaryId(e) == 4);
                else
                    REQUIRE(grid->boundaryId(e) == 5);
            }
        }
    }
}

TEST_CASE("Node Ids - (1D, 2D, 3D", "[grid]") {
    auto g1 = Grid::Periodic(Domain(1, vec({0}), vec({1})), {11});
    auto g2 = Grid::Periodic(Domain(2, vec({0, 0}), vec({1, 1})), {11, 21});
    auto g3 = Grid::Periodic(Domain(3, vec({0, 0, 0}), vec({1, 1, 1})), {11, 21, 31});

    std::vector<std::shared_ptr<Grid>> grids = {g1, g2, g3};

    for(auto grid : grids) {
        for (int i = 0; i < grid->totalPoints(); i++) {
            auto boundaryIds = grid->boundaryIds(i);
            auto ijk = grid->ijk(i);
            for (int d = 0; d < grid->nDim(); d++) {
                if (ijk[d] == 0) {
                    REQUIRE(find(boundaryIds.begin(), boundaryIds.end(), 2 * d) != boundaryIds.end());
                } else if (ijk[d] == grid->numPoints(d) - 1) {
                    REQUIRE(find(boundaryIds.begin(), boundaryIds.end(), 2 * d + 1) != boundaryIds.end());
                }
            }
        }
    }
}

TEST_CASE("Grid With Subdomain Checks - permissions, activeVolume", "[grid]") {
    string mesh_path = "tests/meshes/2Ddualblockmesh.msh";
    GmshReader r(mesh_path);
    vector<shared_ptr<FieldLabel>> fieldLabels = {
            makeInterior("plasma", 0, 1, true),
            makeInterior("dielectric", 0, 10, false),
            makeDirichlet("buffer_lower", 3, {0, -1}, 0),
            makeDirichlet("buffer_upper", 4, {0, -1}, 0),
            makeDirichlet("electrode", 5, {0, -1}, 0),
            makeDirichlet("interface", 6, {0, -1}, 0),
            makeDirichlet("left", 7, {-1, 0}, 0),
            makeDirichlet("right", 8, {1, 0}, 100),
            makeDirichlet("spacing", 8, {0, -1}, 100),
            makeDirichlet("top", 8, {0, 1}, 100),
            makeDirichlet("spacecraft", 8, {0, 1}, 100),
    };

    vector<shared_ptr<ParticleBoundaryLabel>> particleLabels = {
            makeOpen("buffer_lower", 3, {0, -1}),
            makeReflecting("buffer_upper", 4, {0, -1}, 1),
            makeOpen("electrode", 5, {0, -1}),
            makeReflecting("interface", 6, {0, -1}, 1),
            makeOpen("left", 7, {-1, 0}),
            makeOpen("right", 8, {1, 0}),
            makeOpen("spacing", 8, {0, -1}),
            makeOpen("top", 8, {0, 1}),
            makeOpen("spacecraft", 8, {0, 1}),
    };
    auto grid = r.makeGrid(fieldLabels, particleLabels);
    int trials = 100;

    for (int i = 0; i < trials; i++) {
        REQUIRE(grid->isPermitted(grid->randomPermittedLocation()));
        for(auto& domain : grid->subDomains)
            REQUIRE(grid->isPermitted(domain.randomPositionInside()) == domain.particlesPermitted());
    }

    REQUIRE(grid->activeVolume() == 600*(150-13));
}
