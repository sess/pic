# Title: 2D Pulse Test
# Purpose: Tests the dynamics of a pulse on the 2D electrode geometry
# Author/Date: Anthony Corso - 15 Feb 2017
# Comments: Should recreate all of the 2D behavior shown in Siddarth Krishbamoorthy's Thesis (depending on boundary conditions)

T = 500
dt = 0.2
meshFile=tests/meshes/2Ddualblockmesh.msh
gridType = Neutralizing

[PhysicalParams]
    n = 6.1e17
    T = 1

[InteriorProperties]
    setDomain=plasma, Yes, 1
    setDomain=dielectric, No, 9.34

[Particles]
    addSpecies=electron, 1, -1
        initialize=electron, cartesian, (2;2)
        velocity_distribution=electron, x, maxwellian, 0, .4
        velocity_distribution=electron, y, maxwellian, 0, .4

    addSpecies=ion, 72821, 1
        initialize=ion, cartesian, (2;2)
        velocity_distribution=ion, x, maxwellian, 0, .4
        velocity_distribution=ion, y, maxwellian, 0, .4

    addSpecies=neutral, 72821, 0
        initialize=neutral, on_grid, 1.88e21,
        velocity_distribution=neutral, xy, beam, 0


    interaction = coulomb_collisions, electron, ion
    interaction = electron_neutral_collisions, electron, neutral, tests/csvs/argon_electron_cross_section_samples.csv


[BoundaryConditions]
    setFieldBC = buffer_upper, plasma dielectric interface, (0;-1), 9.34, 1
    setFieldBC = interface, plasma dielectric interface, (0;-1), 9.34, 1
    setFieldBC = buffer_lower, neumann, (0;-1), 0
    setFieldBC = right, zero field, (1;0)
    setFieldBC = left, zero field, (-1;0)
    setFieldBC = top, dirichlet, (0;1), 0
    setFieldBC = spacing, neumann, (0;-1), 0
    setFieldBC = electrode, step, (0;-1), 0, -5000, 1, 10000
    setFieldBC = spacecraft, dirichlet, (0;-1), 0

    setParticleBC=buffer_upper, reflecting, (0;-1),1
    setParticleBC=interface, reflecting, (0;-1),1
    setParticleBC=buffer_lower, Open, (0;-1)
    setParticleBC=right, Open, (1;0)
    setParticleBC=left, Open, (-1;0)
    setParticleBC=top, Open, (0;1)
    setParticleBC=spacing, Open, (0;-1)
    setParticleBC=electrode, Open, (0;-1)
    setParticleBC=spacecraft, Open, (0;-1)

    plasmaBoundary = (left;right;top), (electron; ion)

[Output]
   interval = 1

   write = charge_density
   write = potential
   write = species_count
   write = temperature

   analyze = true
