#ifndef SESS_PIC_DIRECTSOLVER_H
#define SESS_PIC_DIRECTSOLVER_H

#include "Definitions.h"

#include "FieldSolver.h"
#include "Simulation.h"
#include "Grid.h"
#include <iostream>
#include <Eigen/IterativeLinearSolvers>
#include <Eigen/Sparse>
#include <Eigen/SparseQR>
#include <Eigen/Dense>

namespace picard {

typedef Eigen::SparseMatrix<double, Eigen::ColMajor> SpMat;
typedef Eigen::Triplet<double> Trip;


class DirectSolver : public FieldSolver {
public:

    DirectSolver(const Grid& grid) { prepareSolver(grid); }
    DirectSolver(){}

    void prepareSolver(const Grid& grid) override;

    void findPotential(Real time, Real dt, Grid& grid) override;

    void calculateIndexMatrix(const Grid& grid);

    void addInteriorCoefficient(int iNode, const Grid& grid);

    void addDirichletCoefficient(int iNode/*, const Grid& grid*/);

    void addNeumannCoefficient(int iNode, const Grid& grid);

    void addInterfaceCoefficient(int iNode, const Grid& grid);

    void addPeriodicCoefficient(int iNode, const Grid& grid);

    void buildRHS(Real time, Real dt, const Grid& grid);

    void addInterfaceRHS(int iNode, const Grid& grid);

    void addNeumannRHS(int iNode, const Grid& grid);




private:
    std::vector<Trip> coefficients;
    SpMat poissonLHSMatrix;

    // SparseQR<SpMat,NaturalOrdering<int>> sparseQRLHS;

    Eigen::SparseLU<SpMat, Eigen::COLAMDOrdering<int>> sparseLULHS;

    //BiCGSTAB<SpMat,IncompleteLUT<double>> sparseBCGSTABLHS;
    //
    // IterativeLinearSolvers::LeastSquaresConjugateGradient <SpMat> sparseQRLHS;
    //LeastSquaresConjugateGradient<SpMat> sparseQRLHS;
    // SparseLU<SpMat,COLAMDOrdering<int>> sparseQRLHS; //change solver to something that can handle rectangular A matrix

    Eigen::VectorXd RHS;

};

}
#endif //SESS_PIC_DIRECTSOLVER_H
