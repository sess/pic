#ifndef SESS_PIC_MULTIGRIDSOLVER_H
#define SESS_PIC_MULTIGRIDSOLVER_H

#include "Definitions.h"
#include "FieldSolver.h"

#include <amgcl/make_solver.hpp>
#include <amgcl/solver/bicgstab.hpp>
#include <amgcl/solver/bicgstabl.hpp>
#include <amgcl/amg.hpp>
#include <amgcl/coarsening/smoothed_aggregation.hpp>
#include <amgcl/relaxation/spai0.hpp>
#include <amgcl/relaxation/spai1.hpp>
#include <amgcl/adapter/crs_tuple.hpp>
#include <amgcl/solver/gmres.hpp>
#include <amgcl/relaxation/ilu0.hpp>
#include <amgcl/solver/skyline_lu.hpp>
#include <amgcl/preconditioner/schur_pressure_correction.hpp>
#include <amgcl/relaxation/as_preconditioner.hpp>

namespace picard {

typedef amgcl::backend::builtin<Real> Backend;


//BCGSTAB
/*
typedef amgcl::make_solver<
        amgcl::amg<
                Backend,
                amgcl::coarsening::smoothed_aggregation,
                amgcl::relaxation::spai0>,
        amgcl::solver::bicgstab<Backend>> MGSolver;// And BiCGStab as iterative solver:
*/

//schur

typedef amgcl::make_solver<
        amgcl::relaxation::as_preconditioner<Backend, amgcl::relaxation::ilu0>,
        amgcl::solver::bicgstab<Backend>
> USolver;

typedef amgcl::make_solver<
        amgcl::amg<Backend, amgcl::coarsening::smoothed_aggregation, amgcl::relaxation::spai0>,
        amgcl::solver::bicgstab<Backend>
> PSolver;

typedef amgcl::make_solver<
        amgcl::preconditioner::schur_pressure_correction<USolver, PSolver>,
        amgcl::solver::bicgstab<Backend>
> MGSolver;

//TODO how does the setup differ from direct approach? any simplification here?
class MultiGridSolver : public FieldSolver {
public:

    MultiGridSolver(const Grid& grid) { prepareSolver(grid); }
    MultiGridSolver(){}

    void prepareSolver(const Grid& grid) override;

    virtual void findPotential(Real time, Real dt, Grid& grid) override;

    void buildMGSolver(const Grid& grid);

    void addInteriorCoefficient(int iNode, const Grid& grid);

    void addDirichletCoefficient(int iNode/*, const Grid& grid*/);

    void addNeumannCoefficient(int iNode, const Grid& grid);

    void addInterfaceCoefficient(int iNode, const Grid& grid);

    void addPeriodicCoefficient(int iNode, const Grid& grid);

    void buildRHS(Real time, Real dt, const Grid& grid);

    void addInterfaceRHS(int iNode, const Grid& grid);

    void addNeumannRHS(int iNode, const Grid& grid);

    std::unordered_map<int, int> columnValueMap;

    void insertWithoutDuplication(int colNumber, double addValue);

    std::shared_ptr<MGSolver> solver{nullptr};

private:
    std::vector<int> rowPointer, col;
    std::vector<char> pMask; //pressure mask for schur
    std::vector<double> RHS;
    std::vector<double> value;
};

}

#endif //SESS_PIC_MULTIGRIDSOLVER_H
