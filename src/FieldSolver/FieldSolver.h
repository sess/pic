#ifndef SESS_PIC_FIELDSOLVER_H
#define SESS_PIC_FIELDSOLVER_H

#include "Definitions.h"
#include "Grid.h"
#include <vector>
#include <algorithm>
#include <string>
#include <fstream>

namespace picard {

class FieldSolver {
public:
    virtual ~FieldSolver() = default;

    virtual void prepareSolver(const Grid& grid) = 0;

    virtual void findPotential(Real time, Real dt, Grid& grid)=0;

    void findElectricField(Grid& grid);

    void calculateInteriorAndBoundaryEfield(int iNode, Grid& grid);

    void calculateInterfaceEfield(int iNode, Grid& grid);

    void differentiatePotential(int, Grid& grid);

    void calculatePeriodicEfield(int, Grid& grid);

    Real differentiatePotential(int iNode,int iNeighborL,int iNeighborR,int derivativeDirection, const Grid& grid);
};

}


#endif //SESS_PIC_FIELDSOLVER_H
