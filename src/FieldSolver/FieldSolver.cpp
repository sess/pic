#include <numeric>
#include "FieldSolver.h"
#include "Label.h"

using namespace std;
using namespace picard;

//findElectricField - > Main electric field finder by central difference
void FieldSolver::findElectricField(Grid& grid) {
    for (int iNode = 0; iNode < grid.totalPoints(); iNode++) {

        switch (grid.fieldLabels[iNode]->type) {

            case FieldLabel::PLASMA_DIELECTRIC_INTERFACE:
                FieldSolver::calculateInterfaceEfield(iNode, grid);
                break;

            case FieldLabel::PERIODIC:
                FieldSolver::calculatePeriodicEfield(iNode, grid);
                break;

            case FieldLabel::ZERO_FIELD:
                for (int iDim = 0; iDim < grid.nDim(); iDim++) //set all fields to 0
                    grid.eField(iNode, iDim) = 0.0;
                break;


            default:
                calculateInteriorAndBoundaryEfield(iNode, grid);
                break;

        }
    }
}



//CalculateInterfaceField - > calculates field at a plasma-dielectric interface
void FieldSolver::calculateInterfaceEfield(int iNode, Grid& grid) {

    int Ngx = grid.numPoints(0);
    int Ngy = grid.numPoints(1);
//    int Ngz = grid.numPoints(2);

    int Ndims = grid.nDim();

    int normalDirection = 0;
    int signNormalDirection = -1;
    for (int iDim = 0; iDim < Ndims; iDim++)
        if (grid.fieldLabels[iNode]->normal[iDim] != 0) {
            normalDirection = iDim;
            signNormalDirection =
                    -grid.fieldLabels[iNode]->normal[iDim] / fabs(grid.fieldLabels[iNode]->normal[iDim]);
            /*THe sign of the formula for signNormalDirection was flipped on 16 Dec 2015 because the convention of normals for interface was changed
             from plasma-pointing to pointing away from the plasma. The sign here was flipped so that the rest of field solver doesn't need to be changed*/
            break;
        }

    //  cout<<normalDirection<<","<<signNormalDirection<<endl;
    for (int iDim = 0; iDim < Ndims; iDim++)
        for (int iDerDir = 0; iDerDir <= iDim; iDerDir++) {

            if (iDerDir == normalDirection) { //normal field requires special treatment

                int nodeNeighborShift =
                        (1 * (iDerDir == 0) + Ngx * (iDerDir == 1) + Ngx * Ngy * (iDerDir == 2)) * signNormalDirection;

                //E_n_interface = E_n_(interface+-1/2) - 0.5 rho_interface*grid_resolution_normal_to_interface
                int iNeighborL =
                        iNode * (nodeNeighborShift > 0) + (iNode + nodeNeighborShift) * (nodeNeighborShift < 0);
                int iNeighborR =
                        iNode * (nodeNeighborShift < 0) + (iNode + nodeNeighborShift) * (nodeNeighborShift > 0);
                //   cout<<iNeighborL<<"\t"<<iNeighborR<<endl;

                grid.eField(iNode, iDerDir) =
                        -1.0 * FieldSolver::differentiatePotential(iNode, iNeighborL, iNeighborR, iDerDir, grid)
                        - 0.5 * grid.getChargeDensity(iNode) * (fabs(grid.position(iNeighborL, iDerDir)
                                                                      - grid.position(iNeighborR, iDerDir)));

            } else { //tangential to surface, field is calculated regularly

                int iNeighborL = iNode - 1 * (iDerDir == 0) - Ngx * (iDerDir == 1) - Ngx * Ngy * (iDerDir == 2);
                int iNeighborR = iNode + 1 * (iDerDir == 0) + Ngx * (iDerDir == 1) + Ngx * Ngy * (iDerDir == 2);


                grid.eField(iNode, iDerDir) =
                        -1.0 * FieldSolver::differentiatePotential(iNode, iNeighborL, iNeighborR, iDerDir, grid);


            }
        }
}


//calculate BoundaryEfield -> calculates boundary E field for interior, dirichlet and neumann
void FieldSolver::calculateInteriorAndBoundaryEfield(int iNode, Grid& grid) {

    int Ngx = grid.numPoints(0);
    int Ngy = grid.numPoints(1);
    int Ngz = grid.numPoints(2);

    int Ndims = grid.nDim();


    for (int iDerDir = 0; iDerDir < Ndims; iDerDir++) {

        int iNeighborL = iNode - 1 * (iDerDir == 0) - Ngx * (iDerDir == 1) - Ngx * Ngy * (iDerDir == 2);
        int iNeighborR = iNode + 1 * (iDerDir == 0) + Ngx * (iDerDir == 1) + Ngx * Ngy * (iDerDir == 2);


        grid.eField(iNode, iDerDir) = -1.0 * FieldSolver::differentiatePotential(iNode, iNeighborL, iNeighborR, iDerDir, grid);

    }


    if (grid.surfaceChargeMap.find(iNode) != grid.surfaceChargeMap.end()) {
        int normalDirection = nonZeroDim(grid.fieldLabels[iNode]->normal);

        vector<int> InteriorPointDifference = {1, Ngx * (Ngy > 1), Ngx * Ngy * (Ngz > 1)}; //if 1D, Ngy=1, if 2D Ngz=1

// interior point index is the dot product of the InteriorPointDifference vector with the negative of the face normal vector

        int InteriorPointIndex = iNode - (int) inner_product(InteriorPointDifference.begin(), InteriorPointDifference.end(),
                                                     grid.fieldLabels[iNode]->normal.begin(), 0.0);

        Real dNormal = fabs(
                grid.position(InteriorPointIndex, normalDirection) - grid.position(iNode, normalDirection));

        grid.eField(iNode, normalDirection) -= 0.5 * grid.getChargeDensity(iNode) * dNormal;

    }
}


//CalculatePeriodicEfield -> calculates Efield for periodic boundaries
void FieldSolver::calculatePeriodicEfield(int iNode, Grid& grid) {

    int Ngx = grid.numPoints(0);
    int Ngy = grid.numPoints(1);
    int Ngz = grid.numPoints(2);

    int Ndims = grid.nDim();

    vector<int> OppositeBoundaryDifference(3);

    OppositeBoundaryDifference = {Ngx - 1, Ngx * (Ngy - 1) * (Ngy > 1), Ngx * Ngy * (Ngz - 1) * (Ngz > 1)};

    // interior point index is the dot product of the InteriorPointDifference vector with the negative of the face normal vector
    int OppositeBoundaryPointIndex = iNode -
            (int)inner_product(OppositeBoundaryDifference.begin(), OppositeBoundaryDifference.end(),
                                                   grid.fieldLabels[iNode]->normal.begin(), 0.0);

    int normalDirection = 0;

    for (int iDim = 0; iDim < Ndims; iDim++)
        if (grid.fieldLabels[iNode]->normal[iDim] != 0) {
            normalDirection = iDim;
            break;
        }
    //   cout<<normalDirection<<endl;
    int iNeighborL, iNeighborR;

    Real dx_right, dx_left;

    for (int iDim = 0; iDim < Ndims; iDim++)
        for (int iDerDir = 0; iDerDir <= iDim; iDerDir++) {

            if (iDerDir == normalDirection) { //normal field requires special treatment

                if (iNode < OppositeBoundaryPointIndex) { //iNode= lower pt, opposite boundary is node with larger index

                    //right neighbor is pt with index one below the opposite boundary
                    iNeighborL = OppositeBoundaryPointIndex - 1 * (iDerDir == 0) - Ngx * (iDerDir == 1) -
                                 Ngx * Ngy * (iDerDir == 2);

                    //left neighbor is the point immediately above
                    iNeighborR = iNode + 1 * (iDerDir == 0) + Ngx * (iDerDir == 1) + Ngx * Ngy * (iDerDir == 2);

                    dx_right = grid.position(iNeighborR, iDerDir) - grid.position(iNode, iDerDir);
                    dx_left = grid.position(OppositeBoundaryPointIndex, iDerDir) -
                              grid.position(iNeighborL, iDerDir);

                } else {//iNode= upper pt, opposite boundary is node with smaller index

                    //right neighbor is pt with index one above the opposite boundary
                    iNeighborR = OppositeBoundaryPointIndex + 1 * (iDerDir == 0) + Ngx * (iDerDir == 1) +
                                 Ngx * Ngy * (iDerDir == 2);

                    //left neighbor is the point immediately below
                    iNeighborL = iNode - 1 * (iDerDir == 0) - Ngx * (iDerDir == 1) - Ngx * Ngy * (iDerDir == 2);
                    //cout<<iNode<<" "<<iNeighborL<<" "<<iNeighborR<<endl;

                    dx_right = grid.position(iNeighborR, iDerDir) -
                               grid.position(OppositeBoundaryPointIndex, iDerDir);
                    dx_left = grid.position(iNode, iDerDir) - grid.position(iNeighborL, iDerDir);

                }

                //E_normal = -(phi_right - phi_left)/(dx_right+dx_left)

                // cout<<dx_right<<" "<<dx_left<<endl;
                grid.eField(iNode, iDerDir) =
                        -(grid.potential[iNeighborR] - grid.potential[iNeighborL]) / (fabs(dx_left) + fabs(dx_right));

            } else { //tangential field is calculated regularly with central difference as before

                iNeighborL = iNode - 1 * (iDerDir == 0) - Ngx * (iDerDir == 1) - Ngx * Ngy * (iDerDir == 2);
                iNeighborR = iNode + 1 * (iDerDir == 0) + Ngx * (iDerDir == 1) + Ngx * Ngy * (iDerDir == 2);
                grid.eField(iNode, iDerDir) = FieldSolver::differentiatePotential(iNode, iNeighborL, iNeighborR,
                                                                                  iDerDir, grid);

            }


        }
}


//differentiatePotential-> differentiates the potential - central difference for interior, one sided for boundary
Real FieldSolver::differentiatePotential(int iNode,int iNeighborL,int iNeighborR,int derivativeDirection, const Grid& grid) {
//derivativeDirection-> 0- x , 1- y ,2 - z

    Real phiLeft = grid.potential[iNode];
    Real phiRight = grid.potential[iNode];
    Real posLeft = grid.position(iNode, derivativeDirection);
    Real posRight = grid.position(iNode, derivativeDirection);


    auto ijk = grid.ijk(iNode);
    if (ijk[derivativeDirection] > 0) {

        phiLeft = grid.potential[iNeighborL];
        posLeft = grid.position(iNeighborL, derivativeDirection);

    }


    if (ijk[derivativeDirection] < grid.numPoints(derivativeDirection) - 1) {

        phiRight = grid.potential[iNeighborR];
        posRight = grid.position(iNeighborR, derivativeDirection);

    }

    Real potentialDifference = (phiRight - phiLeft) / (posRight - posLeft);

    return potentialDifference;
}
