#include "MultiGridSolver.h"
#include "Label.h"

using namespace Eigen;
using namespace std;
using namespace picard;

void MultiGridSolver::prepareSolver(const Grid& grid){
    //cout<<"Coefficient calculation "<<endl;
    int Ndims=grid.nDim();
    int Ngtot=grid.totalPoints();

    int Ngx=grid.numPoints(0);
    int Ngy=grid.numPoints(1);
    int Ngz=grid.numPoints(2);

    int NgInterior=(Ngtot-2)*(Ndims==1)+(Ngtot-2*(Ngx+Ngy-2))*(Ndims==2)+(Ngtot-2*(Ngx*Ngy+Ngy*Ngz+Ngx*Ngz)+4*(Ngx+Ngy+Ngz)-8)*(Ndims==3);

    int NgBoundary=Ngtot-NgInterior;
    int Ninterface=std::max(Ngx,Ngy);

    long int Nzmax=3*Ndims*NgInterior+2*NgBoundary+10*Ninterface;
    //cout<<"Nzmax :"<<Nzmax<<endl;

    pMask.resize(Ngtot);
    value.reserve(Nzmax);
    rowPointer.resize(grid.totalPoints()+1);
    col.reserve(Nzmax);

    RHS.resize(grid.totalPoints());

    buildMGSolver(grid);
}


void MultiGridSolver::buildMGSolver(const Grid& grid) {

    rowPointer[0]=0;
    int nG=grid.totalPoints();
    for (int iNode=0;iNode<nG;iNode++)
    {
        //    cout<<"Coefficients Size (Loop): "<<coefficients.size()<<endl;
        //cout<<"Node #"<<iNode<<" Node Label: "<<(int)grid.nodeLabels[iNode]->poissonLabel<<endl;
        /*double percentageDone=(double)iNode/Ngtot*100.0;
            outputBuffer+="Percentage done calculating coefficient : "+to_string(percentageDone);
        cout<<outputBuffer<<'\r'<<flush;
        outputBuffer.clear();*/
        columnValueMap.clear();

        switch(grid.fieldLabels[iNode]->type)
        {

            case FieldLabel::INTERIOR:
                addInteriorCoefficient(iNode, grid);
                break;
            case FieldLabel::TIME_DEPENDENT_DIRICHLET:
            case FieldLabel::STEP:
            case FieldLabel::DIRICHLET:
                addDirichletCoefficient(iNode);
                break;
            case FieldLabel::NEUMANN:
                addNeumannCoefficient(iNode, grid);
                break;
            case FieldLabel::ZERO_FIELD:
                addNeumannCoefficient(iNode, grid);
                break;
            case FieldLabel::PERIODIC:
                addPeriodicCoefficient(iNode, grid);
                break;
            case FieldLabel::PLASMA_DIELECTRIC_INTERFACE:
                addInterfaceCoefficient(iNode, grid);
                break;

            default: cerr<<"Invalid Option Encountered in Poisson Boundary Conditions! \n"<<endl; break;

        }

         rowPointer[iNode+1]=(int)col.size();

         pMask[iNode]=(rowPointer[iNode+1]-rowPointer[iNode]<=2*grid.nDim()+1); //pressure mask for Schur conditioner. Treats points with stencil<2*Ndim+1 in size as "pressure", rest as flow
    }


    MGSolver::params solverParameters;

    solverParameters.solver.tol=1e-5;
    solverParameters.solver.maxiter=1000;
    solverParameters.precond.pmask=pMask;
    
/*
    ofstream row,column,val;

    row.open("row.dat");
    column.open("col.dat");
    val.open("val.dat");

    for (int i=0;i<rowPointer.size();i++)
        row<<rowPointer[i]<<endl;

    for (int i=0;i<col.size();i++){
        column<<col[i]<<endl;
        val<<value[i]<<endl;
    }

    row.close();
    column.close();
    val.close();*/

    solver = std::make_shared<MGSolver>(boost::tie(nG,rowPointer,col,value),solverParameters);

    return;

}




void MultiGridSolver::insertWithoutDuplication(int colNumber,double addValue){

    auto mapIter=columnValueMap.find(colNumber);

    if(mapIter!=columnValueMap.end()){// if duplicate, finds the position where the column number is stored, adds the given value to the existing value in that entry
        value[mapIter->second]+=addValue;
    }
    else{
        col.push_back(colNumber);
        value.push_back(addValue);

        columnValueMap.insert({colNumber,col.size()-1});
    }

}

//addInteriorCoefficient->Adds coefficients for interior grid points
void MultiGridSolver::addInteriorCoefficient(int iNode, const Grid& grid){

    double plus1=1.0; //i+1
    double minus1=1.0/(grid.position(iNode,0)-grid.position(iNode-1,0))*(grid.position(iNode+1,0)-grid.position(iNode,0));//i-1
    double icontrib=-1.0*(1.0+(grid.position(iNode+1,0)-grid.position(iNode,0))/(grid.position(iNode,0)-grid.position(iNode-1,0)));//i

// 1-D contribution added by default

    insertWithoutDuplication(iNode-1,-minus1);

    insertWithoutDuplication(iNode,-icontrib);

    insertWithoutDuplication(iNode+1,-plus1);



    /*coefficients.push_back(Trip(iNode,iNode+1,plus1));
    coefficients.push_back(Trip(iNode,iNode-1,minus1));
    coefficients.push_back(Trip(iNode,iNode,icontrib));
*/
    //  cout<<plus1<<" "<<icontrib<<" "<<minus1<<" ";
    if (grid.nDim()>1){

        int Ngx=grid.numPoints(0);
        int j=iNode;
        int jplus1=iNode+Ngx;
        int jminus1=iNode-Ngx;


//2-D addition
        plus1=1.0/(grid.position(jplus1,1)-grid.position(jminus1,1))*1.0/(grid.position(jplus1,1)-grid.position(j,1)); //j+1
        plus1*=(grid.position(iNode+1,0)-grid.position(iNode-1,0))*(grid.position(iNode+1,0)-grid.position(iNode,0));

        minus1=1.0/(grid.position(jplus1,1)-grid.position(jminus1,1))*1.0/(grid.position(j,1)-grid.position(jminus1,1)); //j-1
        minus1*=(grid.position(iNode+1,0)-grid.position(iNode-1,0))*(grid.position(iNode+1,0)-grid.position(iNode,0));


        icontrib=-1.0/(grid.position(jplus1,1)-grid.position(jminus1,1))*(grid.position(jplus1,1)-grid.position(jminus1,1))/(grid.position(j,1)-grid.position(jminus1,1))/(grid.position(jplus1,1)-grid.position(j,1));//j
        icontrib*=(grid.position(iNode+1,0)-grid.position(iNode-1,0))*(grid.position(iNode+1,0)-grid.position(iNode,0));

        insertWithoutDuplication(jplus1,-plus1);
        insertWithoutDuplication(jminus1,-minus1);
        insertWithoutDuplication(j,-icontrib);


     /*   coefficients.push_back(Trip(j,jplus1,plus1));
        coefficients.push_back(Trip(j,jminus1,minus1));
        coefficients.push_back(Trip(j,j,icontrib));
*/
        // cout<<plus1<<" "<<icontrib<<" "<<minus1<<endl;
        //  cout<<j<<" \t"<<jplus1<<"\t"<<jminus1<<endl;

    }

    if (grid.nDim()>2)
    {

        int Ngx=grid.numPoints(0);
        int Ngy=grid.numPoints(1);
        int k=iNode;
        int kplus1=iNode+Ngx*Ngy;
        int kminus1=iNode-Ngx*Ngy;


//3-D addition

        plus1=1.0/(grid.position(kplus1,2)-grid.position(kminus1,2))*1.0/(grid.position(kplus1,2)-grid.position(k,2)); //k+1
        plus1*=(grid.position(iNode+1,0)-grid.position(iNode-1,0))*(grid.position(iNode+1,0)-grid.position(iNode,0));


        minus1=1.0/(grid.position(kplus1,2)-grid.position(kminus1,2))*1.0/(grid.position(k,2)-grid.position(kminus1,2)); //k-1
        minus1*=(grid.position(iNode+1,0)-grid.position(iNode-1,0))*(grid.position(iNode+1,0)-grid.position(iNode,0));


        icontrib=-1.0/(grid.position(kplus1,2)-grid.position(kminus1,2))*(grid.position(kplus1,2)-grid.position(kminus1,2))/(grid.position(k,2)-grid.position(kminus1,2))/(grid.position(kplus1,2)-grid.position(k,2));//k
        icontrib*=(grid.position(iNode+1,0)-grid.position(iNode-1,0))*(grid.position(iNode+1,0)-grid.position(iNode,0));


        insertWithoutDuplication(kplus1,-plus1);
        insertWithoutDuplication(kminus1,-minus1);
        insertWithoutDuplication(k,-icontrib);


        /*coefficients.push_back(Trip(k,kplus1,plus1));
        coefficients.push_back(Trip(k,kminus1,minus1));
        coefficients.push_back(Trip(k,k,icontrib))*/;



    }
    //cout<<"Coefficient Size (Interior) : "<<coefficients.size()<<endl;


    return;

}//End addInteriorCoefficient



//addDirichletCoefficient->Adds Dirichlet entries
void MultiGridSolver::addDirichletCoefficient(int iNode/*, const Grid& grid*/){
   // cout<<"adding dirichlet coefficient "<<endl;
   // coefficients.push_back(Trip(iNode,iNode,1.0));
    value.push_back(1.0);
    col.push_back(iNode);
    
    return;
}//End addDirichletCoefficient

//addNeumannCoefficient->Adds coefficients to the direct matrix for Neumann BC
void MultiGridSolver::addNeumannCoefficient(int iNode, const Grid& grid) {
    // //cout<<"Coefficient Size (Neumann Start) : "<<coefficients.size()<<endl;

    int Ngx=grid.numPoints(0);
    int Ngy=grid.numPoints(1);
    int Ngz=grid.numPoints(2);

    vector <int> InteriorPointDifference(3);

    InteriorPointDifference={1,Ngx*(Ngy>1),Ngx*Ngy*(Ngz>1)}; //if 1D, Ngy=1, if 2D Ngz=1

   // coefficients.push_back(Trip(iNode,iNode,1.0));
    value.push_back(1.0);
    col.push_back(iNode);
// interior point index is the dot product of the InteriorPointDifference vector with the negative of the face normal vector

    //  //cout<<"Coefficient Size (Neumann mid) : "<<coefficients.size()<<endl;

    int InteriorPointIndex=iNode-(int)inner_product(InteriorPointDifference.begin(),InteriorPointDifference.end(),grid.fieldLabels[iNode]->normal.begin(),0.0);

    // coefficients.push_back(Trip(iNode,InteriorPointIndex,-1.0));
    value.push_back(-1.0);
    col.push_back(InteriorPointIndex);
    
    // cout<<iNode<<"\t"<<InteriorPointIndex<<endl;
    //cout<<grid.nodeLabels[iNode]->boundaryNormal[0]<<"\t"<<grid.nodeLabels[iNode]->boundaryNormal[1]<<"\t"<<grid.nodeLabels[iNode]->boundaryNormal[2]<<endl;
    //cout<<"Coefficient Size (Neumann end) : "<<coefficients.size()<<endl;

    return;

}//End addNeumannCoefficient


//addPeriodicCoefficient->adds coefficients for periodic BC to direct solver
void MultiGridSolver::addPeriodicCoefficient(int iNode, const Grid& grid) {

    int Ngx=grid.numPoints(0);
    int Ngy=grid.numPoints(1);
    int Ngz=grid.numPoints(2);


    vector <int> OppositeBoundaryDifference(3);

    OppositeBoundaryDifference={Ngx-1,Ngx*(Ngy-1)*(Ngy>1),Ngx*Ngy*(Ngz-1)*(Ngz>1)};

/*    for (int i=0;i<grid.nodeLabels[iNode]->boundaryNormal.size();i++)
        cout<<grid.nodeLabels[iNode]->boundaryNormal[i]<<" ";
    cout<<endl;*/

// interior point index is the dot product of the InteriorPointDifference vector with the negative of the face normal vector
    int OppositeBoundaryPointIndex=iNode-inner_product(OppositeBoundaryDifference.begin(),OppositeBoundaryDifference.end(),grid.fieldLabels[iNode]->normal.begin(),0);

    value.push_back(1.0);
    col.push_back(iNode);

    //   cout<<iNode<<" "<<OppositeBoundaryPointIndex<<endl;
   // coefficients.push_back(Trip(iNode,OppositeBoundaryPointIndex,-1.0));
    value.push_back(-1.0);
    col.push_back(OppositeBoundaryPointIndex);
    //cout<<"Coefficient Size (Periodic) : "<<coefficients.size()<<endl;

    return;

} //End addPeriodicCoefficient

//addInterfaceCoefficient->Adds coefficients to the direct matrix for the interface of plasma and dielectric
void MultiGridSolver::addInterfaceCoefficient(int iNode, const Grid& grid) {

    //static int interfaceExtraEquations = 0;
    int signNormalDirection;

    // cout<<"Epsr1 : "<< epsr1<<" Epsr2 : "<<epsr2<<"\n";
    int Ndims = grid.nDim();

    int normalDirection = -1;
    int pivotDirection = -1;
    vector<int> tempDisplacement(3);
    vector<int> tangentDirection;

    for (int iDim = 0; iDim < Ndims; iDim++) {
        if (grid.fieldLabels[iNode]->normal[iDim] != 0) {
            normalDirection = iDim;
            pivotDirection = (iDim + 1) % Ndims;
            signNormalDirection=-grid.fieldLabels[iNode]->normal[iDim]/abs(grid.fieldLabels[iNode]->normal[iDim]);
            /*THe sign of the formula for signNormalDirection was flipped on 16 Dec 2015 because the convention of normals for interface was changed
            from plasma-pointing to pointing away from the plasma. The sign here was flipped so that the rest of field solver doesn't need to be changed*/
            //cout<<signNormalDirection<<endl;
        }
        else
            tangentDirection.push_back(iDim);
        //cout<<normalDirection<<","<<pivotDirection<<endl;
    }
    double epsBelow=grid.fieldLabels[iNode]->properties[0];//*signNormalDirection;
    double epsAbove=grid.fieldLabels[iNode]->properties[1];//*signNormalDirection;
    /*
     * Old way of adding coefficients for interface - tangential part was combined into its own equation, which led to a rectangular A matrix
     * leading to inaccurate solutions for the equation. Now replaced with one that constructs a square matrix.
     *
     * if (Ndims > 1) {
        //Tangential component equation addition - only for 2-D and 3-D
        for (int iPivot = 0; iPivot <= 1; iPivot++) {

            int dimBefore = (Ndims - 1 + pivotDirection) % Ndims; //dim before 2=1, dim before 1=0 dim before 0 = -1 = 2
            int dimAfter = (pivotDirection + 1) % Ndims;//dim after 0 =1 ,dim after 1=2, dim after 2=3=0

            //cout<<dimBefore<<","<<dimAfter<<endl;

            tempDisplacement[pivotDirection] = iPivot;
            tempDisplacement[dimAfter] = 1 - iPivot;

            //for 2-D dimBefore= dimAfter, so the step above does not make a difference

            for (int iDimbefore = -1; iDimbefore <= 1; iDimbefore += 2) {

                tempDisplacement[dimBefore] = iDimbefore;
                int pointIndex = grid.index(iNode, tempDisplacement);
                //cout<<iNode<<","<<tempDisplacement[0]<<","<<tempDisplacement[1]<<","<<tempDisplacement[2]<<","<<pointIndex<<","<<tempDisplacement[dimBefore] * (2 * iPivot - 1)<<endl;
                // cout<<grid.nGTotal + interfaceExtraEquations<<" "<<pointIndex<<" "<<tempDisplacement[dimBefore] * (2 * iPivot - 1)<< endl;
                coefficients.push_back(Trip(grid.nGTotal + interfaceExtraEquations, pointIndex,
                                            tempDisplacement[dimBefore] * (2 * iPivot - 1)));
            }

        }
       // cout<<endl;

        interfaceExtraEquations++;

    } --- OLD-OBSOLETE*/


    // Coefficient addition - tangential direction

    /*
     * The new way of adding coefficients, replaces the (inormal+1,itangent1+1)th component of phi in the normal equation
     * we wipe out the -epsAbove term from the normal equation, and add epsAbove*( <other phi entries in the tangential equation>)
     * This gives a square matrix, which solves exactly.
     * */


    if(Ndims>1){        //Tangential component equation addition - only for 2-D and 3-D

        double epscoeff_tang=epsAbove*(signNormalDirection==1)+epsBelow*(signNormalDirection==-1);
        for (int it1=0;it1<=1;it1++) { //it1-> displacement in the first tangential direction

            int it2=((it1+1)%2); //it2->displacement in the second tangential direction. it1 and it2 are never together 1. One is 1 and the other is 0, always

            for (int iNorm = -1; iNorm <= 1; iNorm += 2) { //iNorm-> displacement in the normal direction. Is either -1 or +1.

                tempDisplacement[normalDirection] = iNorm;
                tempDisplacement[tangentDirection[0]] = it1;

                if (tangentDirection.size() >
                    1) //it2 is relevant only to 3-D. 2-D has exactly one normal and one tangential direction
                    tempDisplacement[tangentDirection[1]] = it2;

                int pointIndex = iNode + grid.index(tempDisplacement);

                if (iNorm == signNormalDirection && it1 == 1){
                    insertWithoutDuplication(pointIndex,epscoeff_tang); //To wipe out the -epsAbove entry that will come in from the (inormal+1,itangent1+1)th component in the normal equations
                  //  coefficients.push_back(Trip(iNode, pointIndex,
                    //                            epscoeff_tang)); 

                }
                
                else {
                    insertWithoutDuplication(pointIndex,epscoeff_tang * (2 * it1 - 1) * iNorm *
                                                        signNormalDirection);
                   //Adds in the terms in the normal equations to replace the (inormal+1,itangent1+1)th component in the normal equations


                   // coefficients.push_back(Trip(iNode, pointIndex, epscoeff_tang * (2 * it1 - 1) * iNorm *
                                                                 //  signNormalDirection));//Adds in the terms in the normal equations to replace the (inormal+1,itangent1+1)th component in the normal equations

                    /* cout << iNode << "," << tempDisplacement[0] << "," << tempDisplacement[1] << "," <<
                     tempDisplacement[2] << "," << pointIndex << "," << (2 * it1 - 1) * iNorm << "," << epscoeff_tang <<
                     endl;*/

                }
            }
        }


    }






    //Normal Component - for 1-D,2-D,3-D
    int pointIndex;



    // cout<<epsBelow<<" "<<epsAbove<<endl;
    //cout<<signNormalDirection<<endl;
    fill(tempDisplacement.begin(),tempDisplacement.end(),0);

    //iLevel tracks where we are in the normal direction with respect to the interface - -1-below, 0 - at, 1-above
    for (int iLevel=-1;iLevel<=1;iLevel++)
        for (int iTang=0;iTang<std::pow(2,Ndims-1);iTang++) {
//tangent direction coefficients need to be changed between 0->1
            tempDisplacement[abs(normalDirection)] = iLevel;

            int shift0=iTang%2; //tangent direction 0 shift
            int shift1=iTang/2; //tangent direction 1 shift

            if (tangentDirection.size()>0) //if 2-D, track shift 0
                tempDisplacement[tangentDirection[0]]=shift0;
            if(tangentDirection.size()>1) //if 3-D also track shift 1
                tempDisplacement[tangentDirection[1]]=shift1;

            pointIndex= iNode + grid.index(tempDisplacement); //Displaced index - no tangent displacement in 1-D, 0,1 in 2-D, 00 01 10 11 in 3-D

            double epsCoeff=-epsBelow*(iLevel==-1)-epsAbove*(iLevel==1)+(epsBelow+epsAbove)*(iLevel==0);
            // cout<<iNode<<","<<tempDisplacement[0]<<","<<tempDisplacement[1]<<","<<tempDisplacement[2]<<","<<epsCoeff<<endl;

            //epsCoeff/=(fabs(epsAbove)+fabs(epsBelow));//making matrix more diagonally dominant by dividing through by epsr1 +epsr2

            // cout<<iNode<<" "<<pointIndex<<" "<<epsCoeff<<endl;
            insertWithoutDuplication(pointIndex,epsCoeff);

           // coefficients.push_back(Trip(iNode,pointIndex,epsCoeff));


        }
    // cout<<endl;
    //cout<<"Coefficient Size (Interface) : "<<coefficients.size()<<endl;

    return;
}//End addInterfaceCoefficient


//addInterfaceRHS->Creates RHS matrix for the interface between plasma and dielectric
void MultiGridSolver::addInterfaceRHS(int iNode, const Grid& grid) {

    int Ndims = grid.nDim();

    int normalDirection = -1;
//    int pivotDirection;
    vector<int> tempDisplacement(3);
    vector<int> tangentDirection;
    int Ngx=grid.numPoints(0);
    int Ngy=grid.numPoints(1);
    int Ngz=grid.numPoints(2);

    for (int iDim = 0; iDim < Ndims; iDim++) {
        if (grid.fieldLabels[iNode]->normal[iDim] != 0)
            normalDirection = iDim;
        else
            tangentDirection.push_back(iDim);

    }

    double rho_bar=0.0;
    double sig_bar=0.0;

    for (int iTang=0;iTang<std::pow(2,Ndims-1);iTang++) {
//tangent direction coefficients need to be changed between 0->1

        int shift0=iTang%2; //tangent direction 0 shift
        int shift1=iTang/2; //tangent direction 1 shift

        if (tangentDirection.size()>0) //if 2-D, track shift 0
            tempDisplacement[tangentDirection[0]]=shift0;
        if(tangentDirection.size()>1) //if 3-D also track shift 1
            tempDisplacement[tangentDirection[1]]=shift1;

        int pointIndex= iNode + grid.index(tempDisplacement); //Displaced index - no tangent displacement in 1-D, 0,1 in 2-D, 00 01 10 11 in 3-D

        rho_bar+=grid.getChargeDensity(pointIndex);

        sig_bar+=grid.getSurfaceCharge(pointIndex);
        //cout<<iNode<<"\t ("<<tempDisplacement[0]<<","<<tempDisplacement[1]<<")\t"<< pointIndex<<"\t"<<grid.findSurfaceCharge(pointIndex)<<endl;
    }

//    double epsBelow=grid.fieldLabels[iNode]->properties[0];
//    double epsAbove=grid.fieldLabels[iNode]->properties[1];


    vector <int> InteriorPointDifference(3);

    InteriorPointDifference={1,Ngx*(Ngy>1),Ngx*Ngy*(Ngz>1)}; //if 1D, Ngy=1, if 2D Ngz=1

// interior point index is the dot product of the InteriorPointDifference vector with the negative of the face normal vector

    int InteriorPointIndex=iNode-(int)inner_product(InteriorPointDifference.begin(),InteriorPointDifference.end(),grid.fieldLabels[iNode]->normal.begin(),0.0);

    double dNormal=fabs(grid.position(InteriorPointIndex,normalDirection)-grid.position(iNode,normalDirection));




    RHS[iNode]=(rho_bar*dNormal+sig_bar)*dNormal/grid.epsilon_0;
    // RHS[iNode]/=(epsAbove+epsBelow); //for more diagonal dominance
    return;
}//end addInterfaceRHS

//addNeumannRHS- adds the entry for RHS for neumann conditions based  on - surface charge and specified normal field value
void MultiGridSolver::addNeumannRHS(int iNode, const Grid& grid){

    RHS[iNode]=grid.fieldLabels[iNode]->properties[0];
    int normalDirection=0;

    for (int iDim = 0; iDim < grid.nDim(); iDim++){
        if (grid.fieldLabels[iNode]->properties[iDim] != 0) {
            normalDirection = iDim;
            break;
        }
    }

    int Ngx=grid.numPoints(0);
    int Ngy=grid.numPoints(1);
    int Ngz=grid.numPoints(2);

    vector <int> InteriorPointDifference(3);

    InteriorPointDifference={1,Ngx*(Ngy>1),Ngx*Ngy*(Ngz>1)}; //if 1D, Ngy=1, if 2D Ngz=1

// interior point index is the dot product of the InteriorPointDifference vector with the negative of the face normal vector

    int InteriorPointIndex=iNode-(int)inner_product(InteriorPointDifference.begin(),InteriorPointDifference.end(),grid.fieldLabels[iNode]->normal.begin(),0.0);

    double dNormal=fabs(grid.position(InteriorPointIndex,normalDirection)-grid.position(iNode,normalDirection));

    if(grid.surfaceChargeMap.find(iNode) != grid.surfaceChargeMap.end())
        RHS[iNode]+=(grid.getChargeDensity(iNode)*dNormal + grid.getSurfaceCharge(iNode));

    RHS[iNode]*=dNormal/grid.epsilon_0;

    return;

}//end addNeumannRHS

//buildRHS->builds RHS for direct solve

//In order to keep diagonal entries in the A matrix positive - multiply interior equation by -1.
void MultiGridSolver::buildRHS(Real time, Real dt, const Grid& grid){
    int timeIndex=std::max(0,(int)round(time/dt));

    for (int iNode=0;iNode<grid.totalPoints();iNode++)
    {
        auto lb = grid.fieldLabels[iNode];
        switch(lb->type)
        {
            case FieldLabel::INTERIOR:
                RHS[iNode]=(-1.0*(-grid.getChargeDensity(iNode)/grid.fieldLabels[iNode]->properties[0]
                                                        *0.5*(grid.position(iNode+1,0)-grid.position(iNode-1,0))*(grid.position(iNode+1,0)-grid.position(iNode,0))));
                break;

//field properties is size 1 for simple dirichlet, it is a vector of size Nsteps+1 for time dependent Dirichlet.
            case FieldLabel::DIRICHLET:
                RHS[iNode]=grid.fieldLabels[iNode]->properties[0];
                break;

            case FieldLabel::TIME_DEPENDENT_DIRICHLET:
                RHS[iNode]=grid.fieldLabels[iNode]->properties[timeIndex];
                break;

            case FieldLabel::STEP: {
                auto baseV = lb->properties[0];
                auto stepV = lb->properties[1];
                auto startTime = lb->properties[2];
                auto endTime = lb->properties[3];

                RHS[iNode] = (fgeq(time, startTime) && fleq(time, endTime)) ? stepV : baseV;

                break;
            }

            case FieldLabel::NEUMANN:
                MultiGridSolver::addNeumannRHS(iNode, grid);
                break;

            case FieldLabel::ZERO_FIELD: RHS[iNode]=0.0;
                break;

            case FieldLabel::PERIODIC:RHS[iNode]=0.0;
                break;

            case FieldLabel::PLASMA_DIELECTRIC_INTERFACE:
                MultiGridSolver::addInterfaceRHS(iNode, grid);
                break;

            default:
                cerr<<"Invalid Option in Poisson Boundary Conditions! \n"<<endl;
                break;

        }

        // cout<<RHS[iNode]<<endl;

    }


    return;

}//End buildRHS

void MultiGridSolver::findPotential(Real time, Real dt, Grid& grid) {

    buildRHS(time, dt, grid);


    int iters, error;

    boost::tie(iters, error) = (*solver)(RHS, grid.potential);

//    cout << "Iterations : " << iters << endl;
//    cout << "Error : " << error << endl;
    return;

}
