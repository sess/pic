#include "LeapFrog.h"
#include <numeric>

using namespace std;
using namespace picard;

void LeapFrog::step(Particle &p, Real dt, Real t, int nDim, bool useBField) {
    p.r_last = p.r;

    if (t == 0) //TODO see if we can take this out of here
        initialize(p, dt, nDim, useBField);

    updateVelocity(p, dt, nDim, useBField);
    updatePosition(p, dt, nDim);
}

void LeapFrog::timeCenter(Particle& p, Real dt, int nDim, bool useBField) {
    updateVelocity(p, -0.5 * dt, nDim, useBField);
}

void LeapFrog::updateVelocity(Particle& p, Real dt, int nDim, bool useBField) {
    if (useBField)
        applyEMField(p, dt, nDim);
    else
        applyEField(p, dt, nDim);
}

void LeapFrog::updatePosition(Particle& p, Real dt, int nDim) {
    for (int n = 0; n < nDim; n++) {
        p.r[n] = p.r[n] + p.v[n] * dt;
    }
}

void LeapFrog::applyEField(Particle& p, Real dt, int nDim) {
    for (int n = 0; n < nDim; n++) {
        p.v[n] = p.v[n] + (eField[n] * p.q() + force[n]) * dt / p.m();
    }
}

void LeapFrog::applyEMField(Particle& p, Real dt, int nDim) {
    auto omegaC = p.q() * norm(bField) / p.m();
    applyEField(p, 0.5 * dt, nDim); // Do first half acceleration
    rotation(p, dt, omegaC); // Apply rotation
    applyEField(p, 0.5 * dt, nDim); // Do last half acceleration
}

void LeapFrog::rotation(Particle &p, Real dt, Real omegaC) {
    Real theta = omegaC * dt;
    auto k = bField / norm(bField);
    auto v = resize(p.v, 3);
    auto crossProdResult = cross3d(k, v);
    auto term3scale = dot(k, v) * (1.0 - cos(theta));

    auto newV = p.v * cos(theta);
    for (int i = 0; i < newV.size(); i++) {
        newV[i] += crossProdResult[i] * sin(theta) + k[i] * term3scale;
    }
    p.v = newV;
}
