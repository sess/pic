#ifndef SESS_PIC_PARTICLEMOVER_H
#define SESS_PIC_PARTICLEMOVER_H

#include "Definitions.h"
#include "Particle.h"
#include <string>

namespace picard {

class ParticleMover {
public:
    //! Default constructor for a particle mover which inializes all fields to 0
    ParticleMover() : eField(3,0), bField(3,0), force(3,0){}
    virtual ~ParticleMover() = default; //!< Default virtual destructor

    /**
     * Step a particle forward in time by the timestep specified.
     * @param p The particle to be stepped forward
     * @param dt The timestep to take (can be negative)
     * @param t The current simulation time (used for time centering)
     * @param nDim The number of spatial dimensions
     * @param useBField Whether or not to use the magnetic field
     */
    virtual void step(Particle& p, Real dt, Real t, int nDim, bool useBField) = 0;

    /**
     * Initialization function for a new particle. By default this does nothing
     * @param p The particle to have position and velocity initialized
     * @param dt The current simulation timestep
     * @param nDim The number of spatial dimensions
     * @param useBField whether or not the magnetic field should be applied
     */
    virtual void initialize(Particle& p, Real dt, int nDim, bool useBField) = 0;
    
    /**
     * Reset the E and B field back to zero
     */
    void clearFields(){
        eField.assign(3, 0);
        bField.assign(3, 0);
        force.assign(3, 0);
    };
    
    vec EField(){ return eField; } //!< Get the current E-Field in 3 dimensions
    vec BField(){ return bField; } //!< Get the current B-Field in 3 dimensions
    vec Force(){ return force; } //!< Get the current Force in 3 Dimensinos
    
    vec EField(size_t nDim){ return vec(eField.begin(), eField.begin() + nDim);} //! Get the current E-Field for the specified number of dimensions
    vec BField(size_t nDim){ return vec(bField.begin(), bField.begin() + nDim);} //! Get the current B-Field for the specified number of dimensions
    vec Force(size_t nDim){ return vec(force.begin(), force.begin() + nDim);} //! Get the current Force for the specified number of dimensions

    void setForce(const vec& f){
        force = f;
        force.resize(3, 0);
    }

    void addForce(vec f){
        f.resize(3, 0);
        force = force + f;
    }

    /**
     * Set the E-Field to a particular vector. 
     * The vector can be any dimension but the resulting eField that is stored will be in 3D with any added dimensions set to 0
     * @param e The new electric field to set
     */
    void setEField(const vec& e){
        eField = e;
        eField.resize(3, 0);
    }
    
    /**
     * Add a vector contribution to the E-Field;
     * The vector can be any dimension but the resulting eField that is added will be in 3D with any added dimensions set to 0
     * @param e The new electric field to add
     */
    void addEField(vec e){
        e.resize(3,0);
        eField = eField + e;
    }
    
    /**
     * Set the B-Field to a particular vector.
     * The vector can be any dimension but the resulting bField that is stored will be in 3D with any added dimensions set to 0
     * @param b The new magnetic field to set
     */
    void setBField(const vec& b){
        bField = b;
        bField.resize(3, 0);
    }
    
    /**
     * Add a vector contribution to the B-Field;
     * The vector can be any dimension but the resulting bField that is added will be in 3D with any added dimensions set to 0
     * @param e The new magnetic field to add
     */
    void addBField(vec b){
        b.resize(3,0);
        bField = bField + b;
    }
    
protected:
    std::vector<Real> eField; //!< The electric field felt by the current particle being processed
    std::vector<Real> force; //!< Other force acting on the particle
    std::vector<Real> bField; //!< The magnetic field felt by the current particle being processed
};

}

#endif
