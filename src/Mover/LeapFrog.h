#ifndef SESS_PIC_LEAPFROG_H
#define SESS_PIC_LEAPFROG_H

#include "Definitions.h"

#include "ParticleMover.h"
#include <vector>

namespace picard {

class Simulation;

class LeapFrog : public ParticleMover {
public:
    /**
     * Step a particle forward in time by the timestep specified using the leap frog algorithm.
     * Uses the the electric field, magnetic field and current velocity to integrate position forward in time
     * @param p The particle to be stepped forward
     * @param dt The timestep to take (can be negative)
     * @param t The current simulation time (used for time centering)
     * @param nDim The number of spatial dimensions
     * @param useBField Whether or not to use the magnetic field
     */
    void step(Particle& p, Real dt, Real t, int nDim, bool useBField) override;

    /**
     * Intialization for a particle when using the LeapFrog algorithm. initialize calls timeCenter to accurately
     * update the particle velocity
     * @param p The particle to have position and velocity initialized
     * @param dt The current simulation timestep
     * @param nDim The number of spatial dimensions
     * @param useBField whether or not the magnetic field should be applied
     */
    virtual void initialize(Particle& p, Real dt, int nDim, bool useBField) override { timeCenter(p, dt, nDim, useBField); };

    /**
     * Moves the velocity back a half timestep based on the current force
     * This is used at the beginning of simulation when (x,y) and (vx,vy) are specified at i=0.
     * The leapfrog method requires (vx,vy) to be specified at i=-1/2.
     * @param p The particle to be time-centered
     * @param dt The timestep to step the particle by
     * @param nDim The number of spatial dimensions
     * @param useBField whether or not to use the magnetic field in the update
     */
    void timeCenter(Particle& p, Real dt, int nDim, bool useBField);

private:
    void updateVelocity(Particle& p, Real dt, int nDim, bool useBField); //!< Update a particle velocity based on the current E and B fields
    void updatePosition(Particle& p, Real dt, int nDim); //!< Update a particle position based on the current velocity
    void applyEField(Particle& p, Real dt, int nDim); //!< Applies an electric force contribution to the particle's velocity
    void applyEMField(Particle& p, Real dt, int nDim); //!< Applies both an electric force and magnetic force contribution to the particle's velocity
    void rotation(Particle& p, Real dt, Real omegaC); //!< Rotates the particle's velocity vector based on the magnetic field
};
}


#endif //SESS_PIC_LEAPFROG_H
