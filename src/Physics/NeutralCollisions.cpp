#include "NeutralCollisions.h"
#include "Simulation.h"
#include "Interpolator.h"

using namespace picard;
using namespace std;

vec NeutralCollisionBase::scatter(const vec& vinc, Real chi, int nDim) {
    switch(nDim){
        case 1 : return scatter1D(vinc, chi);
        case 2 : return scatter2D(vinc, ((uniform() < 0.5) ? -1.0 : 1.0)*chi);
        case 3 : return scatter3D(vinc, chi, 2.0*pi*uniform());
        default: throw std::runtime_error("NeutralCollisions (scatter) Error: invalid dimension");
    }
}

vec NeutralCollisionBase::scatter1D(const vec& vinc, Real chi) {
    throw runtime_error("can't currently scatter in 1D");
}

vec NeutralCollisionBase::scatter2D(const vec& vinc, Real chi) {
    Real cosChi = cos(chi);
    Real sinChi = sin(chi);
    Real xscat = vinc[0]*cosChi - vinc[1]*sinChi;
    Real yscat = vinc[0]*sinChi + vinc[1]*cosChi;
    return {xscat, yscat};
}

vec NeutralCollisionBase::scatter3D(const vec& vinc, Real chi, Real phi) {
    vec i = {1,0,0};
    Real theta = acos(dot(vinc, i));
    Real a1 = sin(chi)*sin(phi)/sin(theta);
    Real a2 = sin(chi)*cos(phi)/sin(theta);
    return vinc*cos(chi) + a1*cross3d(vinc, i) + a2*cross3d(vinc, cross3d(i,vinc));
}

std::vector<Real> NeutralCollisionBase::cross_sections(Real E) {
    std::vector<Real> sigmas;
    int index = 0;
    for(auto& entry : sigma_samples)
        sigmas.push_back(expInterpolate(E, entry, coefficients[index++]));

    return sigmas;
}

int NeutralCollisionBase::choose_interaction(Real E_sim, Real n_phys, Real v_sim, Simulation& sim, vec r){
    auto& scaling = sim.referenceParams();
    
    auto E_phys = E_sim*scaling.energyScaleEV();
    auto sigmas_phys = cross_sections(E_phys);
    auto sigma_total_phys = sum(sigmas_phys);
    auto nu_sim = sigma_total_phys*n_phys*v_sim*scaling.lengthScale();

    // Interpolate the nu_sim value to the grid
    auto cell = sim.grid().getEnclosingCell(r);
    for (auto i : cell) {
        auto w = sim.interpolator().W(sim.grid().position(i), r, sim.grid().spacing());
        collisionFrequency[i] += w*nu_sim;
    }

    auto P_col = 1.0 - exp(-nu_sim*sim.dt());

    if(uniform() > P_col)
        return -1; // no collision happened

    // A collision happened so choose the appropriate cross section
    auto R = uniform();
    Real sigma_acc = 0;
    int index = 0;
    for(auto& s : sigmas_phys){
        sigma_acc += s;
        if(R < sigma_acc / sigma_total_phys)
            return index;
        index++;
    }
    throw runtime_error("Neutral Collisions Error: Couldn't select a cross section");
};

InteractionResults NeutralCollisionBase::apply(Particle& p, Simulation& simulation) {
    if(p.species_ptr() != charged_species)
        return PARTICLE_UNCHANGED;

    // Sample from the neutrals and convert into neutral frame
    auto neutral = neutrals->generate(p.r);
    p.v = p.v - neutral.v;

    auto n_phys = simulation.interpolator().scalarFieldValue(p.r, neutrals->grid_density, simulation.grid());
    auto v_sim = p.speed();
    auto E_sim = p.kineticEnergy();
    auto collision_type = choose_interaction(E_sim, n_phys, v_sim, simulation,p.r);
    if(collision_type >= 0)
        manage_collision(collision_type, p, simulation);

    // convert back to lab frame
    p.v = p.v + neutral.v;

    return PARTICLE_UPDATED;
}

void NeutralCollisionBase::write(std::string path) {
    // reduce the collision frequencies
    if(MPI_init_called()) {
        // Reduce the charge density on the grid
        check_MPI_call(
                MPI_Allreduce(MPI_IN_PLACE, collisionFrequency.data(), collisionFrequency.size(),
                              PIC_MPI_REAL, MPI_SUM, MPI_COMM_WORLD),
                "Collision Frequency Reduction"
        );
        collisionFrequency = collisionFrequency / (Real)numThreads();
    }

    if(isRoot())
        write1DVector(collisionFrequency, path, stackVectorDefault(), " ");
}

void NeutralCollisionBase::setup(Simulation& simulation) {
    collisionFrequency.assign(simulation.grid().totalPoints(), 0);
}

void NeutralCollisionBase::resetTimestep() {
    collisionFrequency.assign(collisionFrequency.size(), 0);
}

InteractionResults SimpleElectronNeutralCollisions::apply(Particle& p, Simulation& simulation) {
    if(p.species_ptr() != charged_species)
        return PARTICLE_UNCHANGED;

    auto P_col = 1.0 - exp(-nu*simulation.dt());

    if(uniform() > P_col)
        return PARTICLE_UNCHANGED; // no collision happened

    p.v  = scatter(p.v, uniform(0., pi), simulation.grid().nDim());
    return PARTICLE_UPDATED;
}


Real ElectronNeutralCollisions::scatteringAngle(Real E_in_eV) {
    if(feq(E_in_eV,0))
        return 0;
    return std::acos((2.0 + E_in_eV - 2.0*std::pow(E_in_eV + 1.0, uniform())) / E_in_eV);
}

void ElectronNeutralCollisions::manage_collision(int type, Particle& p, Simulation& sim) {
    auto& scaling = sim.referenceParams();

    switch (type){
        case 0 : { // elastic collision
            auto chi = scatteringAngle(p.kineticEnergy() * scaling.energyScaleEV());
            auto dE = 2.0 * p.m() * (1.0 - std::cos(chi)) / (neutrals->m);
            auto vscat = (1.0-dE)*p.speed();
            auto vdir = scatter(normalize(p.v), chi, sim.grid().nDim());
            p.v = vscat * vdir;
            break;
        }
        default : throw runtime_error("(ElectronNeutralCollisions) Invalid collision type: " + to_string(type));
    }
}

void IonNeutralCollisions::manage_collision(int type, Particle& p, Simulation& sim) {
    switch(type){
        case 0 : { // elastic collision
            auto chi = acos(std::sqrt(1.0 - uniform()));
            auto vscat = p.speed() * cos(chi);
            auto vdir = scatter(normalize(p.v), chi, sim.grid().nDim());
            p.v = vscat * vdir;
            break;
        } case 1 : { // charge exchange
            p.v = p.v * 0;
            break;
        } default : throw runtime_error("(IonNeutralCollisions) Invalid collision type: " + to_string(type));
    }
}
