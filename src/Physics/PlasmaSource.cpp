//#include <fstream>
//#include <tgmath.h>
//#include <boost/math/tools/roots.hpp>
//#include <boost/algorithm/string.hpp>
//#include "PlasmaSource.h"
//#include "Simulation.h"
//#include "Particle.h"
//#include "Grid.h"
//#include "ParticleMover.h"
//#include "Interpolator.h"
//
//
//using namespace std;
//using namespace picard;
//
//PlasmaSource::PlasmaSource(vector<string> options, Simulation *simInput) {
//    // Get the required number of options
//    cout << "Inside PlasmaSource()" << endl;
//    sim = simInput;
//    cout << "Required Options: " << requiredOptions << endl;
//    cout << "Options Size: " << options.size() -1 << endl;
//    if(options.size() - 1 != requiredOptions){
//        cerr << "Preprocessor Error: Expected " << requiredOptions << " options for " << options[0] << " PlasmaSource." << endl;
//    }
//    else {
//        if (sim->mpiRank==0){
//            cout << "Setting up the Plasma Source." << endl;
//        }
//        sim = simInput;
////        cout << "Options Values:" << endl;
////        for (int i=0; i<options.size(); i++){
////            cout << "    " << options[i] << endl;
////        }
//        // Parse the species index
//        speciesIndex = stof(options[1]);
//        if (speciesIndex>(sim->nSpecies-1) or speciesIndex<0){
//            cerr << "PlasmaSource species index is not valid!  It must be between 0 and 1-number of particle types" << endl;
//            return;
//        }
//        // Parse the center
//        vector<string> centerComponents;
//        boost::trim_if(options[2], boost::is_any_of("()"));
//        boost::split(centerComponents, options[2], boost::is_any_of(";"), boost::token_compress_on);
//        // Make sure the center vector is the correct dimensions
//        if (centerComponents.size() != sim->nDim){
//            cerr << "Error parsing PlasmaSource center components." << endl;
//            cerr << "numComponents: " << centerComponents.size() << endl;
//            cerr << "Couldn't parse: \'" << options[2] << "\' into " << sim->nDim << " components." << endl;
//        }
//
//        // Put the rest of the source data into the source object
//        radius = stof(options[3]);
//        velT = stof(options[4]);
//
//        // Parse the velocity means
//        vector<string> velMeanComponents;
//        boost::trim_if(options[5], boost::is_any_of("()"));
//        boost::split(velMeanComponents, options[5], boost::is_any_of(";"), boost::token_compress_on);
//        // Make sure the center vector is the correct dimensions
//        if (velMeanComponents.size() != sim->nDim){
//            cerr << "Error parsing PlasmaSource velocity mean components." << endl;
//            cerr << "numComponents: " << velMeanComponents.size() << endl;
//            cerr << "Couldn't parse: \'" << options[6] << "\' into " << sim->nDim << " components." << endl;
//        }
//
//        // Parse the activity file
//        string fileName = options[6];
//        // Parse the filename
//        ifstream sourceFile;
//        sourceFile.open(fileName);
//        if (!sourceFile.good()){
//            cerr << "Error opening source activity file for plasma source." << endl;
//        }
//        else {
//            std::string line;
//            for (int i = 0; i < round(sim->T / sim->dt) + 1; i++) {
//                // Go through each line of the file
//                Real dummyActivity;
//                getline(sourceFile, line);
//                istringstream iss(line);
//                iss >> dummyActivity;
//                activity.push_back(dummyActivity);
//            }
//        }
//        // Get the mass and charge from the species index
//        mass = sim->particleTypes[speciesIndex].m;
//        charge = sim->particleTypes[speciesIndex].q;
//
//        // Push all vector quantities into appropriate class variables
//        for (int i=0; i<sim->nDim; i++){
//            center.push_back(stof(centerComponents[i]));
//            velMean.push_back(stof(velMeanComponents[i]));
//            // velT.push_back(stof(velTempComponents[i])); (velT used to be a vector, we had to parse it accordingly)
//            // Create e vector
//            e.push_back(0.0);
//        }
//        // Create b vector if needed
//        if (sim->grid->hasBField()){
//            for (int i=0; i<3; i++){
//                b.push_back(0.0);
//            }
//        }
//
//        setupComplete = true;
//        if (sim->mpiRank == 0){
//            cout << "Plasma Source Setup complete.  Mass: " << mass << ", Charge: " << charge << endl;
//            cout << "Radius: " << radius << ", " << " Center: " << center << endl;
//        }
//    }
//}
//
//Real PlasmaSource::BoltzmannCDF(Real v, Real R) {
//    return erf(v) - 2*v/sqrt(M_PI)*exp(-v*v) - R;
//}
//
//struct BisectTerminationCondition{
//    bool operator() (Real min, Real max) {
//        return abs(min-max) <= 0.000001;
//    }
//};
//
//Real PlasmaSource::getVelMag() {
//    // Sample from a Boltzmann distribution
//    Real R = sampleUniformDist(0.0,1.0);
//    pair<Real, Real> result;
//    result = boost::math::tools::bisect(boost::bind(&PlasmaSource::BoltzmannCDF, this, _1, R),
//                                        0.0, 50.0, BisectTerminationCondition());
//    return (result.first + result.second) / 2* velT;
//}
//
//
//Real PlasmaSource::sampleNormalDist(Real mean, Real std){
//    std::normal_distribution<Real> d(mean, std);
//    return d(generator);
//}
//
//
//Real PlasmaSource::sampleUniformDist(Real a, Real b){
//    std::uniform_real_distribution<Real> d(a, b);
//    return d(generator);
//}
//
//vector<Real> PlasmaSource::getUnitVector() {
//    std::uniform_real_distribution<Real> d(-1.0, 1.0);
//    vector<Real> point;
//    if (sim->nDim == 1){
//        point.push_back(d(generator));
//    }
//    else if (sim->nDim == 2){
//        Real u = d(generator);
//        Real v = d(generator);
//        while (u*u+v*v >= 1){
//            u = d(generator);
//            v = d(generator);
//        }
//        point.push_back((u*u-v*v)/(u*u+v*v));
//        point.push_back((2*u*v)/(u*u+v*v));
//    }
//    else if (sim->nDim == 3){
//        Real u = d(generator);
//        Real v = d(generator);
//        // use Marsaglia 1972 method to pick points on sphere surface
//        while (u*u+v*v >= 1){
//            u = d(generator);
//            v = d(generator);
//        }
//        point.push_back(2*u*sqrtf(1-u*u-v*v));
//        point.push_back(2*v*sqrtf(1-u*u*v*v));
//        point.push_back((1-2*(u*u+v*v)));
//    }
//    else{
//        cerr << "Incorrect value of sim->nDim found in PlasmaSource::PositionDist" << endl;
//    }
//    return point;
//}
//
//Particle PlasmaSource::makeNewParticle(std::list<Particle>& particleList){
//    // Set the particle's position and velocity
//    vector<Real> unitVector = getUnitVector();
//    Real velMag = getVelMag();
//    vec r(sim->grid->nDim());
//    vec v(sim->grid->nDim());
//    for (int i = 0; i < sim->nDim; i++) {
//        r[i] = unitVector[i] * radius + center[i];
//        v[i] = unitVector[i] * velMag + velMean[i];
//    }
//    return Particle(mass, charge, r, v);
//}
//
//int PlasmaSource::getNumParticlesToInject(){
//    // TODO make function to determine the number of particles to inject!
//    // Find the time step index to use
//    int timeIndex = round(sim->t/sim->dt);
//
//    Real numParticlesFloat = activity[timeIndex];
//
//    Real meanLoad=(int)numParticlesFloat/(double)sim->mpiSize;
//    int numParticlesIntegerPart=floor(meanLoad);
//
//    // cout<<"Mean Load : "<<meanLoad<<"  rank : "<<sim->mpiRank<<endl;
//    if(sim->mpiRank==sim->mpiSize-1){
//        numToInjectFloat=numParticlesIntegerPart+(numParticlesFloat-(int)numParticlesFloat);
//        //cout<<"Rank : "<<sim->mpiRank<<", Num to inject float "<<numToInjectFloat<<endl;
//    }
//    if (sim->mpiRank<sim->mpiSize-1){
//        numToInjectFloat=(Real)numParticlesIntegerPart;
//        //  cout<<"Rank : "<<sim->mpiRank<<" ,Num to inject float "<<numToInjectFloat<<endl;
//    }
//
//    int numPartFloatMod=(int)numParticlesFloat%sim->mpiSize;
//
//    if(sim->mpiRank<numPartFloatMod)
//        numToInjectFloat+=1.0;
//
//    // Take random number to see if we want to injust an extra particle
//    // sample uniform distribution to see if we need to add an extra particle
//    int numParticles;
//
//    if(sim->mpiRank==sim->mpiSize-1){ //only last rank decides injection number probabilistically
//        Real randomVal = sampleUniformDist(0, 1);
//        if (randomVal + floor(numToInjectFloat) < numToInjectFloat)
//            numParticles = ceil(numToInjectFloat);
//        else
//            numParticles = floor(numToInjectFloat);
//    }
//    else{
//        numParticles = (int) numToInjectFloat;
//    }
//    return numParticles;
//}
//
//bool PlasmaSource::apply(vector<std::list<Particle>>& masterParticleList) {
//    // seed the random number generator
//    auto seed = std::chrono::system_clock::now().time_since_epoch().count();
//    generator.seed(seed);
//
//    // make the number of desired particles if the condition is met
//    if (true) {
//        // add particle(s)!
//        int numParticles = getNumParticlesToInject();
//
////        if (sim->mpiRank == 0){
////            cout << "Source Active! adding " << numParticles << " particle(s)!" << endl;
////        }
//        // make a new seed
//        seed = std::chrono::system_clock::now().time_since_epoch().count();
//        generator.seed(seed);
//
//        int attempts;
//        bool particleLoaded;
//
//        for (int iParticle = 0; iParticle < numParticles; iParticle++) {
//            // reset attempts
//            attempts = 0;
//            particleLoaded = false;
//            Particle newParticle(0,0,0,0);
//
//            while (attempts<5) {
//                // make a particle
//                newParticle = makeNewParticle(masterParticleList[speciesIndex]);
//
//                // Interpolate the electric and magnetic fields at the particle location
//                sim->interpolator->gridToParticle(newParticle,*sim->grid, *sim->particleMover, sim->grid->hasBField()); // this updates the e/bfields in particle mover
//
//                // get the e and b used in Boris Push
//                e = charge * sim->particleMover->eField / mass;
//                if (sim->grid->hasBField()) {
//                    b = sim->particleMover->bField / norm(sim->particleMover->bField);
//                }
//
//                // Do a Simple Boris Push with Fractional Time Steps (Cartwright, Verboncoeur, Birdsall 2000)
//                // Get the fractional time step
//                Real f = sampleUniformDist(0.0, 1.0);
//                // Time center the particle
//                sim->particleMover->timeCenter(newParticle, -f * sim->dt, sim->nDim, sim->grid->hasBField());
//                newParticle.r = newParticle.r + (f * sim->dt) * newParticle.v;
//                sim->particleMover->timeCenter(newParticle, (1 - f) * sim->dt, sim->nDim, sim->grid->hasBField());
//
//                // add the particle to the main list if it is allowed in the domain
//                if(sim->grid->isPermitted(newParticle.r)){
//                    masterParticleList[speciesIndex].push_front(newParticle);
//                    break;
//                }
//                attempts++;
//            }
//
//            if (!particleLoaded){
//                cerr << "5 attempts failed to load particle in plasma source" << endl;
//                newParticle = makeNewParticle(masterParticleList[speciesIndex]);
//                sim->particleMover->timeCenter(newParticle, sampleUniformDist(0.0,1.0)*sim->dt, sim->nDim, sim->bFieldFlag);
//                masterParticleList[speciesIndex].push_front(newParticle);
//            }
//            // add the particle to the main list
//            masterParticleList[speciesIndex].push_back(newParticle);
//            sim->interpolator->particleToGrid(newParticle,speciesIndex,*sim->grid);
//        }
//    }
//    return true;
//}
