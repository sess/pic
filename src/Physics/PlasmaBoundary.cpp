#include "PlasmaBoundary.h"
#include "Simulation.h"
#include "Grid.h"
#include "LeapFrog.h"
#include "Interpolator.h"

using namespace picard;
using namespace std;

PlasmaBoundary::PlasmaBoundary(shared_ptr<ParticleBoundaryLabel> label, shared_ptr<Species> species) :
        label(label), species(species){
    normal_dim = nonZeroDim(label->normal);
    // Make sure the velocity distribution is maxwellian in the right dimension
    to_maxwellian(species->velocity_distribution[normal_dim]);
}

void PlasmaBoundary::apply(Simulation& sim){
    int element_index = 0;
    auto& particles = sim.particles().at(species);
    auto maxwellian = to_maxwellian(species->velocity_distribution[normal_dim]);
    auto density = species->reference->simDensity();
    auto direction = -label->normal[normal_dim];

    for(auto& entry : sim.grid().particleBoundaryLabels) {
        if (element_index++ % numThreads() != rank() || entry.second != label)
            continue;

        // Create a domain from the element
        auto d = sim.grid().makeDomain(entry.first);

        // Compute the number of particles to be injected through this element (# particles/area = v*dt*n)
        double frac_particles = density*maxwellian->u_bar(direction) * sim.dt() * d.area();

        double numParticles;
        if (uniform() < modf(frac_particles, &numParticles)) // Correct for decimal using a random number
            numParticles++;

        for (int i = 0; i < numParticles; i++) {
            // Generate a new particle sampled from the velocity distribution
            auto p = species->generate(d.randomPositionInside());
            // The velocity component in the direction normal to the boundary should be sampled from the flux function
            p.v[normal_dim] = maxwellian->flux_sample(direction);

            // Interpolate current field to particle location
            sim.interpolator().gridToParticle(p, sim.grid(), sim.mover(), sim.grid().hasBField());

            // Random number between 0 and 1 that specifies when (during the previous timestep) the particle crossed the boundary
            auto r = uniform();

            // Timecenter to get the current velocity
            sim.mover().initialize(p, r * sim.dt(), sim.grid().nDim(), sim.grid().hasBField());

            // move the particle by the amount that it has travelled since crossing the boundary
            sim.mover().step(p, r * sim.dt(), sim.t(), sim.grid().nDim(), sim.grid().hasBField());

            // Apply the boundary condition and Add the particles to the beginning of the master particle list
            if (sim.applyBoundaryConditions(p) != REMOVE_PARTICLE && sim.grid().isPermitted(p.r))
                particles.push_front(p);
        }
    }
}
