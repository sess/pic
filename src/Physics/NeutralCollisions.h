#ifndef NEUTRAL_COLLISIONS_HPP
#define NEUTRAL_COLLISIONS_HPP

#include "Physics.h"

namespace picard{

//!< Base class for neutral collisions
class NeutralCollisionBase : public LocalPhysics {
public:
    NeutralCollisionBase(SpeciesPtr charged_species, SpeciesPtr neutral_species,
                         const std::vector<std::map<Real,Real>>& sigma_samples)
    : charged_species(charged_species), neutrals(neutral_species), sigma_samples(sigma_samples) {
        for(auto& entry : sigma_samples)
            coefficients.push_back(getExpCoefficients(entry));
    }

    vec scatter(const vec& vinc, Real chi, int nDim);
    vec scatter1D(const vec& vinc, Real chi);
    vec scatter2D(const vec& vinc, Real chi);
    vec scatter3D(const vec& vinc, Real chi, Real phi);

    std::vector<Real> cross_sections(Real E);
    int choose_interaction(Real E_sim, Real n_phys, Real v_sim, Simulation& sim, vec r);
    virtual void manage_collision(int type, Particle& p, Simulation& sim) = 0;
    virtual InteractionResults apply(Particle& p, Simulation& simulation) override;

    SpeciesPtr get_charged_species(){ return charged_species; }
    SpeciesPtr get_neutrals(){ return neutrals; }
    const std::vector<std::map<Real,Real>>& get_sigma_samples(){ return sigma_samples; };
    const std::vector<std::vector<ExpCoeff>>& get_coefficients(){ return coefficients; };
    void write(std::string path) override;
    virtual void setup(Simulation& simulation) override;
    virtual void resetTimestep() override;

protected:
    SpeciesPtr charged_species; //!< The particle species which are interacting with the neutral population
    SpeciesPtr neutrals; //!< The neutral species on the grid

    Real null_collision_probability{1}; //!< The probability value used to reduce the number of particles to be tested for collisions (dynamically updated)
    std::vector<std::map<Real,Real>> sigma_samples; //!< Sample points from the distribution (one for each type of interaction)
    std::vector<std::vector<ExpCoeff>> coefficients; //!< coefficients for exponential interpolation (one for each cross section)
    std::vector<Real> collisionFrequency;//!< Collision frequency on the grid
};

class ElectronNeutralCollisions : public NeutralCollisionBase {
public:
    ElectronNeutralCollisions(SpeciesPtr electrons, SpeciesPtr neutrals,
                              const std::vector<std::map<Real, Real>>& sigmas)
            : NeutralCollisionBase(electrons, neutrals, sigmas) {}

    void manage_collision(int type, Particle& p, Simulation& simulation) override;
    virtual std::string name() override {return "Electron Neutral Collision Frequency";}
    Real scatteringAngle(Real E_in_eV); //!< Scattering angle of the electron (in eV)
};

//! This version has a constant collision frequency and uniform scattering angle
class SimpleElectronNeutralCollisions : public NeutralCollisionBase{
public:
    SimpleElectronNeutralCollisions(SpeciesPtr e, Real nu) : nu(nu), NeutralCollisionBase(e, nullptr, {}){}

    InteractionResults apply(Particle& p, Simulation& simulation) override;
    void manage_collision(int type, Particle& p, Simulation& sim) override{}
    virtual std::string name() override {return "Simple Electron Neutral Collision Frequency";}
    void write(std::string path) override{};
    Real nu; //! Collision frequency
};

class IonNeutralCollisions : public NeutralCollisionBase{
public:
    IonNeutralCollisions(SpeciesPtr ions, SpeciesPtr neutrals, const std::vector<std::map<Real,Real>>& sigma_samples)
            : NeutralCollisionBase(ions, neutrals, sigma_samples){}

    void manage_collision(int type, Particle& p, Simulation& simulation) override;
    virtual std::string name() override {return "Ion Neutral Collision Frequency";}
};


}



#endif