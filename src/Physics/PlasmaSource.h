//#ifndef SESS_PIC_PLASMASOURCE_H
//#define SESS_PIC_PLASMASOURCE_H
//
//#include "Definitions.h"
//#include "Physics.h"
//#include <chrono>
//#include <boost/bind.hpp>
//
//namespace picard {
//
//class Simulation;
//
//class PlasmaSource : public Physics {
//public:
//    Simulation* sim;
//
//    PlasmaSource(std::vector<std::string> options, Simulation* sim);
//
//    Particle makeNewParticle(std::list<Particle>& particleList);
//
//    int getNumParticlesToInject();
//
//    Real sampleUniformDist(Real a, Real b);
//
//    Real sampleNormalDist(Real mean, Real std);
//
//    Real getVelMag();
//
//    std::vector<Real> getUnitVector();
//
//    int requiredOptions = 6; // Required properties to be included (species_index, position_center, radius, thermal_velocity_mag, drift_velocity)
//    bool apply(Particles&);
//
//    Real BoltzmannCDF(Real v, Real R);
//
//    Real numToInjectFloat;
//    int speciesIndex;
//    Real charge;
//    Real mass;
//    std::vector<Real> center;
//    Real radius;
//    std::vector<Real> velMean;
//    Real velT;
//    std::vector<Real> activity;
//
//    std::default_random_engine generator;
//
//    // Domain& domain;
//    // std::vector<normal_distribution<float>>& velDist;
//};
//}
//
//
//#endif //SESS_PIC_PLASMASOURCE_H
