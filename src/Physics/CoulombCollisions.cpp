#include "CoulombCollisions.h"
#include "Simulation.h"
#include "LeapFrog.h"
#include "Interpolator.h"
#include "Grid.h"
#include "Particle.h"
#include <cmath>

using namespace std;
using namespace picard;

/***********************************************************************
 *                       INTER-Species Collisions                      *
 ***********************************************************************/

void CoulombInterSpeciesCollisions::setup(Simulation& simulation) {
    _nu.resize(simulation.grid().totalPoints());
    _nu_e.resize(simulation.grid().totalPoints());
    m = Species::reducedMass(s1, s2);
}

InteractionResults CoulombInterSpeciesCollisions::apply(Particle& p, Simulation& simulation) {
    // Ignore if this is not the right type of particle to collide with
    if (p.species_ptr() != s1)
        return PARTICLE_UNCHANGED;
    
    auto cell  = simulation.grid().getEnclosingCell(p.r);
    for(auto i : cell){
        auto w = simulation.interpolator().W(simulation.grid().position(i), p.r, simulation.grid().spacing());
        auto f = force12(p, i, simulation.grid());
        simulation.mover().addForce(w*f);
    }
    return PARTICLE_UPDATED;
}

Real CoulombInterSpeciesCollisions::coulombLog(int i, const Grid &g) {
    // TODO -- Fill this out with an actual computation
    return 10.0;
}

vec CoulombInterSpeciesCollisions::force12(Particle& p, int i, const Grid &g) {
    auto ref = p.species().reference;
    vec nu = _nu[i];
    vec nu_e = _nu_e[i];

    auto v1 = g.v_avg(s1, i);
    auto v2 = g.v_avg(s2, i);
    auto dv = v1 - v2;

    auto relV = (v1 - p.v) / (g.vsq_avg(s1,i) - v1*v1);
    if(!std::isfinite(product(relV)))
        relV = vec(relV.size(), 0);

    return -nu*m*dv- relV*nu*m*m*dv*dv/s1->m + nu_e*ref->simKb()*(g.temp(s1, i) - g.temp(s2, i))*relV;
}

void CoulombInterSpeciesCollisions::particleToGrid(Simulation& simulation) {
    auto k = simulation.referenceParams().simKb(); // Simulation boltzmann constant
    auto W = simulation.referenceParams().weightScale(); // Simulation weight scale
    auto& g = simulation.grid();
    for(int i=0; i<simulation.grid().totalPoints(); i++) {
        auto lnL = coulombLog(i, g);
        auto vth = sqrt(2.0 * k * (g.temp(s1, i) / s1->m + g.temp(s2, i) / s2->m));
        auto dv = vabs(g.v_avg(s1, i) - g.v_avg(s2, i));
        auto z = dv / vth;
        auto n2 = g.numberDensity(i, s2->id);
        auto f = 2.0 * sqrt_2;

        auto int_term = (0.5 * sqrt_pi * verf(z) - z * vexp(-z * z));
        auto nu = f * int_term * 8.0 * sqrt_pi * s1->q * s1->q * s2->q * s2->q * n2 * lnL /
                  (m * m * 16.0 * pi * pi * W * dv * dv * dv);

        auto exp_term = vexp(-z * z);
        auto nu_e = exp_term * 16.0 * sqrt_pi * s1->q * s1->q * s2->q * s2->q * n2 * lnL /
                    (f * s2->m * s1->m * 16.0 * pi * pi * W * vth * vth * vth);

        if (!std::isfinite(product(nu)))
            nu = vec(nu.size(), 0);
        if (!std::isfinite(product(nu_e)))
            nu_e = vec(nu_e.size(), 0);

        _nu[i] = nu;
        _nu_e[i] = nu_e;
    }
}

void CoulombInterSpeciesCollisions::write(std::string path) {
    if(isRoot()) {
        vector2d<Real> nu_s(_nu.size(), 2);
        for (uint i = 0; i < _nu.size(); i++) {
            nu_s(i, 0) = *max_element(_nu[i].begin(), _nu[i].end());
            nu_s(i, 1) = *max_element(_nu_e[i].begin(), _nu_e[i].end());
        }
        write2DVector(nu_s, path, appendDefault(), {"Momentum", "Energy"});
    }
}

std::string CoulombInterSpeciesCollisions::name() {
    return  "Coulomb Collision Frequency (" + s1->name + " " + s2->name + ")";
}




/***********************************************************************
 *                       INTRA-Species Collisions                      *
 ***********************************************************************/
//
//InteractionResults CoulombIntraSpeciesCollisions::apply(Particle &p, Simulation &simulation) {
//    // Ignore if this is not the right type of particle to collide with
//    if (p.species_ptr() != species)
//        return PARTICLE_UNCHANGED;
//
//    auto cell  = simulation.grid().getEnclosingCell(p.r);
//    for(auto i : cell){
//        auto w = simulation.interpolator().W(simulation.grid().position(i), p.r, simulation.grid().spacing());
//        auto f = force(p, i, simulation.grid(), simulation.timestep());
//        simulation.mover().addForce(w*f);
//    }
//
//    return PARTICLE_UPDATED;
//}
//
//Real CoulombIntraSpeciesCollisions::_nu(int i, const Grid& g){
//    auto m = species->m;
//    auto q = species->q;
//    auto n = g.numberDensity(i, species->id);
//    auto T = g.temp(species, i);
//    return (4.0*sqrt_pi/3.0)*n*q*q*coulombLog(i, g) / (m*m*pow(T/m, 1.5));
//}
//
//vec CoulombIntraSpeciesCollisions::force(Particle &p, int i, const Grid &g, Real dt) {
//    auto m = species->m;
//    auto relV = (getRow(g._averageVelocity[species->id],i) - p.v);
//    auto nu = _nu(i, g);
//    auto A = _A(m, nu, dt, g.temp(species, i));
//    auto force = m*(nu*relV + A);
//
//    return force;
//}
//
//vec CoulombIntraSpeciesCollisions::_A(Real m, Real nu, Real dt, vec T) {
//    Real Amp = 1.77*pow(m/(4.0*pi*nu*dt*T), .5) / dt; //TODO dimensions
//    Real sig = std::sqrt(2.0*nu*dt*T/m);
//    return Amp*vec(1, normal(0, sig));
//}
//
//Real CoulombIntraSpeciesCollisions::coulombLog(int i, const Grid &g) {
//    //TODO fill out
//    return 10;
//}
