#ifndef PlasmaBoundary_h
#define PlasmaBoundary_h

#include "Physics.h"
#include "Label.h"
#include "Particle.h"

namespace picard{

class PlasmaBoundary : public GlobalPhysics {
public:
    PlasmaBoundary(std::shared_ptr<ParticleBoundaryLabel> label, std::shared_ptr<Species> species);

    void apply(Simulation& simulation) override;
    
private:
    std::shared_ptr<ParticleBoundaryLabel> label{nullptr};
    std::shared_ptr<Species> species;
    uint normal_dim; // The dimension that this boundary is normal to
};

}
#endif /* PlasmaBoundary_h */
