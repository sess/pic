#ifndef SESS_PIC_PHYSICS_H
#define SESS_PIC_PHYSICS_H

#include "Particle.h"

namespace picard {
    
class Simulation;


//! Physics that is applied per particle
class LocalPhysics {
public:
    virtual InteractionResults apply(Particle& p, Simulation& simulation) = 0;
    virtual void setup(Simulation& simulation){};
    virtual void particleToGrid(Simulation& simulation){};
    virtual void write(std::string path){};
    virtual std::string name(){return "Local Physics Base";}
    virtual void resetTimestep(){};
};

//! Physics that is applied at each timestep
class GlobalPhysics {
public:
    virtual void apply(Simulation& simulation)  = 0;
    virtual void setup(Simulation& simulation){};
};

}

#endif //SESS_PIC_PHYSICS_H
