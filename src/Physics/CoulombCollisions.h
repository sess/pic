#ifndef CoulombCollisions_h
#define CoulombCollisions_h

#include "Physics.h"

namespace picard{

class CoulombInterSpeciesCollisions : public LocalPhysics {
public:
    CoulombInterSpeciesCollisions(SpeciesPtr s1, SpeciesPtr s2): s1(s1), s2(s2){};

    InteractionResults apply(Particle& p, Simulation& simulation) override;
    void setup(Simulation& simulation) override;
    void particleToGrid(Simulation& simulation) override;
    void write(std::string path) override;
    virtual std::string name() override;

private:
    SpeciesPtr s1{nullptr}; // Species 1
    SpeciesPtr s2{nullptr}; // Species 2

    Real m{0}; //!< reducedMass of the species

    std::vector<vec> _nu; // The on grid momentum collision frequency
    std::vector<vec> _nu_e; // The on grid energy collision frequency

    Real coulombLog(int i, const Grid& g); //!< Returns the Coulomb logarithm at the specied node
    vec force12(Particle& p, int i, const Grid& g); //!< get the force on species 1 by species 2
};


//class CoulombIntraSpeciesCollisions : public LocalPhysics {
//public:
//    explicit CoulombIntraSpeciesCollisions(SpeciesPtr species) : species(species){};
//    InteractionResults apply(Particle& p, Simulation& simulation) override;
//
//private:
//    SpeciesPtr species{nullptr}; // Species
//
//    Real coulombLog(int i, const Grid& g); //!< Returns the Coulomb logarithm at the specified node
//    Real _nu(int i, const Grid& g); //!< Compute collision frequency without scaling factor
//    vec _A(Real m, Real nu, Real dt, vec T); //!< Random thermalization vector
//    vec force(Particle& p, int i, const Grid& g, Real dt); //!< get the force on species 1 by species 1
//};


}
#endif /* CoulombCollisions_h */
