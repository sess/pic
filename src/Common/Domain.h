#ifndef SESS_PIC_DOMAIN_H
#define SESS_PIC_DOMAIN_H

#include <vector>
#include <iostream>
#include "Definitions.h"
#include "VectorUtils.h"
#include "IOUtils.h"

namespace picard {

/**
 * An enum to represent the boundary of a domain.
 * The labels are ordered 0-6 in order to make certain integer math easier. For example the dimension
 * can be extracted by dividing the enumeration integer by 2. Also the min/max value can be found by moding by 2.
 */
enum BoundaryId{
    NONE = -1,
    X_MIN,
    X_MAX,
    Y_MIN,
    Y_MAX,
    Z_MIN,
    Z_MAX,
    INTERIOR
};

/**
 * Convert a string to boundary id.
 * @param id The string to convert to a boundary id
 * @return A valid boundary id if the string is understood, NONE otherwise.
 */
BoundaryId getBoundaryId(std::string id);

/**
 * Get the boundary name of the given id
 * @param id The id to get the name of
 * @return The name of the boundary
 */
std::string toString(BoundaryId id);

/**
 * Get the default normal direction of a domain boundary. Assumes the domain is permissible to particles.
 * This function does not take particle permissions into consideration nor dimensionality
 * @param id The id of the boundary of interest
 * @return The normal of the specified boundary
 */
vec defaultNormal(BoundaryId id);

/**
 * Returns the dimension (0,1,or 2) of the specified id
 * @param id The id to get the dimension from
 * @return the index dimension of the specified id (0,1, or 2). Calculated as (id/2 % 3)
 */
inline int idDimension(BoundaryId id){return id/2 % 3;}

/**
 * Returns -1 if the id represents a minimum dimension, and 1 if the id represents a positive dimension
 * @param id The id to get the min/max multiplier of
 * @return -1 if the id represents a minimum dimension, and 1 if the id represents a positive dimension
 */
inline int idMinMax(BoundaryId id){return (id % 2 == 0) ? -1 : 1;}

struct Intersection {
    Intersection() = default; //!< Default constructor implemented for "no-intersection" value
    Intersection(int count, vec point, int dim, vec normal)
            : count(count), point(point), dim(dim), normal(normal) {}

    int count{0}; //!< The number of sides that were intersected
    vec point; //!< The point of intersection
    BoundaryId id{NONE}; //!< The boundary id of the first boundary that was intersected
    int dim{-1}; //!< The dimension of the side that was intersected
    vec normal; //!< The normal of the side that was intersected
    operator bool() const { return (count > 0); } //!< Check to see if there was any intersection
};

std::ostream& operator<<(std::ostream& os, const Intersection& i);

/**
 * Defines a rectangular region in (1,2,3)-dim space.
 */
class Domain {
public:
    /**
     * Dimensional Constructor for a domain
     * @param nDim The number of dimensions spanned by the domain (1, 2, or 3)
     * @param min The minimum point of the domain (vector of size nDim)
     * @param max The maximum point of the domain (vector of size nDim)
     * @param particles_permitted True if particles are allowed in the domain, false otherwise
     * @param id The id of the domain
     */
    Domain(int nDim, const vec& min, const vec& max, bool particles_permitted = true, int id = -1);

    /**
     * Default constructor for a domain
     * Populates the _min vector with INFINITIES
     * Populates the _max vector with -INFINITIES
     * @param nDim The number of dimensions spanned by the domain (1, 2, or 3)
     * @param particles_permitted True if particles are allowed in the domain, false otherwise
     * @param id The id of the domain
     */
    Domain(int nDim = 0, bool particles_permitted = true, int id = -1)
            : Domain(nDim, vec(nDim, INFINITY), vec(nDim, -INFINITY), particles_permitted, id) {}

    virtual ~Domain() = default; //!< default virtual destructor

    /**
     * Check if a position is strictly outside of the domain
     * @param pos The position to check
     * @return True if the position is outside of the domain, false if it is inside or on a boundary
     */
    bool isOutside(const vec& pos) const { return !isInside(pos); }

    /**
     * Check if a position is outside of the boundary specified
     * @param pos The position to check
     * @param id The id of the boundary to check if the position falls outsideof
     * @return True if the position is outside of the boundary specified, false if it is inside or on that boundary
     */
    bool isOutside(const vec& pos, BoundaryId id) const;

    /**
     * Check if the position is inside of the domain (or on a boundary)
     * @param pos The position to check
     * @return True if the position is inside of the domain or on a boundary, false if it is outside.
     */
    bool isInside(const vec& pos) const;

    /**
     * Check if a point lies on the boundary of the domain
     * @param pos The point to check
     * @return True if the point lies on a boundary domain, false otherwise
     */
    bool isOnBoundary(const vec& pos) const;

    /**
     * Check if a point lies on the specified boundary
     * @param pos The point to check
     * @param id The id of the boundary to see if this position is on
     * @return True if the point lies on the specified boundary, false otherwise
     */
    bool isOnBoundary(const vec& pos, BoundaryId id) const{ return feq(bounds(id), pos[idDimension(id)]) && isInside(pos); }

    /**
     * Get the all of the ids associated with the location specified.
     * @param r The position to get the boundary ids of
     * @return A list of all ids associated with the specified position. If the position is not on a boundary then an empty list is returned.
     */
    std::vector<BoundaryId> boundaryIds(const vec& pos) const;

    /**
     * Check if a ray intersects the domain. Return a positive number if there was an intersection.
     * A 1 is returned if it hit a non-degenerate simplex (face in 3D or edge in 2D)
     * A 2 is returned if it hit a degenerate simplex exactly (edge or vertex in 3D, vertex in 2D)
     * @param ray The ray to check for intersection with the domain
     * @param intersection A vector to populate with the intersection point
     * @param faceDimension An integer to populate with the dimension of the face that was intersected
     * @param normal A vector to populate with the normal direction of the intersected face
     * @return An integer (greater than 0 for intersection), 0 for no intersection. See description for meaning of the positive return value
     */
    Intersection intersects(const std::vector<Real>& ray) const;

    int id() const { return _id; } //!< Get the ID of the domain
    void setId(int id) { _id = id; } //!< Set the ID of the domain

    bool particlesPermitted() const { return _permitted; } //!< Check if particles are allowed in this domain
    void setParticlesPermitted(bool perm) { _permitted = perm; } //!< Set the permission state of the domain

    int nDim() const { return _nDim; } //!< get the dimension of the domain
    void setDimension(int nDim, const vec& min, const vec& max); //!< Set the new dimension of the domain with new min/max points

    inline Real length(int dim) const { return _max[dim] - _min[dim]; } //!< Get the length of the domain in the dimension specified
    vec size() const { return _max - _min; } //!< Get the vector size of the domain
    Real volume() const{ return product(size()); } //!< Get the volume of the domain
    Real area() const; //!< Returns the area of a "flattened" domain. I.e. if a 3D domain has  min[0] = max[0] then this dimension is ignored in the calc
    inline Real min(int dim) const { return _min[dim]; } //!< Get the minimum point int the specified dimension
    vec min() const { return _min; } //!< Get the minimum point in the domain
    void setMin(const vec& minPoint); //!< Set the minimum point in the domain
    void setMin(int dim, Real minVal); //!< Set the minimum point in the specified dimension

    inline Real max(int dim) const { return _max[dim]; } //!< Get the maximum point int the specified dimension
    vec max() const { return _max; } //!< Get the maximum point in the domain
    void setMax(const vec& maxPoint); //!< Set the maximum point in the domain
    void setMax(int dim, Real maxVal); //!< Set the maximum point in the domain

    bool operator==(const Domain& other) const; //!< Check if two domains are identical
    bool operator!=(const Domain& other) const{return !operator==(other); } //!< Check if two domains are not equal

    /**
     * Returns the bound value at the specified boundary id
     * @param id The id of the bound to return
     * @return Returns the min or max value of the dimension of the id (i.e. Z_MIN returns min(2))
     */
    Real bounds(BoundaryId id) const;

    /**
     * Get the outward pointing normal of the face of the domain specified by the id. The id mapping is
     * @param id The id of the face to get the normal of
     * @return The vector that represents the outward points normal of the face. The outward normal is multiplied by -1
     * if the domain is not permitted to particles
     * In this way the outward normal always points outward from the "allowed" domains into the "not allowed" domains
     */
    vec normal(BoundaryId id) const;

    /**
     * Expands the bounds of the domain to fully include the point specified
     * @tparam ArrayT The type of the point to include in the domain
     * @param p The point to expand the domain to accommodate. Must have a number of dimensions greater than or equal tot he domain dimensions
     * @return True if the domain was expanded, False otherwise.
     */
    template<typename ArrayT>
    bool expandToInclude(const ArrayT& p) {
        bool expanded = false;
        for (auto i = 0; i < _nDim; i++) {
            if (p[i] < _min[i]) {
                _min[i] = p[i];
                expanded = true;
            }
            if (p[i] > _max[i]) {
                _max[i] = p[i];
                expanded = true;
            }
        }
        if (expanded)
            refresh();

        return expanded;
    }

    /**
     * Generates a random point inside the domain
     * @return A vec that was randomly generated inside the domain
     */
    vec randomPositionInside() const; //!< Generates a random point inside the domain.

    /**
     * Generates a random position outside of the domain
     * @param buffer The fraction of the domain width that will be used for the width of the region outside of the domain where a point might be generated
     * @return A vec that was randomly generated outside of the domain
     */
    vec randomPositionOutside(Real buffer = 0.3) const;

    /**
     * Generate a random position outside of the specific boundary specified by the id
     * @param id The id of the boundary which you are outside
     * @param buffer The fraction of the domain width that will be used for the width of the region outside of the domain where a point might be generated
     * @return A vec that was randomly generated outside of the specified boundary
     */
    vec randomPositionOutside(BoundaryId id, Real buffer = 0.3) const;

    /**
     * Generates a random position on one or several of the domain boundaries
     * @param boundaries If this is not null then it will store the number of boundaries that the randomly generated point is touching
     * @return A vec that was randomly generated on the boundary of the domain
     */
    vec randomBoundaryPosition(int* boundaries = nullptr) const;

    /**
     * Generates a random position on the boundary specified by the id.
     * @param id The id of the boundary to sample a point from
     * @return A vec that was randomly generated on the desired boundary
     */
    vec randomBoundaryPosition(BoundaryId id) const;

    /**
     * Create a random domain of dimension nDim.
     * Intended for testing purposes so you can easily create a domain of whatever dimension
     * @param nDim The number of spacial dimensions of the domain
     * @param max_size The maximum width of the domain
     * @return A randomly generated domain with the properties specified
     */
    static Domain Random(int nDim, Real max_size = 10.);

protected:
    int _nDim; //!< Number of dimension in the domain
    vec _min; //!< The minimum point in the domain
    vec _max; //!< The maximum point in the domain
    int _id{-1}; //!< The id of the domain
    bool _permitted{true}; //!< Whether or not particles are allowed in the domain

    virtual void refresh(); //!< Called when a physical change is made to the domain. Also Used by derived types to refresh their computed parameters

    /**
     * Takes a position and snaps it to any boundaries that it is out of range of
     * @param pos The position to be "snapped" to the boundary
     * @param boundaries The number of boundaries it was snapped to is stored here if not nullptr
     * @return The new position, snapped to any boundaries it was outside of
     */
    vec snapToBoundary(const vec& pos, int* boundaries = nullptr) const;

    /**
     * Check if the ray intersects the boundary point specified by the id.
     * @param id The id of the boundary point.
     * @param ray The ray to check for intersection
     * @return An Intersection struct containing the results of the intersection test.
     */
    Intersection intersectsPoint(BoundaryId id, const std::vector<Real>& ray) const;

    /**
     * Check if the ray intersects the boundary line specified by the id.
     * Intersection test is computed from :
     * https://rootllama.wordpress.com/2014/06/20/ray-line-segment-intersection-test-in-2d/
     * @param id The id of the boundary line.
     * @param ray The ray to check for intersection
     * @return An Intersection struct containing the results of the intersection test.
     */
    Intersection intersectsLine(BoundaryId id, const std::vector<Real>& ray) const;

    /**
     * Check if the ray intersects the boundary plane specified by the id.
     * @param id The id of the boundary plane.
     * @param ray The ray to check for intersection
     * @return An Intersection struct containing the results of the intersection test.
     */
    Intersection intersectsPlane(BoundaryId id, const std::vector<Real>& ray) const;
};

std::ostream& operator<<(std::ostream& os, const Domain& d); //!< Stream operator overload for the domain class

}


#endif //SESS_PIC_DOMAIN_H
