#ifndef SESS_PIC_BOUNDARYCONDITIONS_H
#define SESS_PIC_BOUNDARYCONDITIONS_H

#include "Definitions.h"
#include "Particle.h"
#include "Grid.h"
#include <boost/math/special_functions/sign.hpp>
#include <chrono>
#include <numeric>
#include <random>

namespace picard {

class Simulation;
class ParticleBoundaryLabel;
class Interpolator;

class BoundaryConditions {
public:
    /**
     * Apply the boundary condition appropriate to the label in the grid intersection that is provided
     * @param intersection The GridIntersection struct that contains intersection information including the label of the element that was intersected.
     * @param p The particle to apply the boundary condition to.
     * @param grid The grid that the particle is on. May be edited to store surface charge density or other parameters
     * @param interp The interpolator used in the simulation to interpolate particle values onto the grid in the case of charge absorption.
     * @return The results of the boundary condition application.
     */
    static InteractionResults apply(GridIntersection intersection, Particle& p, Grid& grid, const Interpolator& interp);

    /**
     * Apply a reflecting boundary condition to the provided particle. The reflection is specular and has a coefficient of restitution provided by the reflecting label.
     * @param p The particle to be reflected
     * @param intersection The intersection and label information to perform the boundary interaction
     * @param grid The grid that the interaction took place on
     * @param The results of the boundary interaction.
     **/
    static InteractionResults reflecting(Particle& p, const GridIntersection& intersection, const Grid& grid);

    /**
     * Apply a scattering boundary condition to the provided particle. Scattering is non-specular reflection and has a coefficient of restitution provided by the scattering label.
     * @param p The particle to be scattered
     * @param intersection The intersection and label information to perform the boundary interaction
     * @param grid The grid that the interaction took place on
     * @param The results of the boundary interaction.
     **/
    static InteractionResults scattering(Particle& p, const GridIntersection& intersection, const Grid& grid);

    /**
     * Apply a periodic boundary condition to the provided particle. E.g. If the particle leaves the x-min boundary it will reenter on the x-max boundary with the same velocity vector.
     * @param p The particle to which the boundary condition is applied
     * @param intersection The intersection and label information to perform the boundary interaction
     * @param grid The grid that the interaction took place on
     * @param The results of the boundary interaction.
     **/
    static InteractionResults periodic(Particle& p, const Grid& grid);

    /**
     * Absorb the provided particle at the boundary, removing from the domain but storing the contributed charge on the surface nodes of the boundary.
     * @param p The particle to be absorbed.
     * @param intersection The intersection and label information to perform the boundary interaction
     * @param grid The grid that the interaction took place on
     * @param The results of the boundary interaction.
     **/
    static InteractionResults absorbing(const Particle& p, const GridIntersection& intersection, Grid& grid,
                               const Interpolator& interp);

    /**
     * Absorb the provided particle at the boundary, removing it from the domain but storing the contributed charge on the surface nodes of the boundary. Inject any secondary emissions caused by the impacting particle.
     * @param p The particle to be absorbed.
     * @param intersection The intersection and label information to perform the boundary interaction
     * @param grid The grid that the interaction took place on
     * @param The results of the boundary interaction.
     **/
    static InteractionResults solid(Particle& p, const GridIntersection& intersection, Grid& grid, const Interpolator& interp);

private:
    static InteractionResults reflectParticle(Particle& p, const GridIntersection& intersection,
                                     Real restCoeff, vec reflectAngle, const Grid& grid);

    static void absorbCharge(Real charge, const GridIntersection& intersection, Grid& grid, const Interpolator& interp);

    static std::list<Particle> injectSecondaryElectrons(int numberofSecondaries,
                                                        const Particle& p, const GridIntersection& intersection,
                                                        const Grid& grid, const Interpolator& interp);

    /**
     * Electron induced emission from dielectric based on Sydorenko's thesis and JRM Vaughan's paper
     */
    static InteractionResults applyElectronDielectricBC(Particle& p, const GridIntersection& intersection,
                                               Grid& grid, const Interpolator& interp);

    /**
     * Kinetic Secondary ejection from ion impact - based on data from Szapiro and Rocca
     */
    static InteractionResults applyIonDielectricBC(Particle& p, const GridIntersection& intersection,
                                          Grid& grid, const Interpolator& interp);

    static Real findSecondaryEnergy(Real R);
};
}

#endif //SESS_PIC_BOUNDARYCONDITIONS_H
