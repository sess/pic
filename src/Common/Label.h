#ifndef SESS_PIC_LABEL_H
#define SESS_PIC_LABEL_H

#include "Definitions.h"
#include "VectorUtils.h"
#include "StringUtils.h"
#include "Domain.h"

#include <map>
#include <vector>
#include <string>
#include <iostream>
#include <memory>
#include <algorithm>

namespace picard {

/**
 * \brief The base class for label Types
 *
 * The base class for a label that contains the shared data members of the FieldLabel and ParticleBoundaryLabel classes.
 * Not used except as a base class.
 */
struct LabelBase {
    /**
     * The basic constructor for the LabelBase Class
     * @param name The name of the label
     * @param id  The id of the label
     * @param normal The outward normal of the label
     * @param properties The list of properties for the label
     */
    LabelBase(std::string name = "", int priority = -1, const vec& normal = {},
              const std::vector<Real>& properties = {}) :
            priority(priority), name(name), normal(normal), properties(properties) {};

    virtual ~LabelBase() = default; //!< Default virtual destructor

    /**
     * \brief The priority of the label.
     *
     * The priority is assembled from the ordering that the labels are provided in the configuration file.
     * For field labels, the priority is needed because Gmsh defines physical Id's to elements and not nodes. Therefore there is an
     * ambiguity as to which id should be used for a particular node which is shared between several elements with
     * different ids. The highest priority label will be given to an ambigous node.
     * For particle boundary labels, if there are duplicate elements, the label with the highest priority is kept
     */
    int priority;
    std::string name; //!< The id of the label. Comes from the GMSH Physical Id.
    vec normal; //!< Normal direction of the associated label.
    std::vector<Real> properties; //!< list of properties associated with the label. The meaning of the values contained here depends on type of label.

};

/**
 * \brief Labels that are used for the FieldSolver.
 *
 * Every node in the mesh is given one of these labels and they define how the matrix is assembled.
 */
class FieldLabel : public LabelBase {
public:
    /**
     * The types of field labels that can be used.
     */
    enum Type {
        DIRICHLET, //!< A Dirichlet boundary condition is applied. Requires 1 argument: The voltage value to be applied.
        TIME_DEPENDENT_DIRICHLET, //!< A Time-varying Dirichlet boundary condition is applied. Requires 1 argument: A filepath to a timeserires defining voltage at each timestep.
        STEP, //!< A Dirichlet boundary condition that specifies a step. Requires 4 arguments: Voltage before step, voltage during step, start time, end time
        NEUMANN, //!< A Neumann boundary condition applied. Requires 1 argument: The voltage gradient value to be applied.
        ZERO_FIELD,//!< Forces the electric field to be zero at the boundary. Requires no additional arguments.
        PLASMA_DIELECTRIC_INTERFACE,//!< Defines an interface between two regions of different dielectric constants. Requires 2 arguments: the dielectric constant "below" and dielectric constant "above".
        PERIODIC,//!< A periodic boundary condition is applied. Requires no additional arguments.
        INTERIOR,//!< Defines a regular interior node. Requires 1 argument: The dielectric constant for the region
        NONE //!< No type specified
    }; //!< The types of field labels that can be used.

    /**
     * The basic constructor for a FieldLabel
     * @param name The name of the label
     * @param type The type of the label
     * @param id The label id
     * @param priority The priority of the label
     * @param normal The outward normal of the label
     * @param properties The list of properties required by the label
     */
    FieldLabel(std::string name = "", Type type = NONE, int priority = 0,
               const vec& normal = {}, const std::vector<Real>& properties = {}) :
            LabelBase(name, priority, normal, properties), type(type) {}

    /**
     * The constructor for a FieldLabel used by the config file parser
     * @param name The name of the label
     * @param type The type of the label represented by a string
     * @param id The label id
     * @param priority The priority of the label
     * @param normal The outward normal of the label
     * @param properties The list of properties required by the label
     */
    FieldLabel(std::string name, std::string type, int priority = 0,
               const vec& normal = {}, const std::vector<Real>& properties = {}) :
            LabelBase(name, priority, normal, properties), type(getType(type)) {}

    Type type; //!< The type of the field label

    /**
     * Compares two FieldLabels to see if they are equivalent.
     * @param other The FieldLabel to compare with
     * @return True if the two labels have the same data members, false otherwise.
     */
    bool operator==(const FieldLabel& other) {
        return (name == other.name &&
                priority == other.priority &&
                type == other.type &&
                normal == other.normal &&
                properties == other.properties);
    }

    int numberOfRequiredArguments() const { return numberOfRequiredArguments(type); } //!< Get the number of arguments required by label of this type


    // Static methods for converting between types and string and finding the number of arguments

    /**
     * Get the enumeration type of a FieldLabel from its name.
     * @param name A string containing the name of the label. Not case-sensitive.
     * @return An FieldLabel::Type with the associated enumeration of the label. Returns FieldLabel::NONE if the string is unrecognized
     */
    static Type getType(std::string name) {
        name = to_lower(name);
        if (name == "dirichlet") return DIRICHLET;
        else if (name == "time dependent dirichlet") return TIME_DEPENDENT_DIRICHLET;
        else if (name == "step") return STEP;
        else if (name == "neumann") return NEUMANN;
        else if (name == "zero field") return ZERO_FIELD;
        else if (name == "plasma dielectric interface") return PLASMA_DIELECTRIC_INTERFACE;
        else if (name == "periodic") return PERIODIC;
        else if (name == "interior") return INTERIOR;
        else if (name == "none") return NONE;
        else
            throw std::runtime_error("Label::getType Error: Unrecognized label name (" + name + ")");
    }

    /**
     * Get the number of required arguments (or inputs) for the given FieldLabel type.
     * @param type The type of the field label of interest
     * @return The number of arguments required by a label of this type. This is the number of arguments expected in the config file definition of the label.
     */
    static int numberOfRequiredArguments(Type type) {
        switch (type) {
            case DIRICHLET :
                return 1;
            case TIME_DEPENDENT_DIRICHLET :
                return 1;
            case STEP :
                return 4;
            case NEUMANN :
                return 1;
            case ZERO_FIELD :
                return 0;
            case PLASMA_DIELECTRIC_INTERFACE :
                return 2;
            case PERIODIC :
                return 0;
            case INTERIOR :
                return 2;
            case NONE :
                return 0;
            default :
                return 0;
        }
    }

    /**
     *  Get the number of required arguments (or inputs) for the given FieldLabel type -- as specified by the name string.
     * @param name The name of the FieldLabel type of interest
     * @return The number of arguments required by a label of this type. This is the number of arguments expected in the config file definition of the label.
     */
    static int numberOfRequiredArguments(std::string name) {
        return numberOfRequiredArguments(getType(name));
    }
};

//!< Stream the type of the field label
inline std::ostream& operator<<(std::ostream& os, FieldLabel::Type type) {
    switch (type) {
        case FieldLabel::DIRICHLET :
            os <<  "Dirichlet";
            break;
        case FieldLabel::TIME_DEPENDENT_DIRICHLET :
            os << "Time Dependent Dirichlet";
            break;
        case FieldLabel::STEP :
            os << "Step";
            break;
        case FieldLabel::NEUMANN :
            os << "Neumann";
            break;
        case FieldLabel::ZERO_FIELD :
            os << "Zero Field";
            break;
        case FieldLabel::PLASMA_DIELECTRIC_INTERFACE :
            os << "Plasma Dielectric Interface";
            break;
        case FieldLabel::PERIODIC :
            os << "Periodic";
            break;
        case FieldLabel::INTERIOR :
            os << "Interior";
            break;
        case FieldLabel::NONE :
            os << "None";
            break;
        default :
            os << "Unrecognized";
            break;
    }
    return os;
}

inline std::ostream& operator<<(std::ostream& os, const FieldLabel& fl) {
    os << "name: " << fl.name << " type: " << fl.type;

    if (fl.normal.size() != 0) {
        os << " outward normal: (" << fl.normal << ")";
    }
    os << " priority: " << fl.priority;

    if (fl.properties.size())
        os << " properties: " << fl.properties;
    return os;
}


/**
 * \brief Labels that are used for the application of particle boundary conditions.
 *
 * Every boundary element in the mesh is given one of these label and they define what happens when a particle crosses a boundary.
 */
class ParticleBoundaryLabel : public LabelBase {
public:
    /**
     * The types of particle boundary condition labels that can be used.
     */
    enum Type {
        OPEN, //!< Open boundary where particles will just be removed from the simulation when passing this boundary. Requires no additional arguments.
        REFLECTING, //!< Boundary that induces specular reflection of a particle that collides with it. Requires 1 argument: The coefficient of restitution.
        SCATTERING, //!< Boundary that induces non-specular reflection of a particle that collides with it. Requires 1 argument: The coefficient of restitution.
        ABSORBING, //!< Boundary that absorbs the charge of any particle that collides with it. Requires no additional arguments.
        PERIODIC, //!< Periodic boundary that wraps around to the opposite facing wall so particle remains in the simulation. Requires no additional arguments.
        /**
         * Solid wall that includes scattering, absorbtion and secondary emission. Requires 10 arguments
         * 6 for electrons, 4 for ions  (models based on Sydorenko, Furman and Pivi for electrons, Szapiro Rocca for ions
         * Solid/Electrons - roughness factor (0-2), gamma at zero energy Emax normal incidence (eV), Energy at gamma max normal,gammaMax normal direction, emission threshold (electron affinity)(eV), crossover energy (eV)
         * Solid/Ions - Emax normal incidence (eV), gammaMax normal direction,  emission threshold (eV), power law coefficient
         */
        SOLID,
        NONE //!< No type specified
    };

    /**
     * The basic constructor for a ParticleBoundaryLabel
     * @param name The name of the label
     * @param type The type of the label
     * @param id The id of the label
     * @param normal The outward normal of the label
     * @param properties The list of required properties used by the label
     */
    ParticleBoundaryLabel(std::string name = "", Type type = NONE, int priority = -1, const vec& normal = {},
                          const std::vector<Real>& properties = {}) :
            LabelBase(name, priority, normal, properties), type(type) {}

    /**
     * The constructor for a ParticleBoundaryLabel used by the config file parser
     * @param name The name of the label
     * @param type The type of the label represented by a string
     * @param id The id of the label
     * @param normal The outward normal of the label
     * @param properties The list of required properties used by the label
     */
    ParticleBoundaryLabel(std::string name, std::string type, int priority = -1, const vec& normal = {},
                          const std::vector<Real>& properties = {}) :
            LabelBase(name, priority, normal, properties), type(getType(type)) {}

    Type type{NONE}; //!< The type of the particle boundary condition label

    /**
     * Compares two ParticleBoundaryLabels to see if they are equivalent.
     * @param other The ParticleBoundaryLabel to compare with
     * @return True if the two labels have the same data members, false otherwise.
     */
    bool operator==(const ParticleBoundaryLabel& other) {
        return (name == other.name &&
                priority == other.priority &&
                type == other.type &&
                normal == other.normal &&
                properties == other.properties);
    }

    int numberOfRequiredArguments() const { return numberOfRequiredArguments(type); } //!< Get the number of arguments required by label of this type


    // Static methods for converting between type <-> string and finding the number of arguments

    /**
     * Get the enumeration of a particle boundary condition label from its name.
     * @param name A string containing the name of the label. Not case-sensitive.
     * @return An ParticleBoundaryLabel::Type with the associated enumeration of the label. Returns ParticleBoundaryLabel::NONE if the string is unrecognized
     */
    static Type getType(std::string name) {
        name = to_lower(name);
        if (name == "open") return OPEN;
        else if (name == "reflecting") return REFLECTING;
        else if (name == "scattering") return SCATTERING;
        else if (name == "absorbing") return ABSORBING;
        else if (name == "periodic") return PERIODIC;
        else if (name == "solid") return SOLID;
        else if (name == "none") return NONE;
        else
            throw std::runtime_error("Label::getType Error: Unrecognized label name (" + name + ")");
    }

    /**
     * Get the number of required arguments (or inputs) for the given ParticleBoundaryLabel type.
     * @param type The type of the ParticleBoundaryLabel of interest
     * @return The number of arguments required by a label of this type. This is the number of arguments expected in the config file definition of the label.
     */
    static int numberOfRequiredArguments(Type type) {
        switch (type) {
            case OPEN :
                return 0;
            case REFLECTING :
                return 1;
            case SCATTERING :
                return 1;
            case ABSORBING :
                return 0;
            case PERIODIC :
                return 0;
            case SOLID :
                return 12;
            case NONE :
                return 0;
            default :
                return 0;
        }
    }

    /**
     *  Get the number of required arguments (or inputs) for the given ParticleBoundaryLabel type -- as specified by the name string.
     * @param name The name of the ParticleBoundaryLabel type of interest
     * @return The number of arguments required by a label of this type. This is the number of arguments expected in the config file definition of the label.
     */
    static int numberOfRequiredArguments(std::string name) { return numberOfRequiredArguments(getType(name)); }
};

inline std::ostream& operator<<(std::ostream& os, ParticleBoundaryLabel::Type type) {
    switch (type) {
        case ParticleBoundaryLabel::OPEN :
            os << "Open";
            break;
        case ParticleBoundaryLabel::REFLECTING :
            os << "Reflecting";
            break;
        case ParticleBoundaryLabel::SCATTERING :
            os << "Scattering";
            break;
        case ParticleBoundaryLabel::ABSORBING :
            os << "Absorbing";
            break;
        case ParticleBoundaryLabel::PERIODIC :
            os << "Periodic";
            break;
        case ParticleBoundaryLabel::SOLID :
            os << "Solid";
            break;
        case ParticleBoundaryLabel::NONE :
            os << "None";
            break;
        default :
            os << "Unrecognized";
            break;
    }
    return os;
}

inline std::ostream& operator<<(std::ostream& os, const ParticleBoundaryLabel& pbl) {
    os << "name: " << pbl.name << " type: " << pbl.type;

    if (pbl.normal.size() != 0) {
        os << " outward normal: (" << pbl.normal << ")";
    }
    os << " priority: " << pbl.priority;
    if (pbl.properties.size())
        os << " properties: " << pbl.properties;
    return os;
}

/****************************************************
 *             Make Field Labels                    *
 ****************************************************/

/**
 *  Create a Dirichlet FieldLabel
 * @param name The label name
 * @param priority The label priority
 * @param normal The outward pointing normal of the label
 * @param voltage The voltage to be applied to nodes with this label
 * @return A shared_ptr to the constructed FieldLabel
 */
inline std::shared_ptr<FieldLabel> makeDirichlet(std::string name, int priority, const vec& normal, Real voltage) {
    return std::make_shared<FieldLabel>(name, FieldLabel::DIRICHLET, priority, normal, std::vector<Real>({voltage}));
}

/**
 *  Create a Dirichlet FieldLabel with an associated boundary Id
 * @param id The boundary id where this label is to be applied
 * @param priority The label priority
 * @param voltage The voltage to be applied to nodes with this label
 * @return A pair of boundary id and shared_ptr to the constructed FieldLabel
 */
inline std::pair<int, std::shared_ptr<FieldLabel>> makeDirichlet(BoundaryId id, int priority, Real voltage) {
    return std::make_pair(id, makeDirichlet(toString(id), priority, defaultNormal(id), voltage));
};


/**
 * Create a Time-Dependent Dirichlet FieldLabel
 * @param name The label name
 * @param priority The label priority
 * @param normal The outward pointing normal of the label
 * @param voltageSeries
 * @return A shared_ptr to the constructed FieldLabel
 */
inline std::shared_ptr<FieldLabel> makeTimeDependentDirichlet(std::string name, int priority, const vec& normal,
                                                              const std::vector<Real>& voltageSeries) {
    return std::make_shared<FieldLabel>(name, FieldLabel::TIME_DEPENDENT_DIRICHLET, priority, normal, voltageSeries);
}

/**
 * Create a Time-Dependent Dirichlet FieldLabel with an associated boundary Id
 * @param id The boundary id where this label is to be applied
 * @param priority The label priority
 * @param voltageSeries
 * @return A pair of boundary id and shared_ptr to the constructed FieldLabel
 */
inline std::pair<int, std::shared_ptr<FieldLabel>> makeTimeDependentDirichlet(BoundaryId id, int priority,
                                                                              const std::vector<Real>& voltageSeries) {
    return std::make_pair(id, makeTimeDependentDirichlet(toString(id), priority, defaultNormal(id), voltageSeries));
};


/**
 * Create a Step Dirichlet FieldLabel
 * @param name The label name
 * @param priority The label priority
 * @param normal The outward pointing normal of the label
 * @param baseVoltage The voltage that this region starts at
 * @param stepVoltage The voltage applied during the step
 * @param startTime The start of the step
 * @param endTime The end of the step
 * @return A shared_ptr to the constructed FieldLabel
 */
inline std::shared_ptr<FieldLabel> makeStep(std::string name, int priority, const vec& normal, Real baseVoltage,
                                            Real stepVoltage, Real startTime, Real endTime) {
    return std::make_shared<FieldLabel>(name, FieldLabel::STEP, priority, normal,
                                        std::vector<Real>({baseVoltage, stepVoltage, startTime, endTime}));
}

/* Create a Step Dirichlet FieldLabel
 * @param id The boundary id where this label is to be applied
 * @param priority The label priority
 * @param normal The outward pointing normal of the label
 * @param baseVoltage The voltage that this region starts at
 * @param stepVoltage The voltage applied during the step
 * @param startTime The start of the step
 * @param endTime The end of the step
 * @return A shared_ptr to the constructed FieldLabel
 */
inline std::pair<int, std::shared_ptr<FieldLabel>> makeStep(BoundaryId id, int priority, Real baseVoltage,
                                                            Real stepVoltage, Real startTime, Real endTime) {
    return std::make_pair(id, makeStep(toString(id), priority, defaultNormal(id), baseVoltage,
                                       stepVoltage, startTime, endTime));
};

/**
 * Create a Neumann FieldLabel
 * @param name The label name
 * @param priority The label priority
 * @param normal The outward pointing normal of the label
 * @param val The value of the derivative of the voltage applied as a neumann condition
 * @return A shared_ptr to the constructed FieldLabel
 */
inline std::shared_ptr<FieldLabel> makeNeumann(std::string name, int priority, const vec& normal, Real val) {
    return std::make_shared<FieldLabel>(name, FieldLabel::NEUMANN, priority, normal, std::vector<Real>({val}));
}

/**
 * Create a Neumann FieldLabel with an associated boundary Id
 * @param id The boundary id where this label is to be applied
 * @param priority The label priority
 * @param val The value of the derivative of the voltage applied as a neumann condition
 * @return A pair of boundary id and shared_ptr to the constructed FieldLabel
 */
inline std::pair<int, std::shared_ptr<FieldLabel>> makeNeumann(BoundaryId id, int priority, Real val) {
    return std::make_pair(id, makeNeumann(toString(id), priority, defaultNormal(id), val));
};

/**
 * Create a Zero-Field FieldLabel
 * @param name The label name
 * @param priority The label priority
 * @param normal The outward pointing normal of the label
 * @return A shared_ptr to the constructed FieldLabel
 */
inline std::shared_ptr<FieldLabel> makeZeroField(std::string name, int priority, const vec& normal) {
    return std::make_shared<FieldLabel>(name, FieldLabel::ZERO_FIELD, priority, normal);
}

/**
 * Create a Zero-Field FieldLabel with an associated boundary Id
 * @param id The boundary id where this label is to be applied
 * @param priority The label priority
 * @return A pair of boundary id and shared_ptr to the constructed FieldLabel
 */
inline std::pair<int, std::shared_ptr<FieldLabel>> makeZeroField(BoundaryId id, int priority) {
    return std::make_pair(id, makeZeroField(toString(id), priority, defaultNormal(id)));
};

//TODO: Double check the meaning of e1 and e2. update the other comments if necessary
/**
 * Create a Plasma-Dielectric Interface FieldLabel
 * @param name The label name
 * @param priority The label priority
 * @param normal The outward pointing normal of the label
 * @param e1 The dielectric constant of the dielectric or the region "below"
 * @param e2 The dielectric constant of the plasma or the region "above"
 * @return A shared_ptr to the constructed FieldLabel
 */
inline std::shared_ptr<FieldLabel> makePlasmaDielectricInterface(std::string name, int priority, const vec& normal,
                                                                 Real e1, Real e2) {
    return std::make_shared<FieldLabel>(name, FieldLabel::PLASMA_DIELECTRIC_INTERFACE, priority, normal,
                                        std::vector<Real>({e1, e2}));
}

/**
 * Create a Plasma-Dielectric Interface FieldLabel with an associated boundary Id
 * @param id The boundary id where this label is to be applied
 * @param priority The label priority
 * @param e1 The dielectric constant of the dielectric or the region "below"
 * @param e2 The dielectric constant of the plasma or the region "above"
 * @return A pair of boundary id and shared_ptr to the constructed FieldLabel
 */
inline std::pair<int, std::shared_ptr<FieldLabel>> makePlasmaDielectricInterface(BoundaryId id, int priority,
                                                                                 Real e1, Real e2) {
    return std::make_pair(id, makePlasmaDielectricInterface(toString(id), priority, defaultNormal(id), e1, e2));
};

/**
 * Create a periodic FieldLabel
 * @param name The label name
 * @param priority The label priority
 * @param normal The outward pointing normal of the label
 * @return A shared_ptr to the constructed FieldLabel
 */
inline std::shared_ptr<FieldLabel> makePeriodicField(std::string name, int priority, const vec& normal) {
    return std::make_shared<FieldLabel>(name, FieldLabel::PERIODIC, priority, normal);
}

/**
 * Create a periodic FieldLabel with an associated boundary Id
 * @param id The boundary id where this label is to be applied
 * @param priority The label priority
 * @return A pair of boundary id and shared_ptr to the constructed FieldLabel
 */
inline std::pair<int, std::shared_ptr<FieldLabel>> makePeriodicField(BoundaryId id, int priority) {
    return std::make_pair(id, makePeriodicField(toString(id), priority, defaultNormal(id)));
};

/**
 * Create an Interior FieldLabel
 * @param name The label name
 * @param priority The label priority
 * @param normal The outward pointing normal of the label
 * @param e The dielectric constant of the medium
 * @return A shared_ptr to the constructed FieldLabel
 */
inline std::shared_ptr<FieldLabel> makeInterior(std::string name, int priority, Real e, bool permitted) {
    auto permission = (permitted) ? 1. : -1.;
    return std::make_shared<FieldLabel>(name, FieldLabel::INTERIOR, priority, vec(),
                                        std::vector<Real>({e, permission}));
}

/**
 * Create an Interior FieldLabel with an associated boundary Id
 * @param id The boundary id where this label is to be applied
 * @param priority The label priority
 * @param e The dielectric constant of the medium
 * @return A pair of boundary id and shared_ptr to the constructed FieldLabel
 */
inline std::pair<int, std::shared_ptr<FieldLabel>> makeInterior(int priority, Real e, bool permitted) {
    BoundaryId id = INTERIOR;
    return std::make_pair(id, makeInterior(toString(id), priority, e, permitted));
};



/****************************************************
 *          Make Particle Boundary Labels           *
 ****************************************************/

/**
 * Create an Open ParticleBoundaryLabel
 * @param name The label name
 * @param normal The outward pointing normal of the label
 * @return A shared_ptr to the constructed ParticleBoundaryLabel
 */
inline std::shared_ptr<ParticleBoundaryLabel> makeOpen(std::string name, int priority, const vec& normal) {
    return std::make_shared<ParticleBoundaryLabel>(name, ParticleBoundaryLabel::OPEN, priority, normal);
}

/**
 * Create an Open ParticleBoundaryLabel with an associated boundary Id
 * @param id The boundary id where this label is to be applied
 * @return A pair of boundary id and shared_ptr to the constructed ParticleBoundaryLabel
 */
inline std::pair<int, std::shared_ptr<ParticleBoundaryLabel>> makeOpen(BoundaryId id, int priority) {
    return std::make_pair(id, makeOpen(toString(id), priority, defaultNormal(id)));
};

/**
 * Create a reflecting ParticleBoundaryLabel
 * @param name The label name
 * @param normal The outward pointing normal of the label
 * @param restitutionCoef The coefficient of restitution for the specular reflection
 * @return A shared_ptr to the constructed ParticleBoundaryLabel
 */
inline std::shared_ptr<ParticleBoundaryLabel> makeReflecting(std::string name, int priority, const vec& normal,
                                                             Real restitutionCoef) {
    return std::make_shared<ParticleBoundaryLabel>(name, ParticleBoundaryLabel::REFLECTING, priority, normal,
                                                   std::vector<Real>({restitutionCoef}));
}

/**
 * Create a reflecting ParticleBoundaryLabel with an associated boundary Id
 * @param id The boundary id where this label is to be applied
 * @param restitutionCoef The coefficient of restitution for the specular reflection
 * @return A pair of boundary id and shared_ptr to the constructed ParticleBoundaryLabel
 */
inline std::pair<int, std::shared_ptr<ParticleBoundaryLabel>> makeReflecting(BoundaryId id, int priority,
                                                                             Real restitutionCoef) {
    return std::make_pair(id, makeReflecting(toString(id), priority, defaultNormal(id), restitutionCoef));
};

/**
 * Create a scattering ParticleBoundaryLabel
 * @param name The label name
 * @param normal The outward pointing normal of the label
 * @param restitutionCoef The coefficient of restitution for the non-specular reflection
 * @return A shared_ptr to the constructed ParticleBoundaryLabel
 */
inline std::shared_ptr<ParticleBoundaryLabel> makeScattering(std::string name, int priority, const vec& normal,
                                                             Real restitutionCoef) {
    return std::make_shared<ParticleBoundaryLabel>(name, ParticleBoundaryLabel::SCATTERING, priority, normal,
                                                   std::vector<Real>({restitutionCoef}));
}

/**
 * Create a scattering ParticleBoundaryLabel with an associated boundary Id
 * @param id The boundary id where this label is to be applied
 * @param restitutionCoef The coefficient of restitution for the non-specular reflection
 * @return A pair of boundary id and shared_ptr to the constructed ParticleBoundaryLabel
 */
inline std::pair<int, std::shared_ptr<ParticleBoundaryLabel>> makeScattering(BoundaryId id, int priority,
                                                                             Real restitutionCoef) {
    return std::make_pair(id, makeScattering(toString(id), priority, defaultNormal(id), restitutionCoef));
};

/**
 * Create an absorbing ParticleBoundaryLabel
 * @param name The label name
 * @param normal The outward pointing normal of the label
 * @return A shared_ptr to the constructed ParticleBoundaryLabel
 */
inline std::shared_ptr<ParticleBoundaryLabel> makeAbsorbing(std::string name, int priority, const vec& normal) {
    return std::make_shared<ParticleBoundaryLabel>(name, ParticleBoundaryLabel::ABSORBING, priority, normal);
}

/**
 * Create an absorbing ParticleBoundaryLabel with an associated boundary Id
 * @param id The boundary id where this label is to be applied
 * @return A pair of boundary id and shared_ptr to the constructed ParticleBoundaryLabel
 */
inline std::pair<int, std::shared_ptr<ParticleBoundaryLabel>> makeAbsorbing(BoundaryId id, int priority) {
    return std::make_pair(id, makeAbsorbing(toString(id), priority, defaultNormal(id)));
};

/**
 * Create a periodic ParticleBoundaryLabel
 * @param name The label name
 * @param normal The outward pointing normal of the label
 * @return A shared_ptr to the constructed ParticleBoundaryLabel
 */
inline std::shared_ptr<ParticleBoundaryLabel> makePeriodicParticle(std::string name, int priority, const vec& normal) {
    return std::make_shared<ParticleBoundaryLabel>(name, ParticleBoundaryLabel::PERIODIC, priority, normal);
}

/**
 * Create a periodic ParticleBoundaryLabel with an associated boundary Id
 * @param id The boundary id where this label is to be applied
 * @return A pair of boundary id and shared_ptr to the constructed ParticleBoundaryLabel
 */
inline std::pair<int, std::shared_ptr<ParticleBoundaryLabel>> makePeriodicParticle(BoundaryId id, int priority) {
    return std::make_pair(id, makePeriodicParticle(toString(id), priority, defaultNormal(id)));
};

//TODO: give these arguments better names and maybe a better description
/**
 * Create a solid ParticleBoundaryLabel
 * @param name The label name
 * @param normal The outward pointing normal of the label
 * @param e_roughness_factor Electron roughness factor (0-2)
 * @param e_gamma_at_zero_energy_Emax_normal_incidence Electron gamma at zero energy Emax normal incidence (eV)
 * @param e_energy_at_gamma_max_normal Electron Energy at gamma max normal
 * @param e_gamma_max_normal_direction Electron gammaMax normal direction
 * @param e_emission_threashold Electron emission threshold (electron affinity)(eV)
 * @param e_crossover_energy Electron crossover energy (eV)
 * @param i_Emax_normal_incidence Ion Emax normal incidence (eV)
 * @param i_gammaMax_normal_direction Ion gammaMax normal direction
 * @param i_emission_threshold  Ion emission threshold (eV)
 * @param i_power_law_coeff Ion power law coefficient
 * @param electron_mass electron mass (for secondary electrons)
 * @param electron_charge electron charge (for secondary electrons)
 * @return A shared_ptr to the constructed ParticleBoundaryLabel
 */
inline std::shared_ptr<ParticleBoundaryLabel> makeSolid(std::string name, int priority, const vec& normal,
                                                        Real e_roughness_factor,
                                                        Real e_gamma_at_zero_energy_Emax_normal_incidence,
                                                        Real e_energy_at_gamma_max_normal,
                                                        Real e_gamma_max_normal_direction,
                                                        Real e_emission_threshold,
                                                        Real e_crossover_energy,
                                                        Real i_Emax_normal_incidence,
                                                        Real i_gammaMax_normal_direction,
                                                        Real i_emission_threshold,
                                                        Real i_power_law_coeff,
                                                        Real electron_mass,
                                                        Real electron_charge) {
    std::vector<Real> props = {
            e_roughness_factor,
            e_gamma_at_zero_energy_Emax_normal_incidence,
            e_energy_at_gamma_max_normal,
            e_gamma_max_normal_direction,
            e_emission_threshold,
            e_crossover_energy,
            i_Emax_normal_incidence,
            i_gammaMax_normal_direction,
            i_emission_threshold,
            i_power_law_coeff,
            electron_mass,
            electron_charge
    };
    return std::make_shared<ParticleBoundaryLabel>(name, ParticleBoundaryLabel::SOLID, priority, normal, props);
}

/**
 * Create a solid ParticleBoundaryLabel with an associated boundary Id
 * @param id The boundary id where this label is to be applied
 * @param e_roughness_factor Electron roughness factor (0-2)
 * @param e_gamma_at_zero_energy_Emax_normal_incidence Electron gamma at zero energy Emax normal incidence (eV)
 * @param e_energy_at_gamma_max_normal Electron Energy at gamma max normal
 * @param e_gamma_max_normal_direction Electron gammaMax normal direction
 * @param e_emission_threashold Electron emission threshold (electron affinity)(eV)
 * @param e_crossover_energy Electron crossover energy (eV)
 * @param i_Emax_normal_incidence Ion Emax normal incidence (eV)
 * @param i_gammaMax_normal_direction Ion gammaMax normal direction
 * @param i_emission_threshold  Ion emission threshold (eV)
 * @param i_power_law_coeff Ion power law coefficient
 * @param electron_mass electron mass (for secondary electrons)
 * @param electron_charge electron charge (for secondary electrons)
 * @return A pair of boundary id and shared_ptr to the constructed ParticleBoundaryLabel
 */
inline std::pair<int, std::shared_ptr<ParticleBoundaryLabel>> makeSolid(BoundaryId id, int priority,
                                                                        Real e_roughness_factor,
                                                                        Real e_gamma_at_zero_energy_Emax_normal_incidence,
                                                                        Real e_energy_at_gamma_max_normal,
                                                                        Real e_gamma_max_normal_direction,
                                                                        Real e_emission_threshold,
                                                                        Real e_crossover_energy,
                                                                        Real i_Emax_normal_incidence,
                                                                        Real i_gammaMax_normal_direction,
                                                                        Real i_emission_threshold,
                                                                        Real i_power_law_coeff,
                                                                        Real electron_mass,
                                                                        Real electron_charge) {
    return std::make_pair(id, makeSolid(toString(id), priority, defaultNormal(id), e_roughness_factor,
                                        e_gamma_at_zero_energy_Emax_normal_incidence,
                                        e_energy_at_gamma_max_normal,
                                        e_gamma_max_normal_direction,
                                        e_emission_threshold,
                                        e_crossover_energy,
                                        i_Emax_normal_incidence,
                                        i_gammaMax_normal_direction,
                                        i_emission_threshold,
                                        i_power_law_coeff,
                                        electron_mass,
                                        electron_charge));
}


}

#endif //SESS_PIC_LABEL_H
