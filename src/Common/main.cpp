#include <iostream>
#include "Simulation.h"
#include <mpi.h>

using namespace std;
using namespace picard;

int main(int argc, char *argv[]) {
    if (MPI_Init(&argc, &argv) != MPI_SUCCESS) {
        cerr << "Failed to Initialize MPI! Exiting " << endl;
        return 0;
    }

    if (isRoot()) {
        cout << "Proceeding with " << numThreads() << " processors " << endl;
        if (argc < 2) {
            cerr << "Please include path to configuration file." << endl;
            return 0;
        }
    }

    // Create and run the simulation
    Simulation sim(Options(argv[1], argc, argv));
    sim.run();

    MPI_Finalize();
    exit(EXIT_SUCCESS);
}
