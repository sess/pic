#include "DirectSolver.h"
#include "MultiGridSolver.h"
#include "LeapFrog.h"
#include <deque>
#include <PlasmaBoundary.h>
#include "CoulombCollisions.h"
#include "NeutralCollisions.h"
#include "Options.h"
#include "IOUtils.h"
#include "Interpolator.h"
#include "GmshReader.h"

using namespace std;
using namespace picard;
namespace po = boost::program_options;

const char* Options::_defaultConfig = "default.cfg";

Options::Options(int argc, char** argv) : argc(argc), argv(argv){
    Species::resetSpeciesIndex();
    initializeProgramOptions();
    referenceParams = make_shared<SpeciesReferenceParameters>();
}

Options::Options(string configFile, int argc, char** argv) : Options(argc, argv) {
    setFromConfig(configFile);
}

void Options::setFromConfig(string configFile) {
    vars.clear();
    _commandLineArguments = parseCommandLine(argc, argv);
    _configFileText = parseConfigFile(configFile);
    _defaultFileText = parseConfigFile(_defaultConfig);
    _lastParsedConfig = configFile;
    setVariablesFromParsedConfig();
}

void Options::setVariablesFromParsedConfig() {
    // Verbosity
    if(!vars["verbose"].empty())
        verbose = vars["verbose"].as<bool>();

    // Time-related parameters
    dt = vars["dt"].as<Real>();
    T = vars["T"].as<Real>();
    t = 0; // start the clock at 0;

    // Set reference params
    if(!vars["SimulationParams.wp"].empty())
        referenceParams->setSimPlasmaFrequency(vars["SimulationParams.wp"].as<Real>());
    if(!vars["SimulationParams.q_m"].empty())
        referenceParams->setSimChargeMassRatio(vars["SimulationParams.q_m"].as<Real>());
    if(!vars["SimulationParams.eps0"].empty())
        referenceParams->setSimVacuumPermittivity(vars["SimulationParams.eps0"].as<Real>());

    if(!vars["PhysicalParams.T"].empty())
        referenceParams->setPhysicalTemperature(vars["PhysicalParams.T"].as<Real>());
    if(!vars["PhysicalParams.n"].empty())
        referenceParams->setPhysicalDensity(vars["PhysicalParams.n"].as<Real>());
    if(!vars["PhysicalParams.m"].empty())
        referenceParams->setPhysicalMass(vars["PhysicalParams.m"].as<Real>());
    if(!vars["PhysicalParams.q"].empty())
        referenceParams->setPhysicalCharge(vars["PhysicalParams.q"].as<Real>());
    if(!vars["PhysicalParams.useScaling"].empty())
        referenceParams->setUseScaling(vars["PhysicalParams.useScaling"].as<bool>());

    // Particle Options
    if(!vars["Particles.reference"].empty())
        referenceSpecies = vars["Particles.reference"].as<string>();

    for (auto& specie : vars["Particles.addSpecies"].as<vector<string>>())
        addSpecies(specie);

    for (auto& vel_dist : vars["Particles.velocity_distribution"].as<vector<string>>())
        addVelocityDistribution(vel_dist);

    for (auto& init : vars["Particles.initialize"].as<vector<string>>())
        addInitType(init);

    if(!vars["Particles.spatial_perturbation"].empty())
        for (auto& spat_pert : vars["Particles.spatial_perturbation"].as<vector<string>>())
            addSpatialPerturbation(spat_pert);


    // Mesh parameters
    if(!vars["meshFile"].empty())
        meshFile = vars["meshFile"].as<string>();

    if(!vars["grid"].empty()) {
        if(!meshFile.empty())
            throw runtime_error("Cannot define a grid using a mesh file and the \"grid\" option. Please remove one");

        grid = parseSimpleGrid(vars["grid"].as<string>());
    }

    // Field and particle boundary Labels
    for (auto& label : vars["InteriorProperties.setDomain"].as<vector<string>>())
        fieldLabels.push_back(parseDomainLabel(label));

    for (auto& label : vars["BoundaryConditions.setFieldBC"].as<vector<string>>())
        fieldLabels.push_back(parseFieldLabel(label));

    for (auto& label : vars["BoundaryConditions.setParticleBC"].as<vector<string>>())
        particleBoundaryLabels.push_back(parseParticleBoundaryLabel(label));

    // Fill types
    if (!vars["gridType"].empty())
        gridType = getGridType(vars["gridType"].as<string>());
    if (!vars["fieldSolverType"].empty())
        solverType = getSolverType(vars["fieldSolverType"].as<string>());
    if (!vars["particleMoverType"].empty())
        moverType = getMoverType(vars["particleMoverType"].as<string>());
    if (!vars["interpolatorType"].empty())
        interpolatorType = getInterpolatorType(vars["interpolatorType"].as<string>());

    // Physics
    if (!vars["Particles.interaction"].empty())
        for (auto& interaction : vars["Particles.interaction"].as<vector<string>>())
            addParticleInteraction(interaction);

    if (!vars["BoundaryConditions.plasmaBoundary"].empty())
        for (auto& pb : vars["BoundaryConditions.plasmaBoundary"].as<vector<string>>())
            addPlasmaBoundary(pb);

    // Outputs
    OutputEntry oe_default; // Construct the "default" Output entry with the global output settings

    if(!vars["Output.interval"].empty())
        oe_default.interval = vars["Output.interval"].as<int>();
    if(!vars["Output.start time"].empty())
        oe_default.startTime = vars["Output.start time"].as<Real>();
    if(!vars["Output.end time"].empty())
        oe_default.endTime = vars["Output.end time"].as<Real>();
    if (!vars["Output.directory"].empty())
        oe_default.setBaseDirectory(addSlash(trim(vars["Output.directory"].as<string>()," \t\"\'")));
    if(!vars["Output.suffix"].empty())
        oe_default.suffix = getSuffix(vars["Output.suffix"].as<std::string>());
    if(!vars["Output.precision"].empty())
        oe_default.precision = vars["Output.precision"].as<int>();
    if(!vars["Output.file extension"].empty())
        oe_default.fileExtension = trim(vars["Output.file extension"].as<std::string>()," \t\"\'");
    if(!vars["Output.timestamp folder"].empty())
        oe_default.timestampFolder = vars["Output.timestamp folder"].as<bool>();
    if(!vars["Output.iterations"].empty()) {
        auto iterations = tokenizeVector<int>(vars["Output.iterations"].as<string>());
        oe_default.iterations = set<int>(iterations.begin(), iterations.end());
    }

    // Fill in the specific output entries, using the default values when other values are not specified
    if (!vars["Output.write"].empty()) {
        for (auto& outputOption : vars["Output.write"].as<vector<string>>()) {
            output.add(parseOutput(outputOption, oe_default));
        }
    }

    // Whether or not to do an automated julia analysis at the end of the sim
    if(!vars["Output.analyze"].empty())
        output.setAutomatedAnalysis(vars["Output.analyze"].as<bool>());

    // Location of the main analysis file
    if(!vars["Output.analysisFile"].empty())
        output.setAnalysisFile(vars["Output.analysisFile"].as<std::string>());

    // System calls to be run after the end of the simulation
    if(!vars["Output.systemCall"].empty())
        output.setSystemCalls(expandMultiToken(vars["Output.systemCall"].as<std::vector<string>>()));


    // Additional julia files to include in the analysis
    if(!vars["Output.additionalJuliaFiles"].empty())
        output.setAdditionalJuliaFiles(expandMultiToken(vars["Output.additionalJuliaFiles"].as<std::vector<string>>()));

    // Comments and log options
    if(!vars["Output.comment"].empty())
        comments = vars["Output.comment"].as<std::vector<string>>();

    if(!vars["Output.logFile"].empty())
        logFile = vars["Output.logFile"].as<std::string>();
}

SimpleGrid Options::parseSimpleGrid(std::string grid) const {
    auto tokens = tokenize(grid);
    checkNumArgs(tokens.size(), 4, grid, "Options (parseSimpleGrid)");
    SimpleGrid g;
    g.nDim = cast<size_t>(tokens[0]);
    g.min = tokenizeVector<Real>(tokens[1]);
    g.max = tokenizeVector<Real>(tokens[2]);
    g.pts = tokenizeVector<int>(tokens[3]);
    return g;
}

void Options::addSpecies(std::string speciesStr) {
    auto tokens = tokenize(speciesStr);
    checkNumArgs(tokens.size(), 3, speciesStr, "Options (addTemplate)");
    auto name = tokens[0];
    // Set the reference species to the first species in the config file (this species will be used for computing densities)
    if(referenceSpecies.empty())
        referenceSpecies = name;
    Real mass = stod(tokens[1]);
    Real charge = stod(tokens[2]);
    species[name] = make_shared<Species>(name, mass, charge, referenceParams);
}

void Options::addInitType(std::string initType) {
    auto tokens = tokenize(initType);
    checkNumArgsAtLeast(tokens.size(), 3, initType, "Options (addInitType");
    auto name = tokens[0];
    if (species.find(name) == species.end())
        throw runtime_error("Options (addInitType) Error: Couldn't find a particle type named " + name);

    auto& init = populationInit[species[name]];
    init.type = getInitType(tokens[1]);
    switch (init.type) {
        case TOTAL_COUNT :
            init.count = stoi(tokens[2]);
            break;
        case GAUSSIAN_DENSITY :
            init.count = stoi(tokens[2]);
            break;
        case CARTESIAN :
            init.spatialSamples = tokenizeVector<int>(tokens[2]);
            break;
        case PARTICLE_DENSITY :
            init.density = stod(tokens[2]);
            break;
        case QUIET_START :
            checkNumArgsAtLeast(tokens.size(), 4, initType, "Options (addInitType");
            init.spatialSamples = tokenizeVector<int>(tokens[2]);
            init.velocitySamples = tokenizeVector<int>(tokens[3]);
            break;
        case ON_GRID :
            init.on_grid_density = tokens[2];
            break;
        default:
            throw runtime_error("Options (addInitType) Error: Didn't recognize init type");
    }
}

void Options::addVelocityDistribution(std::string vel_dist) {
    auto tokens = tokenize(vel_dist);
    checkNumArgsAtLeast(tokens.size(), 4, vel_dist, "Options (addVelocityDistribution)");
    auto name = tokens[0];
    if (species.find(name) == species.end())
        throw runtime_error("Options (addVelocityDistribution) Error: Couldn't find a particle named " + name);

    auto type = to_lower(tokens[2]);
    std::shared_ptr<distribution<Real>> dist;
    std::vector<Real> params = vcast<Real>(vector<string>(tokens.begin() + 3, tokens.end()));
    if (type == "maxwellian") {
        checkNumArgs(params.size(), 2, vel_dist, "Options (addVelocityDistribution - maxwellian)");
        dist = maxwellian::fromTemperature(params[0], params[1]);
    } else if (type == "u_maxwellian") {
        checkNumArgs(params.size(), 2, vel_dist, "Options (addVelocityDistribution - maxwellian)");
        dist = make_shared<maxwellian>(params[0], params[1]);
    } else if (type == "uniform") {
        checkNumArgs(params.size(), 2, vel_dist, "Options (addVelocityDistribution - uniform)");
        dist = make_shared<distribution<Real>>(std::uniform_real_distribution<Real>(params[0], params[1]), type, params);
    } else if (type == "beam"){
        checkNumArgs(params.size(), 1, vel_dist, "Options (addVelocityDistribution - beam)");
        dist = make_shared<distribution<Real>>(std::uniform_real_distribution<Real>(params[0], params[0]), type, params);
    } else {
        throw runtime_error("Options (addVelocityDistribution) Error: Didn't recognize distribution type \"" +
                            type + "\"");
    }

    // Set the relevant components of the velocity distribution
    auto dims = tokens[1];
    auto& velDist = species[name]->velocity_distribution;
    for (auto c : vector<char>({'x','y','z'})) {
        if (contains(dims, c)) {
            int dimIndex = charToDim(c);
            velDist.resize(dimIndex + 1);
            velDist[dimIndex] = dist;
        }
    }
}


void Options::addSpatialPerturbation(std::string spatialPert) {
    auto tokens = tokenize(spatialPert);
    checkNumArgs(tokens.size(), 5, spatialPert, "Options (addSpatialPerturbation");
    auto name = tokens[0];
    if (species.find(name) == species.end())
        throw runtime_error("Options (addSpatialPerturbation) Error: Couldn't find a particle type named " + name);

    auto& init = populationInit[species[name]];
    auto dims = tokens[1];
    auto mode = cast<int>(tokens[2]);
    auto amplitude = cast<Real>(tokens[3]);
    auto phase = cast<Real>(tokens[4]);

    multi_function pert;
    for (auto c : vector<char>({'x','y','z'})) {
        if (contains(dims, c)) {
            int dimIndex = charToDim(c);
            pert.fns.resize(dimIndex + 1, zero_fn());
            // Length scale is taken to be one, the input is scaled by Grid length to be between 0 and 1
            pert.fns[dimIndex] = cosine_mode(mode, 1.0, amplitude, phase);
        }
    }
    init.spatial_perturbations.push_back(pert);
}

void Options::addParticleInteraction(std::string interaction) {
    auto tokens = tokenize(interaction);
    checkNumArgsAtLeast(tokens.size(), 3, interaction, "Options (addParticleInteraction)");
    auto type = to_lower(tokens[0]);
    auto s1 = tokens[1];
    if (species.find(s1) == species.end())
        throw runtime_error("Options (addParticleInteraction) Error: Couldn't find a particle named " + s1);

    // Single particle interaction (no other species needed)
    if (type == "simple_electron_neutral_collisions"){
        localPhysics.push_back(
                make_shared<SimpleElectronNeutralCollisions>(species.find(s1)->second, cast<Real>(tokens[2]))
        );
        return;
    }

    // If we are here then we have an interaction b/w particles
    auto s2 = tokens[2];
    if (species.find(s2) == species.end())
        throw runtime_error("Options (addParticleInteraction) Error: Couldn't find a particle named " + s2);

    if (type == "coulomb_collisions"){
        if(s1 != s2) {
            localPhysics.push_back(make_shared<CoulombInterSpeciesCollisions>(
                    species.find(s1)->second, species.find(s2)->second));
            localPhysics.push_back(make_shared<CoulombInterSpeciesCollisions>(
                    species.find(s2)->second, species.find(s1)->second));
        } else{
            throw std::runtime_error("Options (addParticleInteraction) Error: Can't handle intra species collisions currently");
//            localPhysics.push_back(make_shared<CoulombIntraSpeciesCollisions>(species.find(s1)->second));
        }
    } else if (type == "electron_neutral_collisions"){
        checkNumArgs(tokens.size(), 4, interaction, "Options (addParticleInteraction)");
        localPhysics.push_back(
                make_shared<ElectronNeutralCollisions>(
                        species.find(s1)->second,
                        species.find(s2)->second,
                        std::vector<std::map<Real,Real>>({parseToMap(tokens[3])})
                )
        );
    } else if (type == "ion_neutral_collisions"){
        checkNumArgs(tokens.size(), 5, interaction, "Options (addParticleInteraction)");
        localPhysics.push_back(
                make_shared<IonNeutralCollisions>(
                        species.find(s1)->second,
                        species.find(s2)->second,
                        std::vector<std::map<Real,Real>>({parseToMap(tokens[3]), parseToMap(tokens[4])})
                )
        );
    } else
        throw runtime_error("Options (addParticleInteraction) Error: Did not recognize interaction type " + tokens[0]);
}

void Options::addPlasmaBoundary(std::string plasmaBoundary) {
    auto tokens = tokenize(plasmaBoundary);
    checkNumArgs(tokens.size(), 2, plasmaBoundary, "Options (addPlasmaBoundary)");
    auto label_names = tokenizeVector<string>(tokens[0]);
    auto species_names = tokenizeVector<string>(tokens[1]);

    for (auto species_name : species_names) {
        for (auto label_name : label_names) {

            auto species_it = species.find(species_name);
            if (species_it == species.end())
                throw runtime_error(
                        "Options (addPlasmaBoundary) Error: Couldn't find a particle named " + species_name);

            auto species_ptr = species_it->second;
            auto label = findLabelByName(particleBoundaryLabels, label_name);

            globalPhysics.push_back(make_shared<PlasmaBoundary>(label, species_ptr));
        }
    }
}

tuple<string, string, vec, vector<string>> Options::parseLabel(string labelStr) {
    auto tokens = tokenize(labelStr);

    if (tokens.size() < 2)
        throw runtime_error("Options Error (parseLabel): " + labelStr +
                            "could not be parsed into (name, type, normal, properties...");

    auto name = tokens[0];
    auto type = tokens[1];
    if(tokens.size() > 2) {
        auto normal = normalize(tokenizeVector<Real>(tokens[2]));
        vector<string> properties(tokens.begin() + 3, tokens.end());
        return make_tuple(name, type, normal, properties);
    } else{
        auto normal = defaultNormal(getBoundaryId(name));
        return make_tuple(name, type, normal, vector<string>());
    }
}

shared_ptr<FieldLabel> Options::parseDomainLabel(string labelStr){
    auto tokens = tokenize(labelStr);
    checkNumArgs(tokens.size(), 3, labelStr, "Options (parseFieldLabel)");
    return makeInterior(tokens[0], fieldPriority++, stod(tokens[2])*referenceParams->simVacuumPermittivity(),
                        lowerEq(tokens[1], "yes"));
}


shared_ptr<FieldLabel> Options::parseFieldLabel(string labelStr) {
    auto labelParams = parseLabel(labelStr);
    auto newLabel = make_shared<FieldLabel>(
            get<0>(labelParams), // Type string
            get<1>(labelParams), // Name
            fieldPriority++, // Priority
            get<2>(labelParams) // Normal vec
    );
    checkNumArgs(get<3>(labelParams).size(), newLabel->numberOfRequiredArguments(), labelStr,
                 "Options (parseFieldLabel)");

    if (newLabel->type == FieldLabel::TIME_DEPENDENT_DIRICHLET) {
        auto file = get<3>(labelParams)[0];
        newLabel->properties = parse1DVector(file);
        checkNumArgs(newLabel->properties.size(), (int)round(T / dt) + 1, "From file: " + file,
                     "Options (parseFieldLabel)");
    } else {
        newLabel->properties = vcast<Real>(get<3>(labelParams));
    }
    return newLabel;
}

shared_ptr<ParticleBoundaryLabel> Options::parseParticleBoundaryLabel(string labelStr) {
    auto labelParams = parseLabel(labelStr);
    auto newLabel = make_shared<ParticleBoundaryLabel>(
            get<0>(labelParams), // Type string
            get<1>(labelParams), // Name
            particlePriority++, // Priority
            get<2>(labelParams) // Normal vec
    );

    auto properties = get<3>(labelParams);
    if (newLabel->type == ParticleBoundaryLabel::SOLID) {
        checkNumArgs(properties.size(), newLabel->numberOfRequiredArguments() - 1, labelStr,
                     "Options (parseParticleBoundaryLabel)");
        auto electronName = properties[10];
        auto e_it = species.find(electronName);
        if (e_it == species.end())
            throw runtime_error("Options (parseParticleBoundaryLabel) Error: Couldn't find a particle called " +
                                electronName + " for secondary emission");
        std::vector<std::string> float_properties(properties.begin(), prev(properties.end()));
        newLabel->properties = concatenate(vcast<Real>(float_properties), {e_it->second->m, e_it->second->q});

    } else {
        newLabel->properties = vcast<Real>(properties);
    }
    checkNumArgs(newLabel->properties.size(), newLabel->numberOfRequiredArguments(), labelStr,
                 "Options (parseParticleBoundaryLabel)");
    return newLabel;
}


void Options::fillBField(std::shared_ptr<Grid> grid) const {
    if (!vars["InteriorProperties.bField"].empty()) {
        grid->bField = lenientParse2DVector(vars["InteriorProperties.bField"].as<string>(), grid->totalPoints());
        if(grid->bField.cols() != 3)
            cerr << "Warning (Options, bField Parse): bField was found to only have " << grid->bField.cols()
                 << " dimensions. Dimensions up to 3 will be filled in with 0" << endl;
        if(!grid->bField.size())
            throw runtime_error("Options (bField): bField parameter was set but could not be parsed");
    }
}

shared_ptr<Grid> Options::makeGrid() const {
    if(!meshFile.empty()) {
        GmshReader r(meshFile);
        auto grid = r.makeGrid(fieldLabels, particleBoundaryLabels, gridType);
        fillBField(grid);
        return grid;
    } else if(!grid.empty()){
        Domain d(grid.nDim, grid.min, grid.max);
        auto fieldLabelMap = createLabelMap(fieldLabels);
        auto particleLabelMap = createLabelMap(particleBoundaryLabels);
        switch (gridType) {
            case BASIC_GRID :
                return Grid::SingleDomain(d,grid.pts, fieldLabelMap, particleLabelMap);
            case NEUTRALIZING_GRID :
                return NeutralizingGrid::SingleDomain(d, grid.pts, fieldLabelMap, particleLabelMap);
            default:
                throw runtime_error("Options (makeGrid) Error: Did not recognize type");
        }
    }
    return nullptr;
}

shared_ptr<FieldSolver> Options::makeFieldSolver(const Grid& grid) const {
    switch (solverType) {
        case DIRECT_SOLVER :
            return make_shared<DirectSolver>(grid);
        case MULTIGRID_SOLVER :
            return make_shared<MultiGridSolver>(grid);
        default:
            throw runtime_error("Options (makeFieldSolver) Error: Did not recognize type");
    }
}

shared_ptr<ParticleMover> Options::makeParticleMover() const {
    switch (moverType) {
        case LEAP_FROG_INTEGRATOR :
            return make_shared<LeapFrog>();
        default:
            throw runtime_error("Options (setParticleMover) Error: Did not recognize type");
    }
}

shared_ptr<Interpolator> Options::makeInterpolator() const {
    switch (interpolatorType) {
        case LINEAR_INTERPOLATOR :
            return make_shared<Interpolator>();
        default:
            throw runtime_error("Options (setInterpolator) Error: Did not recognize type");
    }
}

Particles Options::makeParticles(const Grid& grid) const {
    Particles particles;
    for (auto& entry : species)
        particles[entry.second] = makeParticleList(entry.second, grid);
    return particles;
}

list <Particle> Options::makeParticleList(std::shared_ptr<Species> species, const Grid& grid) const {
    // Check that initialization exists
    if(populationInit.find(species) == populationInit.end())
        throw runtime_error("Options (makeParticleList) Error: Couldn't find a population initialization entry for " +
                                    species->name);
    list<Particle> parts;

    // Pull initialization out and use it to fill list of particles
    auto& init = populationInit.at(species);
    switch (init.type) {
        case QUIET_START:
            parts = PopulationGenerator::generate_quiet_start_particles(species, grid, init.spatialSamples,
                                                                        init.velocitySamples);
            break;
        case TOTAL_COUNT:
            parts = PopulationGenerator::generate_spatially_uniform_particles(species, grid, init.count);
            break;
        case GAUSSIAN_DENSITY:
            parts = PopulationGenerator::generate_gaussian_density(species, grid, init.count);
            break;
        case CARTESIAN:
            parts = PopulationGenerator::generate_spatially_cartesian_particles(species, grid, init.spatialSamples);
            break;
        case PARTICLE_DENSITY:
            parts = PopulationGenerator::generate_spatially_uniform_particles(species, grid,
                                         (size_t)init.density * grid.activeVolume());
            break;
        case ON_GRID :
            species->grid_density = lenientParse1DVector(init.on_grid_density, grid.totalPoints());
            break;
        default:
            throw runtime_error("Options (makeParticleList) Error: Didn't recognize init type.");
    }

    // Perturb the population using the specified perturbation functions
    PopulationGenerator::perturb_position(grid, parts, init.spatial_perturbations);
    return parts;
}

OutputEntry Options::parseOutput(std::string outputOption, OutputEntry defaultOE) {
    // Parse the options
    auto tokens = tokenize<std::deque>(outputOption);
    checkNumArgsAtLeast(tokens.size(), 1, outputOption, "Options (parseOutput)");

    auto target = getTarget(tokens[0]);
    if(target == UNKNOWN_TARGET)
        throw runtime_error("Options (parseOutput) Error: Did not recognize write target: " + tsfs(target));

    tokens.pop_front(); // Drop the target and you are left with just the options
    OutputEntry oe(defaultOE);
    oe.target = target;
    oe.setDefaultPrefix();

    auto vm = getValueMap(tokens);
    if(vm.find("interval") != vm.end())
        oe.interval = getValue<int>(vm, "interval");
    if(vm.find("prefix") != vm.end())
        oe.prefix = trim(getValue<std::string>(vm, "prefix")," \t\"\'");
    if(vm.find("start time") != vm.end())
        oe.startTime = getValue<Real>(vm, "start time");
    if(vm.find("end time") != vm.end())
        oe.endTime = getValue<Real>(vm, "end time");
    if(vm.find("indices") != vm.end())
        oe.indices = getValue<std::vector<ulong>>(vm, "indices");
    if(vm.find("directory") != vm.end())
        oe.setBaseDirectory(addSlash(trim(getValue<std::string>(vm, "directory"), " \t\"\'")));
    if(vm.find("file extension") != vm.end())
        oe.fileExtension = trim(getValue<std::string>(vm, "file extension")," \t\"\'");
    if(vm.find("suffix") != vm.end())
        oe.suffix = getSuffix(getValue<std::string>(vm, "suffix"));
    if(vm.find("precision") != vm.end())
        oe.precision = getValue<int>(vm, "precision");
    if(vm.find("timestamp folder") != vm.end())
        oe.timestampFolder = getValue<bool>(vm, "timestamp folder");
    if(vm.find("iterations") != vm.end()) {
        auto iterations =  getValue<vector<int>>(vm, "iterations");
        oe.iterations = set<int>(iterations.begin(), iterations.end());
    }
    if(vm.find("skip") != vm.end())
        oe.skip = getValue<bool>(vm, "skip");

    // An interval of 0 throws a floating point excepetion. 0 implies no acceptable interval so set to max_int
    if(oe.interval == 0)
        oe.interval = std::numeric_limits<int>::max();

    return oe;
}


std::string Options::parseConfigFile(string configFile) {
    if(exists(configFile)) {
        po::store(po::parse_config_file<char>(configFile.data(), *descriptions), vars);
        po::notify(vars);
        return readAll(configFile);
    }
    if(configFile != _defaultConfig)
        throw runtime_error("Options (parseConfigFile) Warning:" + configFile + " does not exist");
    return "";
}

std::string Options::parseCommandLine(int argc, char** argv) {
    if(argv) {
        po::store(po::parse_command_line(argc, argv, *descriptions), vars);
        po::notify(vars);
        std::string str;
        for(int i=0; i<argc; i++)
            str += string(argv[i]) + " ";
        return str;
    }
    return "";
}

void Options::initializeProgramOptions() {
    descriptions = make_shared<po::options_description>("PIC options");
    descriptions->add_options()
            ("verbose", po::value<bool>(), "Verbosity of simulation")

            ("dt", po::value<Real>(), "Simulation Timestep")
            ("T", po::value<Real>(), "Simulation End Time")
            ("meshFile", po::value<string>(), "Path to the mesh file")
            ("grid", po::value<string>(), "Simple grid definition - nDim, min, max, pts")
            ("gridType", po::value<string>(), "Type of grid to be used - Basic/Neutralizing")
            ("fieldSolverType", po::value<string>(), "Type of field solver - Direct/Multigrid")
            ("particleMoverType", po::value<string>(),
             "Type of the particle mover - Leap Frog is the only option right now")
            ("interpolatorType", po::value<string>(),
             "Type of the interpolator - \"basic\" is the only option right now")
            ("SimulationParams.wp", po::value<Real>(), "Simulation plasma frequency - defaults to 1")
            ("SimulationParams.q_m", po::value<Real>(), "Simulation charge to mass ratio (of reference species) - defaults to 1")
            ("SimulationParams.eps0", po::value<Real>(), "Simulation vacuum permittivity - defaults to 1")
            ("PhysicalParams.T", po::value<Real>(), "Reference plasma Temperature - defaults to 1 eV")
            ("PhysicalParams.n", po::value<Real>(), "Reference density - defaults to reference species simulation density")
            ("PhysicalParams.m", po::value<Real>(), "Reference mass - defaults to electron mass")
            ("PhysicalParams.q", po::value<Real>(), "Reference charge - defaults to fundamental charge")
            ("PhysicalParams.useScaling", po::value<bool>(), "Wether or not to turn scaling on")
            ("InteriorProperties.setDomain", po::value<vector<string>>()->multitoken(),
             "Option to domain properties including particle permissions and dielectric constant")
            ("InteriorProperties.bField", po::value<string>(),
             "Optional magnetic field setting. It can be a 3 component vector (assigned anywhere) or a file path specifying the bfield at each node point. If unable to parse into one of these then the bfield won't be used")
            ("BoundaryConditions.setFieldBC", po::value<vector<string>>()->multitoken(),
             "Option to set the field boundary condition. BC label, grid label, optional values")
            ("BoundaryConditions.setParticleBC", po::value<vector<string>>()->multitoken(),
             "Option to set the field boundary condition. BC label, grid label, optional values")
            ("BoundaryConditions.plasmaBoundary", po::value<vector<string>>()->multitoken(),
             "Option to add a plasma boundary. BC label, species name")
            ("Particles.reference", po::value<string>(), "The reference species (for density scaling)")
            ("Particles.addSpecies", po::value<vector<string>>()->multitoken(),
             "Add a new particle template -- (name, mass, charge)")
            ("Particles.initialize", po::value<vector<string>>()->multitoken(),
             "how to initialize the species (total number, density, quiet start")
            ("Particles.velocity_distribution", po::value<vector<string>>()->multitoken(),
             "A velocity distribution for a particular direction")
            ("Particles.spatial_perturbation", po::value<vector<string>>()->multitoken(),
             "A sinusoidal perturbation with a specified mode, amplitude and phase")
            ("Particles.interaction", po::value<vector<string>>()->multitoken(), "Interaction physics between particles")
            ("Output.interval", po::value<int>(), "Write interval of output files")
            ("Output.start time", po::value<Real>(), "Start time of writing output files")
            ("Output.end time", po::value<Real>(), "End time of writing output files")
            ("Output.directory", po::value<string>(), "Directory to write output file(s)")
            ("Output.suffix", po::value<string>(), "The suffix of the filenames of the output files. Use time or iteration")
            ("Output.precision", po::value<int>(), "The precision of the floating point simulation data written to disk")
            ("Output.file extension", po::value<string>(), "The file extension used for the files created")
            ("Output.timestamp folder", po::value<bool>(), "Whether to not to nest the output files in a folder marked with timestamp")
            ("Output.iterations", po::value<string>(), "vector of ints representing the iterations to be written")
            ("Output.write", po::value<vector<string>>()->multitoken(),
             "Add a type of output for the simulation. Provide target name and options interval, prefix, start/end time, indices")
            ("Output.comment", po::value<vector<string>>()->multitoken()->composing(),
             "Comments about this particular configuration")
            ("Output.logFile", po::value<string>(), "File path to a csv log file that records settings and results")
            ("Output.analyze", po::value<bool>(), "Whether or not to automatically run the output data through the default analysis suite")
            ("Output.analysisFile", po::value<std::string>(), "location of the main analysis file")
            ("Output.additionalJuliaFiles", po::value<vector<string>>()->multitoken(), "Add julia files for analysis (comma-separated or multiple entries)")
            ("Output.systemCall", po::value<vector<string>>()->multitoken(), "System call to be run after the end of the simulation and analysis")
            ;
}

void Options::writeConfigs(std::string directory) const {
    if(!isRoot())
        return;

    // Write command line options
    if (!_commandLineArguments.empty())
        write(directory + "command_line_args.txt", _commandLineArguments);

    // Write config file contents
    if (!_configFileText.empty() && !_lastParsedConfig.empty())
        write(directory + tokenize(_lastParsedConfig, "\\/").back(), _configFileText);

    // Write default config file contents
    if (!_defaultFileText.empty())
        write(directory + _defaultConfig, _defaultFileText);
}

void Options::deleteConfigs(std::string directory) const {
    if(!isRoot())
        return;

    // Remove command line options
    std::string cmd_args = directory + "command_line_args.txt";
    remove(cmd_args.c_str());

    // Remove config file contents
    if (!_lastParsedConfig.empty()) {
        std::string config_path = directory + tokenize(_lastParsedConfig, "\\/").back();
        remove(config_path.c_str());
    }

    // Remove default config file contents
    std::string default_path = directory + _defaultConfig;
    remove(default_path.c_str());
}

std::vector<std::string> Options::logHeaders() const {
    vector<string> headers;
    for(auto& entry : logEntries())
        headers.push_back(entry.first);
    return headers;
}

std::vector<std::string> Options::logValues() const {
    vector<string> values;
    for(auto& entry : logEntries())
        values.push_back(entry.second);
    return values;
}

std::vector<std::pair<std::string, std::string>> Options::logEntries() const{
    std::vector<std::pair<std::string, std::string>> log = {
            {"Config File", _lastParsedConfig},
            {"Verbose", cast<string>(verbose)},
            {"T", cast<string>(T)},
            {"dt", cast<string>(dt)},
            {"Mesh File", meshFile},
            {"Grid", cast<string>(grid)},
            {"Field Solver Type: ", tsfs(solverType)},
            {"Grid Type", tsfs(gridType)},
            {"Particle Mover Type", tsfs(moverType)},
            {"Interpolator Type", tsfs(interpolatorType)},
            {"SetDomain", stack(vars["InteriorProperties.setDomain"].as<vector<string>>())},
            {"B-Field", vars["InteriorProperties.bField"].empty() ? string() : vars["InteriorProperties.bField"].as<string>()},
            {"Field BCs", stack(vars["BoundaryConditions.setFieldBC"].as<vector<string>>())},
            {"Particle BCs", stack(vars["BoundaryConditions.setParticleBC"].as<vector<string>>())},
            {"Particle Definitions", stack(vars["Particles.addSpecies"].as<vector<string>>())},
            {"Particle Init", stack(vars["Particles.initialize"].as<vector<string>>())},
            {"Particle Velocity Distribution", stack(vars["Particles.velocity_distribution"].as<vector<string>>())},
            {"Log File", logFile},
            {"Comments", stack(comments)}
    };
    return log;
}

std::vector<std::string> Options::expandMultiToken(const std::vector<std::string>& tokens) {
    vector<string> out;
    for(auto& entry : tokens)
        out = concatenate(out, tokenize(entry));
    return out;
}

picard::InitType picard::getInitType(std::string typeStr) {
    typeStr = to_lower(typeStr);
    if (typeStr == "total") {
        return TOTAL_COUNT;
    } else if (typeStr == "density") {
        return PARTICLE_DENSITY;
    } else if (typeStr == "quiet start" || typeStr == "quiet") {
        return QUIET_START;
    } else if (typeStr == "cartesian") {
        return CARTESIAN;
    } else if (typeStr == "on_grid") {
        return ON_GRID;
    } else if (typeStr == "gaussian") {
        return GAUSSIAN_DENSITY;
    } else {
        cerr << "Options (getInitType) Warning: Did not recognize type " + typeStr + ". Returning UNKNOWN_INIT" << endl;
        return UNKNOWN_INIT;
    }
}

Real PopulationInit::computeDensity(const Grid& grid) const {
    auto numCells = grid.totalCells();
    auto vol = grid.activeVolume();

    switch (type) {
        case TOTAL_COUNT :
            return Real(count) / vol;
        case GAUSSIAN_DENSITY :
            return Real(count) / vol;
        case PARTICLE_DENSITY :
            return density;
        case QUIET_START :
            return Real(product(spatialSamples)*product(velocitySamples)*numCells) / vol;
            break;
        case CARTESIAN :
            return Real(product(spatialSamples)*numCells) / vol;
        case ON_GRID :
            throw std::runtime_error("PopulationInit (computeDensity) Error: on_grid option doesn't have a particle density");
            break;
        default :
            throw std::runtime_error("PopulationInit (computeDensity) Error: unrecognized init option");
            break;
    }
}

std::ostream& picard::operator<<(std::ostream& os, InitType type) {
    switch (type) {
        case TOTAL_COUNT :
            os << "Total";
            break;
        case GAUSSIAN_DENSITY :
            os << "Gaussian";
            break;
        case PARTICLE_DENSITY :
            os << "Density";
            break;
        case QUIET_START :
            os << "Quiet Start";
            break;
        case CARTESIAN :
            os << "Cartesian";
            break;
        case ON_GRID :
            os << "On_Grid";
            break;
        default:
            os << "Unknown Particle Initialization Type";
    }
    return os;
}

GridType picard::getGridType(std::string typeStr) {
    typeStr = to_lower(typeStr);
    if (typeStr == "basic") {
        return BASIC_GRID;
    } else if (typeStr == "neutralizing") {
        return NEUTRALIZING_GRID;
    } else {
        cerr << "Options (getGridType) Warning: Did not recognize type " + typeStr + ". Returning UNKNOWN_GRID" << endl;
        return UNKNOWN_GRID;
    }
}

std::ostream& picard::operator<<(std::ostream& os, GridType type) {
    switch (type) {
        case BASIC_GRID :
            os << "Basic";
            break;
        case NEUTRALIZING_GRID :
            os << "Neutralizing";
            break;
        default:
            os << "Unknown Grid";
    }
    return os;
}

FieldSolverType picard::getSolverType(std::string typeStr) {
    typeStr = to_lower(typeStr);
    if (typeStr == "direct") {
        return DIRECT_SOLVER;
    } else if (typeStr == "multigrid") {
        return MULTIGRID_SOLVER;
    } else {
        cerr << "Options (getSolverType) Warning: Did not recognize type " + typeStr +
                ". Returning UNKNOWN_SOLVER" << endl;
        return UNKNOWN_SOLVER;
    }
}

std::ostream& picard::operator<<(std::ostream& os, FieldSolverType type) {
    switch (type) {
        case DIRECT_SOLVER :
            os << "Direct";
            break;
        case MULTIGRID_SOLVER :
            os << "Multigrid";
            break;
        default:
            os << "Unknown Solver";
    }
    return os;
}

ParticleMoverType picard::getMoverType(std::string typeStr) {
    typeStr = to_lower(typeStr);
    if (typeStr == "leap frog") {
        return LEAP_FROG_INTEGRATOR;
    } else {
        cerr << "Options (getMoverType) Warning: Did not recognize type " + typeStr +
                ". Returning UNKNOWN_MOVER" << endl;
        return UNKNOWN_MOVER;
    }
}

std::ostream& picard::operator<<(std::ostream& os, ParticleMoverType type) {
    switch (type) {
        case LEAP_FROG_INTEGRATOR :
            os << "Leap Frog";
            break;
        default:
            os << "Unknown Mover";
    }
    return os;
}

InterpolatorType picard::getInterpolatorType(std::string typeStr) {
    typeStr = to_lower(typeStr);
    if (typeStr == "linear") {
        return LINEAR_INTERPOLATOR;
    } else {
        cerr << "Options (getInterpolatorType) Warning: Did not recognize type " + typeStr +
                ". Returning UNKNOWN_INTERPOLATOR" << endl;
        return UNKNOWN_INTERPOLATOR;
    }
}

std::ostream& picard::operator<<(std::ostream& os, InterpolatorType type) {
    switch (type) {
        case LINEAR_INTERPOLATOR :
            os << "Linear";
            break;
        default:
            os << "Unknown Interpolator";
    }
    return os;
}

std::ostream& picard::operator<<(std::ostream& os, SimpleGrid g) {
    os << g.nDim << ", " << setDelimiter(";") << g.min << setDelimiter(";") << g.max << setDelimiter(";") << g.pts;
    return os;
}


