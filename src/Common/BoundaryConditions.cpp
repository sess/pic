#if _WIN32 || _WIN64
#define _USE_MATH_DEFINES
#include <cmath>
#else
#include <math.h>
#endif


#include "BoundaryCondition.h"
#include "Simulation.h"
#include "Grid.h"
#include "Interpolator.h"
#include "Label.h"

using namespace picard;
using namespace std;


InteractionResults BoundaryConditions::apply(GridIntersection intersection, Particle& p, Grid& grid, const Interpolator& interp) {

    switch (intersection.label->type) {
        case ParticleBoundaryLabel::OPEN :
            return REMOVE_PARTICLE;
        case ParticleBoundaryLabel::REFLECTING :
            return reflecting(p, intersection, grid);
        case ParticleBoundaryLabel::SCATTERING :
            return scattering(p, intersection, grid);
        case ParticleBoundaryLabel::PERIODIC :
            return periodic(p, grid);
        case ParticleBoundaryLabel::ABSORBING :
            return absorbing(p, intersection, grid, interp);
        case ParticleBoundaryLabel::SOLID :
            return solid(p, intersection, grid, interp);
        default:
            throw runtime_error("BoundaryConditions (decode) Error: Didn't recognize label type");
    }
}

InteractionResults BoundaryConditions::reflecting(Particle& p, const GridIntersection& intersection, const Grid& grid) {
    auto restCoeff = intersection.label->properties[0];
    return reflectParticle(p, intersection, restCoeff, {}, grid);
}

InteractionResults BoundaryConditions::scattering(Particle& p, const GridIntersection& intersection, const Grid& grid) {
    auto restCoeff = intersection.label->properties[0];
    vector<Real> reflectAngle = {0.0, 0.0}; //altitude, azimuth (where applicable)

    distribution<Real> randAngle1 = std::uniform_real_distribution<Real>(0.0, M_PI / 2.0);
    distribution<Real> randAngle2 = std::uniform_real_distribution<Real>(0.0, 2.0 * M_PI);
    distribution<Real> R_bin = std::binomial_distribution<int>(1, 0.5);

    if (grid.nDim() == 2) {
        reflectAngle[0] = randAngle1();
        reflectAngle[1] = M_PI *
                          R_bin(); //this angle is either 0 or pi with equal probability, allowing scattering on both sides of the normal
    }
    if (grid.nDim() == 3) {
        reflectAngle[0] = randAngle1();
        reflectAngle[1] = randAngle2();
    }

    return reflectParticle(p, intersection, restCoeff, reflectAngle, grid);
}

InteractionResults BoundaryConditions::periodic(Particle& p, const Grid& grid) {
    //TODO: Make not doubly periodic - as in, can handle one direction periodic, other not.
    //Applied only when the entire main domain boundary is periodic
    //A small number of cases where the particle triggers two boundaries may be mishandled.

    for (int i = 0; i < grid.nDim(); i++) {
        auto domLength = grid.length(i);
        auto remainder = fmod(p.r[i] - grid.min(i), domLength);
        if (remainder < 0.0)
            remainder += domLength;

        p.r[i] = grid.min(i) + remainder;
    }
    return PARTICLE_UPDATED;
}

InteractionResults BoundaryConditions::absorbing(const Particle& p, const GridIntersection& intersection, Grid& grid, const Interpolator& interp) {
    absorbCharge(p.q(), intersection, grid, interp);
    return REMOVE_PARTICLE;
}

InteractionResults BoundaryConditions::solid(Particle& p, const GridIntersection& intersection, Grid& grid,
                                    const Interpolator& interp) {
    if (p.q() < 0)
        return applyElectronDielectricBC(p, intersection, grid, interp);
    else
        return applyIonDielectricBC(p, intersection, grid, interp);
}

InteractionResults
BoundaryConditions::reflectParticle(Particle& p, const GridIntersection& intersection, Real restCoeff, vec reflectAngle,
                                    const Grid& grid) {
    int normalDirection = nonZeroDim(intersection.normal);
    int Ndims = grid.nDim();

    if (intersection.count > 1) {
        //for corners or edges simply point the particle back from where it came - very small errors associated with doing this, ignoring for now.
        p.v = -p.v;
    } else {
        if (reflectAngle.empty()) {
            // Specular reflection of the particle about the wall normal
            for (int iDim = 0; iDim < Ndims; iDim++)
                if (iDim == normalDirection)
                    p.v[iDim] = -restCoeff * p.v[iDim];
        } else {
            // Nonspecular reflection (scattering) about the wall normal
            Real finalSpeed = restCoeff * norm(p.v);
            int iTang = 0;
            for (int iDim = 0; iDim < Ndims; iDim++) {
                if (iDim == normalDirection) {
                    //normal velocity directed away from the wall
                    p.v[iDim] = finalSpeed * cos(reflectAngle[0]) * (-1.0 * sign(intersection.normal[normalDirection]));
                } else {
                    if (iTang == 0) {
                        p.v[iDim] = finalSpeed * sin(reflectAngle[0]) * cos(reflectAngle[1]);
                        iTang++;
                    } else {
                        p.v[iDim] = finalSpeed * sin(reflectAngle[0]) * sin(reflectAngle[1]);
                    }
                }
            }
        }
    }

    //time center after reflection
    auto dist = p.r - intersection.point;
    Real timeOfFlight = norm(dist) / norm(p.v);
    p.r_last = intersection.point;
    p.r = intersection.point + p.v * timeOfFlight;

    if (!grid.isPermitted(p.r)) {
        auto i_nonzero = nonZeroDim(intersection.normal);
//        p.r_last[i_nonzero] = intersection.point[i_nonzero];
        moveEps(p.r_last[i_nonzero], -intersection.normal[i_nonzero]);
        return REAPPLY;
    }
    return PARTICLE_UPDATED;
}

void BoundaryConditions::absorbCharge(Real q, const GridIntersection& intersection, Grid& grid,
                                      const Interpolator& interp) {
    auto Ndims = grid.nDim();
    vector<int> neighborIndex;
    vector<int> tangDir, tempDisplacement = {0, 0, 0};
    int normalDirection = nonZeroDim(intersection.normal);
    tangDir = zeroDims(intersection.normal);

    int lowerLeft = grid.index(intersection.point);
    //populate grid point neighbor index vector
    for (int iTang = 0; iTang < std::pow(2, Ndims - 1); iTang++) {
        int shift0 = iTang % 2; //tangent direction 0 shift
        int shift1 = iTang / 2; //tangent direction 1 shift

        if (Ndims > 1)
            tempDisplacement[tangDir[0]] = shift0;
        if (Ndims > 2)
            tempDisplacement[tangDir[1]] = shift1;

        if (intersection.count == 2 && !grid.isPermitted(intersection.point + grid.position(tempDisplacement)))
            break;

        int nbrIndexTemp = lowerLeft + grid.index(tempDisplacement);
        neighborIndex.push_back(nbrIndexTemp);
    }

    vector<Real> neighborLocation(Ndims);

    for (uint ii = 0; ii < neighborIndex.size(); ii++) {
        neighborLocation = grid.position(neighborIndex[ii]);
        auto sc_it = grid.surfaceChargeMap.find(neighborIndex[ii]);
        if (sc_it != grid.surfaceChargeMap.end()) {
            auto weight = interp.W(neighborLocation, intersection.point, grid.spacing());
            grid.surfaceChargeLocal[sc_it->second] += q * weight * grid.spacing(normalDirection) / grid.cellVolume();
        }
    }
}


/*real findSecondaryEnergy(real R) {

    //finds secondary energy using energy distribution in Taccogna, using Newton Raphson
    real func,der;
    real guess=15.0;
    real err=100.0;
    real tol=1e-3;

    while(fabs(err)>tol)
    {
        func=pow(guess,3)*(1.0-R)+3.0*(1.0-R)*guess*guess-3.0*R*guess-R;
        der=3.0*guess*guess*(1.0-R)+6.0*guess*(1.0-R)-3.0*R;

        err=func/(1.0*der);
        guess=guess-err;

    }

    return guess;

}*/

Real BoundaryConditions::findSecondaryEnergy(Real R) {
    Real guessLower = 0.0;
    Real guessUpper = 60.0;
    Real tol = 1e-3;
    Real midPt = guessLower;
    int Nmax = 20;
    int iter = 0;

    while (iter < Nmax) {
        midPt = 0.5 * (guessLower + guessUpper);

        Real func = 1.0 - R - (3.0 * midPt + 1.0) / (std::pow(midPt + 1.0, 3.0));
        if (fabs(func) < tol || 0.5 * (guessUpper - guessLower) < tol)
            return midPt;

        Real funcLower = 1.0 - R - (3.0 * guessLower + 1.0) / (std::pow(guessLower + 1.0, 3.0));
//        Real funcUpper = 1.0 - R - (3.0 * guessUpper + 1.0) / (pow(guessUpper + 1.0, 3.0));

        if (signbit(funcLower) == signbit(func))
            guessLower = midPt;
        else
            guessUpper = midPt;

        iter++;
    }
    return midPt;
}

list <Particle> BoundaryConditions::injectSecondaryElectrons(int numberofSecondaries,
                                                             const Particle& p, const GridIntersection& intersection,
                                                             const Grid& grid, const Interpolator& interp) {

    Particle templateElectron;/*(intersection.label->properties[10], intersection.label->properties[11])*/;
    // TODO: get the electron from the species list
    int numberInjected = 0;
    int Ndims = grid.nDim();
    int normalDirection = nonZeroDim(intersection.normal);

    Real eSec;
    Real phiAvg;
    Real electronAffinity = intersection.label->properties[4];

    list <Particle> insertedSecondaries;

    auto dist = p.r - intersection.point;
    auto timeOfFlight = norm(dist) / norm(p.v);

    distribution<Real> R = std::uniform_real_distribution<Real>(0.0, 1.0);
    distribution<Real> R_bin = std::binomial_distribution<int>(1, 0.5);
    vector<Real> secondaryAngles = {0.0, 0.0};
    phiAvg = interp.scalarFieldValue(intersection.point, grid.potential, grid);

    for (int iSec = 0; iSec < numberofSecondaries; iSec++) {
        eSec = findSecondaryEnergy(R()) * electronAffinity;

        //only generate positive total energy secondaries
        if (eSec + sign(templateElectron.q()) * phiAvg >= 0) {
            if (Ndims > 1) {
                secondaryAngles[0] = asin(R());
                //this angle is either 0 or pi with equal probability, allowing scattering on both sides of the normal
                secondaryAngles[1] = M_PI * R_bin();
            }
            if (Ndims > 2)
                secondaryAngles[1] = 2.0 * M_PI * R();

            Real secSpeed = std::sqrt(2.0 * eSec / templateElectron.m() * abs(templateElectron.q()));
            Particle insertElectron = templateElectron;
            insertElectron.r = intersection.point;

            if (intersection.count == 1) {
                int iTang = 0;
                //particle reinjected at wall location with velocity vector rotated about wall normal
                for (int iDim = 0; iDim < Ndims; iDim++) {
                    if (iDim == normalDirection) {
                        Real direction = -sign(intersection.normal[normalDirection]);
                        insertElectron.v[iDim] = secSpeed * cos(secondaryAngles[0]) * (direction);
                    } else {
                        if (iTang == 0) {
                            insertElectron.v[iDim] = secSpeed * sin(secondaryAngles[0]) * cos(secondaryAngles[1]);
                            iTang++;
                        } else
                            insertElectron.v[iDim] = secSpeed * sin(secondaryAngles[0]) * sin(secondaryAngles[1]);

                    }
                }
            } else {
                Real ePrim = 0.5 * p.m() / fabs(p.q()) * dot(p.v, p.v);
                for (int iDim = 0; iDim < Ndims; iDim++) {
                    insertElectron.v[iDim] = -p.v[iDim];
                    insertElectron.v[iDim] *= eSec / ePrim; //inject secondaries at the decided secondary energy
                }
            }
            insertElectron.r_last = intersection.point;

            //update position of the secondary electron since it was released at the moment of impact
            insertElectron.r = intersection.point + insertElectron.v * timeOfFlight;

            insertedSecondaries.push_back(insertElectron);
            numberInjected++;
        } else {
            cout << "Not injected : " << eSec << " " << sign(templateElectron.q()) << " " << phiAvg << " ";\
            cout << "location: " << intersection.point << endl;
        }
    }

    for (auto p_it = insertedSecondaries.begin(); p_it != insertedSecondaries.end();) {
        if (!grid.isPermitted(p_it->r)) {
            p_it = insertedSecondaries.erase(p_it);
        } else {
            p_it++;
//            interp.particleToGrid(*p_it, electronSpeciesIndex, grid); //TODO can we ignore this? seems inconsistent because order of particle processing would matter. Now the interpolation happens outside of the particle loop so it really doesnt work
        }
        /*Secondary is assumed to have left the domain otherwise, this is an approximation. The wall next to the
         * ejecting surface could be absorbing or reflecting, but that implementation requires a list to be resized
         * every time the injectSecondary procedure is called, which is expensive.
         */
    }

    return insertedSecondaries;
}

InteractionResults BoundaryConditions::applyElectronDielectricBC(Particle& p, const GridIntersection& intersection,
                                                        Grid& grid, const Interpolator& interp) {
    Particle templateElectron; /*(intersection.label->properties[10], intersection.label->properties[11])*/;
    //TODO: Get the template electron from the species list
    vector<int> tangDir = zeroDims(intersection.normal);
    int normalDirection = nonZeroDim(intersection.normal);
    int Ndims = grid.nDim();

    Real roughness = intersection.label->properties[0];
    Real gammaAtZeroEnergy = intersection.label->properties[1];
    Real eMaxNormal = intersection.label->properties[2];
    Real gammaMaxNormal = intersection.label->properties[3];
    Real emissionThreshold = intersection.label->properties[4];
    Real crossoverEnergy = intersection.label->properties[5];
    Real primaryEnergy = sum(0.5 * p.m() * (p.v * p.v) / fabs(p.q()));

    Real tangSpeed = 0.0;
    for (uint iDim = 0; iDim < tangDir.size(); iDim++)
        tangSpeed += std::pow(p.v[tangDir[iDim]], 2);
    tangSpeed = std::sqrt(tangSpeed);

    Real incidenceAngle = atan2(tangSpeed, fabs(p.v[normalDirection]));
    Real angleFactorEmax = 1.0 + roughness / M_PI * incidenceAngle * incidenceAngle;
    Real angleFactorGmax = 1.0 + 0.5 * roughness / M_PI * incidenceAngle * incidenceAngle;
    Real eMaxAngle = eMaxNormal * angleFactorEmax;
    Real gammaMaxAngle = gammaMaxNormal * angleFactorGmax;

    Real v = std::max((primaryEnergy - emissionThreshold) / (eMaxAngle - emissionThreshold), (Real) 0.0);
    Real k = 0.62 * (primaryEnergy < eMaxAngle) + 0.25 * (primaryEnergy >= eMaxAngle);

    Real gammaV = gammaMaxAngle * std::pow(v * exp(1.0 - v), k); //Vaughan Gamma
    Real gammaE = 0.03 * gammaV + gammaAtZeroEnergy * exp(-primaryEnergy / crossoverEnergy);
    Real gammaI = 0.07 * gammaV;
    Real gammaT = 0.9 * gammaV;
    Real gammaTot = gammaE + gammaI + gammaT;

    distribution<Real> R = std::uniform_real_distribution<Real>(0.0, 1.0);
    distribution<Real> R_bin = std::binomial_distribution<int>(1, 0.5);

    vector<Real> scatterAngle;
    if (Ndims == 2) {
        scatterAngle.push_back(0.5 * M_PI * R());
        scatterAngle.push_back(M_PI * R_bin()); //azimuth either 0 or pi
    }
    if (Ndims == 3) {
        scatterAngle.push_back(
                0.5 * M_PI * R()); //Altitude scatter angle - 0 to pi/2 (angle measured from the surface normal)
        scatterAngle.push_back(2.0 * M_PI * R()); //azimuth scatter angle - 0 to 2 pi
    }

    Real randomNumber = R();
    if (randomNumber > gammaE && randomNumber < gammaE + gammaI) {
        // NOTE: Removed charge absorbtion code here because I don't think it was doing anything
        // Inject inelastically scattered electron
        return reflectParticle(p, intersection, R(), scatterAngle, grid);
    } else if (randomNumber < gammaE) {
        // NOTE: Removed charge absorbtion code here because I don't think it was doing anything
        //Elastically scatter electron
        return reflectParticle(p, intersection, 1.0, scatterAngle, grid);
    } else if (gammaTot < 1.0) {
        //Inject true secondary (probabilisticly to get decimal)
        if (R() < gammaTot) {
            auto injected = injectSecondaryElectrons(1, p, intersection, grid, interp);
            absorbCharge(p.q() - templateElectron.q(), intersection, grid, interp); // Losing electrons from surface
            return InteractionResults(REMOVE_PARTICLE, injected);
        }

        //gained one electron, lost one, no change in charge
        return REMOVE_PARTICLE;
    } else if (gammaTot >= 1.0) {
        // Inject gammaTot electrons (probablistically inject electron to get the decimal)
        int toInject = (int) gammaTot;
        if (R() < gammaTot - (int) gammaTot)
            toInject++;

        auto injected = injectSecondaryElectrons(toInject, p, intersection, grid, interp);
        absorbCharge(p.q() - templateElectron.q() * injected.size(), intersection, grid, interp);

        return InteractionResults(REMOVE_PARTICLE, injected);
    } else{
        throw runtime_error("Error in applyElectronDielectricBC: conditions did not match any if statement");
    }
}


InteractionResults BoundaryConditions::applyIonDielectricBC(Particle& p, const GridIntersection& intersection,
                                                   Grid& grid, const Interpolator& interp) {
    Particle templateElectron; /*(intersection.label->properties[10], intersection.label->properties[11])*/;
    //TODO: Get the template electron from the species list
    auto tangDir = zeroDims(intersection.normal);
    int normalDirection = nonZeroDim(intersection.normal);

    Real eMaxNormal = intersection.label->properties[6];
    Real gammaMaxNormal = intersection.label->properties[7];
    Real emissionThreshold = intersection.label->properties[8];
    Real powerLawCoefficient = intersection.label->properties[9];
    Real primaryEnergy = sum(0.5 * p.m() * (p.v * p.v) / fabs(p.q())); //Primary energy in simulation eV

    Real tangSpeed = 0.0;
    for (uint iDim = 0; iDim < tangDir.size(); iDim++)
        tangSpeed += std::pow(p.v[tangDir[iDim]], 2);
    tangSpeed = std::sqrt(tangSpeed);


    Real incidenceAngle = atan2(tangSpeed, fabs(p.v[normalDirection]));
    Real maxangle = M_PI / 2.01;
    incidenceAngle = std::min(incidenceAngle, maxangle); // handling the singularity of grazing incidence
    Real gammaRatio = 2.0 * std::pow(primaryEnergy / eMaxNormal, powerLawCoefficient) / (1.0 + primaryEnergy / eMaxNormal);
    Real gamma = 0 * (primaryEnergy < emissionThreshold) +
                 gammaRatio * gammaMaxNormal / cos(incidenceAngle) * (primaryEnergy >= emissionThreshold);


    distribution<Real> R = std::uniform_real_distribution<Real>(0.0, 1.0);
    if (gamma >= 1) {
        // Inject gammaTot electrons (probablistically inject electron to get the decimal)
        int toInject = (int) gamma;
        if (R() < gamma - (int) gamma)
            toInject++;

        auto injected = injectSecondaryElectrons(toInject, p, intersection, grid, interp);
        absorbCharge(p.q() - templateElectron.q() * injected.size(), intersection, grid, interp);
        return InteractionResults(REMOVE_PARTICLE, injected);

    } else {
        if (R() < gamma) {
            auto injected = injectSecondaryElectrons(1, p, intersection, grid, interp);
            absorbCharge(p.q() - templateElectron.q(), intersection, grid, interp);
            return InteractionResults(REMOVE_PARTICLE, injected);
        }
        return REMOVE_PARTICLE;
    }
}

