#include <fstream>
#include <mpi.h>
#include <boost/program_options.hpp>
#include <boost/algorithm/string.hpp>
#include "Simulation.h"
#include "Interpolator.h"
#include "ParticleMover.h"
#include "FieldSolver.h"
#include "Physics.h"

using namespace std;
using namespace picard;

Simulation::Simulation(const Options& options){
    setup(options);
}

void Simulation::setup(const Options& options) {
    tic("setup");
    Species::resetSpeciesIndex();
    _options = options;

    // Verbosity
    setVerbose(options.verbose);

    // Timing
    _dt = options.dt;
    _T = options.T;
    _t = options.t;
    _iteration = options.iteration;

    if(shouldSpeak())
        cout << "Constructing Grid, Field Solver, Particle Mover and Interpolator from Options..." << endl;

    // Objects
    _grid = options.makeGrid();
    if(!_grid) {
        if (shouldSpeak())
            cout << "No grid was found. skipping the rest of setup" << endl;
        toc();
        return;
    }
    scaleParticles(options);

    _solver = options.makeFieldSolver(grid());
    _mover = options.makeParticleMover();
    _interpolator = options.makeInterpolator();

    if(shouldSpeak())
        cout << "Making particles in domain..." << endl;

    // Particles
    _particles = options.makeParticles(grid());
    _species = getSpeciesMap(_particles);
    _referenceParams = options.referenceParams;


    auto s_count = species_count(); // Outside of verbose() because all threads need to execute
    if(shouldSpeak()){
        cout << "Created:" << endl;
        for(auto& entry : s_count)
            cout << "    " << entry.second << " " << entry.first->name << "s" << endl;
    }

    if(shouldSpeak())
        cout << "Building physics modules from options..." << endl;

    // Physics
    _localPhysics = options.localPhysics;
    _globalPhysics = options.globalPhysics;

    // Output
    _output = options.output;

    if(shouldSpeak())
        cout << "Simulation setup complete!" << endl;

    toc();
}

//TODO This needs improvement. was in options but didnt have access to all of the reduction functions need to combine across threads
void Simulation::scaleParticles(const Options& options) {

    // Set the density of the reference species
    auto refSpeciesPtr = options.species.at(options.referenceSpecies);
    auto& init = options.populationInit.at(refSpeciesPtr);
    options.referenceParams->setSimDensity(init.computeDensity(grid()));

    // Update the mass and charge of the species traits TODO: This is ugly and kind of confusing, can we find a way of removing?
    for (auto& entry : options.species) {
        entry.second->updateMassCharge();
        for(auto d : entry.second->velocity_distribution.getDistributions()) {
            auto m = dynamic_pointer_cast<maxwellian>(d);
            if(m)
                m->convertTempToVelocity(entry.second->m, entry.second->reference->simKb());
        }

    }
}

void Simulation::message() {
    cout << setFloatPrecision(3) << 100.0*time()/endTime() << "% (iteration: " << iteration() << " time: " << t() << ")" << endl;
}

void Simulation::prepare() {
    tic("prepare");
    if(shouldSpeak())
        cout << "Preparing output directories..." << endl;

    // Setup output folder with timestep if required
    _output.addTimestampFolderToDirectory(timestamp());
    _output.makeDirectories(); // make all of the required output directories
    _output.clearSuffixFreeContents(); // Clear the output files that are going to be appended to
    writeConfigs();

    if(shouldSpeak())
        cout << "Allocating Grid Data..." << endl;

    // Allocate Grid data
    _grid->allocateGridData(_particles.size());

    if(shouldSpeak())
        cout << "Preparing Initial Grid state Data..." << endl;

    // Prepare the charge density and other particle-averaged quantities
    _grid->zeroOutFields();
    particleToGrid();

    reduce(); // reduce to get the net charge distribution on the grid

    _grid->setupChargeDensity(); // Setup grid if there is a fixed background

    solveFields(); // Solve for the fields on the grid due to the initial charge density

    for(auto& p : _localPhysics)
        p->setup(*this);
    for(auto& p : _globalPhysics)
        p->setup(*this);

    for(auto phys : _localPhysics)
        phys->particleToGrid(*this);

    if(shouldSpeak())
        cout << "Writing Initial Output..." << endl;

    writeOutput(); // Write the output for timestep 0

    if(shouldSpeak())
        message(); // Write the timestep message for the zeroth timestep
    toc();
}

void Simulation::particleToGrid() {
    tic("particle to grid");
    vector2d<Real> sumOfWeights = vector2d<Real>::Constant(_grid->totalPoints(), _particles.size(), 0);

    for(auto& species : _particles){
        for(auto& p : species.second){
            _interpolator->particleToGrid(p, species.first->id, *_grid, &sumOfWeights);
        }
    }

    if(MPI_init_called()) {
        // Reduce average velocity, average sq velocity, and sumOfWeights arrays
        for (auto s = 0; s < _particles.size(); s++) {
            check_MPI_call(
                    MPI_Allreduce(MPI_IN_PLACE, _grid->_averageVelocity[s].data(), _grid->_averageVelocity[s].size(),
                                  PIC_MPI_REAL, MPI_SUM, MPI_COMM_WORLD),
                    "Average Velocity Reduction"
            );

            check_MPI_call(
                    MPI_Allreduce(MPI_IN_PLACE, _grid->_averageSqVelocity[s].data(), _grid->_averageSqVelocity[s].size(),
                                  PIC_MPI_REAL, MPI_SUM, MPI_COMM_WORLD),
                    "Average Sq Velocity Reduction"
            );
        }

        check_MPI_call(
                MPI_Allreduce(MPI_IN_PLACE, sumOfWeights.data(), sumOfWeights.size(),
                              PIC_MPI_REAL, MPI_SUM, MPI_COMM_WORLD),
                "Sum of Weights Reduction"
        );
    }


    // Normalize by the weights to get the average
    for (uint i = 0; i < _grid->totalPoints(); i++) {
        for (int speciesIndex = 0; speciesIndex < _particles.size(); speciesIndex++) {
            if(sumOfWeights(i,speciesIndex) != 0) {
                _grid->_averageVelocity[speciesIndex].row(i) /= sumOfWeights(i, speciesIndex);
                _grid->_averageSqVelocity[speciesIndex].row(i) /= sumOfWeights(i, speciesIndex);
            }
        }
    }

    // Compute std. deviation of velocity once the mean has been computed
    for(auto& species : _particles){
        for(auto& p : species.second){
            auto cell = _grid->getEnclosingCell(p.r);
            for (auto i : cell) {
                auto w = _interpolator->W(_grid->position(i), p.r, _grid->spacing());
                for(auto n =0; n<_grid->nDim(); n++) {
                    auto diff = p.v[n] - _grid->_averageVelocity[species.first->id](i,n);
                    if(sumOfWeights(i,species.first->id) != 0)
                        _grid->_velocityVariance[species.first->id](i,n) += w * (diff * diff) / sumOfWeights(i,species.first->id);
                }
            }
        }

        if(MPI_init_called()) {
            // Reduce the velocity variance
            check_MPI_call(
                    MPI_Allreduce(MPI_IN_PLACE, _grid->_velocityVariance[species.first->id].data(),
                                  _grid->_velocityVariance[species.first->id].size(),
                                  PIC_MPI_REAL, MPI_SUM, MPI_COMM_WORLD),
                    "Velocity Variance Reduction"
            );
        }
    }

    for(auto s : particles()) {
        auto species = s.first;
        for (int i = 0; i < _grid->totalPoints(); i++) {
            vec temp = species->temperature(_grid->v_var(species, i));
            for (int n = 0; n < _grid->nDim(); n++) {
                _grid->_temperature[species->id](i, n) = temp[n];
            }
        }
    }
    toc();
}

void Simulation::solveFields() {
    tic("solve fields");
    _solver->findPotential(_t,_dt, *_grid);
    _solver->findElectricField(*_grid);
    toc();
}

void Simulation::writeOutput() {
    tic("write output");
    _output.write(*this);
    toc();
}

void Simulation::increment() {
    _iteration ++;
    _t = _iteration*_dt;
}

void Simulation::run() {

    tic("total run");
    setTimestamp();

    prepare(); // Setup anything that needs to be done before the first simulation iteration

    if(shouldSpeak())
        cout << "Stepping..." << endl;

    // Iterate the simulation until the end time is reached
    //TODO: Uncomment
//    try {
        while (fgeq(_T - _dt, _t)) {
            step();
        }
//    } catch(const runtime_error& error){
//        _error_message = error.what();
//        cerr << "Error! " << _error_message << endl;
//    }

    //TODO add a post-processing step here?

    _system_end_time = std::chrono::system_clock::now();
    toc();
    log();

    tic("analyze");
    if(_output.automatedAnalysis())
        _output.analyze(*this);
    _output.runSystemCalls();

    toc();

    logProfileResults();
    if(shouldSpeak())
        cout << profileResults() << endl;
}

void Simulation::step() {
    for(auto phys : _localPhysics)
        phys->resetTimestep();

    int speciesIndex = 0;
    // Loop through each species in the simulation
    for (auto& species : _particles) {

        auto& pList = species.second;
        auto p_iter = pList.begin();

        // Iteration through each particle of this species
        while (p_iter != pList.end()) {

            Particle& p = *p_iter;

            tic("grid to particle");
            // Interpolate the fields
            _interpolator->gridToParticle(p, *_grid, *_mover, _grid->hasBField());
            toc();

            tic("local physics");
            // Apply Physics to the particle.
            for (auto phys : _localPhysics)
                phys->apply(p, *this);
            toc();

            tic("particle mover");
            _mover->step(p, _dt, _t, _grid->nDim(), _grid->hasBField());
            toc();

            tic("bcs");
            // Apply boundary conditions
            if (applyBoundaryConditions(p) == REMOVE_PARTICLE)
                p_iter = pList.erase(p_iter);
            else
                p_iter++;
            toc();
        }
        speciesIndex++;
    }


    // Apply the timestep physics
    for (auto phys : _globalPhysics)
        phys->apply(*this);

    _grid->zeroOutFields();
    particleToGrid();

    reduce();
    solveFields();

    for(auto phys : _localPhysics)
        phys->particleToGrid(*this);

    increment();
    writeOutput();
    if(shouldSpeak())
        message();
}

ParticleStatus Simulation::applyBoundaryConditions(Particle& p) {
    int attempt = 0;
    InteractionResults bcResults;
    do {
        // Check if the particle intersects the grid or any subdomain
        auto intersection = _grid->intersectsBoundary(p.ray());

        // Process the intersection (multiple times if necessary) and add any injected particles
        if (intersection) {
            bcResults = BoundaryConditions::apply(intersection, p, *_grid, *_interpolator);
            for (auto& newp : bcResults.particlesToAdd)
                _particles[newp.species_ptr()].push_front(newp);
        }

    }  while (bcResults.status == REAPPLY && attempt++ < maxBCTries);
    return bcResults.status;
}

void Simulation::reduce(){
    if(!MPI_init_called())
        return;

    // Reduce the charge density on the grid
    check_MPI_call(
            MPI_Allreduce(MPI_IN_PLACE, _grid->chargeDensity.data(), _grid->chargeDensity.size(),
                          PIC_MPI_REAL,MPI_SUM,MPI_COMM_WORLD),
            "Charge Density Reduction"
    );

    check_MPI_call(
            MPI_Allreduce(MPI_IN_PLACE, _grid->numberDensity.data(), _grid->numberDensity.size(),
                          PIC_MPI_REAL,MPI_SUM,MPI_COMM_WORLD),
            "Number Density Reduction"
    );

    // Reduce the surface charge density
    check_MPI_call(
            MPI_Allreduce(_grid->surfaceChargeLocal.data(), _grid->surfaceChargeGlobal.data(),
                          _grid->surfaceChargeLocal.size(), PIC_MPI_REAL, MPI_SUM, MPI_COMM_WORLD),
            "Surface Charge Density Reduction"
    );
}

std::vector<Real> Simulation::kinetic_energy() const {
    return global_accumulate_species("Kinetic Energy", _particles, &Particle::kineticEnergy);
}

std::vector<Real> Simulation::potential_energy() const {
    return global_accumulate_species("Potential Energy", _particles, &Particle::potentialEnergy, grid(), interpolator());
}

std::vector<Real> Simulation::total_energy() const {
    return global_accumulate_species("Total Energy", _particles, &Particle::totalEnergy, grid(), interpolator());
}

size_t Simulation::particle_count() const {
    size_t local_count = local_particle_count();
    if(!MPI_init_called())
        return local_count;

    size_t global_count;
    check_MPI_call(
            MPI_Allreduce(&local_count, &global_count, 1, MPI_UNSIGNED_LONG, MPI_SUM, MPI_COMM_WORLD),
            "Total Particle Count Reduction"
    );

    return global_count;
}

size_t Simulation::local_particle_count() const {
    size_t particle_count = 0;
    for(auto& species : _particles)
        particle_count += species.second.size();
    return particle_count;
}

std::map<SpeciesPtr, size_t> Simulation::species_count() const {
    std::vector<size_t> local_sc(numSpecies());
    auto global_sc = local_sc;

    // Create local array of sizes (more efficient for large arrays)
    int index = 0;
    for (auto& species : _particles)
        local_sc[index++] = species.second.size();

    if(MPI_init_called()) {
        // Reduce the array
        check_MPI_call(
                MPI_Allreduce(local_sc.data(), global_sc.data(), numSpecies(), MPI_UNSIGNED_LONG, MPI_SUM, MPI_COMM_WORLD),
                "Species Count Reduction"
        );
    }

    // Convert to map for easy access
    std::map<SpeciesPtr, size_t> species_count;
    index = 0;
    for (auto& species : _particles)
        species_count[species.first] = global_sc[index++];

    return species_count;
}

size_t Simulation::species_count(SpeciesPtr species) const {
    auto it = _particles.find(species);
    if(it == _particles.end())
        throw runtime_error("Simulation (species_count) Error: Couldn't find species");

    size_t local_sc = it->second.size();
    if(!MPI_init_called())
        return local_sc;

    size_t global_count;
    check_MPI_call(
            MPI_Allreduce(&local_sc, &global_count, 1, MPI_UNSIGNED_LONG, MPI_SUM, MPI_COMM_WORLD),
            "Single species count reduction"
    );
    return global_count;
}

Real Simulation::particle_fraction() const {
    return (Real) local_particle_count() / (Real) particle_count();
}

void Simulation::writeConfigs() const {
    for(auto& dir : _output.directories()){
        _options.writeConfigs(dir + addSlash(OutputEntry::infoFolder));
    }
}

void Simulation::removeConfigs() const {
    for(auto& dir : _output.directories()){
        _options.deleteConfigs(dir + addSlash(OutputEntry::infoFolder));
    }
}

void Simulation::cleanupOutput() const {
    _output.removeOutputFiles();
    removeConfigs();
    // Remove profiling results
    for(auto& dir : _output.directories()) {
        string profile_results_file = dir + addSlash(OutputEntry::infoFolder) + "profile_results.csv";
        remove(profile_results_file.c_str());
    }
    _output.removeDirectories();
}

void Simulation::setTimestamp() {
    _system_start_time = std::chrono::system_clock::now();
    auto epoch = _system_start_time.time_since_epoch();
    auto millicast = std::chrono::duration_cast<std::chrono::milliseconds>(epoch);
    _timestamp = millicast.count();
    if(MPI_init_called())
        check_MPI_call(MPI_Bcast(&_timestamp, 1, MPI_LONG, 0, MPI_COMM_WORLD), "Simulation::setTimestamp Bcast");
}

long Simulation::duration() const {
    return std::chrono::duration_cast<std::chrono::milliseconds>(_system_end_time - _system_start_time).count();
}

void Simulation::log() const {
    if(!isRoot() || _options.logFile.empty() )
        return;

    if(isEmpty(_options.logFile)) // Write the headers of the log file
        append(_options.logFile, logHeaders(), setDelimiter(", ") << setEnd("\n"));

    append(_options.logFile, logValues(), setDelimiter(", ") << setEnd("\n") << setElementWrapping("\"", "\"")); // Write the log file values
}

void Simulation::logProfileResults() const {
#ifdef ENABLE_PROFILING
    for(auto& dir : _output.directories()){
        std::string res = profileResults();
        if(!res.empty())
            write(dir + addSlash(OutputEntry::infoFolder) + "profile_results.csv", profileResultsCSV());
    }
#endif
}

std::vector<std::string> Simulation::logHeaders() const {
    vector<string> headers;
    for(auto& entry : logEntries())
        headers.push_back(entry.first);

    return concatenate(headers, _options.logHeaders());
}

std::vector<std::string> Simulation::logValues() const {
    vector<string> values;
    for(auto& entry : logEntries())
        values.push_back(entry.second);
    return concatenate(values, _options.logValues());
}

std::vector<std::pair<std::string, std::string>> Simulation::logEntries() const {
    auto startT = chrono::system_clock::to_time_t(_system_start_time);
    auto endT = chrono::system_clock::to_time_t(_system_end_time);
    std::vector<std::pair<std::string, std::string>> log = {
            {"Timestamp", to_string(timestamp())},
            {"Processors", to_string(numThreads())},
            {"Wall Time Start", trim(tsfs(ctime(&startT)),"\n")},
            {"Wall Time End", trim(tsfs(ctime(&endT)), "\n")},
            {"Wall Time Duration", to_string(duration()) + " ms"},
            {"Iterations", to_string(iteration())},
            {"Simulation Time at completion", to_string(time())},
            {"Error Message", _error_message}
    };
    return log;
}

std::vector<std::string> Simulation::species_names() const{
    vector<string> names(numSpecies(), "unknown");
    for(auto& entry : _species)
        names[entry.first] = entry.second->name;
    return names;
}

