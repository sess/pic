#ifndef SCALING_H
#define SCALING_H

namespace picard{

//TODO: Right now all of the scaling assumes that we are using the electron mass as the scaling mass. We may want to make this an option

// Common plasma physical constants
constexpr double ELECTRON_MASS = 9.11e-31; // kg
constexpr double VACUUM_PERMITTIVITY = 8.85e-12; // F/m
constexpr double FUNDAMENTAL_CHARGE = 1.602e-19; // Coulomb
constexpr double BOLTZMANN_CONSTANT = 1.38e-23; // J/K
constexpr double EV_TO_KELVIN = 1.160452e4; // eV/K

inline Real eV_to_Kelvin(Real T){ return T*EV_TO_KELVIN; }

// Common plasma parameter expressions
inline Real plasma_frequency(Real n, Real e, Real m, Real eps0){
    return std::sqrt(n*e*e/(m*eps0));
}

inline Real debye_length(Real eps0, Real k, Real T, Real n, Real e){
    return std::sqrt(eps0*k*T/(n*e*e));
}

inline Real thermal_velocity(Real k, Real T, Real m){
    return std::sqrt(2.0*k*T/m);
}

struct SpeciesReferenceParameters {
public:
    // Default values are set below
    SpeciesReferenceParameters() { recalculate(); }

    //Functions for getting simulation/physical/scaling parameters
    Real simPlasmaFrequency() const { return _omega_p_sim; }
    Real simVacuumPermittivity() const { return _eps0_sim; }
    Real simChargeMassRatio() const { return _q_m_sim; }
    Real simDensity() const { return _n_sim; }
    Real simCharge() const { return _q_sim; }
    Real simMass() const { return _m_sim; }
    Real simKb() const { return _kb_sim; }
    Real physicalTemperature() const { return _T_phys; }
    Real physicalDensity() const { return _n_phys; }
    Real physicalMass() const { return _m_phys; }
    Real physicalCharge() const { return _q_phys; }
    Real energyScale() const { return _energy_scale; }
    Real energyScaleEV() const { return _energy_scale_eV; }
    Real temperatureScaleEV() const { return _T_scale_eV; }
    Real velocityScale() const { return _v_scale; }
    Real velocitySqScale() const { return _v_sq_scale; }
    Real weightScale() const { return _weight_scale; }
    Real massScale() const { return _m_scale; }
    Real chargeScale() const { return _q_scale; }
    Real lengthScale() const { return _x_scale; }
    Real timeScale() const { return _t_scale; }
    Real densityScale() const { return _n_scale; }
    Real potentialScale() const { return _potential_scale; }
    Real electricFieldScale() const {return _electric_field_scale; }
    Real chargeDensityScale() const {return _charge_density_scale; }

    // Functions for setting properties
    void setSimPlasmaFrequency(Real omega_p_sim) {
        _omega_p_sim = omega_p_sim;
        recalculate();
    }

    void setSimVacuumPermittivity(Real eps0_sim) {
        _eps0_sim = eps0_sim;
        recalculate();
    }

    void setSimChargeMassRatio(Real q_m_sim) {
        _q_m_sim = q_m_sim;
        recalculate();
    }

    void setSimDensity(Real n_sim) {
        _n_sim = n_sim;
        recalculate();
    }

    void setPhysicalTemperature(Real T_phys) {
        _scaling_enabled = true;
        _T_phys = T_phys;
        recalculate();
    }

    void setPhysicalDensity(Real n_phys) {
        _scaling_enabled = true;
        _n_phys = n_phys;
        recalculate();
    }

    void setPhysicalMass(Real mass){
        _scaling_enabled = true;
        _m_phys = mass;
        recalculate();
    }

    void setPhysicalCharge(Real charge){
        _scaling_enabled = true;
        _q_phys = charge;
        recalculate();
    }

    void setUseScaling(bool use_scaling){
        _scaling_enabled = use_scaling;
        recalculate();
    }

    bool getUseScaling() const {
        return _scaling_enabled;
    }

    std::string defaultScalingCSVOutput() const {
        std::string s = "Scaling For,Scaling Value,Units";
        s += "\nSimulation Plasma Frequency," + cast<std::string>(_omega_p_sim)+",";
        s += "\nSimulation Vacuum Permittivity," + cast<std::string>(_eps0_sim)+",";
        s += "\nSimulation Charge Mass Ratio," + cast<std::string>(_q_m_sim)+",";
        s += "\nSimulation Number Density," + cast<std::string>(_n_sim)+",";
        s += "\nSimulation Charge," + cast<std::string>(_q_sim)+",";
        s += "\nSimulation Mass," + cast<std::string>(_m_sim)+",";
        s += "\nSimulation Kb," + cast<std::string>(_kb_sim)+",";
        s += "\nPhysical Temperature," + cast<std::string>(_T_phys)+"," + temperature_units(true);
        s += "\nPhysical Number Density," + cast<std::string>(_n_phys)+"," + number_density_units(true);
        s += "\nPhysical Mass," + cast<std::string>(_m_phys)+"," + mass_units(true);
        s += "\nPhysical Charge," + cast<std::string>(_q_phys)+"," + charge_units(true);
        s += "\nEnergy," + cast<std::string>(1)+"," + energy_units(false);
        s += "\nTemperature," + cast<std::string>(1)+"," + temperature_units(false);
        s += "\nVelocity," + cast<std::string>(1)+"," + velocity_units(false);
        s += "\nVelocity Sq," + cast<std::string>(1)+"," + velocity_sq_units(false);
        s += "\nParticle Weight," + cast<std::string>(1)+",";
        s += "\nMass," + cast<std::string>(1)+","+mass_units(false);
        s += "\nCharge," + cast<std::string>(1)+","+charge_units(false);
        s += "\nLength," + cast<std::string>(1)+","+length_units(false);
        s += "\nTime," + cast<std::string>(1)+","+time_units(false);
        s += "\nFrequency," + cast<std::string>(1)+","+frequency_units(false);
        s += "\nNumber Density," + cast<std::string>(1)+","+number_density_units(false);
        s += "\nPotential," + cast<std::string>(1)+","+potential_units(false);
        s += "\nElectric Field," + cast<std::string>(1)+","+electric_field_units(false);
        s += "\nCharge Density," + cast<std::string>(1)+","+charge_density_units(false);
        return s;
    }

    std::string toCSVOutput() const{
        std::string s = "Scaling For,Scaling Value,Units";
        s += "\nSimulation Plasma Frequency," + cast<std::string>(_omega_p_sim)+",";
        s += "\nSimulation Vacuum Permittivity," + cast<std::string>(_eps0_sim)+",";
        s += "\nSimulation Charge Mass Ratio," + cast<std::string>(_q_m_sim)+",";
        s += "\nSimulation Number Density," + cast<std::string>(_n_sim)+",";
        s += "\nSimulation Charge," + cast<std::string>(_q_sim)+",";
        s += "\nSimulation Mass," + cast<std::string>(_m_sim)+",";
        s += "\nSimulation Kb," + cast<std::string>(_kb_sim)+",";
        s += "\nPhysical Temperature," + cast<std::string>(_T_phys)+"," + temperature_units(true);
        s += "\nPhysical Number Density," + cast<std::string>(_n_phys)+"," + number_density_units(true);
        s += "\nPhysical Mass," + cast<std::string>(_m_phys)+"," + mass_units(true);
        s += "\nPhysical Charge," + cast<std::string>(_q_phys)+"," + charge_units(true);
        s += "\nEnergy," + cast<std::string>(_energy_scale_eV)+"," + energy_units(_scaling_enabled);
        s += "\nTemperature," + cast<std::string>(_T_scale_eV)+"," + temperature_units(_scaling_enabled);
        s += "\nVelocity," + cast<std::string>(_v_scale)+"," + velocity_units(_scaling_enabled);
        s += "\nVelocity Sq," + cast<std::string>(_v_sq_scale)+"," + velocity_sq_units(_scaling_enabled);
        s += "\nParticle Weight," + cast<std::string>(_weight_scale)+",";
        s += "\nMass," + cast<std::string>(_m_scale)+","+mass_units(_scaling_enabled);
        s += "\nCharge," + cast<std::string>(_q_scale)+","+charge_units(_scaling_enabled);
        s += "\nLength," + cast<std::string>(_x_scale)+","+length_units(_scaling_enabled);
        s += "\nTime," + cast<std::string>(_t_scale)+","+time_units(_scaling_enabled);
        s += "\nFrequency," + cast<std::string>(1.0/_t_scale)+","+frequency_units(_scaling_enabled);
        s += "\nNumber Density," + cast<std::string>(_n_scale)+","+number_density_units(_scaling_enabled);
        s += "\nPotential," + cast<std::string>(_potential_scale)+","+potential_units(_scaling_enabled);
        s += "\nElectric Field," + cast<std::string>(_electric_field_scale)+","+electric_field_units(_scaling_enabled);
        s += "\nCharge Density," + cast<std::string>(_charge_density_scale)+","+charge_density_units(_scaling_enabled);
        return s;
    }


private:
    std::string energy_units(bool s) const { return (s) ? "eV" : "n_{sim} k_B T "; }
    std::string temperature_units(bool s) const { return (s) ? "eV" : "T"; }
    std::string velocity_units(bool s) const { return (s) ? "m/s" : "v_{th}"; }
    std::string number_density_units(bool s) const { return (s) ? "m^{-3}" : "n_e / n_{sim}"; }
    std::string velocity_sq_units(bool s) const { return (s) ? "m^2/s^2" : "v_{th}^2"; }
    std::string mass_units(bool s) const { return (s) ? "kg" : "m_{ref} n_{sim}"; }
    std::string charge_units(bool s) const { return (s) ? "C" : "q_{ref} n_{sim}"; }
    std::string length_units(bool s) const { return (s) ? "m" : "\\lambda_D"; }
    std::string time_units(bool s) const { return (s) ? "s" : "1/\\omega_{p}"; }
    std::string frequency_units(bool s) const { return (s) ? "s^{-1}" : "\\omega_{p}"; }
    std::string potential_units(bool s) const { return (s) ? "V" : "k_B T / q_{ref}"; }
    std::string electric_field_units(bool s) const { return (s) ? "V/m" : "k_B T / q_{ref} \\lambda_D"; }
    std::string charge_density_units(bool s) const { return (s) ? "C/m^3" : "q_{ref} n"; }

    // Simulation properties - inputs
    Real _omega_p_sim{1}; //!< Reference plasma frequency
    Real _eps0_sim{1}; //!< Permitivity of free space
    Real _kb_sim{1}; //!< Simulation boltzmann constant
    Real _q_m_sim{1}; //!< Charge to mass ratio
    Real _n_sim{1}; //!< The simulation density of the reference species

    // Simulation properties - computed
    Real _q_sim; //!< The reference charge in simulation units
    Real _m_sim; //!< The reference mass in simulation units

    // Physical plasma properties - inputs
    Real _T_phys{1}; //!< The physical plasma temperature (in eV)
    Real _n_phys{1}; //!< The physical plasma density
    Real _m_phys{ELECTRON_MASS}; //!< The physical mass scale
    Real _q_phys{FUNDAMENTAL_CHARGE}; //!< The physical charge scale

    bool _scaling_enabled{false}; //!< Whether or not the physical density was actually included

    // Scaling params - computed
    Real _energy_scale{1}; //!< The energy scaling
    Real _energy_scale_eV{1}; //!< The energy scaling (in eV)
    Real _T_scale_eV{1}; //!< The Temperature scale in eV
    Real _v_scale{1}; //!< The velocity scaling
    Real _v_sq_scale{1}; //!< The squared velocity scaling
    Real _weight_scale{1}; //!< Particle weighting
    Real _m_scale{1}; //!< Mass scaling
    Real _q_scale{1}; //!< Charge scaling
    Real _x_scale{1}; //!< Length scale
    Real _t_scale{1}; //!< Time scale
    Real _n_scale{1}; //!< Density scaling
    Real _potential_scale{1}; //!< Potential Scale
    Real _electric_field_scale{1}; //!< Electric Field scaling
    Real _charge_density_scale{1}; //!< Charge Density Scaling

    void compute_q_sim(){
        _q_sim = _omega_p_sim*_omega_p_sim*_eps0_sim/(_n_sim*_q_m_sim);
    }

    void compute_m_sim(){
        _m_sim = _q_sim/_q_m_sim;
    }

    void compute_kb_sim(){
        _kb_sim = 1.0/_n_sim;
    }

    void compute_n_scale(){
        _n_scale = _n_phys / _n_sim;
    }

    void compute_v_scale(){
        _v_scale = thermal_velocity(BOLTZMANN_CONSTANT,
                                    eV_to_Kelvin(_T_phys),
                                    _m_phys) / std::sqrt(2.0);
        _v_sq_scale = _v_scale*_v_scale;
    }

    void compute_energy_scale(){
        _energy_scale = _n_sim*BOLTZMANN_CONSTANT*eV_to_Kelvin(_T_phys);
    }

    void compute_energy_scale_eV(){
        _energy_scale_eV = _n_sim*_T_phys;
    }

    void compute_temperature_scale_eV(){
        _T_scale_eV = _T_phys;
    }

    void compute_x_scale(){
        _x_scale = debye_length(VACUUM_PERMITTIVITY,
                                BOLTZMANN_CONSTANT,
                                eV_to_Kelvin(_T_phys),
                                _n_phys,
                                _q_phys);
    }

    void compute_t_scale(){
        _t_scale = 1.0 / plasma_frequency(_n_phys,
                                          _q_phys,
                                          _m_phys,
                                          VACUUM_PERMITTIVITY);
    }

    void compute_weight_scale(){
        _weight_scale = _x_scale*_x_scale*_x_scale*_n_scale;
    }

    void compute_m_scale(){
        _m_scale = _m_phys*_n_sim;
    }

    void compute_q_scale(){
        _q_scale = _q_phys*_n_sim;
    }

    void compute_potential_scale(){
        _potential_scale = _T_phys;
    }

    void compute_electric_field_scale(){
        _electric_field_scale = _potential_scale / _x_scale;
    }

    void compute_charge_density_scale(){
        _charge_density_scale = _q_scale*_n_scale;
    }

    void reset_scalings_to_one(){
        _energy_scale = 1;
        _energy_scale_eV = 1;
        _T_scale_eV = 1;
        _v_scale = 1;
        _v_sq_scale = 1;
        _potential_scale = 1;
        _electric_field_scale = 1;
        _charge_density_scale = 1;
        _weight_scale = 1;
        _m_scale = 1;
        _q_scale = 1;
        _x_scale = 1;
        _t_scale = 1;
        _n_scale = 1;
        _potential_scale = 1;
        _electric_field_scale = 1;
        _charge_density_scale = 1;
    }


    //! recompute all parameters
    void recalculate() {
        compute_q_sim();
        compute_m_sim();
        compute_kb_sim();

        if(!_scaling_enabled) {
            reset_scalings_to_one();
            return;
        }

        compute_n_scale();
        compute_v_scale();
        compute_energy_scale();
        compute_energy_scale_eV();
        compute_temperature_scale_eV();
        compute_x_scale();
        compute_t_scale();
        compute_weight_scale();
        compute_m_scale();
        compute_q_scale();
        compute_potential_scale();
        compute_electric_field_scale();
        compute_charge_density_scale();
    }

};

typedef std::shared_ptr<SpeciesReferenceParameters> ReferenceParams;

}

#endif