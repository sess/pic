#ifndef SESS_PIC_PARTICLE_H
#define SESS_PIC_PARTICLE_H

#include "Definitions.h"
#include "Distributions.h"
#include "Scaling.h"
#include <iostream>
#include <vector>
#include <map>
#include <list>

namespace picard {

class Grid;
class Interpolator;
typedef std::shared_ptr<const struct Species> SpeciesPtr; //!< shared pointer that serves as the species id
typedef std::map<SpeciesPtr, std::list<class Particle>> Particles; //!< Particle list that contains all particles
typedef std::map<size_t, SpeciesPtr> SpeciesIndex;//!< Map of species index to species ptr

SpeciesIndex getSpeciesMap(const Particles& particles); //!< Get map of species by index from the list of particles

/**
 * The particle class stores simulation particles
 *
 * Keeps track of a particles unique index, velocity, present/last position
 */
class Particle {
public:
    /**
     * Constructor for creating simulation particles with a specified dimension
     * @param m The mass of the particle
     * @param q The charge of the particle
     * @param nDim The dimension of the particle
     */
    Particle(int nDim, const Species* traits = nullptr)
            : r(nDim, 0), r_last(nDim, 0), v(nDim, 0), _traits(traits) {}

    /**
     * Default constructor. Also constructor used for creating simulation particles with a known position and velocity
     * @param m The mass of the particle
     * @param q The charge of the particle
     * @param nDim The dimension of the particle
     */
    Particle(const vec& r = {}, const vec& v = {}, const Species* traits = nullptr)
            : r(r), r_last(r), v(v), _traits(traits) {}

    const Species& species() const{ return *_traits; }; //!< Get the traits of this species
    SpeciesPtr species_ptr() const; //!< Get the species ptr to use as an id
    size_t speciesId() const; //!< Get the species index/id of this particle
    Real m() const; //!< The mass of the particle
    Real q() const; //!< The charge of the particle
    std::string name() const; //!< Get the name of this particle

    vec r; //!< The current position of the particle
    vec r_last; //!< The position the particle occupied on the last timestep
    vec v; //!< The current velocity of the particle

    std::vector<Real> ray() const { return concatenate(r_last, r); } //!< Get the ray from the las position to the current
    
    //! Kinetic energy of the particle. Calculated as KE = \frac{1}{2} m \vec{v}\cdot \vec{v}.
    Real kineticEnergy() const;

    //! Get the particle speed
    Real speed() const{ return norm(v); }
    
    /**
     * Electric potential energy of the particle. Calculated as EPE = q * \Phi
     * @param grid The grid that stores the potential field.
     * @param interp The interpolator used to interpolate the potential to the particle's position.
     **/
    Real potentialEnergy(const Grid& grid, const Interpolator& interp) const;
    
    /**
     * Total energy of the particle. Calculated as the addition of kinetic and potential energies.
     * @param grid The grid that stores the potential field.
     * @param interp The interpolator used to interpolate the potential to the particle's position.
     **/
    Real totalEnergy(const Grid& grid, const Interpolator& interp) const;

    /**
     * Comparison operator for two particles (comparing only their mass and charge)
     * @param other The other particle to comare
     * @return True if this particle is less than the other, false otherwise
     */
    bool operator<(const Particle& other) const;

private:
    const Species* _traits{nullptr}; //!< points to the SpeciesTraits of the particle.

    static long highest_index; //!< The largest index of any particle
};

std::ostream& operator<<(std::ostream& os, const Particle& p); //!< Stream operator for the Particle class

typedef std::list<Particle>::iterator pIterator; //!< Iterator for the particle list

//! Enumeration that describes the state of the particle after a boundary interaction takes place
enum ParticleStatus {
    REMOVE_PARTICLE, //!< Remove the particle from the master lists (has been absorbed or has left the domain)
    PARTICLE_UPDATED, //!< Changes were made to the particle velocit or position
    PARTICLE_UNCHANGED, //!< no updates were made to the particle
    REAPPLY //!< Boundary condition was not fully resolved, run the process again
};

std::ostream& operator<<(std::ostream& os, const ParticleStatus& ps); //!< Streaming operator for ParticleStatus

//! The outcome of a particle-boundary interaction.
struct InteractionResults {
    InteractionResults(ParticleStatus status = PARTICLE_UNCHANGED, std::list<Particle> particlesToAdd = {})
            : status(status), particlesToAdd(particlesToAdd) {}

    ParticleStatus status; //!< The status of the particle after the boundary condition. Indicates if anything else needs to be done with the particle such as removing it.
    std::list<Particle> particlesToAdd; //!< A list of new particles that should be added to the master list
};

std::ostream& operator<<(std::ostream& os, const InteractionResults& ps); //!< Streaming operator for BCResults


//!Container for global particle characteristics
struct Species : std::enable_shared_from_this<Species> {
    Species(std::string name, Real m_mref, Real q_qref, std::shared_ptr<SpeciesReferenceParameters> reference = nullptr)
            : id(getNextId()), name(name), m_mref(m_mref), m(m_mref), q_qref(q_qref), q(q_qref), reference(reference) {}

    size_t id; //!< Type id of the particle (if from config then this is the order in which it was parsed)
    std::string name; //!< The name of the particle
    Real m_mref; //! Ratio of this particles mass to the reference mass
    Real q_qref; //! Ratio of this particles charge to the reference charge
    Real m; //!< The mass of the particle
    Real q; //!< The charge of the particle
    multi_distribution<Real> velocity_distribution; //!< Particle velocity distribution
    std::shared_ptr<SpeciesReferenceParameters> reference; //!< The reference parameters for this species
    std::vector<Real> grid_density; //!< a vector storing the particle density on the grid.

    void updateMassCharge(); //!< update the mass and charge of the species based on the local ratios and the reference parameters

    vec temperature(const vec& velocity_variance) const;//!< Get the temperature of a population of particles fromt the velocity variance of the population
    static Real reducedMass(Real m1, Real m2); //!< Get the reduced mass of two particles
    static Real reducedMass(SpeciesPtr p1, SpeciesPtr p2){return reducedMass(p1->m, p2->m);}; //!< Get the reduced mass of two particles

    /**
     * Create a new particle at the specified location with a velocity sampled from the associated velocity distribution.
     * @param r The location of the particle
     * @return A newly generated particle
     */
    Particle generate(const vec& r) const;

    std::shared_ptr<const Species>c_ptr() const { return shared_from_this(); } //!< Get the shared pointer to the object (const version)
    std::shared_ptr<Species> ptr() { return shared_from_this(); } //!< Get the shared pointer to the object

    static std::vector<std::string> headers(); //!< Get the headers for the species traits (for writing traits to disk)
    static size_t numSpecies(){return highestIndex + 1;} //!< Get the number of species created so far
    static void resetSpeciesIndex(){highestIndex = 0;} //!< reset the species index counter
private:
    static size_t highestIndex; //!< highest current index of a species
    static size_t getNextId(){return highestIndex++;};//! Get the next species id
};

 //! A set of functions for creating and manipulating a population of particles
class PopulationGenerator {
public:

    static std::list<Particle> generate_gaussian_density(SpeciesPtr species, const Grid &grid, size_t count);

    static std::list<Particle> generate_spatially_uniform_particles(SpeciesPtr species, const Grid &grid, size_t count);

    static std::list<Particle> generate_spatially_cartesian_particles(SpeciesPtr species, const Grid &grid,
                                                                      ivec spatSamples);

    static std::list<Particle> generate_quiet_start_particles(SpeciesPtr species, const Grid &grid,
                                                              const ivec& spatSamples, const ivec& velSamples);

    static void perturb_position(const Grid &grid, std::list<Particle> &particles, std::vector<multi_function> spatial_perturbations);


    static void perturb_position(const Grid &grid, std::list<Particle> &particles, multi_function spatial_perturbation) {
        perturb_position(grid, particles, std::vector<multi_function>({spatial_perturbation}));
    }

private:

    /**
     * loops over the velocity cdf to fill all particles at a particular spatial coord
     * @param specie The specie whose traits determine the distribution of particles
     * @param r The position that all particles in this sub distribution will be placed at
     * @param particles The particle list to populate
     * @param grid The grid to get the dimensionality of the distribution
     * @param velSamples The number of velocity samples in each dimension
     */
    static void fillCDFVelSamples(SpeciesPtr specie, const vec& r, std::list<Particle>& particles, const Grid& grid,
                                  const ivec& velSamples);

};

std::ostream& operator<<(std::ostream& os, const Species& type); //!< Stream the SpeciesTraits


/**
 * Accumulate a particle quantity per species
 * @tparam R The return type of the quantity
 * @tparam Args The argument types that go into the desired function call
 * @param particles The list of particles to accumulate from
 * @param fn The Particle function to call on each particle and accumulate
 * @param args The arguments that go into the function call
 * @return The total value summed up over all particles
 */
template <typename R, typename ... Args>
std::vector<R> accumulate_species(const Particles& particles, R (Particle::*fn)(const Args&...) const, const Args&... args){
    std::vector<R> total(particles.size(), 0);
    for(auto& species : particles){
        for(auto& p : species.second){
            total[species.first->id] += (p.*fn)(args...);
        }
    }
    return total;
};

}

#endif //SESS_PIC_PARTICLE_H
