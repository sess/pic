#ifndef SESS_PIC_OUTPUT_H
#define SESS_PIC_OUTPUT_H

#include <string>
#include <vector>
#include <list>
#include <set>
#include "StringUtils.h"
#include "IOUtils.h"
#include "Particle.h"

namespace picard {

class Grid;
class Interpolator;
class Simulation;

//!< The target quantity to output. Some are full vector fields at each timestep while others are just a single number.
enum OutputTarget {
    POSITION = 0, //!< Particle Positions (One row for each particle, one file per timestep)
    VELOCITY, //!< Particle Velocity (One row for each particle, one file per timestep)
    SPECIES_COUNT, //!< The total number of particles of each species at the given timestep
    ELECTRIC_FIELD, //!< Electric Field On Grid (One row for each grid point, one file per timestep)
    MAGNETIC_FIELD, //!< Magnetic Field On Grid (One row for each grid point, one file per timestep)
    VELOCITY_AVERAGE, //!< average velocity on the grid
    VELOCITY_VARIANCE, //!< Velocity variance on the grid
    NUMBER_DENSITY, //!< number density on the grid
    TEMPERATURE, //!< Temperature on grid
    POTENTIAL, //!< Electric Potential On Grid (One row for each grid point, one file per timestep)
    CHARGE_DENSITY, //!< Charge Density on Grid (One row for each grid point, one file per timestep)
    SURFACE_CHARGE_DENSITY, //!< Surface charge density wherever it is stored (One row for each stored grid point, one file per timestep)
    KINETIC_ENERGY, //!< Kinetic energy of each species
    POTENTIAL_ENERGY, //!< Potential energy of each species
    TOTAL_ENERGY, //!< Total energy of each species
    SPECIES_TEMPERATURE, //!< average temperature of each species (in each dimension)
    SPECIES_VELOCITY, //!< average velocity of each species (in each dimension)
    PHYSICS, //!< Enables the output of the physics modules

    NUMBER_OF_TARGETS, //!< The number of output targets (used for iteration)
    OUTPUT_ALL, //!< Use this to output all targets
    UNKNOWN_TARGET //!< An unrecognized target, ignored
};

/**
 * Parse the OutputTarget from an input string
 * @param name The string to be parsed
 * @return The output Target, UNKNOWN_TARGET if not recognized
 */
OutputTarget getTarget(std::string name);

//! Stream the Output Target Type
std::ostream& operator<<(std::ostream& os, OutputTarget type);

enum OutputSuffix {
    TIME, //!< The suffix of the file name will be the simulation time of the dataset
    ITERATION, //!< The suffix of the file name will be the iteration number of the dataset
    UNKNOWN_SUFFIX //!< An unrecognized option, defaults to 
};

/**
 * Parse the OutputSuffix from an input string
 * @param name The string to be parsed
 * @return The OutputSuffix, UNKNOWN_SUFFIX if not recognized
 */
OutputSuffix getSuffix(std::string name);

//! Stream the Output Target Type
std::ostream& operator<<(std::ostream& os, OutputSuffix type);

/**
 * A single output rule. Specifies a target to output (like total energy, or charge density) plus other parameters
 * Such as the file name prefix, the write interval, the start time, etc.
 * These populate the output class which managers the writing of them.
 */
class OutputEntry {
public:
    OutputEntry(OutputTarget target = UNKNOWN_TARGET,
                int interval = 1,
                std::string prefix = "",
                Real startTime = 0,
                Real endTime = std::numeric_limits<Real>::infinity(),
                const std::vector<ulong>& indices = {},
                std::string directory = "./output/",
                std::string fileExtension = ".csv",
                OutputSuffix suffix = ITERATION,
                int precision = MAX_FLOATING_PRECISION,
                bool timestampFolder = true,
                std::set<int> iterations = {}) :
            target(target), interval(interval), prefix(prefix), startTime(startTime), endTime(endTime),
            indices(indices), directory(directory), originalDirectory(directory), timestampFolder(timestampFolder),
            fileExtension(fileExtension), suffix(suffix), precision(precision), iterations(iterations) {
        if(prefix == "")
            setDefaultPrefix();
    }

    OutputTarget target{UNKNOWN_TARGET}; //!< The target to be output
    int interval{1}; //!< The interval spacing between output of this target
    std::string prefix; //!< The file prefix of the entry
    Real startTime; //!< The start time of the output. (No writes before this time)
    Real endTime; //!< The end time of output. (No writes after this time)
    std::vector<ulong> indices; //!< A set of indices (for particle or grid nodes). Empty means all
    std::string directory; //!< The output directory of this entry (potentially modified with a timestamp subfolder)
    std::string originalDirectory; //!< The original output directory provided by the user
    bool timestampFolder; //!< Place output files into a folder labelled with a timestamp
    std::string fileExtension; //!< The file extension of this entry
    OutputSuffix suffix; //!< The suffix style of this entry
    int precision; //!< The floating point precision of the output
    std::set<int> iterations; //!< The list of iterations at which this data should be written
    bool skip{false}; //!< Whether or not the output entry should be skipped

    /**
     * Check if this entry should be written to disk this timestep or not
     * @param iteration The iteration of the simulation
     * @param time The time of the simulation
     * @return True if the iteration coincides with the write interval and the current time is withing the start and end bounds
     */
    bool shouldWrite(int iteration, double time) const;

    bool useSuffix() const;//!< Check to see if this output target uses a suffix on each iteration
    bool usePrefix() const; //!< Check to see if this output target uses its prefix

    /**
     * Get the full file path of the output entry using simulation time or iteration as the suffix
     * @param simulation The simulation to get the time or iteration count from
     * @param pre_prefix An additional string appended to the beginning of the file name (used for rank information)
     * @return A string that is the full filePath of the output entry
     */
    std::string filePath(const Simulation& simulation, std::string pre_prefix = "") const;

    /**
     * Get the full file path of the output entry assuming no suffix is used
     * @param pre_prefix An additional string appended to the beginning of the file name (used for rank information)
     * @return A string that is the full filePath of the output entry
     */
    std::string filePath(std::string pre_prefix = "") const;

    /**
     * Check if two entries would output identical files if written at the same time
     * @param other The other entry to compare
     * @return True if the two entries would write an identical file, False otherwise.
     */
    bool isEqualIfWritten(const OutputEntry& other) const;

    /**
     * Check to see if two OutputEntry objects are identical in every way
     * @param other The other OutputEntry object to compare against
     * @return True  if the objects are identical, false otherwise
     */
    bool operator==(const OutputEntry& other) const;

    std::string subFolder() const; //!< Get the subfolder that this target falls within

    //! Set the prefix of the output entry to the default value
    void setDefaultPrefix() { prefix = tsfs(target);}

    //! Set the directory of the output entry (changes both the directory and originalDirectory parameter)
    void setBaseDirectory(std::string direc){
        directory = direc;
        originalDirectory = direc;
    }


    static const std::string infoFolder; //!< folder to store high level sim information (away from the large amount of data
    static const std::string dataFolder; //!< folder to store the actual data files

private:
    static const std::set<OutputTarget> ignoresSuffix;//!< Set of output targets that ignore the suffix (i.e. files that get appended to)
    static const std::set<OutputTarget> ignoresPrefix;//!< Set of output targets that ignore the prefix (names are decided otherwise)
    static const std::set<OutputTarget> isData;//!< Set of output targets that go into the data subfolder
    static const std::set<OutputTarget> isInfo;//!< Set of output targets that go into the info subfolder
};

std::ostream& operator<<(std::ostream& os, OutputEntry entry); //!< Stream the OutputEntry

class Output {
public:
     //! Add an entry to be output to disk
    //! @param oe The output entry to add to the list of entries
    void add(const OutputEntry& oe){ _entries.push_back(oe); };

    /**
     * Add a list of OutputEntries with default values using a template OutputEntry and a list of targets
     * @param default_oe Template for default settings
     * @param targets List of targets to be added
     */
    void add(const OutputEntry& default_oe, const std::vector<OutputTarget>& targets);

    void makeDirectories() const; //!< make all of the needed output directories
    void analyze(const Simulation& simulation) const; //!< Print commands used to analyize
    void runSystemCalls() const; //!< Run the system calls that have been stored
    void setSystemCalls(const std::vector<std::string>& systemCalls){_systemCalls = systemCalls;} //!< sets the list of system calls
    const std::vector<std::string>& systemCalls(){return _systemCalls;} //!< Get the list of system calls to be run
    void setAnalysisFile(std::string analysisFile){_analysisFile = analysisFile;} //!< Set the location of the analysis file
    std::string analysisFile(){return _analysisFile;} //!< Get the location of the analysis file
    void setAutomatedAnalysis(bool analyze){_automatedAnalysis = analyze;}; //! Sets the boolean that controls default analysis to the given value
    void setAdditionalJuliaFiles(const std::vector<std::string>& files){ _additionalJuliaFiles = files; } //!< Set the additional Julia files
    const std::vector<std::string>& additionalJuliaFiles(){return _additionalJuliaFiles;} //!< Get the list of additional julia files
    bool automatedAnalysis(){ return _automatedAnalysis; }
    std::set<std::string> directories() const; //!< Get a unique list of directories that output files will be written to
    void clearSuffixFreeContents(); //!< Clear the contents all that files that will be appended to each timestep (suffix-free)
    void removeOutputFiles() const; //!< Delete all output files written to so far
    void removeDirectories() const; //!< Deletes all empty directories that were used by the output
    /**
     * Check to see if the list of outputs already has the specified entry (if they were both to written)
     * @param entry The output entry to compare to the contents of the container
     * @param outputs The set of output entries to check for a repeat in
     * @return True if there is an OutputEntry in outputs that "matches" with the entry via the isEqualIfWritten function, False otherwise.
     */
    bool isRepeat(const OutputEntry& entry, const std::vector<OutputEntry>& outputs) const;

    //! Calls isRepeat using the full list of output entries
    bool isRepeat(const OutputEntry& entry) const { return isRepeat(entry, _entries); };

    bool hasTarget(OutputTarget target); //!< check the list of entries for the specified target

    //! Get the output entries for manipulation or clearing
    std::vector<OutputEntry>& entries() {return _entries;}
    const std::vector<OutputEntry>& entries() const {return _entries;}

    /**
     * Set the output directory to a new path for all the outputs.
     * @param directory The new directory path to output the files
     */
    void setDirectory(std::string directory);

    void setTimestampFolder(bool use_timestamp); //!< Set whether the output files should be placed in a folder marked by a timestamp within the folder specified

    void addTimestampFolderToDirectory(long timestamp); //!< Add the timestamp directly to output entries that require it

    /**
     * Set the file extension of the output files to an arbitrary string for all the outputs.
     * Make sure to include the '.' if a conventional extension is desired
     * @param extension The desired file extension (e.g. ".csv" for comma-separated files or ".txt" for plain text or "" for no extension)
     */
    void setFileExtension(std::string extension);

    /**
     * Set the type of suffix of the output files (either by time or by iteration) for all the outputs
     * @param suffix The type of suffix to use for output files
     */
    void setSuffix(OutputSuffix suffix);

    /**
     * Set the precision of the floating-point output of the simulation for all the outputs.
     * Some data will be lost as a floating point number is converted to a finite decimal string representation.
     * See std::setPrecision for a description of how the precision is computed from the integers
     * @param precision The level of precision desired
     */
    void setPrecision(int precision);

    /**
     * Write all outputs to disk
     * @param simulation The simulation whose output is to be written to disk
     */
    void write(const Simulation& simulation);

    // Functions for writing each target to disk
    void writePositions(std::string path, const Particles& particles, const std::vector<ulong>& indices, int precision);
    void writeVelocities(std::string path, const Particles& particles, const std::vector<ulong>& indices, int precision);
    void writeSpeciesTraits(std::string path, const Particles& particles, int precision);
    void writeElectricField(std::string path, const Grid& grid, const std::vector<ulong>& indices, int precision);
    void writeMagneticField(std::string path, const Grid& grid, const std::vector<ulong>& indices, int precision);
    void writePotential(std::string path, const Grid& grid, const std::vector<ulong>& indices, int precision);
    void writeChargeDensity(std::string path, const Simulation& sim, const std::vector<ulong>& indices, int precision);
    void writeVelocityAverage(std::string path, const Simulation& sim, const std::vector<ulong>& indices, int precision);
    void writeVelocityVariance(std::string path, const Simulation& sim, const std::vector<ulong>& indices, int precision);
    void writeNumberDensity(std::string path, const Simulation& sim, const std::vector<ulong>& indices, int precision);
    void writeTemperature(std::string path, const Simulation& sim, const std::vector<ulong>& indices, int precision);
    void writeSurfaceChargeDensity(std::string path, const Simulation& sim, const std::vector<ulong>& indices, int precision);
    void writeKineticEnergy(std::string path, const Simulation& simulation, int precision);
    void writePotentialEnergy(std::string path, const Simulation& simulation, int precision);
    void writeTotalEnergy(std::string path, const Simulation& simulation, int precision);
    void writeSpeciesTemperature(std::string path, const Simulation& simulation, int precision);
    void writeSpeciesVelocity(std::string path, const Simulation& simulation, int precision);
    void writeSpeciesCount(std::string path, const Simulation& simulation, int precision);
    void writeGrid(std::string path, const Grid& grid, int precision);

private:
    std::string _analysisFile{"analysis/analyze.jl"}; //!< Location of the analysis file
    bool _automatedAnalysis{false}; //!< Whether or not the output data should autmatically be analyzed
    std::vector<std::string> _additionalJuliaFiles; //!< File names of julia files to perform at the end of the traditional analysis
    std::vector<std::string> _systemCalls; //!< list of system calls to run on completion of simulation


    /**
     * Calls the correct write function based on the target output
     * @param entry The Output entry to pass to the write function
     * @param simulation The simulation from which the data comes
     */
    void distributeWrite(const OutputEntry& entry, const Simulation& simulation);

    /**
     * Write all output targets with settings specified by the default OutputEntry
     * @param defaultOe The OutputEntry will default settings
     * @param simulation The simulation whose data is being output
     */
    void writeAll(const OutputEntry& defaultOe, const Simulation& simulation);

    std::vector<OutputEntry> _entries; //!< The set of entries to write
    std::set<std::string> _usedFiles; //!< List of file paths used to write to. Stored for cleanup if desired
    static const std::string speciesIdHeader; //!< The header used to denote the species id header

};

}

#endif
