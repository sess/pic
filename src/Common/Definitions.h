#ifndef DEFINITIONS_H
#define DEFINITIONS_H

#include <vector>
#include <array>
#include <random>
#include <cmath>
#include <Eigen/Dense>

//#define ENABLE_PROFILING // Comment out this line if you want to turn profiling off
#include "Profiler.hpp"

namespace picard {

// Precision
//#if USE_DOUBLES > 0
typedef double Real;
#define PIC_MPI_REAL MPI_DOUBLE
#define ABS_EPSILON 5e-14
#define REL_EPSILON 1e-11

#if __APPLE__
    typedef unsigned long ulong;
#elif _WIN32 || _WIN64
	typedef unsigned long ulong;
	typedef unsigned int uint;
	typedef int mode_t;
#endif

//#else
//typedef float Real;
//#define PIC_MPI_REAL MPI_FLOAT
//#define ABS_EPSILON 1e-6
//#define REL_EPSILON 1e-4
//#endif

// Bit manipulation
#define CHECK_BIT(var,pos) (bool)((var) & (1<<(pos)))

// Vectors
template<typename T>
using shortVec = std::vector<T>; //!< Vector of size nDim

template<typename T>
using vector2d = Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>; //!< Dynamic 2d vector

typedef shortVec<Real> vec; //!< Floating precision shortVec used to store position, velocity, etc
typedef shortVec<int> ivec; //!< Integer shortVec used to integer values like cartesian ijk

// Variables
constexpr double pi = 3.141592653589793;
constexpr double sqrt_2 = 1.41421356237309504880168872;
constexpr double sqrt_pi = 1.77245385090551602729816;
constexpr double sqrt_2_pi = 2.506628274631000502415765;
constexpr double sqrt_half_pi = 1.2533141373155002512;


// Random number generation
typedef std::mt19937 generator; //!< The type of generator to be used


}


#endif
