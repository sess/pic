#ifndef SESS_PIC_DISTRIBUTIONS_H
#define SESS_PIC_DISTRIBUTIONS_H

#include "Definitions.h"
#include "VectorUtils.h"
#include "IOUtils.h"
#include <iostream>
#include <functional>
#include <memory>
#include "MathUtils.h"

namespace picard {

/**
 * Function that creates a new engine for each thread. Seeded with a truly non-deterministic random
 * number generator
 * @return A reference to the random number generator
 */
inline generator& engine() {
    thread_local static std::random_device rd;
    thread_local static generator eng(rd());
    return eng;
}

template<typename T = Real>
using distribution_fn = std::function<T(generator & )>; //!< Generic function for storing distributions

/**
 * Distribution class for storing different types of distribution functions
 * Has the ability to use the default generator
 */
template<typename T = Real>
class distribution {
public:
    distribution() = default; //!< Default constructor
    virtual ~distribution() = default; //!< virtual destructor for polymorphism
    distribution(const distribution_fn<T>& fn, std::string name = "", const std::vector<T>& params = {})
            : _distribution(fn), _name(name), _params(params) {} //!< Construct from distribution function
    template<typename DIST>
    distribution(const DIST& dist, std::string name = "", const std::vector<T>& params = {})
            : _distribution(distribution_fn<T>(dist)), _name(name),
              _params(params) {} //!< Construct from anything that can be turned into a distribution

    T operator()() const { return _distribution(engine()); } //!< Sample with the default engine
    T operator()(generator& g) const { return _distribution(g); } //!< Sample with the provided generator

    std::string name() const { return _name; }; //!< Get the name of the distribution
    const std::vector<Real>& params() const { return _params; } //!< Get the params associated with the distribution
protected:
    distribution_fn<T> _distribution; //!< The distribution function to be called
    std::string _name; //!< optional name for the distribution
    std::vector<T> _params; //!< optional params for the distribution
};

template <typename T>
std::ostream& operator<<(std::ostream& os, const distribution<T>& d) {
    os << d.name() << "[" << d.params() << "]";
    return os;
}

/**
 * A class to hold multiple distributions simultaneously
 */
template<typename T = Real>
class multi_distribution {
public:
    /**
     * Default constructor.
     * @param distributions A vector a distributions to hold onto
     */
    multi_distribution(const std::vector<std::shared_ptr<distribution<T>>>& distributions = {}) : _distributions(distributions) {}

    /**
     * push back a distribution to the internal storage container
     * @param dist The distribution to push back
     */
    void push_back(std::shared_ptr<distribution<T>> dist) { _distributions.push_back(dist); }

    void resize(size_t sz) { _distributions.resize(sz); }//!< resize the number of distributions

    /**
     * Get the underlying vector of distributions for setting or manipulation
     * @return A reference to the underlying distributions.
     */
    std::vector<std::shared_ptr<distribution<T>>>& getDistributions() { return _distributions; }
    const std::vector<std::shared_ptr<distribution<T>>>& getDistributions() const { return _distributions; }

    /**
     * Get and modify a particular distribution as specified by the index
     * @param i The index of the distribution to get
     * @return A reference to the distribution stored at index i
     */
    std::shared_ptr<distribution<T>>& operator[](int i) { return _distributions[i]; }

    /**
     * Get const reference to a particular distribution as specified by the index
     * @param i The index of the distribution to get
     * @return A reference to the distribution stored at index i
     */
    const std::shared_ptr<distribution<T>>& operator[](int i) const { return _distributions[i]; }

    size_t size() const { return _distributions.size(); } //!< Get the number of distributions stored

    /**
     * Get a vector containing the result of a single sampling of each distribution
     * @return A vector<Real> containing the results of the sampling
     */
    std::vector<T> operator()() const {
        std::vector<T> res;
        for (auto& dist : _distributions)
            res.push_back((*dist)());
        return res;
    }

    /**
      * Get a vector containing the result of a single sampling of each distribution starting at the the start index and getting the next size samples
      * @param start Starting location of the sampling
      * @param numSamples Number of samples to take
      * @return A vector<Real> containing the results of the sampling
      */
    std::vector<T> operator()(int start, int numSamples) const {
        std::vector<T> res;
        for (int i=start; i<start + numSamples && i<size(); i++)
            res.push_back((*_distributions[i])());
        return res;
    }

    /**
     * Sample the ith distribution that is stored
     * @param i The index of the distribution to sample
     * @return A value sampled from the desired distribution
     */
    T operator()(int i) const { return (*_distributions[i])(); }

    /**
     * Get the list of parameters at the specified index. Each entry in the vector comes from one distribution
     * @param i The index of the parameter
     * @return A vector of this parameter from each distribution
     */
    vec param(uint i){
        vec p;
        for(auto& d : _distributions)
            p.push_back(d->params()[i]);
        return p;
    }

private:
    std::vector<std::shared_ptr<distribution<T>>> _distributions; //!< Internal container of distributions
};

template <typename T>
std::ostream& operator<<(std::ostream& os, const multi_distribution<T>& md) {
    os << "(" << md.getDistributions() << ")";
    return os;
}

template<typename T>
std::vector<T> samples(size_t N, const distribution<T>& dist) {
    std::vector<T> samps(N);
    for (uint i = 0; i < N; i++) {
        samps[i] = dist();
    }
    return samps;
}


/***************************************************************************
 *           This is where the distributions are defined                   *
 ***************************************************************************/

/**
 * Create a uniform Real distribution in multiple dimensions
 * @param min The minimum bound of the uniform distribution (inclusive)
 * @param max The maximum bound of the uniform distribution (inclusive)
 * @return A multi_distribution containing the uniform distributions
 */
inline multi_distribution<Real> uniform_multi_distribution(const std::vector<Real>& min, const std::vector<Real>& max) {
    multi_distribution<Real> md;
    for (uint i = 0; i < min.size(); i++) {
        distribution<Real> d(std::uniform_real_distribution<Real>(min[i], max[i]), "uniform", {min[i], max[i]});
        md.push_back(std::make_shared<distribution<Real>>(d));
    }
    return md;
}

/**
 * Create a uniform int distribution in multiple dimensions
 * @param min The minimum bound of the uniform distribution (inclusive)
 * @param max The maximum bound of the uniform distribution (inclusive)
 * @return A multi_distribution containing the uniform distributions
 */
inline multi_distribution<int>
uniform_int_multi_distribution(const std::vector<int>& min, const std::vector<int>& max) {
    multi_distribution<int> md;
    for (uint i = 0; i < min.size(); i++) {
        distribution<int> d(std::uniform_int_distribution<int>(min[i], max[i]), "uniform", {min[i], max[i]});
        md.push_back(std::make_shared<distribution<int>>(d));
    }
    return md;
}

/**
 * Create and sample a single value from a uniform real distribution between min and max (inclusive)
 * @param min The minimum bound of the uniform distribution (inclusive)
 * @param max The maximum bound of the uniform distribution (inclusive)
 * @return A real value sampled from the uniform distribution between min and max (inclusive)
 */
inline Real uniform(Real min, Real max) {
    distribution<Real> dist = std::uniform_real_distribution<Real>(min, max);
    return dist();
}

/**
 * Create and sample a single value from a uniform real distribution between 0 and 1 (inclusive)
 * This version creates a static version of the uniform_real_distribution because the bounds are known
 * @return A real value sampled from the uniform distribution between 0 and 1 (inclusive)
 */
inline Real uniform() {
    static distribution<Real> dist = std::uniform_real_distribution<Real>(0, 1);
    return dist();
}

/**
 * Create and sample a single value from a uniform int distribution between min and max (inclusive)
 * @param min The minimum bound of the uniform distribution (inclusive)
 * @param max The maximum bound of the uniform distribution (inclusive)
 * @return A real value sampled from the uniform int distribution between min and max (inclusive)
 */
inline int uniform_int(int min, int max) {
    distribution<int> dist = std::uniform_int_distribution<int>(min, max);
    return dist();
}

/**
 * Create and sample a single value from a uniform int distribution between 0 and std::numeric_limits<int>::max() (inclusive)
 * This version creates a static version of the uniform_int_distribution because the bounds are known
 * @return A real value sampled from the uniform int distribution between 0 and std::numeric_limits<int>::max() (inclusive)
 */
inline int uniform_int() {
    static distribution<int> dist = std::uniform_int_distribution<int>(0, std::numeric_limits<int>::max());
    return dist();
}

/**
 * Create and sample a single value from a multi-dimensional uniform distribution
 * @param min The minimum bound of the uniform distribution (inclusive)
 * @param max The maximum bound of the uniform distribution (inclusive)
 * @return A vector of real values sampled from the uniform distribution
 */
inline std::vector<Real> uniform(const std::vector<Real>& min, const std::vector<Real>& max) {
    auto dist = uniform_multi_distribution(min, max);
    return dist();
}

/**
 * Create and sample a single value from a multi-dimensional uniform distribution
 * @param min The minimum bound of the uniform distribution (inclusive)
 * @param max The maximum bound of the uniform distribution (inclusive)
 * @return A vector of int values sampled from the uniform distribution
 */
inline std::vector<int> uniform_int(const std::vector<int>& min, const std::vector<int>& max) {
    auto dist = uniform_int_multi_distribution(min, max);
    return dist();
}


/**
 * Create a normal distribution in multiple dimensions
 * @param mean The means of the normal distributions
 * @param stddev The standard deviations of the normal distributions
 * @return A multi_distribution containing the normal distributions
 */
inline multi_distribution<Real>
normal_multi_distribution(const std::vector<Real>& mean, const std::vector<Real>& stddev) {
    multi_distribution<Real> md;
    for (uint i = 0; i < mean.size(); i++) {
        distribution<Real> d(std::normal_distribution<Real>(mean[i], stddev[i]), "normal", {mean[i], stddev[i]});
        md.push_back(std::make_shared<distribution<Real>>(d));
    }
    return md;
}

/**
 * Create and sample from a normal distribution
 * @param mean The mean of the normal distribution
 * @param stddev The standard deviation of the normal distribution
 * @return A value sampled from the defined normal distribution
 */
inline Real normal(Real mean = 0, Real stddev = 1) {
    distribution<Real> dist = std::normal_distribution<Real>(mean, stddev);
    return dist();
}

/**
 * Create and sample from a normal distribution in multiple dimensions
 * @param mean The means of the Gaussians
 * @param stddev The standard deviations of the Gaussians
 * @return A sample from the multi_distribution containing the normal distributions
 */
inline std::vector<Real> normal(const std::vector<Real>& mean, const std::vector<Real>& stddev) {
    auto dist = normal_multi_distribution(mean, stddev);
    return dist();
}

//! Maxwell-Boltzmann distribution with some extra bells and whistles
class maxwellian : public distribution<Real>{
public:
    maxwellian() = default;
    /**
     * Constructor for a maxwellian distribution
     * @param u_avg The average velocity of the maxwellian
     * @param u_therm The thermal velocity of the distribution -- 2\sigma^2 = v_{therm}^2
     */
    maxwellian(Real u_avg, Real u_therm) { setParams(u_avg, u_therm); }

    /**
     * Constructs a new maxwellian distribution with average velocity and temperature.
     * This distribution is not ready to use, it must be finalized with a call to setMass in order to compute the thermal velocity
     * @param u_avg The average velocity of the maxwellian
     * @param T The temperature of the maxwellian
     * @return A newly constructured maxwellian that is not ready for use
     */
    static std::shared_ptr<maxwellian> fromTemperature(Real u_avg, Real T){
        auto m = std::make_shared<maxwellian>();
        m->setVandT(u_avg, T);
        return m;
    }

    void setVandT(Real u_avg, Real T){
        _u_avg = u_avg;
        _T = T;
    }
    void convertTempToVelocity(Real m, Real kB){
        if(std::isfinite(_T))
            setParams(_u_avg, std::sqrt(kB*_T/m));
        else
            throw std::runtime_error("maxwellian (setMass) Error: Temperature was never set");
    }
    void setParams(Real u_avg, Real u_therm){
        _distribution = std::normal_distribution<Real>(u_avg, u_therm); // c++ distribution for sampling
        _name = "maxwellian";
        _params = {u_avg, u_therm};
        _u_avg = u_avg;
        _u_therm = u_therm;
        _u_bar_pos = u_bar(u_avg, u_therm, 1);
        _u_bar_neg = u_bar(u_avg, u_therm, -1);
        _u_max_pos = u_max(u_avg, u_therm, 1);
        _u_max_neg = u_max(u_avg, u_therm, -1);
        _f_max_pos = flux(_u_max_pos, 1);
        _f_max_neg = flux(_u_max_neg, -1);
        envelope_pos = std::normal_distribution<Real>(_u_max_pos, u_therm );
        envelope_neg = std::normal_distribution<Real>(_u_max_neg, u_therm );
    }

    Real u_avg(){ return _u_avg; } //!< Get the average velocity of the maxwellian
    Real u_therm(){ return _u_therm; } //!< Get the thermal velocity of the maxwellian

    /**
     * Get the average velocity of the flux of particles in the specified direction
     * @param direction The direction of the flux either \pm 1
     * @return The average velocity of the flux of particles in the specified direction
     */
    Real u_bar(Real direction){
        return (direction > 0) ? _u_bar_pos : _u_bar_neg;
    }

    /**
     * Compute the average velocity of the flux of particles in the specified direction
     * @param u_avg The average velocity of the underlying maxwellian
     * @param u_therm The thermal velocity of the underlying maxwellian
     * @param direction The direction of the flux either \pm 1
     * @return The average velocity of the flux of particles in the specified direction
     */
    static Real u_bar(Real u_avg, Real u_therm, Real direction){
        return (0.5*u_avg)*(direction + std::erf(u_avg / (sqrt_2*u_therm))) +
                (u_therm/std::sqrt(2.0*pi))*std::exp(-u_avg*u_avg/(2.0*u_therm*u_therm));
    }

    /**
     * Get the particle flux density in the specified direction at the velocity specified
     * (i.e. probability that a particle has velocity between v and v + dv per unit area per unit time)
     * f(u) = (1/(ubar*u_th*sqrt(pi))*u*exp(-(u-u_avg)^2/u_th^2)
     * @param v The velocity at which to evaluate the flux density
     * @param direction The direction of the particle flux
     * @return The flux density at the specified velocity in the specified direction
     */
    Real flux(Real v, Real direction){
        Real A = 1.0/(_u_therm*std::sqrt(2.0*pi)*u_bar(direction));
        Real dv = (v - _u_avg);
        return std::abs(A*v*std::exp(-dv*dv/(2.0*_u_therm*_u_therm)));
    }

    /**
     * The velocity that is most probable for the one way particle flux.
     * Found by taking the derivative of uf(u) w.r.t. u and setting it equal to zero
     * @param direction The direction of the flux
     * @return The max velocity
     **/
    Real u_max(Real direction) {
        return (direction > 0) ? _u_max_pos : _u_max_neg;
    }

    /**
     * The velocity that is most probable for the one way particle flux.
     * Found by taking the derivative of uf(u) w.r.t. u and setting it equal to zero
     * @param u_avg The average velocity of the underlying maxwellian
     * @param u_therm The thermal velocity of the underlying maxwellian
     * @param direction The direction of the flux
     * @return The max velocity
     */
    static Real u_max(Real u_avg, Real u_therm, Real direction){
        return 0.5*(direction*std::sqrt(u_avg*u_avg + 4.0*u_therm*u_therm) + u_avg);
    }

    /**
     * The value of the flux function at u_max
     * @param direction The direction of the flux
     * @return The value of the flux function at u_max
     */
    Real f_max(Real direction) {
        return (direction > 0) ? _f_max_pos : _f_max_neg;
    }

    /**
     * The function that envelopes the one-way flux density function
     * It is a guassian with a peak at the maximum of the one-way flux density
     * It is always greater than the one_way flux density
     * @param v The velocity to evaluate the envelope at
     * @param direction The direction of the flux
     * @return The value of the envelope at v
     */
    Real flux_envelope(Real v, Real direction){
        Real dv = (v - u_max(direction));
        return f_max(direction)*std::exp(-dv*dv / (2.0*_u_therm*_u_therm));
    }

    /**
     * Sample from the envelope function
     * @param direction The direction of the flux
     * @return A sample from the envelope function
     */
    Real envelope_sample(Real direction){
        return (direction > 0) ? envelope_pos() : envelope_neg();
    }

    /**
     * Sample the one-way flux function to get the velocity of a particle
     * Employs the rejection sampling technique - https://en.wikipedia.org/wiki/Rejection_sampling
     * @param direction The direction of the flux
     * @return The velocity sampled from the one-way flux function
     */
    Real flux_sample(Real direction){
        int attempts = 0;
        while(attempts++ < 1000) {
            Real u = uniform(); // Random uniform sample from 0 to 1
            Real y = envelope_sample(direction); // Random sample from envelope distribution
            if(y*direction > 0 && u < flux(y, direction) / flux_envelope(y, direction)) // Check if the value should be accepted
                return y;
        }

        throw std::runtime_error("maxwellian (flux_sample) Error: Couln't sample a velocity from flux distribution, "
                                         "1000 tries exceeded ");
    }

private:
    Real _T{std::nan("")}; //!< The temperature of the distribution (only set when fromTemp is used)
    Real _u_avg; //!< the average velocity of the maxwellian
    Real _u_therm{std::nan("")}; //!< The thermal velocity of the maxwellian

    Real _u_bar_pos; //!< The average velocity of particle flux in positive direction
    Real _u_bar_neg; //!< The average velocity of particle flux in negative direction

    Real _u_max_pos; //!< The most probable velocity in the positive direction
    Real _u_max_neg; //!< The most probable velocity in the negative direction

    Real _f_max_pos; //!< The max probability in the positive direction
    Real _f_max_neg; //!< The max probability in the negative direction

    distribution<Real> envelope_pos; //!< The envelope for the flux function sampling (positive flux)
    distribution<Real> envelope_neg; //!< The envelope for the flux function sampling (negative flux)
};

/**
 * Create a maxwellian distribution in multiple dimensions
 * @param mean The means of the normal distributions
 * @param stddev The standard deviations of the normal distributions
 * @return A multi_distribution containing the normal distributions
 */
inline multi_distribution<Real>
maxwellian_multi_distribution(const std::vector<Real>& mean, const std::vector<Real>& v_therm) {
    multi_distribution<Real> md;
    for (uint i = 0; i < mean.size(); i++)
        md.push_back(std::make_shared<maxwellian>(mean[i], v_therm[i]));
    return md;
}


inline std::shared_ptr<maxwellian> to_maxwellian(std::shared_ptr<distribution<Real>> dist){
    auto m = std::dynamic_pointer_cast<maxwellian>(dist);
    if(m)
        return m;
    else
        throw std::runtime_error("Error (to_maxwellian): Distribution " + dist->name() +
                                         " was not maxwellian, conversion failed.");
}

/**
 * Get the inverse of the error function
 * @param x A value from -1 to 1
 * @return The returns i in erf(i) = x
 */
inline double erfinv(double x) {
    const double a = 0.140012;
    if ((x <= -1.0) || (x >= 1.0))
        throw std::runtime_error(
                "Distributions (erfinv) Error: erfinv argument out of range (-1,1). Got " + std::to_string(x));

    double r1 = 2.0 / (pi * a) + log(1 - x * x) / 2.0;
    double result = sign(x) * std::sqrt(std::sqrt(r1 * r1 - log(1 - x * x) / (1.0 * a)) - r1);
    return result;
}

/**
 * Get a sample point from the cdf of a normal distribution
 * When the "sampleValue" argument is varied uniformly, the result are samples that come from a gaussian exactly
 *
 * @param mean The mean of the normal distribution
 * @param stddev The std deviation of the normal distribution
 * @param samplePoint A value between 0 and 1 -- a sample along the y axis
 * @return The value of the normal distribution relating to the provided sample point
 */
inline double normal_cdf_sample(double mean, double stddev, double samplePoint) {
    return std::sqrt(2.0) * stddev * (erfinv(2. * (samplePoint) + 0.001 - 1.)) + mean;
}

}


#endif
