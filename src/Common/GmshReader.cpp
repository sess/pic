#include "Grid.h"
#include "Label.h"
#include "GmshReader.h"
#include "Threads.h"
#include <set>

using namespace std;
using namespace picard;

int GmshElement::numIndices(int elType) {
    switch (elType) {
        case POINT :
            return 1;
        case BAR :
            return 2;
        case QUAD :
            return 4;
        case HEX :
            return 8;
        default:
            throw runtime_error("GmshReader Error (numIndices): Element type (" + to_string(elType) +
                                ") not a supported type.");
    }
}

int GmshElement::dimension(int elType) {
    switch (elType) {
        case POINT :
            return 0;
        case BAR :
            return 1;
        case QUAD :
            return 2;
        case HEX :
            return 3;
        default:
            throw runtime_error("GmshReader Error (dimension): Element type (" + to_string(elType) +
                                ") not a supported type.");
    }
}

const std::string GmshReader::nodeToken = "$Nodes";
const std::string GmshReader::endNodeToken = "$EndNodes";
const std::string GmshReader::elementToken = "$Elements";
const std::string GmshReader::endElementToken = "$EndElements";
const std::string GmshReader::labelToken = "$PhysicalNames";
const std::string GmshReader::endLabelToken = "$EndPhysicalNames";

GmshReader::GmshReader(std::string fileName) {
    parse(fileName);
}

void GmshReader::parse(string fileName) {
    _fileName = fileName; // Store the most recently parsed file

    ifstream infile(fileName);
    if (!infile.is_open())
        throw runtime_error("GmshReader Error (parse): Failed to open file: " + fileName);

    string line;
    while (getline(infile, line)) {
        if (line == nodeToken)
            parseNodes(infile);

        if (line == elementToken)
            parseElements(infile);

        if (line == labelToken)
            parseLabels(infile);
    }

    // Determine the Mesh dimension from the max label dimension
    for (auto label : _labels)
        _nDim = std::max(_nDim, label.second.dim);
}

int GmshReader::getNumEntries(ifstream& infile) {
    string line;
    getline(infile, line);
    istringstream iss(line);
    int numEntries;
    if (!(iss >> numEntries))
        throw runtime_error("GmshReader Error (getNumEntries): Couldn't find number of entries at line: " + line);

    return numEntries;
}

void GmshReader::confirmEndToken(ifstream& infile, string endToken) {
    string line;
    getline(infile, line);

    if (line != endToken)
        throw runtime_error("GmshReader Error (confirmEndToken): Didn't see end token at line: " + line +
                            ". Expected \"" + endToken + "\"");
}

void GmshReader::parseNodes(ifstream& infile) {
    string line;
    int numNodes = getNumEntries(infile);
    for (auto node = 0; node < numNodes; node++) {
        getline(infile, line);
        istringstream iss2(line);

        GmshNode n;
        if (!(iss2 >> n.id >> n.r[0] >> n.r[1] >> n.r[2]))
            throw runtime_error("GmshParser Error (readFile): Couldn't read in all node dimensions. At line: " + line);

        _nodes.push_back(n);
    }

    confirmEndToken(infile, endNodeToken);
}

void GmshReader::parseElements(ifstream& infile) {
    string line;
    int numElements = getNumEntries(infile);
    for (auto element = 0; element < numElements; element++) {
        getline(infile, line);
        istringstream iss(line);

        GmshElement e;

        // Read element type information
        if (!(iss >> e.id >> e.type >> e.numTags) || e.numTags == 0)
            throw runtime_error("GmshReader Error (parseElements): Couldn't read in element info. At line " + line);

        // Read the tags associated with the element
        e.tags.resize(e.numTags);
        for (auto tag = 0; tag < e.numTags; tag++) {
            if (!(iss >> e.tags[tag]))
                throw runtime_error("GmshReader Error (parseElements): Couldn't read " + to_string(e.numTags) +
                                    " element tags at line " + line);
        }
        e.physicalId = e.tags[0];

        // Read the index list of the element type
        auto num_indices = GmshElement::numIndices(e.type);
        e.indices.resize(num_indices);
        for (auto index = 0; index < num_indices; index++) {
            if (!(iss >> e.indices[index]))
                throw runtime_error("GmshReader Error (parseElements): Couldn't read " + to_string(num_indices) +
                                    " indices at line " + line);
        }

        _elements.push_back(e); // Set the physical id to be used for labeling
    }

    confirmEndToken(infile, endElementToken);
}

void GmshReader::parseLabels(ifstream& infile) {
    string line;
    int numLabels = getNumEntries(infile);
    for (auto label = 0; label < numLabels; label++) {
        getline(infile, line);
        istringstream iss(line);

        GmshLabel l;
        if (!(iss >> l.dim >> l.id >> l.name))
            throw runtime_error("GmshReader Error (parseLabels): Couldn't read in label info. At line " + line);

        // Convert the name to lowercase and then store in the label container
        l.name = to_lower(l.name);
        l.name = trim(l.name, "\"");
        _labels[l.id] = l;
    }
    confirmEndToken(infile, endLabelToken);
}

shared_ptr<Grid> GmshReader::makeGrid(vector<shared_ptr<FieldLabel>> fieldLabels,
                                      vector<shared_ptr<ParticleBoundaryLabel>> particleBCLabels, GridType type) {
    sort();

    // Get the domain, and number of points in each dim
    Domain d = getDomain();
    ivec numGridPts = getNumGridPoints();

    shared_ptr<Grid> grid;
    switch (type) {
        case BASIC_GRID :
            grid = make_shared<Grid>(d, numGridPts);
            break;
        case NEUTRALIZING_GRID :
            grid = make_shared<NeutralizingGrid>(d, numGridPts);
            break;
        default:
            throw runtime_error("GMshReader (makeGrid) Error: Did not recognize type");
    }
    // Add the nodes to the grid
    auto& positions = grid->getPoints();
    for (uint n = 0; n < _nodes.size(); n++)
        for (auto i = 0; i < _nDim; i++)
            positions(n, i) = _nodes[n].r[i];

    // Make a map to access the labels
    map<string, shared_ptr<FieldLabel>> field;
    for (auto fl : fieldLabels)
        field[fl->name] = fl;

    map<string, shared_ptr<ParticleBoundaryLabel>> particle;
    for (auto pl : particleBCLabels)
        particle[pl->name] = pl;

    // use labels to populate, domains, node labels, and element labels
    grid->subDomains = getSubDomains(field);

    grid->fieldLabels.assign(_nodes.size(), nullptr);
    for (auto& e : _elements) {
        auto& name = _labels[e.physicalId].name;
        if (field.find(name) == field.end())
            throw runtime_error("GmshReader Error (makeGrid): Could not find a field label for gmshlabel: " + name);
        auto proposedLabel = field[name];
        for (auto i : e.indices) {
            auto currLabel = grid->fieldLabels[i];
            if (!currLabel || currLabel->priority < proposedLabel->priority)
                grid->fieldLabels[i] = proposedLabel;
        }
    }
    grid->addDirichletLabelIfNeeded();

    int boundaryElementSize = std::pow(2, _nDim - 1);
    for (auto& e : _elements) {
        if (e.dimension() == _nDim - 1) { // Only use boundary elements
            auto& name = _labels[e.physicalId].name;
            if (particle.find(name) == particle.end())
                throw runtime_error(
                        "GmshReader Error (makeGrid): Could not find a particle label for gmshlabel: " + name);

            auto proposedLabel = particle[name];
            Element boundaryElement(_nDim-1);

            for (auto i = 0; i < boundaryElementSize; i++)
                boundaryElement[i] = e.indices[i];

            fastSort4(boundaryElement, std::greater<int>());
            auto res = grid->particleBoundaryLabels.find(boundaryElement);
            if (res == grid->particleBoundaryLabels.end() || res->second->priority < proposedLabel->priority)
                grid->particleBoundaryLabels[boundaryElement] = proposedLabel;
        }
    }

    return grid;
}

vector<Domain> GmshReader::getSubDomains(const map<string, shared_ptr<FieldLabel>>& fieldLabels) {
    map<int, Domain> distinct_domains;
    for (auto& e : _elements) {
        // Use only the elements of the highest dimension for region information
        if (e.dimension() == _nDim) {
            auto res = distinct_domains.insert({e.physicalId, Domain(_nDim, true, e.physicalId)});
            auto& d = res.first->second;
            for (auto i : e.indices) {
                d.expandToInclude(_nodes[i].r);
            }
        }
    }

    vector<Domain> domains;
    for (auto entry : distinct_domains) {
        domains.push_back(entry.second);
        auto labelName = _labels[entry.first].name;
        auto& label = fieldLabels.at(labelName);
        if (label->type != FieldLabel::INTERIOR)
            throw runtime_error("GmshReader Error (getSubDomains): The label " + label->name +
                                " is not of type INTERIOR but was associated with an element of dim: "
                                + to_string(_nDim));
        domains.back().setParticlesPermitted(label->properties[1] > 0);
    }
    return domains;
}

Domain GmshReader::getDomain() {
    Domain d(_nDim);
    for (auto& n : _nodes)
        d.expandToInclude(n.r);

    return d;
}

ivec GmshReader::getNumGridPoints() {
    ivec ng(_nDim, 1);

    uint stride = 1;
    auto r0 = _nodes[0].r;
    for (auto dim = 0; dim < _nDim - 1; dim++) {
        auto index = stride;
        while (index < _nodes.size() && _nodes[index].r[dim] != r0[dim])
            index += stride;

        ng[dim] = index / stride;
        stride *= index / stride;
    }

    ng[_nDim - 1] = _nodes.size() / stride;
    return ng;
}

void GmshReader::sort() {
    auto tolerance = std::pow(getDomain().volume() / _nodes.size(), 1.0 / (Real) _nDim) * 0.1;
    set<GmshNode, SpatialLessThan> sortedNodes(SpatialLessThan(tolerance, _nDim));
    map<int, int> removedNodes; // A map from the removed node id to the id of the node that remained

    // Create a sorted list of nodes with no duplicates
    for (auto& n : _nodes) {
        auto it = sortedNodes.find(n);
        if (it == sortedNodes.end())
            sortedNodes.insert(n);
        else
            removedNodes[n.id] = it->id;
    }

    // Construct mapping between the node "id" to its actual index
    map<int, int> index;
    int count = 0;
    for (auto& n : sortedNodes)
        index[n.id] = count++;

    for (auto& entry : removedNodes)
        index[entry.first] = index[entry.second];

    // Replace the node list with new ordered list and update the index list of the elements
    _nodes = vector<GmshNode>(sortedNodes.begin(), sortedNodes.end());
    for (auto& element : _elements)
        for (auto& i : element.indices)
            i = index[i];
}

map<string, Domain> GmshReader::getLabelDomains() {
    map<string, Domain> labelDomains;

    for (auto& e : _elements) {
        auto res = labelDomains.insert({_labels[e.physicalId].name, Domain(_nDim)});
        for (auto i : e.indices) {
            res.first->second.expandToInclude(_nodes[i].r);
        }
    }
    return labelDomains;
}

bool SpatialLessThan::operator()(const GmshNode& n1, const GmshNode& n2) const {
    // compare most significant dimension down to least significant dimension in a lexicographical sense
    for (auto i = nDim - 1; i >= 0; i--) {
        if (feq(n1.r[i], n2.r[i]))
            continue;
        else
            return n1.r[i] < n2.r[i];
    }
    return false;
}
