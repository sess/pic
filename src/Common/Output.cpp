#include <unordered_set>
#include "IOUtils.h"
#include "Output.h"
#include "Particle.h"
#include "Grid.h"
#include "Simulation.h"
#include "Physics.h"
#include <cstdlib>

using namespace picard;
using namespace std;

const std::string OutputEntry::infoFolder = "info";
const std::string OutputEntry::dataFolder = "data";
const std::string Output::speciesIdHeader = "Species Id";

const std::set<OutputTarget> OutputEntry::ignoresSuffix = {
        SPECIES_COUNT,
        KINETIC_ENERGY,
        POTENTIAL_ENERGY,
        TOTAL_ENERGY,
        SPECIES_TEMPERATURE,
        SPECIES_VELOCITY
};

const std::set<OutputTarget> OutputEntry::ignoresPrefix = {
    PHYSICS
};


const std::set<OutputTarget> OutputEntry::isData = {
        POSITION,
        VELOCITY,
        SPECIES_COUNT,
        ELECTRIC_FIELD,
        MAGNETIC_FIELD,
        VELOCITY_AVERAGE,
        VELOCITY_VARIANCE,
        NUMBER_DENSITY,
        TEMPERATURE,
        POTENTIAL,
        CHARGE_DENSITY,
        SURFACE_CHARGE_DENSITY,
        KINETIC_ENERGY,
        POTENTIAL_ENERGY,
        TOTAL_ENERGY,
        SPECIES_TEMPERATURE,
        SPECIES_VELOCITY,
        PHYSICS,
};

const std::set<OutputTarget> OutputEntry::isInfo = {};


// TODO: Output processor load (i.e. fraction of particles per node)

bool OutputEntry::shouldWrite(int iteration, double time) const{
    return (iterations.find(iteration) != iterations.end()) ||
            ((iteration % interval == 0) && fgeq(time, startTime) && fleq(time, endTime));
}

bool OutputEntry::isEqualIfWritten(const OutputEntry& other) const{
    return target == other.target
           && prefix == other.prefix
           && indices == other.indices
           && timestampFolder == other.timestampFolder;
}

bool OutputEntry::operator==(const OutputEntry& other) const {
    return isEqualIfWritten(other)
           && other.interval == interval
           && other.startTime == startTime
           && other.endTime == endTime
           && other.iterations == iterations
           && other.directory == directory
           && other.originalDirectory == originalDirectory
           && other.fileExtension == fileExtension
           && other.suffix == suffix
           && other.precision == precision
           && other.iterations == iterations
           && other.skip == skip;
}

bool OutputEntry::useSuffix() const {
    return ignoresSuffix.find(target) == ignoresSuffix.end();
}

bool OutputEntry::usePrefix() const {
    return ignoresPrefix.find(target) == ignoresPrefix.end();
}

std::string OutputEntry::filePath(const Simulation& simulation, string pre_prefix) const {
    string fullPath = directory + subFolder() + pre_prefix;
    if(usePrefix())
       fullPath += prefix;

    if(useSuffix()) {
        fullPath += "_";
        if (suffix == TIME)
            fullPath += "t" + to_string(simulation.time());
        else if (suffix == ITERATION)
            fullPath += "i" + to_string(simulation.iteration());
    }
    return fullPath + fileExtension;
}

std::string OutputEntry::filePath(string pre_prefix) const {
    return directory + subFolder() + pre_prefix + prefix + fileExtension;
}

string OutputEntry::subFolder() const {
    if(isData.find(target) != isData.end())
        return addSlash(dataFolder);
    else if(isInfo.find(target) != isInfo.end())
        return addSlash(infoFolder);
    return "";
}

std::ostream& picard::operator<<(std::ostream& os, OutputEntry entry){
    os << "write=" << entry.target << ", interval=" << entry.interval << ", prefix=" << entry.prefix << ", start="
       << entry.startTime << ", end=" << entry.endTime << ", indices=(" << entry.indices << "),  directory="
       << entry.directory << ", original directory=" << entry.originalDirectory << ", file extension="
       << entry.fileExtension << ", suffix=" << entry.suffix << ", precision=" << entry.precision
       << ", timestampFolder=" << entry.timestampFolder << ", iterations=("
       << std::vector<int>(entry.iterations.begin(), entry.iterations.end()) << "), skip=" << to_string(entry.skip);
    return os;
}

void Output::add(const OutputEntry& default_oe, const std::vector<OutputTarget>& targets) {
    for(auto& target : targets){
        OutputEntry oe(default_oe);
        oe.target = target;
        oe.setDefaultPrefix();
        add(oe);
    }
}

bool Output::isRepeat(const OutputEntry& entry, const std::vector<OutputEntry>& outputs) const {
    for(auto& oe : outputs){
        if(oe.isEqualIfWritten(entry))
            return true;
    }
    return false;
}

bool Output::hasTarget(OutputTarget target) {
    for(auto& entry : _entries){
        if(entry.target == target)
            return true;
    }
    return false;
}

std::set<std::string> Output::directories() const {
    std::set<std::string> dirs;
    for (auto& e : entries())
        dirs.insert(e.directory);
    return dirs;
}

void Output::makeDirectories() const {
    for (auto& e : entries()) {
        makeDirectory(e.originalDirectory);
        string dir = e.directory;
        makeDirectory(dir);
        makeDirectory(addSlash(dir + OutputEntry::infoFolder));
        makeDirectory(addSlash(dir + OutputEntry::dataFolder));
    }
}

void Output::removeOutputFiles() const {
    for (auto& path : _usedFiles)
        remove(path.c_str());
}

void Output::removeDirectories() const {
    for (auto& dir : directories()) {
        removeDirectoryIfEmpty(addSlash(dir + OutputEntry::dataFolder));
        removeDirectoryIfEmpty(addSlash(dir + OutputEntry::infoFolder));
        removeDirectoryIfEmpty(dir);
    }
}

void Output::analyze(const Simulation& simulation) const {
    if(!isRoot())
        return;

    if(simulation.shouldSpeak())
        cout << "Analyzing the results with julia..." << endl;

    for (auto& dir : directories()) {
        stringstream s;
        s << "julia " << _analysisFile << " " << dir << " " << simulation.timestep() ;
        for(auto entry : _additionalJuliaFiles)
            s << " " << entry;
        s << endl;
        if(simulation.shouldSpeak())
            cout << s.str() << endl;

        auto res = system(s.str().c_str());
    }
}

void Output::runSystemCalls() const {
    if(!isRoot() || _systemCalls.empty())
        return;

    cout << "Running system calls..." << endl;
    // Set the output directories to a variable called OUT_DIRS
    string out_dirs;
    for(auto dir : directories())
        out_dirs += dir + " ";

#ifdef _WIN32
	_putenv_s("OUT_DIRS", out_dirs.c_str());
#else
    setenv("OUT_DIRS", out_dirs.c_str(), true);
#endif
    // run the system calls
    for(auto s : _systemCalls){
        cout << s << endl;
        auto res = system(s.c_str());
    }
    cout << "Done running system calls" << endl;
}

void Output::clearSuffixFreeContents() {
    for (auto& e : entries()) {
        if(!e.useSuffix())
            clear(e.filePath());
    }
}

void Output::setDirectory(std::string directory) {
    for(auto& entry : _entries)
        entry.directory = directory;
}

void Output::setSuffix(OutputSuffix suffix) {
    for(auto& entry : _entries)
        entry.suffix = suffix;
}

void Output::setPrecision(int precision) {
    for(auto& entry : _entries)
        entry.precision = precision;
}

void Output::setFileExtension(std::string extension) {
    for(auto& entry : _entries)
        entry.fileExtension = extension;
}

void Output::setTimestampFolder(bool use_timestamp) {
    for(auto& entry : _entries)
        entry.timestampFolder = use_timestamp;
}

void Output::addTimestampFolderToDirectory(long timestamp) {
    for (auto& entry : _entries) {
        if (entry.timestampFolder)
            entry.directory = entry.originalDirectory + to_string(timestamp) + "/";
    }
}

void Output::write(const Simulation& simulation) {
    vector<OutputEntry> used;
    for (auto& e : entries()) {
        if (e.shouldWrite(simulation.iteration(), simulation.time()) && !isRepeat(e, used)) {
            used.push_back(e);
            distributeWrite(e, simulation);
        }
    }
}

void Output::writeAll(const OutputEntry& defaultOe, const Simulation& simulation){
    for(int i=0; i<NUMBER_OF_TARGETS; i++){
        OutputTarget target = (OutputTarget)i;
        if(hasTarget(target))
            continue;
        OutputEntry oe(defaultOe);
        oe.target = target;
        oe.setDefaultPrefix();
        distributeWrite(oe, simulation);
    }
}

void Output::distributeWrite(const OutputEntry& entry, const Simulation& simulation) {
    // Check to see if this entry should be skipped
    if(entry.skip)
        return;

    // TODO: move this to a separate section that is unrelated to the specific output entry.
    // TODO: Can this be combined with the other things that get written once (i.e. config, command line args, etc)
    // Write out needed simulation info if anything is going to written
    if (isRoot() && simulation.iteration() == 0) {
        // Write out the grid dimensions
        auto gridPath = entry.directory + addSlash(OutputEntry::infoFolder) + "grid.csv";
        if (_usedFiles.find(gridPath) == _usedFiles.end()) {
            _usedFiles.insert(gridPath);
            writeGrid(gridPath, simulation.grid(), entry.precision);
        }

        // Write out the species Traits
        auto speciesTraitsPath = entry.directory + addSlash(OutputEntry::infoFolder) + "Species_Traits.csv";
        if (_usedFiles.find(speciesTraitsPath) == _usedFiles.end()) {
            _usedFiles.insert(speciesTraitsPath);
            writeSpeciesTraits(speciesTraitsPath, simulation.particles(), entry.precision);
        }

        // Write out the scaling parameters
        auto scalingPath = entry.directory + addSlash(OutputEntry::infoFolder) + "Scaling.csv";
        if (_usedFiles.find(scalingPath) == _usedFiles.end()) {
            _usedFiles.insert(scalingPath);
            picard::write(scalingPath, simulation.referenceParams().toCSVOutput());
        }

        auto def_scalingPath = entry.directory + addSlash(OutputEntry::infoFolder) + "default_scaling.csv";
        if (_usedFiles.find(def_scalingPath) == _usedFiles.end()) {
            _usedFiles.insert(def_scalingPath);
            picard::write(def_scalingPath, simulation.referenceParams().defaultScalingCSVOutput());
        }
    }

    // Section for targets that will be written once per thread (does not require root thread only)
    string path = entry.filePath(simulation, "rank" + to_string(rank()) + "_");
    switch (entry.target) {
        case POSITION :
            _usedFiles.insert(path);
            writePositions(path, simulation.particles(), entry.indices, entry.precision);
            return;
        case VELOCITY :
            _usedFiles.insert(path);
            writeVelocities(path, simulation.particles(), entry.indices, entry.precision);
            return;
        default :
            break;
    }


    path = entry.filePath(simulation);
    // Section for targets that require a reduction
    switch (entry.target) {
        case SPECIES_COUNT :
            _usedFiles.insert(path);
            writeSpeciesCount(path, simulation, entry.precision);
            return;
        case KINETIC_ENERGY :
            _usedFiles.insert(path);
            writeKineticEnergy(path, simulation, entry.precision);
            return;
        case POTENTIAL_ENERGY :
            _usedFiles.insert(path);
            writePotentialEnergy(path, simulation, entry.precision);
            return;
        case TOTAL_ENERGY :
            _usedFiles.insert(path);
            writeTotalEnergy(path, simulation, entry.precision);
            return;
        case SPECIES_TEMPERATURE :
            _usedFiles.insert(path);
            writeSpeciesTemperature(path, simulation, entry.precision);
            return;
        case SPECIES_VELOCITY :
            _usedFiles.insert(path);
            writeSpeciesVelocity(path, simulation, entry.precision);
            return;
        case PHYSICS :
            for (auto p : simulation.localPhysics()) {
                auto physPath = entry.filePath(simulation, clean(p->name()));
                p->write(physPath);
                _usedFiles.insert(physPath);
            }
            return;
        case OUTPUT_ALL :
            writeAll(entry, simulation);
            return;
        default :
            break;
    }

    // Section for root thread only outputs (don't require a reduction)
    if (isRoot()) {
        switch (entry.target) {
            case ELECTRIC_FIELD :
                _usedFiles.insert(path);
                writeElectricField(path, simulation.grid(), entry.indices, entry.precision);
                return;
            case MAGNETIC_FIELD :
                _usedFiles.insert(path);
                writeMagneticField(path, simulation.grid(), entry.indices, entry.precision);
                return;
            case VELOCITY_AVERAGE :
                _usedFiles.insert(path);
                writeVelocityAverage(path, simulation, entry.indices, entry.precision);
                return;
            case VELOCITY_VARIANCE :
                _usedFiles.insert(path);
                writeVelocityVariance(path, simulation, entry.indices, entry.precision);
                return;
            case NUMBER_DENSITY :
                _usedFiles.insert(path);
                writeNumberDensity(path, simulation, entry.indices, entry.precision);
                return;
            case TEMPERATURE :
                _usedFiles.insert(path);
                writeTemperature(path, simulation, entry.indices, entry.precision);
                return;
            case POTENTIAL:
                _usedFiles.insert(path);
                writePotential(path, simulation.grid(), entry.indices, entry.precision);
                return;
            case CHARGE_DENSITY :
                _usedFiles.insert(path);
                writeChargeDensity(path, simulation, entry.indices, entry.precision);
                return;
            case SURFACE_CHARGE_DENSITY :
                _usedFiles.insert(path);
                writeSurfaceChargeDensity(path, simulation, entry.indices, entry.precision);
                return;
            default:
                cerr << "Warning (Output::distributeWrite): Found unknown write target ("
                     << (int) entry.target << "). Skipping" << endl;
                return;
        }
    }
}

void Output::writePositions(string filePath, const Particles& particles, const vector<ulong>& indices, int precision) {
    if (particles.empty())
        return;

    tic("write positions");
    unordered_set<int> index_set(indices.begin(), indices.end());
    vector<string> output;
    std::string header = speciesIdHeader + ", ";
    size_t pindex = 0;
    for (auto& species : particles) {
        for (auto& p : species.second) {
            if (index_set.empty() || index_set.find(pindex) != index_set.end()) {
                if(output.empty())
                    header += tsfs(dimensionalHeader(p.r.size())) + "\n";

                std::stringstream s;
                s << p.speciesId() << "," << appendDefault(precision) << p.r;
                output.push_back(s.str());
            }
            pindex++;
        }
    }

    write1DVector(output, filePath, setDelimiter(""), header);
    toc();
}

void Output::writeVelocities(string filePath, const Particles& particles, const vector<ulong>& indices, int precision) {
    if (particles.empty())
        return;

    tic("write velocities");
    unordered_set<int> index_set(indices.begin(), indices.end());
    vector<string> output;
    std::string header = speciesIdHeader + ", ";
    size_t pindex = 0;
    for (auto& species : particles) {
        for (auto& p : species.second) {
            if (index_set.empty() || index_set.find(pindex) != index_set.end()) {
                if(output.empty())
                    header += tsfs(dimensionalHeader(p.v.size(), "v")) + "\n";

                std::stringstream s;
                s << p.speciesId() << "," << appendDefault(precision) << p.v;
                output.push_back(s.str());
            }
            pindex++;
        }
    }

    write1DVector(output, filePath, setDelimiter(""), header);
    toc();
}

void Output::writeSpeciesTraits(string filePath, const Particles& particles, int precision) {
    if (particles.empty())
        return;

    tic("write species traits");
    append(filePath, Species::headers());
    for (auto& species : particles) {
        append(filePath, *species.first);
    }
    toc();
}

void Output::writeSpeciesCount(std::string filePath, const Simulation& simulation, int precision) {
    tic("write species count");
    auto sc = simulation.species_count();

    if (isRoot()) {
        // Append species names header on time appending to file
        if(isEmpty(filePath))
            append(filePath, concatenate(concatenate({"Time"}, simulation.species_names()), {"Total"}));

        append(filePath, simulation.time(), setFloatPrecision(precision) << castToString() << setEnd(","));
        std::vector<size_t> count;
        for (auto& species : sc)
            count.push_back(species.second);
        count.push_back(sum(count));
        append(filePath, count);
    }
    toc();
}

void Output::writeElectricField(string filePath, const Grid& grid, const vector<ulong>& indices, int precision) {
    tic("write efield");
    write2DVector(grid.eField, filePath, appendDefault(precision, indices), dimensionalHeader(grid.nDim(), "E"));
    toc();
}

void Output::writeMagneticField(string filePath, const Grid& grid, const vector<ulong>& indices, int precision) {
    tic("write bfield");
    if(grid.hasBField())
      write2DVector(grid.bField, filePath, appendDefault(precision, indices), dimensionalHeader(grid.nDim(), "B"));
    toc();
}

void Output::writePotential(string filePath, const Grid& grid, const vector<ulong>& indices, int precision) {
    tic("write phi");
    write1DVector(grid.potential, filePath, stackVectorDefault(precision, indices), "Potential");
    toc();
}

void Output::writeChargeDensity(string filePath, const Simulation& sim, const vector<ulong>& indices, int precision) {
    tic("write charge density");
    auto& grid = sim.grid();
    auto cp = grid.chargeDensity;
    auto r = grid.chargeDensity.rows();
    auto c = grid.chargeDensity.cols();
    cp.conservativeResize(r, c + 1);

    for (int i = 0; i < grid.totalPoints(); i++)
        cp(i, c) = grid.getChargeDensity(i);

    write2DVector(cp, filePath, appendDefault(precision, indices), concatenate(sim.species_names(), {"total"}));
    toc();
}

void Output::writeSurfaceChargeDensity(string filePath, const Simulation& sim, const vector<ulong>& indices, int precision) {
    auto& grid = sim.grid();
    if (grid.surfaceChargeMap.empty())
        return;

    tic("write surface charge");
    if(isEmpty(filePath))
        append(filePath, sim.species_names());

    if (!indices.empty())
        cerr << "Warning! no support for index-specific surface charge yet" << endl;
    for (auto& entry : grid.surfaceChargeMap) {
        append(filePath, entry.first, setEnd(","));
        append(filePath, grid.surfaceChargeGlobal[entry.second], appendDefault(precision));
    }
    toc();
}

void Output::writeKineticEnergy(std::string path, const Simulation& simulation, int precision) {
    tic("kinetic energy");
    auto ke = simulation.kinetic_energy();
    if(isRoot()) {
        if(isEmpty(path))
            append(path, concatenate(concatenate({"Time"}, simulation.species_names()), {"All Species"}));

        auto row = concatenate(concatenate({simulation.time()}, ke), {sum(ke)});
        append(path, row, appendDefault(precision));
    }
    toc();
}

void Output::writePotentialEnergy(std::string path, const Simulation& simulation, int precision) {
    tic("potential energy");
    auto pe = simulation.potential_energy();
    if(isRoot()) {
        if(isEmpty(path))
            append(path, concatenate(concatenate({"Time"}, simulation.species_names()), {"All Species"}));

        auto row = concatenate(concatenate({simulation.time()}, pe), {sum(pe)});
        append(path, row, appendDefault(precision));
    }
    toc();
}

void Output::writeTotalEnergy(std::string path, const Simulation& simulation, int precision) {
    tic("total energy");
    auto te = simulation.total_energy();
    if(isRoot()) {
        if(isEmpty(path))
            append(path, concatenate(concatenate({"Time"}, simulation.species_names()), {"All Species"}));

        auto row = concatenate(concatenate({simulation.time()}, te), {sum(te)});
        append(path, row, appendDefault(precision));
    }
    toc();
}

void Output::writeSpeciesTemperature(std::string path, const Simulation& simulation, int precision) {
    tic("species temp");
    if(isRoot()) {
        if(isEmpty(path)) {
            vector<string> headers = {"Time"};
            for(auto s : simulation.species_names())
                for (auto dim : dimensionalHeader(simulation.grid().nDim(), "T_"))
                    headers.push_back(s + " " + dim);
            append(path, headers);
        }

        vec row = {simulation.time()};
        for (auto& s : simulation.particles()) {
            vector2d<Real> temp_avg = simulation.grid()._temperature[s.first->id].colwise().mean();
            row = concatenate(row, getRow(temp_avg, 0));
        }

        append(path, row, appendDefault(precision));
    }
    toc();
}

void Output::writeSpeciesVelocity(std::string path, const Simulation& simulation, int precision){
    tic("species vel");
    if(isRoot()) {
        if(isEmpty(path)) {
            vector<string> headers = {"Time"};
            for(auto s : simulation.species_names())
                for (auto dim : dimensionalHeader(simulation.grid().nDim(), "V_"))
                    headers.push_back(s + " " + dim);
            append(path, headers);
        }

        vec row = {simulation.time()};
        for (auto& s : simulation.particles()) {
            vector2d<Real> v = simulation.grid()._averageVelocity[s.first->id].colwise().mean();
            row = concatenate(row, getRow(v, 0));
        }
        append(path, row, appendDefault(precision));
    }
    toc();
}

void Output::writeVelocityAverage(std::string path, const Simulation& sim, const std::vector<ulong> &indices, int precision) {
    tic("write grid_v_avg");
    auto& grid = sim.grid();

    vector2d<Real> avgVel(grid._averageVelocity.size()*grid.totalPoints(), grid.nDim());
    vector<int> ids;
    for(uint s =0; s<grid._averageVelocity.size(); s++) {
        for (uint i = 0; i < grid._averageVelocity[s].rows(); i++){
            ids.push_back(sim.getSpecies(s)->id);
            for (uint j = 0; j < grid._averageVelocity[s].cols(); j++)
                avgVel(s*grid.totalPoints() + i, j) = grid._averageVelocity[s](i,j);
        }
    }
    auto header = concatenate({speciesIdHeader}, dimensionalHeader(sim.grid().nDim(),"V_Avg_"));
    write2DVector(avgVel, path, appendDefault(precision, indices), header, ids);
    toc();
}

void Output::writeVelocityVariance(std::string path, const Simulation& sim, const std::vector<ulong> &indices, int precision) {
    tic("write v_var");
    auto& grid = sim.grid();

    vector2d<Real> avgVel(grid._velocityVariance.size()*grid.totalPoints(), grid.nDim());
    vector<int> ids;
    for(uint s =0; s<grid._velocityVariance.size(); s++) {
        for (uint i = 0; i < grid._velocityVariance[s].rows(); i++){
            ids.push_back(sim.getSpecies(s)->id);
            for (uint j = 0; j < grid._velocityVariance[s].cols(); j++)
                avgVel(s*grid.totalPoints() + i, j) = grid._velocityVariance[s](i,j);
        }
    }
    auto header = concatenate({speciesIdHeader}, dimensionalHeader(sim.grid().nDim(),"V_var_"));
    write2DVector(avgVel, path, appendDefault(precision, indices), header, ids);
    toc();
}

void Output::writeNumberDensity(std::string path, const Simulation& sim, const std::vector<ulong> &indices, int precision) {
    tic("write number density");
    write2DVector(sim.grid().numberDensity, path, appendDefault(precision, indices), sim.species_names());
    toc();
}

void Output::writeTemperature(std::string path, const Simulation& sim, const std::vector<ulong> &indices, int precision) {
    tic("write temperature");
    auto& grid = sim.grid();

    vector2d<Real> avgVel(grid._temperature.size()*grid.totalPoints(), grid.nDim());
    vector<int> ids;
    for(uint s =0; s<grid._temperature.size(); s++) {
        for (uint i = 0; i < grid._temperature[s].rows(); i++){
            ids.push_back(sim.getSpecies(s)->id);
            for (uint j = 0; j < grid._temperature[s].cols(); j++)
                avgVel(s*grid.totalPoints() + i, j) = grid._temperature[s](i,j);
        }
    }
    auto header = concatenate({speciesIdHeader}, dimensionalHeader(sim.grid().nDim(),"T_"));
    write2DVector(avgVel, path, appendDefault(precision, indices), header, ids);
    toc();
}

void Output::writeGrid(std::string path, const Grid& grid, int precision) {
    clear(path);
    vector<std::string> gridHeaders = {"ndim", "xmin", "ymin", "zmin", "xmax", "ymax", "zmax", "nx", "ny", "nz"};
    append(path, gridHeaders);
    append(path, grid.nDim(), setEnd(","));
    append(path, resize(grid.min(), 3, 0.), setDelimiter(",") << setEnd(",") << setFloatPrecision(precision));
    append(path, resize(grid.max(), 3, 0.), setDelimiter(",") << setEnd(",") << setFloatPrecision(precision));
    append(path, resize(grid.numPoints(), 3, 1));
}


OutputTarget picard::getTarget(string name){
    name = picard::to_lower(name);
    if(name == "position")
        return POSITION;
    else if(name == "velocity")
        return VELOCITY;
    else if(name == "species_count")
        return SPECIES_COUNT;
    else if(name == "electric_field")
        return ELECTRIC_FIELD;
    else if(name == "magnetic_field")
        return MAGNETIC_FIELD;
    else if(name == "velocity_average")
        return VELOCITY_AVERAGE;
    else if(name == "velocity_variance")
        return VELOCITY_VARIANCE;
    else if(name == "number_density")
        return NUMBER_DENSITY;
    else if(name == "temperature")
        return TEMPERATURE;
    else if(name == "potential")
        return POTENTIAL;
    else if(name == "charge_density")
        return CHARGE_DENSITY;
    else if(name == "surface_charge_density")
        return SURFACE_CHARGE_DENSITY;
    else if(name == "kinetic_energy")
        return KINETIC_ENERGY;
    else if(name == "potential_energy")
        return POTENTIAL_ENERGY;
    else if(name == "total_energy")
        return TOTAL_ENERGY;
    else if(name == "species_temperature")
        return SPECIES_TEMPERATURE;
    else if(name == "species_velocity")
        return SPECIES_VELOCITY;
    else if(name == "physics")
        return PHYSICS;
    else if (name == "all")
        return OUTPUT_ALL;
    else
        return UNKNOWN_TARGET;
}

ostream& picard::operator<<(ostream& os, OutputTarget type) {
    switch(type){
        case POSITION :
            os << "Position";
            break;
        case VELOCITY :
            os << "Velocity";
            break;
        case SPECIES_COUNT :
            os << "Species_Count";
            break;
        case ELECTRIC_FIELD :
            os << "Electric_Field";
            break;
        case MAGNETIC_FIELD :
            os << "Magnetic_Field";
            break;
        case VELOCITY_AVERAGE:
            os << "Velocity_Average";
            break;
        case VELOCITY_VARIANCE:
            os << "Velocity_Variance";
            break;
        case NUMBER_DENSITY:
            os << "Number_Density";
            break;
        case TEMPERATURE :
            os << "Temperature";
            break;
        case POTENTIAL :
            os << "Potential";
            break;
        case CHARGE_DENSITY :
            os << "Charge_Density";
            break;
        case SURFACE_CHARGE_DENSITY :
            os << "Surface_Charge_Density";
            break;
        case KINETIC_ENERGY :
            os << "Kinetic_Energy";
            break;
        case POTENTIAL_ENERGY :
            os << "Potential_Energy";
            break;
        case TOTAL_ENERGY :
            os << "Total_Energy";
            break;
        case SPECIES_TEMPERATURE :
            os << "Species_Temperature";
            break;
        case SPECIES_VELOCITY :
            os << "Species_Velocity";
            break;
        case PHYSICS :
            os << "Physics";
            break;
        case OUTPUT_ALL:
            os << "All";
            break;
        default:
            os << "Unknown_Target";
    }
    return os;
}

picard::OutputSuffix picard::getSuffix(string name) {
    name = picard::to_lower(name);
    if(name == "time")
        return TIME;
    else if(name == "iteration")
        return ITERATION;
    else
        return UNKNOWN_SUFFIX;
}

ostream& picard::operator<<(ostream& os, OutputSuffix type) {
    switch(type){
        case TIME :
            os << "Time";
            break;
        case ITERATION :
            os << "Iteration";
            break;
        default:
            os << "Unknown_Suffix";
    }
    return os;
}
