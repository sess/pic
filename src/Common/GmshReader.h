#ifndef SESS_PIC_GMSH_READER_H
#define SESS_PIC_GMSH_READER_H

#include <string>
#include <vector>
#include "Definitions.h"
#include "Options.h"
#include "Domain.h"

namespace picard {

class Grid;
class FieldLabel;
class ParticleBoundaryLabel;

constexpr int POINT = 0;
constexpr int BAR = 1;
constexpr int QUAD = 3;
constexpr int HEX = 5;

//! A struct that stores a temporary Gmsh element for creation of a grid (raw information from the *.msh file).
struct GmshElement {
    int id; //!< The element id (or index) that specifies this element as a unique entity
    int type; //!< The gmsh element type
    int numTags; //!< Number of tags associated with the element
    int physicalId; //!< The physical ID of the element, used for labeling.
    std::vector<int> tags; //!< The tags associated with the element
    std::vector<int> indices; //!< List of indices that make up this element

    int numIndices() { return numIndices(type); }//!< Get the number of indices in a element of this type
    int dimension() { return dimension(type); }//!< Get the dimensionality of an element of this type

    static int numIndices(int elType);//!< get the number of indices of an element of the supplied typ
    static int dimension(int elType); //!< Get the dimension of an element of the supplied type
};

//! A struct that stores a temporary Gmsh point for creation of a grid (raw information from the *.msh file).
struct GmshNode {
    /**
     * Default constructor for a GmshNode. Initializes the position vector to a zero-vector of size three
     */
    GmshNode() : r(3, 0) {}

    int id; //!< The point id (or index) that specifies this point as a unique entity
    vec r; //!< Location of the node. Stored as 3D but only the first nDim components are used
};

/**
 * A comparator that orders 3D points in space.
 *
 * Node n1 is considered less than node n2 if the lexicographical less than (compare x, then y, then z) evaluates
 * to true and n1 is not within the specified tolerance of n2.
 */
struct SpatialLessThan {
    /**
     * Constructor for the spatialLessThan comparator.
     * @param tolerance The specified tolerance the determines if two nodes occupy the same location in space
     */
    SpatialLessThan(Real tolerance, int nDim) : tolerance(tolerance), nDim(nDim) {}

    Real tolerance; //!< Maximum separation two nodes can have to be considered overlapping
    int nDim; //!< The number of dimensions to compare

    bool operator()(const GmshNode& n1, const GmshNode& n2) const;
};

//! A struct that stores a temporary Gmsh label (or "physical-name") for creation of a grid (raw information from the *.msh file).
struct GmshLabel {
    int dim; //!< Physical dimension of the element to be labeled
    int id; //!< The label id of the
    std::string name; //!< the name of the label
};

class GmshReader {
public:
    /**
     * Constructs a GmshReader from a file name by parsing the file and loading it into the temporary data structures.
     * @param fileName The path to the file to be parsed.
     */
    GmshReader(std::string fileName);

    /**
     * Make a grid out of the Gmsh file provided by the filename. Labels are required before loading.
     * @param fieldLabels List of FieldLabels to be used to label nodes
     * @param particleBCLabels List of particleBoundaryLables to used to label elements
     * @param type The type of grid to be constructed
     * @return A constructed grid upon success, otherwise a nullptr
     */
    std::shared_ptr<Grid> makeGrid(std::vector<std::shared_ptr<FieldLabel>> fieldLabels,
                                   std::vector<std::shared_ptr<ParticleBoundaryLabel>> particleBCLabels,
                                   GridType type = BASIC_GRID);

    /**
     * Open, read, and parse the file specified to fill out the temporary Gmsh data structures.
     * This function is called upon invocation of the GmshReader(string filename) constructor.
     * This function needs to be called before a grid can be created from the contents of the reader.
     * @param fileName The path to the file to be read.
     */
    void parse(std::string fileName);

    /**
     * Sorts the list of GmshNodes, removes duplicates and fixes the element indices.
     *
     * The sorting is such that x is the fastest varying direction, followed by y, then z.
     * The removing of duplicates works by establishing an estimated separation between nodes and if two nodes are found
     * within 1/10 that distance then they are considered the same node and the node with the larger index is removed.
     */
    void sort();

    /**
     * Get the dimension of the mesh as determined by the parser
     * @return The dimension of the mesh as determined by the parser
     */
    int nDim() { return _nDim; };

    /**
     * Get the Gmsh Elements read from the file that was last parsed
     * @return A const reference to the vector of elements contained in the parsed file. Order is preserved.
     */
    const std::vector<GmshElement>& elements() { return _elements; }

    /**
     * Get the Gmsh nodes read from the file that was last parsed
     * @return A const reference to the Gmsh nodes that were read from the parsed file. Order is preserved.
     */
    const std::vector<GmshNode>& nodes() { return _nodes; }

    /**
     * Get the Gmsh labels read from the file that was last parsed
     * @return A const reference to a map from label Id to the GmshLabel
     */
    const std::map<int, GmshLabel>& labels() { return _labels; }

    /**
     * Get the last file that was parsed by this reader
     * @return A string of the filepath of the last file to be parsed by this reader
     */
    std::string lastParsed() { return _fileName; }

    /**
     * Set all of the subdomains in the Grid with permissions and all
     * @param grid A pointer to the grid for get node locations
     * @param fieldLabels A mapping field labels to populate domains
     */
    std::vector<Domain> getSubDomains(const std::map<std::string, std::shared_ptr<FieldLabel>>& fieldLabels);

    /**
     * Get the domain that encapsulates all of the nodes currently stored by the parser
     * @return The Domain that encapsulates all of the nodes.
     */
    Domain getDomain();

    /**
     * Get the number of grid points in each dimension
     * @return An ivec of length nDim that gives the number of grid points in each dimension
     */
    ivec getNumGridPoints();

    /**
     * Get a map of domains that specify the range of a particular label
     * @return A map from the label name (a string) and the Domain that represents its furthest extent
     */
    std::map<std::string, Domain> getLabelDomains();


private:
    int _nDim{-1}; //!< The dimension of the mesh as determined by the Gmsh reader
    std::string _fileName; //!< The filename of the last file to be parsed
    std::vector<GmshElement> _elements; //!< List of elements read from the Gmsh file
    std::vector<GmshNode> _nodes; //!< List of nodes read from the Gmsh file
    std::map<int, GmshLabel> _labels; //!< Map of Gmsh "Physical Id" to a label specified by the "physical-name"

    // Tokens for parsing the Gmsh file
    static const std::string nodeToken; //!< Start token of the nodes section in a gmsh file
    static const std::string endNodeToken; //!< End token of the nodes section in a gmsh file
    static const std::string elementToken; //!< Start token of the elements section in a gmsh file
    static const std::string endElementToken; //!< End token of the elements section in a gmsh file
    static const std::string labelToken; //!< Start token of the label section in a gmsh file
    static const std::string endLabelToken; //!< End token of the label section in a gmsh file

    void parseNodes(std::ifstream& infile); //!< Rules for parsing nodes out a Gmsh file
    void parseElements(std::ifstream& infile); //!< Algorithm for parsing elements out of a Gmsh file
    void parseLabels(std::ifstream& infile); //!< Alogirhtm for parsing labels out of a Gmsh file
    int getNumEntries(std::ifstream& infile); //!< Read the number of entries in a given section
    void confirmEndToken(std::ifstream& infile, std::string endToken); //!< Confirm the section ends with the proper token
};

}


#endif
