#ifndef SESS_PIC_GRID_H
#define SESS_PIC_GRID_H

#include <vector>
#include <array>
#include <map>
#include <unordered_map>
#include <memory>
#include <set>
#include <Eigen/Dense>
#include "Definitions.h"
#include "Domain.h"
#include "Particle.h"

namespace picard {

class Label;
class ParticleBoundaryLabel;
class FieldLabel;

/**
 * Struct that represents an intersection with a Grid.
 * Derives from the regular Intersection struct in domain but adds a data member that is the ParticleBoundaryLabel
 * that was intersected.
 */
struct GridIntersection : public Intersection {
    GridIntersection(const Intersection& intersection) : Intersection(intersection) {}
    std::shared_ptr<ParticleBoundaryLabel> label{nullptr}; //!< The ParticleBoundaryLabel that is was intersected
    int domainIndex{-1}; //!< The index of the subdomain that was intersected
};

std::ostream& operator<<(std::ostream& os, const GridIntersection& i); // Stream operator for grid intersection

/**
 * Defines a structured cartesian rectangular domain for a simulation
 *
 * The grid will contain any number of subdomains that have different properties
 * The grid also contains field data (like the eField, bField, and potential, etc)
 */
class Grid : public Domain {
public:
    /**
     * Grid constructor that does not allocate grid data
     * @param d The domain to be constructed from
     * @param numPts The number of grid points in each dimension
     */
    Grid(Domain d, const ivec& numPts);

    virtual ~Grid() = default; //!< virtual destructor for derived types

    Real epsilon_0{1.0}; //!< Dielectric constant in free space (TODO is this description correct?)

    // Grid Domains
    std::vector<Domain> subDomains; // std::vector of all the subdomains within the major domain
    std::vector<std::shared_ptr<FieldLabel>> fieldLabels; //<! One field label per node
    std::map<Element, std::shared_ptr<ParticleBoundaryLabel>> particleBoundaryLabels; //!< Particle boundary labels are stored by element

    // Field data
    vector2d<Real> eField; //!< The electric field stored at each node (nGTotal rows, and nDim columns)
    vector2d<Real> bField; //!< The magnetic field stored at each node (nGTotal rows, and 3 columns)
    std::vector<Real> potential; //!< The electric potential at each node

    // Boundary data
    std::unordered_map<int, int> surfaceChargeMap; //!< Maps node index to index in the surfaceCharge containers
    std::vector<Real> surfaceChargeLocal; //!< local surface charge (populated by each thread)
    std::vector<Real> surfaceChargeGlobal; //!< global surface charge (reduced from all threads)

    // Particle data
    vector2d<Real> chargeDensity; //!< The charge density of each species (nGtotal rows, and nSpecies columns)
    vector2d<Real> numberDensity; //!< The number density of each species (nGtotal rows, and nSpecies columns) TODO: This can be combined with chargeDensity
    std::vector<picard::vector2d<Real>> _averageVelocity; //!< The average velocity of particles at this grid point
    std::vector<picard::vector2d<Real>> _averageSqVelocity; //!< The average velocity of particles at this grid point
    std::vector<picard::vector2d<Real>> _velocityVariance; //!< The variance of the velocity at this grid point
    std::vector<picard::vector2d<Real>> _temperature; //!< Temperature of each species

    // Accessing particle data on the grid by species and node
    vec v_avg(SpeciesPtr s, int node) const { return getRow(_averageVelocity[s->id], node); }
    vec vsq_avg(SpeciesPtr s, int node) const { return getRow(_averageSqVelocity[s->id], node); }
    vec v_var(SpeciesPtr s, int node) const { return getRow(_velocityVariance[s->id], node); }
    vec temp(SpeciesPtr s, int node) const { return getRow(_temperature[s->id], node); }

    /**
     * Finds an intersection point between the ray and the grid (including any of its subdomains)
     * @param ray The ray cast to check for intersection
     * @return A GridIntersection struct which includes the intersection point, the label of the intersected element
     * and several other parameters related to the intersection point.
     */
    GridIntersection intersectsBoundary(const std::vector<Real>& ray) const;

    // Grid point accessors
    int totalPoints() const { return nGTotal; } //!< Total number of grid points
    int totalCells() const { return product(numPoints() - 1); }; //!< Total number of grid cells
    ivec numPoints() const { return numGridPtsXYZ; } //!< Get the number of grid points in each dimension (as a vector)
    int numPoints(int dim) const { return numGridPtsXYZ[dim]; } //!< Get the number of grid points in each dimension
    void setNumPoints(ivec pts); //!< set the number of points in each dimension of the grid

    vec spacing() const { return axisRes; } //!< Get the grid spacing in each dimension
    Real spacing(int dim) const { return axisRes[dim]; } //!< Get the grid spacing in the specified dimension

    Real cellVolume() const { return _cellVolume; } //!< The the volume of a single cell

    vec position(int index) const { return getRow(nodePositions, index); } //!< Get the position of the node at the specified index
    Real position(int index, int dim) const { return nodePositions(index, dim); } //!< Get the dim-component of the position of the node at the specified index
    vec position(const ivec& ijk) const { return position(index(ijk)); } //!< Get the position of the node given by the ijk coords
    Real position(const ivec& ijk, int dim) const { return position(index(ijk), dim); } //!< Get the dim-component of the position of the node given by the ijk coords
    vector2d <Real>& getPoints() { return nodePositions; } //!< Get the node positions vector for filling

    ivec ijk(const vec& position) const; //!< Get the ijk coords of the provided position (grid point rounded down in each dimension)
    ivec ijk(int index) const{ return picard::ijk(index, numGridPtsXYZ, _nDim); }; //!< Get the ijk coords of the provided index

    int index(const ivec& ijk) const; //!< Get the index of the provided ijk coord
    int index(const vec& position) const { return index(ijk(position)); }; // !< Get the index of the node at the provided position (grid point rounded down in each dimension)

    bool hasBField() const { return bField.size() > 0; } //!< Check if the grid has a b-field

    /**
     * allocate all of the grid (field) quantities based on the provided settings
     * @param nSpecies The number of species that are going to be active on the grid. Used for the charge density vector
     */
    virtual void allocateGridData(int nSpecies);

    /**
     * Get the surface charge at the node specified
     * @param node The node index to get the surface charge at
     * @return The surface charge if the node is absorbing charge, 0 otherwise
     */
    Real getSurfaceCharge(int node) const;

    /**
     * Add charge to a surface node
     * @param node The node for charge to be added to
     * @param charge_contribution The amount of charge to be added
     */
    void contributeSurfaceCharge(int node, Real charge_contribution);

    /**
     * This function reorders label priority for the external boundaries after everything in the grid has been set up (including field BCs)
     * Each external boundary point has its priority degraded to one below the nearest interior point. This is done so that if there are any
     * internal boundaries in the domain which intersect with the external boundaries, then the labels on the internal boundaries take precedence
     * over the external boundaries at the corner points. This only affects the application of particle BCs, not field BCs.
     *
     * Corners between two external boundaries are not considered for reordering
     */
    void reorderLabelPriority();

    /**
     * Get the total charge density at the particular node of interest
     * The function is virtual so that other grids can change how the charge density is calculated
     * @param node The node to get the charge density at
     * @return The total charge density at the specified node.
     */
    virtual Real getChargeDensity(int node) const { return chargeDensity.row(node).sum(); }

    void zeroOutFields(); //!< Zero-out all entries in the charge density

    virtual void setupChargeDensity() {}; //!< Prepares the charge density. Does nothing in the base class

    Real activeVolume() const; //!< Get the total volume of the grid that is allowed to have particles in it

    bool isPermitted(const vec& r) const; //!< Check if a particular point is allowed to have a particle (i.e. its in a permitted subdomain)

    vec randomPermittedLocation() const; //!< Get a random location within one of the permitted subdomains (uniformly distributed)

    void fillNodeLocations(); //!< Calculate and fill the node locations based on grid domain and number of pts in each dim
    std::set<Element> boundaryElements() const; //!< Calculates all boundary elements

    std::vector<BoundaryId> boundaryIds(int nodeIndex) const; //!< Get all of the boundary ids of the node
    BoundaryId boundaryId(Element element) const; //!< Get the id of the boundary element
    int numBoundaries(int nodeIndex) const { return boundaryIds(nodeIndex).size(); } //!< Get the number of boundaries a node is on


    /**
     * Get the node that is adjacent to n1 in the direction of the displacement vector provided
     * If the connected node would be out of bounds then the displacement vector is reversed in that dimension so that
     * the returned node is a valid one.
     * @param n1 The starting node
     * @param disp The displacement vector
     * @return A node adjacent to n1 in the direction of the displacment vector. If the node would be out of bounds
     * then the displacement vector is reversed in the needed directions.
     */
    ivec getAdjacentNode(const ivec& n1, const ivec& disp) const;

    /**
     * Get the boundary element that is up and to the right of the node specified. (I.e. go in the positive direction
     * of each dimension).
     * You also have to specify the dimension to keep constant for the boundary element
     * @param nodeIndex The node that is the lower-left corner of the element
     * @param dim The dimension to keep constant
     * @return The element that this node is a part of
     */
    Element getBoundaryElement(int nodeIndex, int dim) const;
    
    Domain makeDomain(const Element& e) const; //!< create a domain from an element of a grid

    Cell getEnclosingCell(const vec& position) const; //!< Get the enclosing cell of the specified position

    void addDirichletLabelIfNeeded(); //!< Add a dirichlet label to one node if no dirichlet label is present. This is required when the system is underdefined (i.e. all periodic or neumann)

    /**
     * Creates a single-domain Grid that is periodic (For both field and particle labels) on all sides
     * @param d The domain that specifies the location of the grid
     * @param numPoints The number of points in each dimension
     * @return A shared_ptr to the newly constructed Grid
     */
    static std::shared_ptr<Grid> Periodic(const Domain& d, const ivec& numPoints);

    /**
     * Creates a single-domain Grid that has field labels and particle labels as specified
     * @param d The domain that specifies the location of the grid
     * @param numPoints The number of points in each dimension
     * @param fieldLabels The field labels ordered by face id (0->-x, 1->+x, ...5->+z, 6 interior)
     * @param particleLabels The particle labels ordered by face id (0->-x, 1->+x, ...5->+z)
     * @return A shared_ptr to the newly constructed Grid
     */
    static std::shared_ptr<Grid> SingleDomain(const Domain& d, const ivec& numPoints,
                                              std::map<int, std::shared_ptr<FieldLabel>> fieldLabels,
                                              std::map<int, std::shared_ptr<ParticleBoundaryLabel>> particleLabels);

protected:
    int nGTotal = 1;//!< Total number of grid points // TODO compute this on the fly?
    vector2d <Real> nodePositions;//!< positions of all the nodes in the grid // TODO compute this on the fly?
    ivec numGridPtsXYZ; //!< The number of grid points in each dimension
    vec axisRes; //!< Axis resolution in each dimension (dx, dy, dz)// TODO compute this on the fly?
    Real _cellVolume{1};//!< The volume of a cell // TODO compute this on the fly?

    void refresh() override; //!< Refresh function to be called with Domain dimensions change or grid points change
};

std::ostream& operator<<(std::ostream& os, const Grid& g);

/**
 * Derived grid type that has a neutralizing background charge distribution that is added to the total charge
 * distribution when getChargeDensity is called
 */
class NeutralizingGrid : public Grid {
public:
    /**
     * Grid constructor that does not allocate grid data
     * @param d The domain to be constructed from
     * @param numPts The number of grid points in each dimension
     */
    NeutralizingGrid(Domain d, const ivec& numPts) : Grid(d, numPts) {}

    /**
     * Base class copy constructor to great Neutralizing grids from all places as a Grid
     * @param grid The grid to copy from
     */
    NeutralizingGrid(const Grid& grid) : Grid(grid){}

    std::vector<Real> neutralizingBackground; //!< Background charge distribution at each node

    /**
     * Get the charge distribution at the specified node. Adds the background distribution at that node as well
     * @param iNode The node to get the charge distribution at
     * @return The total charge distribution (background distribution included)
     */
    Real getChargeDensity(int iNode) const override;

    void setupChargeDensity() override; //!< set neutralizingBackground to the negative of the current charge density and then set charge density to 0
    void allocateGridData(int nSpecies) override; //!< Allocates grid data and neutralizing background

    /**
     * Creates a single-domain NeutralizingGrid that is periodic (For both field and particle labels) on all sides
     * @param d The domain that specifies the location of the grid
     * @param numPoints The number of points in each dimension
     * @return A shared_ptr to the newly constructed Grid
     */
    static std::shared_ptr<NeutralizingGrid> Periodic(const Domain& d, const ivec& numPoints); //!< Create a periodic grid from the domain specified

    /**
     * Creates a single-domain NeutralizingGrid that has field labels and particle labels as specified
     * @param d The domain that specifies the location of the grid
     * @param numPoints The number of points in each dimension
     * @param fieldLabels The field labels ordered by face id (0->-x, 1->+x, ...5->+z, 6 interior)
     * @param particleLabels The particle labels ordered by face id (0->-x, 1->+x, ...5->+z)
     * @return A shared_ptr to the newly constructed Grid
     */
    static std::shared_ptr<NeutralizingGrid> SingleDomain(const Domain& d, const ivec& numPoints,
                                              std::map<int, std::shared_ptr<FieldLabel>> fieldLabels,
                                              std::map<int, std::shared_ptr<ParticleBoundaryLabel>> particleLabels);
};

}


#endif
