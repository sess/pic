#include "Interpolator.h"
#include "Grid.h"
#include "ParticleMover.h"

using namespace std;
using namespace picard;

void Interpolator::particleToGrid(Particle& p, int speciesIndex, Grid& grid, vector2d<Real>* sumOfWeights) const {
    auto cell = grid.getEnclosingCell(p.r);
    for (auto i : cell) {
        auto w = W(grid.position(i), p.r, grid.spacing());
        auto w_v = w / (grid.cellVolume() / std::pow(2.0, grid.numBoundaries(i)));

        grid.chargeDensity(i, speciesIndex) +=  w_v * p.q();
        grid.numberDensity(i, speciesIndex) +=  w_v;
        for (auto n =0; n<grid.nDim(); n++) {
            grid._averageVelocity[speciesIndex](i, n) += w * p.v[n];
            grid._averageSqVelocity[speciesIndex](i, n) += w * p.v[n] * p.v[n];
        }
        if (sumOfWeights)
            (*sumOfWeights)(i, speciesIndex) += w;
    }
}

void Interpolator::gridToParticle(Particle& p, const Grid& grid, ParticleMover& mover, bool bFieldFlag) const{
    mover.clearFields();
    auto cell = grid.getEnclosingCell(p.r);
    for(auto i : cell){
        if (bFieldFlag)
            mover.addBField(getRow(grid.bField, i) * W(grid.position(i), p.r, grid.spacing()));
        mover.addEField(getRow(grid.eField, i) * W(grid.position(i), p.r, grid.spacing()));
    }
}

Real Interpolator::scalarFieldValue(const vec& position, const std::vector<Real>& field, const Grid& grid) const{
    Real val = 0;
    tic("get enclosing cell");
    auto cell = grid.getEnclosingCell(position);
    toc();

    tic("add w");
    for(auto i : cell)
        val += W(grid.position(i), position, grid.spacing())*field[i];
    toc();

    return val;
}

double Interpolator::W(const vec& nodePosition, const vec& particlePosition, const vec& spacing) const{
    double returnVal = 1;
    for (uint i = 0; i < nodePosition.size(); i++) {
        Real relPos = abs(particlePosition[i] - nodePosition[i]);
        returnVal *= abs(spacing[i] - relPos) / spacing[i];
    }
    return returnVal;
}



