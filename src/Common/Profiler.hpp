#ifndef PROFILER_HPP
#define PROFILER_HPP

#include <iostream>
#include <string>
#include <deque>
#include <chrono>
#include <map>

namespace picard {

typedef std::chrono::high_resolution_clock::time_point time_p;

template <typename time_preference>
std::string rep_to_string(){
    if(std::is_same<time_preference,std::chrono::milliseconds>::value)
        return "ms";

    if(std::is_same<time_preference,std::chrono::microseconds>::value)
        return "us";

    if(std::is_same<time_preference,std::chrono::nanoseconds>::value)
        return "ns";

    if(std::is_same<time_preference,std::chrono::seconds>::value)
        return "sec";

    if(std::is_same<time_preference,std::chrono::minutes>::value)
        return "min";

    if(std::is_same<time_preference,std::chrono::hours>::value)
        return "hr";

    return "UNKNOWN";
}

//! Represents an interval of time
struct TimeInterval{
    TimeInterval(time_p start, time_p end) : start(start), end(end) {}
    time_p start; //!< Start of the timer
    time_p end; //!< End of the timer

    template <typename time_preference>
    std::chrono::duration<typename time_preference::rep, typename time_preference::period> duration(){
        return std::chrono::duration_cast<time_preference>(end - start);
    }
};

//! Class for recording and printing time intervals (used for profiling)
class Profiler{
public:
    //! Clear all started and finished timers
    void clearAll(){
        started_timers.clear();
        finished_intervals.clear();
    }
    /**
     * Start a new timer in the profiler with the specified id.
     * @param id A string that defines the id of this time. Timers with the same ids will be combined when printed
     */
    void start(std::string id){
        started_timers.push_front(std::make_pair(id, std::chrono::high_resolution_clock::now()));
    }

    //! Stop the most recently started timer
    void stop(){
        auto& most_recent_start = started_timers.front();
        auto id = most_recent_start.first;
        auto start = most_recent_start.second;
        auto end = std::chrono::high_resolution_clock::now();

        // Add the new duration to the existing timer of the same id (or create a new on if necessary)
        if(finished_intervals.find(id) == finished_intervals.end())
            finished_intervals[id] = std::chrono::nanoseconds(0);

        finished_intervals[id] += TimeInterval(start, end).duration<std::chrono::nanoseconds>();

        started_timers.pop_front();
    }


    //!< Print out the total time spent on each id recorded
    template <typename time_preference = std::chrono::seconds>
    std::string results(std::string delim = ": ", std::string name_wrapping = ""){
        if (!started_timers.empty()) {
//            for(auto& entry: started_timers)
//                std::cerr << entry.first << std::endl;

            throw std::runtime_error("Error! Unstopped timers remain!");
        }

        // Convert the intervals to the desired duration


        // Sort the intervals from smallest to largest
        std::multimap<double, std::string> sorted_intervals;
        for(auto& entry : converted_intervals<time_preference>())
            sorted_intervals.insert(std::make_pair(entry.second.count(), entry.first));

        // Write the results to a string
        std::string res;
        for(auto& entry : sorted_intervals)
            res += name_wrapping + entry.second + name_wrapping + delim +
                    std::to_string(entry.first) + " " + rep_to_string<time_preference>() + "\n";
        return res;
    }

    //! Get the list of stored intervals
    template <typename time_preference>
    std::map<std::string, std::chrono::duration<double, typename time_preference::period>> getIntervals() const{
        return converted_intervals<time_preference>();
    }
private:

    template <typename time_preference>
    std::map<std::string, std::chrono::duration<double, typename time_preference::period>> converted_intervals() const{
        std::map<std::string, std::chrono::duration<double, typename time_preference::period>> converted_intervals;
        for (auto& entry : finished_intervals)
            converted_intervals[entry.first] = entry.second;
        return converted_intervals;
    }
    std::deque<std::pair<std::string, time_p>> started_timers; //!< list of started (but not yet stopped timers
    std::map<std::string, std::chrono::duration<int64_t, std::nano>> finished_intervals; //!< total durations that have been recorded
};

//! Get the global profiler instance
inline Profiler& getProfilerInstance(){
    thread_local static Profiler p;
    return p;
}

inline void tic(std::string id){
#ifdef ENABLE_PROFILING
    auto& p = getProfilerInstance();
    p.start(id);
    return;
#endif
}

inline void toc(){
#ifdef ENABLE_PROFILING
    auto& p = getProfilerInstance();
    p.stop();
#endif
}

template <typename time_preference = std::chrono::seconds>
inline std::string profileResultsCSV(){
#ifdef ENABLE_PROFILING
    auto& p = getProfilerInstance();
    return p.results<time_preference>(" ", "\"");
#endif
    return "";
}

template <typename time_preference = std::chrono::seconds>
inline std::string profileResults(){
#ifdef ENABLE_PROFILING
    auto& p = getProfilerInstance();
    return p.results<time_preference>();
#endif
    return "";
}


inline void clearProfiler(){
#ifdef ENABLE_PROFILING
    auto& p = getProfilerInstance();
    p.clearAll();
#endif
}



}



#endif