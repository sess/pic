#ifndef MATH_UTILS_H
#define MATH_UTILS_H

namespace picard{

/**
 * Compares two floating point numbers to see if they are approximately equal
 * Checks to see fi the absolute difference is smaller than the floating point precision threshold
 * If not, it checks if difference is less than a relative difference (for values close to 0)
 * @param v1 The first floating point number
 * @param v2 The second floating point number
 * @return True if the numbers are "approximately equal"
 */
inline bool feq(Real v1, Real v2) {
    auto abs_diff = std::abs(v1 - v2);
    if (abs_diff <= ABS_EPSILON)
        return true;
    return abs_diff <= ((std::abs(v1) < std::abs(v2) ? std::abs(v2) : std::abs(v1)) * REL_EPSILON);
}

/**
 * Check if v1 is less than or approximately equal to v2
 * @param v1 The first floating point number to see if it is less than or equal to the second
 * @param v2 The second floating point number
 * @return True if v1 is less than or approximately equal to v2. Uses the feq function for the approx equal
 */
inline bool fleq(Real v1, Real v2) {
    if (feq(v1, v2))
        return true;
    else
        return v1 < v2;
}

/**
 * Check if v1 is greater than or approximately equal to v2
 * @param v1 The first floating point number to see if it is greater than or equal to the second
 * @param v2 The second floating point number
 * @return True if v1 is greater than or approximately equal to v2. Uses the feq function for the approx equal
 */
inline bool fgeq(Real v1, Real v2) {
    if (feq(v1, v2))
        return true;
    else
        return v1 > v2;
}

template<typename T>
inline int sign(T val) {
    if (val == 0)
        return 0;
    return (std::signbit(float(val))) ? -1 : 1;
}

/**
 * Moves a floating point vector by a small amount in the principle direction suppplied
 * Note that this keeps a static variable for where to start in determining how much to move
 * Only moves in the principle directions
 * @param v The vector to move
 * @param direction Index of the direction to move it in
 */
inline void moveEps(Real& v, Real direction) {
    static Real significant_eps = ABS_EPSILON;
    auto origVal = v;
    v += significant_eps * direction;

    while (feq(origVal, v)) {
        significant_eps *= 2.;
        v += significant_eps * direction;
    }
}


//! Defines a sinusoidal mode of the form $A \cos(m (2 \pi) x/L + \phi)$
//! where $m$ is the mode number, $L$ is the length scale and $\phi$ is the phase
struct cosine_mode{
    cosine_mode(uint mode_number, Real length, Real amplitude, Real phase)
            : mode_number(mode_number), length(length), amplitude(amplitude), phase(phase){}

    uint mode_number; //!< mode number of cosine (0 to infinity)
    Real length; //!< The length parameter that specifies scale
    Real amplitude; //!< the amplitude of the sinusoid
    Real phase; //!< The phase in radians of the sinusoid

    //! access operator to get the value of the sinusoid at a particular position
    Real operator()(Real x){
        Real val = Real(mode_number)*2.*pi*x/length + phase;
        return amplitude*std::cos(val);
    }
};

//! defines a functor that returns zero regardless of the input
struct zero_fn{
    Real operator()(Real){ return 0; }
};

//! Structure to encapsulate multiple functions and access them with a vector of inputs
struct multi_function{
    std::vector<Real> operator()(std::vector<Real> inputs){
        std::vector<Real> output;
        for(int i=0; i<inputs.size(); i++){
            if(i >= fns.size())
                output.push_back(0);
            else
                output.push_back(fns[i](inputs[i]));
        }
        return output;
    }
    std::vector<std::function<Real(Real)>> fns;
};


//! Defines the needed coefficients for an exponential interpolation
struct ExpCoeff{
    ExpCoeff(Real m, Real C) : m(m), C(C) {}
    Real m; //!< Exponent for the exponential
    Real C; //!< Scaling coefficient

    bool operator==(const ExpCoeff& other) const {return m==m && C==C; }
};

inline std::vector<ExpCoeff> getExpCoefficients(const std::map<Real,Real>& samples){
    std::vector<ExpCoeff> coeffs;

    if(samples.size() < 2) //If there are less then 2 samples we can't draw a line
        return coeffs;

    auto lb = samples.begin();
    auto ub = std::next(lb);

    do {
        auto x1 = lb->first;
        auto y1 = lb->second;

        auto x2 = ub->first;
        auto y2 = ub->second;

        auto m = log(y1/y2) / log(x1/x2);
        auto b = log(y1) - m*log(x1);
        auto C = exp(b);

        coeffs.push_back(ExpCoeff(m, C));
        lb++;
        ub++;
    } while (ub != samples.end());

    return coeffs;
}

inline Real expInterpolate(Real x, const std::map<Real,Real>& samples, const std::vector<ExpCoeff>& coeffs){
    if(samples.empty())
        throw std::runtime_error("expInterpolate Error: No sample points to interpolate with.");

    // use constant extrapolation
    if( x <= samples.begin()->first)
        return samples.begin()->second;
    auto last = std::prev(samples.end());
    if( x >= last->first)
        return last->second;

    // Otherwise use exponential interpolation
    int index = 0;
    for(auto lb = samples.begin(); lb != last; lb ++){
        auto up = std::next(lb);
        if(x >= lb->first && x <= up->first)
            return coeffs[index].C*std::pow(x, coeffs[index].m);
        index++;
    }
    throw std::runtime_error("expInterplate Error: Couldn't find value " + std::to_string(x) + " in interpolation.");
}

}


#endif