#ifndef SESS_PIC_SIMULATION_H
#define SESS_PIC_SIMULATION_H

#include <boost/program_options.hpp>
#include <string>
#include <list>
#include <vector>
#include <mpi.h>
#include <memory>
#include "Threads.h"
#include "Options.h"
#include "BoundaryCondition.h"
#include "CastUtils.h"

namespace picard {

/**
 * The class that defines a simulation.
 *
 * The simulation contains all relevant objects for a simulation (grid, interpolator, particles, field solver, etc).
 * It controls the timestepping, boundary condition application, and data output.
 */
class Simulation {
public:
    /**
     * Create a simulation from a set of options (using move semantics)
     * @param options The options to construct the simulation from
     */
    Simulation(const Options& options);

    Simulation(std::string str) : Simulation(Options(str)){} //!< Simulation constructed from a configuration file

    Simulation() : Simulation(Options()) {}; //!< Default empty simulation

    /**
     * Set the simulation options from a set options passed in.
     * This is to change the setup of a simulation after it has already been constructed
     * @param options The options class to use to setup
     */
    void setup(const Options& options);

    void run(); //! Run the simulation to termination
    void step(); //!< Step the simulation by one timestep

    /**
     * Function for applying the boundary condition to a particle.
     * Checks for grid intersection and if there is one, applies the relevant boundary condition and returns particle status
     * @param p The particle getting the boundary condition applied to it
     * @return The status of the particle after the boundary condition was applied
     */
    ParticleStatus applyBoundaryConditions(Particle& p);

    //! reset the simulation time and iteration count. Everything else is left alone.
    void resetTime() {
        _t = 0;
        _iteration = 0;
    }

    /**********************************************************************
     *         Getters and Setters for Simulation data members            *
     **********************************************************************/
    Particles& particles() { return _particles; } //!< Get a reference to the simulation particles
    const Particles& particles() const { return _particles; } //!< Get a const reference to the simulation particles

    ParticleMover& mover() { return ptr_to_ref(_mover, "Particle Mover"); }; //!< Get a reference to the particle mover
    const ParticleMover& mover() const { return ptr_to_cref(_mover, "Particle Mover"); }; //!< Get a const reference to the particle mover

    Interpolator& interpolator() { return ptr_to_ref(_interpolator, "Interpolator"); }//!< Get a reference to the grid interpolation scheme
    const Interpolator& interpolator() const { return ptr_to_ref(_interpolator, "Interpolator"); }//!< Get a const reference to the grid interpolation scheme

    FieldSolver& solver() { return ptr_to_ref(_solver, "FieldSolver"); }//!< Get a reference to the field solver
    const FieldSolver& solver() const { return ptr_to_ref(_solver, "FieldSolver"); }//!< Get a const reference to the field solver

    Grid& grid() { return ptr_to_ref(_grid, "Grid"); }//!< Get a reference to the computational grid
    const Grid& grid() const { return ptr_to_ref(_grid, "Grid"); }//!< Get a const reference to the computation grid

    std::vector<std::shared_ptr<LocalPhysics>>& localPhysics() { return _localPhysics; } //!< Get a reference to the list of physics applied to each particle
    const std::vector<std::shared_ptr<LocalPhysics>>& localPhysics() const { return _localPhysics; } //!< Get a const reference to the list of physics applied to each particle

    std::vector<std::shared_ptr<GlobalPhysics>>& globalPhysics() { return _globalPhysics; } //!< Get a reference to the list of physics applied at each timestep
    const std::vector<std::shared_ptr<GlobalPhysics>>& globalPhysics() const { return _globalPhysics; } //!< Get a reference to the list of physics applied at each timestep

    // Simulation timestepping parameters
    Real timestep() const { return _dt; } //!< Get the current timestep size of the simulation
    Real dt() const {return _dt; } //!< Get the current timestep size of the simulation
    Real endTime() const { return _T; } //!< Get the end time of the simulation
    Real T() const { return _T; } //!< Get the end time of the simulation
    Real time() const { return _t; } //!< Get the current simulation time
    Real t() const { return _t; } //!< Get the current simulation time
    uint iteration() const{return _iteration;} //!< Get the current iteration number of the simulation

    Output& outputs() { return _output; } //!< Get a reference to the output settings for the simulation
    const Output& outputs() const { return _output; } //!< Get a const reference to the output settings for the simulation

    // Simulation particle quantities
    size_t numSpecies() const{ return std::prev(_species.end())->first + 1; } //!< get the number of species in the simulation
    SpeciesPtr getSpecies(size_t index) const{return _species.at(index); } //!< get the species at the specified index
    std::vector<std::string> species_names() const; //!< get a list of species names
    std::vector<Real> kinetic_energy() const; //!< Get the total kinetic energy of the particles in the simulation (added up across threads)
    std::vector<Real> potential_energy() const; //!< Get the total potential energy of the particles in the simulation (added up across threads)
    std::vector<Real> total_energy() const; //!< Get the total energy of the particles (kinetic + potential)
    size_t particle_count() const; //!< Get the total particles in the simulation (added up across threads)
    size_t local_particle_count() const; //!< Get the total number of particles on this thread
    std::map<SpeciesPtr, size_t> species_count() const; //!< Get the number of particles of each species (added up across threads)
    size_t species_count(SpeciesPtr) const; //!< Get the number of particles of the specified species (added up across threads)
    Real particle_fraction() const; //!< Get the fraction of particles contained on this thread
    const SpeciesReferenceParameters& referenceParams() const {return *_referenceParams; } //!< Get the reference parameters for the simulation
    SpeciesReferenceParameters& referenceParams() {return *_referenceParams; } //!< Get the reference parameters for the simulation

    void setVerbose(bool verbose){ _verbose = verbose; } //!< set the verbose flag to the desired setting
    bool shouldSpeak() const{return _verbose && isRoot(); } //!< Check the value of the verbose flag and isRoot

    void resetErrorMessage(){_error_message = "";} //!< Clears the error message from the simulation
    long timestamp() const {return _timestamp;}; //!< The timestamp (in millisecond ticks) of the start of the simulation
    long duration() const; //!< Get the wall-time duration of the simulation 
    void writeConfigs() const; //!< Write configuration files for this run to each output directory
    void removeConfigs() const; //!< Delete configuration files written by "writeConfigs" used mostly for testing
    void cleanupOutput() const; //!< Delete all output and configuration files from the run

    void log() const; //!< Log the results of the simulation to the desired
    void logProfileResults() const; //!< Log profile results, if there are any recorded
private:
    bool _verbose{false}; //!< Whether or not more messages should be displayed during simulation run

    Particles _particles; //!< The particles in the simualtion
    SpeciesIndex _species; //!< Map of species by index
    std::shared_ptr<SpeciesReferenceParameters> _referenceParams{nullptr}; //!< The reference parameters for the simulation

    std::shared_ptr<ParticleMover> _mover{nullptr}; //!< The particle mover
    std::shared_ptr<Interpolator> _interpolator{nullptr}; //!< The grid interpolator
    std::shared_ptr<FieldSolver> _solver{nullptr}; //!< The field solver
    std::shared_ptr<Grid> _grid{nullptr}; //!< The computational grid

    std::vector<std::shared_ptr<LocalPhysics>> _localPhysics; //!< Physics that is applied to each particle
    std::vector<std::shared_ptr<GlobalPhysics>> _globalPhysics; //!< Physics that is applied at each timestep

    Real _dt{0.1}; //!< The timestep of the simulation
    Real _T{1}; //!< The End time of the simulation
    Real _t{0}; //!< The current time of the simulation
    uint _iteration{0}; //!< The current iteration of the simulation

    Output _output; //!< output options of the simulation

    Options _options; //!< The options class used to construct the simulation

    int maxBCTries{10}; //!< The number of times a boundary condition will be applied to the same particle on a given timestep

    std::chrono::system_clock::time_point _system_start_time; //!< start time of the simulation
    std::chrono::system_clock::time_point _system_end_time; //!< end time of the simulation
    long _timestamp{0}; //!< The timestamp of the simulation (in millisecond ticks)
    std::string _error_message; //!< Most recent error message

    void scaleParticles(const Options& options); //!< scale the particles by density

    std::vector<std::string> logHeaders() const; //!< Get the headers
    std::vector<std::string> logValues() const; //!< Get the data to populate the csv files
    std::vector<std::pair<std::string, std::string>> logEntries() const; //!< Place to define the log functions

    void prepare(); //!< Actions taken before the first timestep (charge density calc and first field solve)

    void particleToGrid(); //!< compute all grid-averaged quantities

    void solveFields(); //!< Function that kicks off all relevant field solves on the grid

    void writeOutput(); //!< Calls the necessary functions to output the simulation data at each timestep

    void increment(); //!< Increments the simulation time and iteration

    void message(); //!< Code for writing message at each timestep

    void reduce(); //!< Parallel reduction step called at the end of timestep. (only simulation-necessary reductions)

    void setTimestamp(); //!< sets the timestamp of the simulation and syncs it across processes
};

/**
 * Accumulate a particle quantity across all particles across all threads
 * @tparam Args The argument types that go into the desired function call
 * @param property_name the name of the property being accumulated (for error messaging if MPI fails)
 * @param particles The list of particles to accumulate from
 * @param fn The Particle function to call on each particle and accumulate
 * @param args The arguments that go into the function call
 * @return The total value summed up over all particles
 */
template <typename ... Args>
std::vector<Real> global_accumulate_species(std::string property_name, const Particles& particles,
                    Real (Particle::*fn)(const Args&...) const, const Args&... args){

    auto local = accumulate_species(particles, fn, args...);

    if(!MPI_init_called())
        return local;

    tic("mpi call");
    std::vector<Real> total(local.size());
    check_MPI_call(
            MPI_Allreduce(local.data(), total.data(), local.size(), PIC_MPI_REAL, MPI_SUM, MPI_COMM_WORLD),
            property_name + " Reduction"
    );
    toc();
    return total;
};



}

#endif //SESS_PIC_SIMULATION_H
