#ifndef SESS_PIC_IO_UTILS_H
#define SESS_PIC_IO_UTILS_H

#include <string>
#include <iomanip>
#include <vector>
#include <map>
#include <set>
#include <fstream>
#include <sys/types.h>
#include <sys/stat.h>
#if defined(_WIN64) || defined(_WIN32)
	#include <io.h>
	#include <direct.h>
#else
	#include <unistd.h>
#endif
#include "VectorUtils.h"
#include "CastUtils.h"
#include "StringUtils.h"



namespace picard {

#define MAX_FLOATING_PRECISION std::numeric_limits<long double>::digits10 + 10

// Vector streaming utilities

//! A struct that contains options for streaming vectors and arrays.
struct StreamOptions{
    StreamOptions(std::string delimiter = "", std::string lElementWrap = "", std::string rElementWrap = "",
                  std::string lBookend = "", std::string rBookend = "", bool cast_element_to_string = false,
                  int precision = MAX_FLOATING_PRECISION, std::vector<ulong> subset = {}) :
            delimiter(delimiter), lElementWrap(lElementWrap), rElementWrap(rElementWrap), lBookend(lBookend),
            rBookend(rBookend), cast_element_to_string(cast_element_to_string), precision(precision), subset(subset) {}

    std::string delimiter; //!< The delimiter between elements
    std::string lElementWrap; //!< puts this string to the left of each element
    std::string rElementWrap; //!< puts this string to the right of each element
    std::string lBookend; //!< puts this string to the left of the whole vector/array
    std::string rBookend; //!< puts this string to the right of the whole vector/array

    bool cast_element_to_string; //!< Whether or not to use string casting on the elements before writing
    int precision; //!< Precision in the case of casting floats to string
    std::vector<ulong> subset; //!< Subset of elements in the vector to print. if empty all elements are printed

    std::ostream* stream{nullptr}; //!< the stream that this is used on
};

//! Set the delimiter of the vector elements to the specified string
inline StreamOptions setDelimiter(std::string delim){
    return StreamOptions(delim);
}

//! Set the element wrapping to the left and right strings specified
inline StreamOptions setElementWrapping(std::string lElementWrap, std::string rElementWrap){
    return StreamOptions("", lElementWrap, rElementWrap);
}

//! Set the bookends of the vector to the left and right strings specified
inline StreamOptions setBookends(std::string lBookend, std::string rBookend){
    return StreamOptions("", "", "", lBookend, rBookend);
}

//! Set the rBookend only
inline StreamOptions setEnd(std::string rBookend){
    return StreamOptions("", "", "", "", rBookend);
}

//! Cast elements to string (using cast<string>) before streaming
inline StreamOptions castToString(){
    return StreamOptions("","","","","",true);
}

//! Precision for casting floating point numbers to strings
inline StreamOptions setFloatPrecision(int precision){
    return StreamOptions("","","","","",false,precision);
}

inline StreamOptions setSubset(const std::vector<ulong>& subset){
    return StreamOptions("","","","","",false,MAX_FLOATING_PRECISION, subset);
}

//! Combining stream settings for stacked calls (adds strings together, ors the cast_element_to_string option)
inline StreamOptions operator<<(StreamOptions l, StreamOptions r){
    std::set<ulong> subset(l.subset.begin(), l.subset.end());
    for(auto& entry : r.subset)
        subset.insert(entry);

    StreamOptions ret(
            l.delimiter + r.delimiter,
            l.lElementWrap + r.lElementWrap,
            l.rElementWrap + r.rElementWrap,
            l.lBookend + r.lBookend,
            l.rBookend + r.rBookend,
            l.cast_element_to_string || r.cast_element_to_string,
            std::min(l.precision, r.precision),
            std::vector<ulong>(subset.begin(), subset.end())
    );
    ret.stream = l.stream;
    return ret;
}

//! The default set of stream options for the append function
inline StreamOptions appendDefault(int precision = MAX_FLOATING_PRECISION,
                                   const std::vector<ulong>& subset = {}){
    return setDelimiter(",") << setEnd("\n") << castToString() << setFloatPrecision(precision) << setSubset(subset);
}

//! The default set of stream options for stacking a vector on output
inline StreamOptions stackVectorDefault(int precision = MAX_FLOATING_PRECISION,
                                   const std::vector<ulong>& subset = {}){
    return setDelimiter("\n") << setEnd("\n") << castToString() << setFloatPrecision(precision) << setSubset(subset);
}

//! Stream operator for the stream settings (simply store the stream for later use)
inline StreamOptions operator<<(std::ostream& os, StreamOptions settings){
    settings.stream = &os;
    return settings;
}


//! Function for stream a multi-element container with all possible settings. used by vector and array
template<typename VEC>
inline std::ostream& streamWithOptions(StreamOptions s, const VEC& vec){
    auto& os = *s.stream;

    auto N = s.subset.empty() ? vec.size() : s.subset.size();

    os << s.lBookend;
    if(vec.size()) {
        for (uint i = 0; i < N; i++) {
            auto index = s.subset.empty() ? i : (uint) s.subset[i];
            os << s.lElementWrap;
            if(s.cast_element_to_string)
                os << cast<std::string>(vec[index]);
            else
                os << vec[index];
            os << s.rElementWrap;
            if(i != N-1)
                os << s.delimiter;
        }
    }

    os << s.rBookend;
    return os;
}

//! Streaming operator for scalar types and stream options
template<typename T>
inline std::ostream& operator<<(StreamOptions s, const T& val){
    auto& os = *s.stream;

    os << s.lBookend << s.lElementWrap;
    if(s.cast_element_to_string)
        os << cast<std::string>(val);
    else
        os << val;
    os << s.rElementWrap << s.rBookend;
    return os;
}

//! Streaming operator for vector with all possible settings
template<typename T>
inline std::ostream& operator<<(StreamOptions s, const std::vector<T>& vec) {
    return streamWithOptions(s, vec);
}

//! Streaming operator for array with all possible settings
template<typename T, size_t S>
inline std::ostream& operator<<(StreamOptions s, const std::array<T, S>& arr) {
    return streamWithOptions(s, arr);
}

//! Default behavior for the vector streaming operation
template <typename T>
inline std::ostream& operator<<(std::ostream& os, const std::vector<T>& vec) {
    os << setDelimiter(",") << vec;
    return os;
};

//! Default behavior for the array streaming operation
template <typename T, size_t S>
inline std::ostream& operator<<(std::ostream& os, const std::array<T, S>& arr) {
    os << setDelimiter(",") << arr;
    return os;
};

// Stack a vector of strings into a single string
inline std::string stack(const std::vector<std::string>& strs){
    std::stringstream s;
    s << setDelimiter("\n") << strs;
    return s.str();
}

/**
 * Parse a set of options into a key-value strings of a map.
 * Each key-value pair should have the form "key = value"
 * Note that this storage converts everything to lower case
 * @param str The string that contains the key-value pairs
 * @return A map containing the key-value string pairs
 */
template<template <typename...> class Container, typename...T>
std::map<std::string, std::string> getValueMap(const Container<T...>& tokens){
    std::map<std::string, std::string> values;
    for(auto& s : tokens){
        auto key_val = tokenize(s,"=");
        checkNumArgs(key_val.size(), 2, s, "IO Utils (getValueMap)");
        values[to_lower(key_val[0])] = key_val[1];
    }

    return values;
};


//! Implementation of the getValue function for basic template type
template<typename T, typename enable = void>
struct getValue_impl{
    static T getValue(const std::map<std::string, std::string>& valueMap, const std::string& key){
        return cast<T>(valueMap.at(key));
    }
};

//! Implementation of the getValue function for getting vector values
template<template<typename...> class VEC, typename... Args>
struct getValue_impl<VEC<Args...>, typename std::enable_if<!std::is_same<VEC<Args...>, std::string>::value>::type> {
    static VEC<Args...> getValue(const std::map<std::string, std::string>& valueMap, const std::string& key){
        auto tokens = tokenizeVector<std::string>(valueMap.at(key));
        return cast<VEC<Args...>>(tokens);
    };
};
/**
 * Get a value from the value map, cast into the desired type
 * @tparam T The type for the value to be cast into
 * @param valueMap The value map that stores the key-value pairs
 * @param key The key (or name of option) of the value to get
 * @return The value stored by the key, cast into the specified type
 */
template <typename T>
T getValue(const std::map<std::string, std::string>& valueMap, const std::string& key){
    return getValue_impl<T>::getValue(valueMap, key);
}

/**
 * Parses the provided file into a 1D vector, split by any character in the provided delimiter string
 * @param fileName The name of the file to be parsed
 * @param header Whether or not this file has headers in it
 * @param delimiter char used to split values
 * @return A vector of Reals, read from the file
 */
inline std::vector<Real> parse1DVector(std::string fileName, bool header = false, std::string delimiter = " ,;") {
    std::vector<Real> reals;
    std::ifstream file(fileName.c_str());
    if (!file.is_open())
        throw std::runtime_error("Parse1DVector Error: Couldn't open file: " + fileName);

    std::string value;
    if(header)
        getline(file, value);
    while (file.good()) {
        getline(file, value);
        for (auto str : tokenize(value, delimiter))
            reals.push_back(std::stod(str));
    }

    return reals;
}

/**
 * Parses the provided file into a 2D vector, split by any character in the provided delimiter strings
 * @param fileName The name of the file to be parsed
 * @param header If the file has headers in the first row (to be skipped)
 * @param row_delim character used to distinguish rows in the file
 * @param col_delim string of characters, any of which are used to distinguish cols in the file
 * @return A vector of type T, read from the file
 */
template <typename T>
inline vector2d<T> parse2DVector(std::string fileName, bool header = false, char row_delim = '\n', std::string col_delim = " ,;") {
    vector2d<T> reals;
    std::ifstream file(fileName.c_str());
    if (!file.is_open())
        throw std::runtime_error("Parse2DVector Error: Couldn't open file: " + fileName);

    std::string value;
    if(header)
        getline(file, value, row_delim);
    while (file.good()) {
        getline(file, value, row_delim);
        auto row = vcast<T>(tokenize(value, col_delim));
        if (!row.empty())
            push_back(reals, row);
    }
    file.close();

    return reals;
}


/**
 * Parses the provided file into a 2D vector, split by any character in the provided delimiter strings
 * @param fileName The name of the file to be parsed
 * @param header Whether or not the file has a header
 * @param row_delim character used to distinguish rows in the file
 * @param col_delim string of characters, any of which are used to distinguish cols in the file
 * @return A vector of Reals, read from the file
 */
inline vector2d<Real> parse2DVector(std::string fileName, bool header = false, char row_delim = '\n', std::string col_delim = " ,;"){
    return parse2DVector<Real>(fileName, header, row_delim, col_delim);
}

/**
 * Parses the provided string into a 1d vector. If the string represents a filepath then the vector will be populated from the contents of the file.
 * Otherwise the string is interpreted as a single value to be assigned "rows" times.
 * If it cannot be parsed at all, an exception will be thrown.
 * @param str The string to be parsed
 * @param rows The number of rows that should be constructed in the case of a single vector
 * @param header Whether or not the file has a header
 * @param delimiter The set of characters that separate vector entries
 * @return The constructed vector2d<Real>. Throws an exception if it cannot be parsed properly
 */
inline std::vector<Real> lenientParse1DVector(std::string str, int rows,  bool header = false, std::string delimiter = " ,;") {
    std::ifstream file(str.c_str());
    std::vector<Real> vec1D;
    if (file.is_open()) {
        // String represented a filepath -- read it in
        file.close();
        vec1D = parse1DVector(str, header, delimiter);
        if (vec1D.size() != rows)
            throw std::runtime_error("Error (lenientParse1DVector): File: " + str +
                                     " did not contain the expected number of rows (" + std::to_string(rows) + ")");
        return vec1D;
    }
    // String might represent a singular vector try to parse it
    try {
        auto val = cast<Real>(str);
        for (int i = 0; i < rows; i++)
            vec1D.push_back(val);
        return vec1D;
    } catch (...) {
        throw std::runtime_error("Error (lenientParse1DVector): Couldn't parse " + str + " as file or as vector");
    }
}

#define IGNORE_ROWS -1
/**
 * Parses the provided string into a 2d vector. If the string represents a filepath then the vector will be populated from the contents of the file.
 * Otherwise the string is interpreted as a single vector to be assigned "rows" times.
 * If it cannot be parsed at all, an exception will be thrown.
 * @param str The string to be parsed
 * @param rows The number of rows that should be constructed in the case of a single vector.
 *              If a negative number is supplied then the rows will be ignored -- the vecotr will have as many rows as the file did, or 1 row if it was a vector to be parsed.
 * @param row_delim The character that defines rows if the string is filepath
 * @param col_delim The string of characters used to parse columns if the string is a filepath
 * @return The constructed vector2d<Real>. Throws an exception if it cannot be parsed properly
 */
inline vector2d <Real> lenientParse2DVector(std::string str, int rows, bool header = false, char row_delim = '\n',
                                            std::string col_delim = " ,;") {
    std::ifstream file(str.c_str());
    vector2d<Real> vec2d;
    if (file.is_open()) {
        // String represented a filepath -- read it in
        file.close();
        vec2d = parse2DVector(str, header, row_delim, col_delim);
        if (rows >= 0 && vec2d.rows() != rows) // a negative number indicates that number of rows should not be checked
            throw std::runtime_error("Error (lenientParse2DVector): File: " + str +
                                     " did not contain the expected number of rows (" + std::to_string(rows) + ")");
        return vec2d;
    }
    // String might represent a singular vector try to parse it
    try {
        auto reals = tokenizeVector<Real>(str);
        for (int i = 0; i < rows; i++)
            push_back(vec2d, reals);
        if(rows < 0) // a negative number indicates that you just want the vector as parsed (i.e. 1 row)
            push_back(vec2d, reals);
        return vec2d;
    } catch (...) {
        throw std::runtime_error("Error (lenientParse2DVector): Couldn't parse " + str + " as file or as vector");
    }
}

inline std::map<Real, Real> parseToMap(std::string str){
    auto cross_section_vec = lenientParse2DVector(str, IGNORE_ROWS, true);
    std::map<Real, Real> cross_section_samples;
    if(cross_section_vec.size() == 1) // This means only a single value was provide
        cross_section_samples[0] = cross_section_vec(0,0);
    else if( cross_section_vec.cols() == 2) {
        for (uint i = 0; i < cross_section_vec.rows(); i++) {
            cross_section_samples[cross_section_vec(i, 0)] = cross_section_vec(i, 1);
        }
    } else{
        throw std::runtime_error("parseToMap Error: could not parse"
                                         " cross section " + str + " into a map (energy, sigma)");
    }
    return cross_section_samples;
};

/**
 * Write a 1D vector to a plain text file that is comma-separated
 * @tparam T  The type that is stored in the 1D vector to be written
 * @param vec The vector to be written to a file
 * @param filePath The full filepath of the destination file
 * @param subset A subset of indices to write to disk. If empty, the whole vector is written.
 * @param precision The floating point precision of the output
 */
template<typename T>
inline void write1DVector(const std::vector<T>& vec, std::string filePath, StreamOptions options = appendDefault(), std::string header = "") {
    tic("write1Dvector");
    std::ofstream file(filePath.c_str());
    if (!file.is_open())
        throw std::runtime_error("Error (write1DVector): Couldn't open file: " + filePath);

    if(!header.empty())
        file << header << options.rBookend;

    file << options << vec;

    file.close();
    toc();
}

/**
 * Write a 2D vector to a plain text file that is comma-separated for columns, and newline separated for rows
 * @tparam T  The type that is stored in the 2D vector to be written
 * @param vec The vector to be written to a file
 * @param filePath The full filepath of the destination file
 * @param options output options for the writing
 * @param headers Headers for the top of the file
 * @param ids ids to list as a column before the actual data
 */
template<typename T, typename INDEX = size_t>
inline void write2DVector(const vector2d <T>& vec, std::string filePath, StreamOptions options = appendDefault(),
                          const std::vector<std::string>& headers = {}, const std::vector<int>& ids = {}) {
    std::ofstream file(filePath.c_str());
    if (!file.is_open())
        throw std::runtime_error("Error (write2DVector): Couldn't open file: " + filePath);

    // Append the headers to the file
    if(!headers.empty())
        file << options << headers;

    auto subset = options.subset;
    options.subset.clear();

    // Get the number of entries to write to disk
    auto N = subset.empty() ? (size_t) vec.rows() : subset.size();
    for (size_t i = 0; i < N; i++) {
        if(!ids.empty())
            file << ids[i] << options.delimiter;
        file << options << getRow(vec, subset.empty() ? i : (size_t) subset[i]);
    }
    file.close();
}

/**
 * Check if a file exists
 * @param filePath The path to check if the file exists
 * @return True if the file exists, false otherwise
 */
inline bool exists(std::string filePath){
    std::ifstream file(filePath.c_str());
    return file.good();
}

/**
 * Clear the contents of a file
 * @param filePath The path of the file to clear
 */
inline void clear(std::string filePath){
    if(exists(filePath)) {
        std::ofstream file(filePath.c_str());
        if (!file.is_open())
            throw std::runtime_error("Error (clear): Couldn't open file: " + filePath);
        file.close();
    }
}

/**
 * Check if a file has anything in it
 * @param filePath The path of the file to check
 * @return True if the file is completely empty, false otherwise.
 */
inline bool isEmpty(std::string filePath){
    if(!exists(filePath))
        return true;

    std::ifstream filestr(filePath);
    if (!filestr.is_open())
        throw std::runtime_error("Error (clear): Couldn't open file: " + filePath);
    filestr.seekg(0, std::ios::end); // put the "cursor" at the end of the file
    int length = filestr.tellg(); // find the position of the cursor
    filestr.close();

    return length == 0;
}

/**
 * Read the entire contents of a file into a string
 * @param filePath The path of the file to read
 * @return A string containing the contents of a string
 */
inline std::string readAll(std::string filePath){
    std::ifstream filestr(filePath);
    if (!filestr.is_open())
        throw std::runtime_error("Error (readAll): Couldn't open file: " + filePath);
    std::string str;

    filestr.seekg(0, std::ios::end);
    str.reserve(filestr.tellg());
    filestr.seekg(0, std::ios::beg);

    str.assign((std::istreambuf_iterator<char>(filestr)),
               std::istreambuf_iterator<char>());

    return str;
}

/**
 * Write a single string to a file
 * @param filePath The path of the file to open
 * @param val The value to write
 */
inline void write(std::string filePath, std::string val){
    std::ofstream file(filePath.c_str());
    if (!file.is_open())
        throw std::runtime_error("Error (write): Couldn't open file: " + filePath);

    file << val;
    file.close();
}


/**
 * Append a something to a file
 * @param filePath The path of the file to append to
 * @param vals The values to append to the file
 * @param options The struct of StreamOptions for determining how the vector should be written
 */
template <typename T>
inline void append(std::string filePath, const T& val, StreamOptions options = appendDefault()) {
    std::ofstream file(filePath.c_str(), std::ofstream::out | std::ofstream::app);
    if (!file.is_open())
        throw std::runtime_error("Error (appendToFile): Couldn't open file: " + filePath);

    file << options << val;
    file.close();
}

/**
 * Create a directory (cross platform)
 * @param path The path of the directory to create
 */
inline void makeDirectory(std::string path){
    mode_t nMode = 0733; // UNIX style permissions
    int nError = 0;
#if defined(_WIN32)
    nError = _mkdir(path.c_str()); // can be used on Windows
#else
    nError = mkdir(path.c_str(),nMode); // can be used on non-Windows
#endif
    if (nError != 0 && errno != EEXIST) {
        if(errno == ENOENT)
            std::cerr << "Directory does not exist. Can only make one folder deep" << std::endl;
        throw std::runtime_error("IO (makeDirectory) Error: Could not make directory: " + path +
                                         " with error " + std::to_string(errno));
    }
}

/**
 * Removes directory if its empty
 * @param path The path of the directory to remove
 */
inline void removeDirectoryIfEmpty(std::string path){
#if defined(_WIN32)
	auto r = _rmdir(path.c_str());
#else
	auto r = rmdir(path.c_str());
#endif
}

}


#endif
