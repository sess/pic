#ifndef SESS_PIC_OPTIONS_HPP
#define SESS_PIC_OPTIONS_HPP

#include <string.h>
#include <boost/program_options.hpp>
#include <list>
#include "Label.h"
#include "Particle.h"
#include "Output.h"


namespace picard {

class Grid;
class FieldSolver;
class ParticleMover;
class Interpolator;
class LocalPhysics;
class GlobalPhysics;

/**
 * Grid characteristics for creating a grid without a mesh file
 */
struct SimpleGrid{
    size_t nDim; //!< Number of spatial dimensions of the grid
    vec min; //!< The min point of the grid
    vec max; //!< The max point of the grid
    ivec pts; //!< The number of grid points in each dimension
    bool empty() const{ return min.empty(); } //!< check if the grid is empty
};

//! Ways to initialize a particle list
enum InitType {
    TOTAL_COUNT, //!< The total number of particles
    PARTICLE_DENSITY, //!< The number of particles per unit volume
    QUIET_START, //!< Quiet start (uniform sampling of the cdf)
    CARTESIAN, //!< Spatial distribution that is cartesian sampled on the grid
    ON_GRID, //!< Initialize a species on the background grid (no actual particles are created)
    GAUSSIAN_DENSITY, //!< initialize a half gaussian density
    UNKNOWN_INIT
};

InitType getInitType(std::string typeStr); //!< Get the InitType from a string (not case sensitive)
std::ostream& operator<<(std::ostream& os, InitType type); //!< Stream the InitType

//! Initialization attributes for a species
struct PopulationInit{
    std::vector<multi_function> spatial_perturbations; //!< vector of spatial perturbations (to be applied for any population generation)
    InitType type{UNKNOWN_INIT}; //!< The initialization type
    size_t count{0}; //!< The total number of particles (use in conjunction with TOTAL_COUNT InitType)
    Real density{0}; //!< Particle density (used in conjunction with PARTICLE_DENSITY InitType)
    ivec spatialSamples; //!< Number of spatial samples in each dimension (used in conjunction with QUIET_START)
    ivec velocitySamples; //!< Number of velocity samples in each dimension (used in conjunction with QUIET_START)
    std::string on_grid_density; //!< A string that either contains a constant density value or a filename containing one density value per grid point (used in conjunction with ON_GRID)

    Real computeDensity(const Grid& grid) const; //!< Get the density from this initialization type

};

/**
 * The types of grids that are available
 */
enum GridType{
    BASIC_GRID, //!< The basic background grid
    NEUTRALIZING_GRID, //!< Background grid that assumes the plasma is quasi-neutral (TODO better descr)
    UNKNOWN_GRID, //!< Unknown grid type, used when the parsed grid type is not recognized
};

/**
 * The types of field solvers that are available
 */
enum FieldSolverType{
    DIRECT_SOLVER, //!< Direct solver -- used for 1D and 2D problems mostly
    MULTIGRID_SOLVER, //!< Multi-grid solver implemented with AMGCL-- used for 3D problems
    UNKNOWN_SOLVER //!< Unknown solver type, used when the parsed solver type is not recognized
};

/**
 * The types of ParticleMovers that are available
 */
enum ParticleMoverType{
    LEAP_FROG_INTEGRATOR, //!< The Leap-Frog particle mover
    UNKNOWN_MOVER, //!< Unknown solver type, used when the parsed solver type is not recognized
};

/**
 * The types of Interpolators that are available
 */
enum InterpolatorType{
    LINEAR_INTERPOLATOR, //!< The basic linear interpolator
    UNKNOWN_INTERPOLATOR, //!< Unknown solver type, used when the parsed solver type is not recognized
};


/**
 * Options class defines the options that the simulation uses to build.
 * The options class can be populated from a configuration file or created in the code from the API.
 * The purpose of including this class is to provide one level of indirection between the config file and the simulation setup
 * so that it can be more versatile in the future.
 */
class Options {
public:
    /**
     * Default constructor for the options class
     * All values are left at their default. Cannot run a simulation with defaults because there will be no labels, particles, etc.
     */
    Options(int argc = 0, char** argv = nullptr);
    
    /**
     * Construct options from the configuration file specified.
     * Default constructs the options object and then calls "setFromConfig" with the specified config file
     * @param configFile The path to the configuration file to be parsed
     */
    Options(std::string configFile, int argc = 0, char** argv = nullptr);

    // Timing
    double dt{0}; //!< Simulation Timestep
    double T{0}; //!< Max Simulation Time
    double t{0}; //!< Simulation start time
    uint iteration{0}; //!< Starting simulation iteration

    // Mesh
    std::string meshFile; //!< Contains the filepath to the *.msh mesh to be used in the simulation
    SimpleGrid grid; //!< A simple grid to be created in place of a mesh file

    //Labels
    std::vector<std::shared_ptr<FieldLabel>> fieldLabels; //!< All of the FieldLabels defined for the Grid
    std::vector<std::shared_ptr<ParticleBoundaryLabel>> particleBoundaryLabels; //!< All of the ParticleBoundaryLabels defined for the Grid

    // Object types and instances
    GridType gridType{BASIC_GRID}; //!< The type of Grid to construct
    FieldSolverType solverType{DIRECT_SOLVER}; //!< The type of FieldSolver to construct
    ParticleMoverType moverType{LEAP_FROG_INTEGRATOR}; //!< The type of ParticleMover to construct
    InterpolatorType interpolatorType{LINEAR_INTERPOLATOR}; //!< The type of Interpolator to construct

    //Particles
    std::map<std::string, std::shared_ptr<Species>> species; //!< The map from particle name to SpeciesTraits for filling the particle list
    std::map<std::shared_ptr<Species>, PopulationInit> populationInit; //!< mapping of how populations should be initialized
    std::shared_ptr<SpeciesReferenceParameters> referenceParams; //!< reference parameters to compute species mass and charge
    std::string referenceSpecies; //!< name of the reference species (for density scaling)

    // Physics
    std::vector<std::shared_ptr<LocalPhysics>> localPhysics; //!< Physics applied to individual particles
    std::vector<std::shared_ptr<GlobalPhysics>> globalPhysics; //!< Physics applied at each timestep

    // Outputs
    Output output; //!< The output options for the simulation

    std::vector<std::string> comments; //!< String of comments for this set of options
    std::string logFile; //!< filepath to a log file for storing all the settings from this run
    bool verbose{true}; //!< whether or not the code should run in verbose mode

    /**
     * set all of the setting in this object to be those parsed from the configuration file.
     * @param configFile The path of the configuration file to be parsed
     */
    void setFromConfig(std::string configFile);

    std::shared_ptr<Grid> makeGrid() const; //!< Construct a Grid based on the settings stored and return it
    std::shared_ptr<FieldSolver>  makeFieldSolver(const Grid& grid) const; //!< Construct a FieldSolver based on the settings stored and return it
    std::shared_ptr<ParticleMover>  makeParticleMover() const; //!< Construct a ParticleMover based on the settings stored and return it
    std::shared_ptr<Interpolator> makeInterpolator() const; //!< Construct a Interpolator based on the settings stored and return it
    Particles makeParticles(const Grid& grid) const; //!< Create and return all particles based on the initialization parameters
    std::list<Particle> makeParticleList(std::shared_ptr<Species> species, const Grid& grid) const; //!< Fill the specified particle based on the initialization parameters
    std::string lastParsedConfig() const{ return _lastParsedConfig; } //!< Get the last parsed configuration file path

    std::vector<std::string> logHeaders() const; //!< Headers for all config options
    std::vector<std::string> logValues() const; //!< Convert these options to a CSV list for lookup later
    void writeConfigs(std::string directory) const; //!< Write the configuration options used to construct these options to the desired directory
    void deleteConfigs(std::string directory) const; //!< Delete configuration options written by writeConfigs in the desired directory
private:

    int fieldPriority{0}; //!< The current highest field label priority
    int particlePriority{0}; //!< The current highest particle label priority

    void initializeProgramOptions(); //!< Initialize the program options descriptions container to specify what the options class expects to find in the configuration file.

    /**
     * Parses the config file specified by fileName and stores the results in a boost::variable_map
     * @param fileName The config file path to be parsed
     * @return The string contents of the configuration file. Empty string if the file could not be opened.
     */
    std::string parseConfigFile(std::string fileName);

    /**
     * Parses the command line arguments and stores the results in a boost::variable_map
     * @param argc The number of command line arguments
     * @param argv The command line arguments passed in
     * @return The contents fo the command line arguments
     */
    std::string parseCommandLine(int argc, char** argv);
    
    void setVariablesFromParsedConfig(); //!< Sets the local variables from the variable map that has been created by parsing a configuration file

    void fillBField(std::shared_ptr<Grid> grid) const; //!< Populates the grids b-field based on the a file or vector

    SimpleGrid parseSimpleGrid(std::string grid) const; //!< Parse the simple grid parameters from the grid option
    /**
     * Parses basic label parameters from the provided string
     * @param labelStr The string from the config file to be parsed into a label
     * @return a tuple containing the label information. The order is "name", "type", normal vector, properties list (not converted to Real yet)
     */
    std::tuple<std::string, std::string, vec, std::vector<std::string>> parseLabel(std::string labelStr);
    
    /**
     * Parses a FieldLabel from the provided string
     * @param labelStr The string from the config file to be parsed into a label
     * @return A shared_ptr<FieldLabel> to the newly created label.
     */
    std::shared_ptr<FieldLabel> parseFieldLabel(std::string labelStr);

    /**
     * Parses a FieldLabel from the provided string from the setDomain configuration option
     * @param labelStr The string from the config file to be parsed into a label
     * @return A shared_ptr<FieldLabel> to the newly created label.
     */
    std::shared_ptr<FieldLabel> parseDomainLabel(std::string labelStr);

    /**
     * Parses a ParticleBounaryLabel from the provided string
     * @param labelStr The string from the config file to be parsed into a label
     * @return A shared_ptr<ParticleBounaryLabel> to the newly created label.
     */
    std::shared_ptr<ParticleBoundaryLabel> parseParticleBoundaryLabel(std::string labelStr);

    void addSpecies(std::string speciesStr); //!< Add a new template particle from the "addParticle" string

    void addInitType(std::string initType); //!< Assign the initialization type from the "intialize" string

    void addVelocityDistribution(std::string vel_dist); //!< Assign the velocity distribution from the "velocity_distribution" string

    void addSpatialPerturbation(std::string vel_dist); //!< Assign the velocity distribution from the "velocity_distribution" string

    void addParticleInteraction(std::string interaction); //!< Introduce an interparticle interaction
    void addPlasmaBoundary(std::string plasmaBoundary); //!< Add a plasma boundary

    //! Lookup a label by name
    template <typename LABEL_T>
    std::shared_ptr<LABEL_T> findLabelByName(const std::vector<std::shared_ptr<LABEL_T>>& labels, std::string name){
        for(auto l : labels){
            if (l->name == name)
                return l;
        }
        throw std::runtime_error("Options (findLabelByName) Error: Could not find a label with name: " + name);
    }

    std::vector<std::string> expandMultiToken(const std::vector<std::string>& token); //!< creates a vector of strings from another vector of strings that may have comma-separated values in each string. So {"1,2,3", "4"} becomes {"1", "2", "3", "4"}

    OutputEntry parseOutput(std::string outputOption, OutputEntry defaultOE); //!< Adds an output entry to the output container from a string
    std::vector<std::pair<std::string, std::string>> logEntries() const; //!< place where log entries are defined

    std::shared_ptr<boost::program_options::options_description> descriptions; //!< Options for the configuration file
    boost::program_options::variables_map vars; //!< Variables read from the configuration file
    std::string _lastParsedConfig; //!< The last configuration file to be parsed
    std::string _configFileText; //!< The contents of the configuration file
    std::string _defaultFileText; //!< The contents of the default configuration file
    std::string _commandLineArguments; //!< The command line arguments use to invoke the application

    int argc; //!< Number of command line arguments
    char** argv; //!< The command line arguments

    static const char* _defaultConfig; //!< The filepath to the default configuration file
};

GridType getGridType(std::string typeStr); //!< Get the GridType from a string (not case sensitive)
std::ostream& operator<<(std::ostream& os, GridType type);//!< Stream the GridType

FieldSolverType getSolverType(std::string typeStr); //!< Get the FieldSolverType from a string (not case sensitive)
std::ostream& operator<<(std::ostream& os, FieldSolverType type); //!< Stream the FieldSolverType

ParticleMoverType getMoverType(std::string typeStr); //!< Get the ParticleMoverType from a string (not case sensitive)
std::ostream& operator<<(std::ostream& os, ParticleMoverType type); //!< Stream the ParticleMoverType

InterpolatorType getInterpolatorType(std::string typeStr); //!< Get the InterpolatorType from a string (not case sensitive)
std::ostream& operator<<(std::ostream& os, InterpolatorType type); //!< Stream the InterpolatorType

std::ostream& operator<<(std::ostream& os, SimpleGrid g); //!< Stream the InterpolatorType

//! Convert vector of labels into a mapping with label id. Assumes the name of the label corresponds to a boundary id
template <typename LABEL_T>
std::map<int, std::shared_ptr<LABEL_T>> createLabelMap(const std::vector<std::shared_ptr<LABEL_T>>& labels){
    std::map<int, std::shared_ptr<LABEL_T>> m;
    for(auto l : labels)
        m[getBoundaryId(l->name)] = l;
    return m;
};

}

#endif
