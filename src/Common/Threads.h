#ifndef THREADS_HPP
#define THREADS_HPP

#include <mpi.h>
#include <string>


namespace picard {

/**
 * Check if the MPI call was successful. Throws an exception if it was not
 * @param code The return code from the MPI call
 * @param call_name The name of the call that was made (for an exception message)
 */
inline void check_MPI_call(int code, std::string call_name) {
    if(code != MPI_SUCCESS){
        throw std::runtime_error("Error in MPI call: " + call_name);
    }
}

/**
 * Check if MPI init has been called.
 * @return True if MPI init has been called, false otherwise
 */
inline bool MPI_init_called() {
    int init_called;
    check_MPI_call(MPI_Initialized( &init_called ), "MPI_Initialized");
    return (bool)init_called;
}

//! Get the number of threads from MPI
inline int query_MPI_Num_Threads(){
    int size = 1;
    if(MPI_init_called())
        MPI_Comm_size(MPI_COMM_WORLD, &size);
    return size;
}

//! Get the number of threads (queries MPI the first time and then keeps returning that value)
inline int numThreads(){
    thread_local static int _numThreads = query_MPI_Num_Threads();
    return _numThreads;
}

//! Get the rank of this thread from MPI
inline int query_MPI_Rank(){
    int rank = 0;
    if(MPI_init_called())
        MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    return rank;
}

//! Get the rank of this thread (queries MPI the first time and then keeps returning that value)
inline int rank(){
    thread_local static int _rank = query_MPI_Rank();
    return _rank;
}

//! Check whether this thread is the root thread (rank 0)
inline bool isRoot(){
    return rank() == 0;
}


/**
 * Split an integer amongst the specified threads
 * @param total The count to be split up amongst the threads
 * @param rank The thread index
 * @param size The total number of threads
 * @return The amount that the specified thread gets of the total
 */
template<typename T>
inline T getThreadLoad(T total) {
    T countPerThread = (T) floor((double) total / (double) numThreads());
    T leftover = total % numThreads();

    // the remainder of particles are equally distributed amongst the processors
    if (rank() < leftover)
        countPerThread++;

    return countPerThread;
}

}
#endif