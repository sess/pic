#include <cmath>
#include <map>
#include "Domain.h"
#include "Particle.h"
#include "Distributions.h"
#include "StringUtils.h"

using namespace std;
using namespace picard;

Domain::Domain(int nDim, const vec& min, const vec& max, bool particles_permitted, int id)
        : _nDim(nDim), _min(min), _max(max), _id(id), _permitted(particles_permitted) {
    refresh();
}

void Domain::setMin(const vec& minPoint) {
    _min = minPoint;
    refresh();
}

void Domain::setMin(int dim, Real minVal)  {
    _min[dim] = minVal;
    refresh();
}

void Domain::setMax(const vec& maxPoint)  {
    _max = maxPoint;
    refresh();
}

void Domain::setMax(int dim, Real maxVal) {
    _max[dim] = maxVal;
    refresh();
}

void Domain::setDimension(int nDim, const vec& min, const vec& max) {
    _nDim = nDim;
    _min = min;
    _max = max;
    refresh();
}

Real Domain::bounds(BoundaryId id) const {
    int index = idDimension(id);
    if(id < 0 || index >= nDim())
        throw runtime_error("Domain (bounds) error: id (" + to_string(id) +
                                    ") out of bounds for a domain of dimension " + to_string(nDim()));
    bool min = (id % 2 == 0);
    if (min)
        return _min[index];
    else
        return _max[index];
}

vec Domain::normal(BoundaryId id) const {
    int multiplier = (_permitted) ? 1 : -1;
    auto norm = defaultNormal(id)*multiplier;
    norm.resize(_nDim);
    return norm;
}

void Domain::refresh() {
    _min.resize(_nDim);
    _max.resize(_nDim);
}

bool Domain::isOutside(const vec& pos, BoundaryId id) const {
    int dim = idDimension(id);
    int mult = idMinMax(id);
    return bounds(id)*mult < pos[dim]*mult  && !isOnBoundary(pos,id);
}

bool Domain::isInside(const vec& pos) const {
    for (int n = 0; n < _nDim; n++) {
        if (!(fgeq(pos[n], _min[n]) && fleq(pos[n], _max[n])))
            return false;
    }
    return true;
}

bool Domain::isOnBoundary(const vec& pos) const {
    if(isInside(pos)) {
        for (int n = 0; n < _nDim; n++) {
            if (feq(pos[n], _min[n]) || feq(pos[n], _max[n]))
                return true;
        }
    }
    return false;
}

std::vector<BoundaryId> Domain::boundaryIds(const vec& pos) const {
    vector<BoundaryId> ids;
    for (int n = 0; n < 2*_nDim; n++) {
        auto id = (BoundaryId)n;
        if(isOnBoundary(pos, id))
            ids.push_back(id);
    }
    return ids;
}

Intersection Domain::intersects(const vector<Real>& ray) const {
    vector<Real> startPoint(ray.begin(), ray.begin() + _nDim);
    vector<Real> currentPos(ray.begin() + _nDim, ray.begin() + 2 * _nDim);

    // check if the particle is currently outside of permitted domain or inside a non-permitted domain
    if (((isInside(currentPos)  && _permitted) || (isOutside(currentPos) && !_permitted)) && !isOnBoundary(currentPos))
        return Intersection();

    // Check if particle started in a not-allowed position so don't count an intersection
    if (((isOutside(startPoint) && _permitted) || (isInside(startPoint) && !_permitted)) || isOnBoundary(startPoint))
        return Intersection();

    Intersection intersection;
    for (int i = 0; i < _nDim * 2; i++) {
        if (intersection)
            break;
        switch (_nDim) {
            case 3 :
                intersection = intersectsPlane((BoundaryId)i, ray);
                break;
            case 2 :
                intersection = intersectsLine((BoundaryId)i, ray);
                break;
            case 1 :
                intersection = intersectsPoint((BoundaryId)i, ray);
                break;
            default:
                throw runtime_error("Domain (intersects) Error; can't compute intersection nDim  " + to_string(_nDim));
        }
        intersection.id = (BoundaryId)i;
        intersection.dim = idDimension(intersection.id);
        intersection.normal = this->normal(intersection.id);
    }
    return intersection;
}

Intersection Domain::intersectsPoint(BoundaryId id, const vector<Real>& ray) const {
    Intersection intersection;
    Real val = bounds(id);
    if ((ray[0] <= val && ray[1] >= val) || (ray[1] <= val && ray[0] >= val)) {
        intersection.point = {val};
        intersection.count = 1;
    }
    return intersection;
}

Intersection Domain::intersectsLine(BoundaryId id, const vector<Real>& ray) const {
    vector<Real> v2 = {ray[2] - ray[0], ray[3] - ray[1]};
    auto nDirIndex = id / 2;
    auto otherDim = (nDirIndex + 1) % _nDim;
    // get reference point
    vector<Real> o(_nDim);
    o[otherDim] = _min[otherDim];
    auto bound = bounds(id);
    o[nDirIndex] = bound;

    vector<Real> v3(_nDim);
    v3[otherDim] = 0;
    v3[nDirIndex] = length(otherDim);
    v3[0] = -v3[0];

    vector<Real> v1 = {o[0] - ray[0], o[1] - ray[1]};

    Intersection intersection;
    auto v2_dot_v3 = dot(v2, v3);
    if (v2_dot_v3 == 0) {
        return intersection;
    }
    auto t1 = cross2d(v2, v1) / v2_dot_v3;
    auto t2 = dot(v1, v3) / v2_dot_v3;

    if (fgeq(t1, 0) && fleq(t1, 1) && fgeq(t2, 0) && fleq(t2, 1)) {
        intersection.point = {ray[0] + t2 * v2[0], ray[1] + t2 * v2[1]};
        intersection.point[nDirIndex] = bound; // Reduce floating point error by fixing the intersection dim

        if (feq(t1, 0) || feq(t1, 1))
            intersection.count = 2; // Intersects a corner
        else
            intersection.count = 1;
    }
    return intersection;
}

Intersection Domain::intersectsPlane(BoundaryId id, const vector<Real>& ray) const {
    vector<Real> deltaR = {ray[3] - ray[0], ray[4] - ray[1], ray[5] - ray[2]};
    int nDirIndex = id / 2;
    // Get the other two dimensions
    auto dim1 = (nDirIndex + 1) % _nDim;
    auto dim2 = (nDirIndex + 2) % _nDim;

    // Get planar reference point
    auto S1 = _min;
    auto bound = bounds(id);
    S1[nDirIndex] = bound;

    // Compute plane normal
    vector<Real> S21(3, 0), S31(3, 0);
    S21[dim1] = length(dim1);
    S31[dim2] = length(dim2);
    vector<Real> n;
    n = cross3d(S21, S31);

    Intersection intersection;
    // Check if parallel
    if (dot(deltaR, n) != 0) {
        // Compute t and M
        vector<Real> ray0_s1_diff = {ray[0] - S1[0], ray[1] - S1[1], ray[2] - S1[2]};
        Real t = -dot(n, ray0_s1_diff) / dot(n, deltaR);
        if (t >= 0 && t <= 1) {
            intersection.point = {ray[0] + t * deltaR[0], ray[1] + t * deltaR[1], ray[2] + t * deltaR[2]};
            intersection.point[nDirIndex] = bound;

            vector<Real> M_S1_diff = {intersection.point[0] - S1[0],
                                      intersection.point[1] - S1[1],
                                      intersection.point[2] - S1[2]};

            // Compute u,v and check if they are contained in the plane
            Real u = dot(M_S1_diff, S21); Real u_max = dot(S21, S21);
            Real v = dot(M_S1_diff, S31); Real v_max = dot(S31, S31);

            if (fgeq(u, 0) && fleq(u, u_max) && fgeq(v, 0) && fleq(v, v_max)) {
                if (feq(u, 0) || feq(u, u_max) || feq(v, 0) || feq(v, v_max))
                    intersection.count = 2;// intersects on edge/corner
                else
                    intersection.count = 1;
            }
        }
    }
    return intersection;
}

Domain Domain::Random(int nDim, Real max_width) {
    auto minVec = uniform(vec(nDim, -max_width), vec(nDim, max_width));
    auto maxVec = minVec + uniform(vec(nDim, 0), vec(nDim, max_width));
    return Domain(nDim, minVec, maxVec);
}

vec Domain::randomPositionInside() const {
    return uniform(_min, _max);
}

vec Domain::randomPositionOutside(Real buffer) const {
    int maxIteration = 1000;
    int iteration = 0;

    // Setup the domain using the desired buffer size
    auto mn = _min - size() * (buffer / 2.);
    auto mx = _max + size() * (buffer / 2.);

    // Select random points until we find one outside the domain
    while (iteration < maxIteration) {
        auto pos = uniform(mn, mx);
        if (isOutside(pos)) {
            return pos;
        }
        iteration++;
    }
    throw std::runtime_error(
            "Error in Domain::randomPositionOutside: Took too many tries to find a point outside the domain");
}

vec Domain::randomPositionOutside(BoundaryId id, Real buffer) const {
    auto v = randomBoundaryPosition(id);
    int dim = idDimension(id);
    v[dim] += idMinMax(id)*uniform(0,length(dim)*buffer);
    return v;
}

vec Domain::randomBoundaryPosition(int* nBoundaries) const {
    return snapToBoundary(randomPositionOutside(), nBoundaries);
}

vec Domain::randomBoundaryPosition(BoundaryId id) const {
    int trials = 0;
    vec v;
    while(trials < 5){
        trials++;
        v = randomPositionInside();
        if(!isOnBoundary(v)){
            v[idDimension(id)] = bounds(id);
            break;
        }
    }
    return v;
}

vec Domain::snapToBoundary(const vec& pos_in, int* nBoundaries) const {
    auto pos = pos_in;
    int snaps = 0;
    for (int i = 0; i < _nDim; i++) {
        if (pos[i] <= min(i)) {
            pos[i] = min(i);
            snaps++;
        }
        if (pos[i] >= max(i)) {
            pos[i] = max(i);
            snaps++;
        }
    }
    if (nBoundaries)
        (*nBoundaries) = snaps;
    return pos;
}

bool Domain::operator==(const Domain& other) const {
    return (nDim() == other.nDim() && min() == other.min() && max() == other.max()
            && id() == other.id() && particlesPermitted() == other.particlesPermitted());
}

Real Domain::area() const {
    Real a = 1;
    for(uint i=0; i<_nDim; i++){
        if(!feq(min(i), max(i)))
            a *= max(i) - min(i);
    }
    return a;
}

BoundaryId picard::getBoundaryId(string id) {
    id = to_lower(id);
    if(id == "xmin")
        return X_MIN;
    else if(id == "xmax")
        return X_MAX;
    else if(id == "ymin")
        return Y_MIN;
    else if(id == "ymax")
        return Y_MAX;
    else if(id == "zmax")
        return Z_MAX;
    else if(id == "zmin")
        return Z_MIN;
    else if(id == "interior")
        return INTERIOR;
    else
        return NONE;
}

std::string picard::toString(BoundaryId id) {
    switch(id){
        case X_MIN : return "xmin";
        case X_MAX : return "xmax";
        case Y_MIN : return "ymin";
        case Y_MAX : return "ymax";
        case Z_MIN : return "zmin";
        case Z_MAX : return "zmax";
        case INTERIOR : return "interior";
        default : return "unknown";
    }
}

vec picard::defaultNormal(BoundaryId id) {
    switch (id) {
        case X_MIN : return {-1, 0, 0};
        case X_MAX : return {1, 0, 0};
        case Y_MIN : return {0, -1, 0};
        case Y_MAX : return {0, 1, 0};
        case Z_MIN : return {0, 0, -1};
        case Z_MAX : return {0, 0, 1};
        default : return {};
    }
}

ostream& picard::operator<<(ostream& os, const Domain& d) {
    os << "Domain id: " << d.id();
    os << " Particles Permitted: " << d.particlesPermitted() << " ";
    for (int n = 0; n < d.nDim(); n++) {
        if (n == 0)
            os << "x-range: (";
        if (n == 1)
            os << "y-range: (";
        if (n == 2)
            os << "z-range: (";
        os << d.min(n) << ", " << d.max(n) << ") ";
    }
    os << endl;
    return os;
}

std::ostream& picard::operator<<(std::ostream& os, const Intersection& i){
    os << "Intersection at (" << i.point << "), at boundary: " << toString(i.id) << " with count: " << i.count;
    return os;
}
