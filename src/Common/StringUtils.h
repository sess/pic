#ifndef SESS_PIC_STRING_UTILS_H
#define SESS_PIC_STRING_UTILS_H

#include <string>
#include <vector>
#include <fstream>
#include "VectorUtils.h"
#include "CastUtils.h"
#include <cctype>

namespace picard {

/**
 * Clean a string for use in a file name.
 * replaces spaces with underscores
 * @param s
 * @return The cleaned string
 */
inline std::string clean(std::string s){
    replace(s.begin(), s.end(), ' ', '_' );
    return s;
}

/**
 * Get the lowercase version of the provided string
 * @param A string to be lowercased
 * @return A new string that is the lowercase version of the argument string
 */
inline std::string to_lower(std::string s) {
    std::transform(s.begin(), s.end(), s.begin(), ::tolower);
    return s;
}

/**
 * Adds a forward slash to the end of a string if it does not already have one at the end
 * @param s The string to add the slash to
 * @return The string with a slash at the end
 */
inline std::string addSlash(std::string s){
    if(s.back() != '/')
        s.push_back('/');
    return s;
}

/**
 * Compares the lowercase version of two strings
 * @param s1 The first string to compare
 * @param s2 The second string to compare
 * @return True if the lowercase versions of the strings are the same, false otherwise.
 */
inline bool lowerEq(std::string s1, std::string s2) {
    return to_lower(s1) == to_lower(s2);
}

/**
 * Check if a string contains the specified character (not case sensitive)
 * @param str The string to search
 * @param c The character to check
 * @return True if the string contains at least one instance of the character, false otherwise
 */
inline bool contains(std::string str, char c) {
    str = to_lower(str);
    c = std::tolower(c);
    return str.find(c) != std::string::npos;
}


/**
 * Trims the specified string of all leading and trailing characters that are contained within the whitespace string
 * @param str The string to be trimmed
 * @param delimiter The string of characters to be removed from the string on the leading and trailing end
 * @return The trimmed string
 */
inline std::string trim(const std::string& str, const std::string& delimiter = " \t") {
    const auto strBegin = str.find_first_not_of(delimiter);
    if (strBegin == std::string::npos)
        return ""; // no content

    const auto strEnd = str.find_last_not_of(delimiter);
    const auto strRange = strEnd - strBegin + 1;

    return str.substr(strBegin, strRange);
}

/**
 * Splits the provided string by the provided delimiter to create a vector of string tokens. Each token is trimmed of leading and trailing characters that are defined in the whitespace
 * @param str The string to be tokenized
 * @param delimiter The set of characters to split by
 * @param whitespace Characters to remove from the beginning and end of each token
 * @return A vector of string tokens
 */
template<template<typename...>class Container = std::vector>
inline Container<std::string> tokenize(std::string str, const std::string& delimiter = ",",
                                         const std::string& whitespace = " \t") {
    Container<std::string> tokens;
    while (str.size()) {
        size_t pos = str.find_first_of(delimiter);
        auto token = trim(str.substr(0, pos), whitespace);
        if (!token.empty())
            tokens.push_back(token);
        if (pos == std::string::npos)
            break;
        str.erase(0, pos + 1);
    }
    return tokens;
}

/**
 * Splits the provided string as a vector meaning it uses the ";" for the delimitor and "()" as whitespace
 * @param str The string to be tokenized
 * @return A vector of string tokens
 */
template <typename T>
inline std::vector<T> tokenizeVector(std::string str) {
    return vcast<T>(tokenize(str, std::string(";"), std::string("( )\t")));
}

/**
 * Checks if the provided number of arguments matches the expected number
 * @param providedArgs Number of arguments provided
 * @param expectedArgs Number of arguments expected
 * @return Throws a runtime_error if the two do not match. Otherwise do nothing.
 */
inline void checkNumArgs(int providedArgs, int expectedArgs, std::string parseLine = "", std::string location = "") {
    if (providedArgs != expectedArgs) {
        std::string message = "Error! the number of arguments provided (" + std::to_string(providedArgs) +
                              ") does not match the number of arguments expected (" + std::to_string(expectedArgs) +
                              ").";
        if (!parseLine.empty())
            message += " Line: " + parseLine;

        if (!location.empty())
            message = location + " " + message;

        throw std::runtime_error(message);
    }
}

/**
 * Checks if the provided number of arguments is at least as large the expected number
 * @param providedArgs Number of arguments provided
 * @param expectedArgs Number of arguments expected
 * @return Throws a runtime_error if the two do not match. Otherwise do nothing.
 */
inline void checkNumArgsAtLeast(int providedArgs, int expectedArgs, std::string parseLine = "",
                                std::string location = "") {
    if (providedArgs < expectedArgs) {
        std::string message = "Error! the number of arguments provided (" + std::to_string(providedArgs) +
                              ") is fewer than the number of arguments expected (" + std::to_string(expectedArgs) +
                              ").";
        if (parseLine != "")
            message += " Line: " + parseLine;

        if (location != "")
            message = location + " " + message;

        throw std::runtime_error(message);
    }
}

/**
 * Get the character relating to the specified dimensino ('x' for 0, 'y' for 1, 'z' for 2)
 * @param dim The dimension of the character to get
 * @return If the dim is less than 3 then 'x' for 0, 'y' for 1, 'z' for 2. '?' otherwise
 */
inline char dimToChar(int dim) {
    switch (dim) {
        case 0 :
            return 'x';
        case 1 :
            return 'y';
        case 2 :
            return 'z';
        default :
            return '?';
    }
}

/**
 * Output a vector of strings that represents the header to a nDim dimensional dataset
 * @param nDim Number of dimensions for the header
 * @param prefix The prefix to the header (i.e. for velocity use "v")
 * @return A vector of strings that represents the header
 */
inline std::vector<std::string> dimensionalHeader(int nDim, std::string prefix =  ""){
    switch (nDim) {
        case 1 :
            return {prefix + "x"};
        case 2 :
            return {prefix + "x", prefix + "y"};
        case 3 :
            return {prefix + "x", prefix + "y", prefix+"z"};
        default:
            return {};
    }
}

/**
 * Get the index dimension of the specified dimension (0 for 'x', 1 for 'y', 2 for 'z'). Not case sensitive
 * @param c The character specifying the dimension ('x', 'y' or 'z')
 * @return The index for the specified dimension (0 for 'x', 1 for 'y', 2 for 'z'). -1 for an unrecognized char
 */
inline int charToDim(char c) {
    c = std::tolower(c);
    switch (c) {
        case 'x' :
            return 0;
        case 'y' :
            return 1;
        case 'z' :
            return 2;
        default :
            return -1;
    }
}


}


#endif
