#ifndef SESS_PIC_VEC_HPP
#define SESS_PIC_VEC_HPP

#include "Definitions.h"
#include "Element.h"
#include "MathUtils.h"
#include <Eigen/Dense>
#include <map>

namespace picard {

/***** Streaming operations forward declare -- implemented in IOUtils ******/
template<typename T> std::ostream& operator<<(std::ostream &os, const std::vector<T> &vec);
template<typename T, size_t S> std::ostream& operator<<(std::ostream &os, const std::array<T, S> &arr);


/**********************************
 *      vector2d operations       *
 **********************************/
template <typename T, typename VEC>
inline void push_back(vector2d<T>& vec2d, VEC newRow) {
    auto r = vec2d.rows();
    if (vec2d.cols() == newRow.size() || vec2d.size() == 0) {
        auto c = newRow.size();
        auto temp = vec2d;
        vec2d.resize(r + 1, c);
        for(int i=0; i<r; i++)
            for(int j=0; j<c; j++)
                vec2d(i,j) = temp(i,j);
        for (size_t i = 0; i < c; i++)
            vec2d(r, i) = newRow[i];
    } else {
        throw std::runtime_error(
                "Vector2D Error (push_back): Cannot push_back a vector of size " + std::to_string(newRow.size()) +
                " to a vector2d with " + std::to_string(vec2d.cols()) + " columns.");
    }
}

template <typename T>
inline std::vector<T> getRow(const vector2d<T>& vec2d, size_t i){
    Eigen::ArrayXd row = vec2d.row(i);
    return std::vector<T>(row.data(), row.data() + row.size());
}

template <typename T>
inline std::vector<T> getCol(const vector2d<T>& vec2d, size_t i){
    Eigen::ArrayXd col = vec2d.col(i);
    return std::vector<T>(col.data(), col.data() + col.size());
}

/********************************************
 *            Vector Operations             *
 ********************************************/

template <typename T>
std::vector<T> concatenate(const shortVec<T>& v1, const shortVec<T>& v2) {
    std::vector<T> ray(v1.begin(), v1.end());
    ray.insert(ray.end(), v2.begin(), v2.end());
    return ray;
}

template <typename T>
inline shortVec<T> cross3d(const shortVec<T>& a, const shortVec<T>& b) {
    shortVec<T> c(3);
    c[0] = a[1] * b[2] - a[2] * b[1];
    c[1] = a[2] * b[0] - a[0] * b[2];
    c[2] = a[0] * b[1] - a[1] * b[0];
    return c;
}

template <typename T>
inline Real cross2d(const shortVec<T>& v1, const shortVec<T>& v2) {
    return (v1[0] * v2[1]) - (v1[1] * v2[0]);
}

template <typename T>
inline T dot(const shortVec<T>& a, const shortVec<T>& b) {
    T c = 0;
    for (uint i = 0; i < a.size(); i++)
        c += a[i] * b[i];
    return c;
}

template <typename T>
inline shortVec<T> resize(shortVec<T> a, size_t size = 3, const T& fill = 0) {
    for (auto i = a.size(); i < size; i++)
        a.push_back(fill);
    return a;
}

template <typename T>
inline Real norm(const shortVec<T>& a, Real p = 2) {
    Real result = 0;
    for (uint i = 0; i < a.size(); i++)
        result += std::pow(std::abs(a[i]), p);
    return std::pow(result, 1.0 / p);
}

template <typename T>
inline shortVec<T> normalize(shortVec<T> a, int p=2) {
    auto s = norm(a,p);
    if(s == 0)
        return a;
    for (uint i = 0; i < a.size(); i++)
        a[i] /= s;
    return a;
}

template <typename T>
inline T sum(const shortVec<T>& a) {
    T result = 0;
    for (uint i = 0; i < a.size(); i++)
        result += a[i];
    return result;
}

template <typename T>
inline T product(const shortVec<T>& a) {
    if(!a.size())
        return 0;

    T result = 1;
    for (uint i = 0; i < a.size(); i++)
        result *= a[i];
    return result;
}

template<typename T>
uint nonZeroDim(const shortVec<T>& vec) {
    for (uint i = 0; i < vec.size(); i++) {
        if (vec[i] != 0)
            return i;
    }
    return vec.size();
}

inline ivec ijk(int index, ivec numPts, int nDim) {
    ivec res(nDim);
    for (int i = 0; i < nDim; i++) {
        res[i] = (index % numPts[i]);
        index = (index - res[i]) / numPts[i];
    }
    return res;
}

/**
 * Returns a displacement vector (length 3) that is the binary value of the integer provided
 * i.e. if i=0 -> disp = (0,0,0)
 *      if i=1 -> disp = (0,0,1)
 *      if i=5 -> disp = (1,0,1)
 *      etc...
 * @param i The integer to be converted into binary
 * @return A displacement vector
 */
inline ivec displacement(unsigned int i){
    return {CHECK_BIT(i, 0), CHECK_BIT(i,1), CHECK_BIT(i,2)};
}

/**
 * returns a vector of all dimensions that are zero of the provided vector
 */
template<typename T>
inline std::vector<int> zeroDims(const shortVec<T>& vec) {
    std::vector<int> tang;
    for (uint i = 0; i < vec.size(); i++)
        if (vec[i] == 0)
            tang.push_back(i);

    return tang;
}

/**
 * returns a vector containing the sign of each parameter
 */
template <typename T>
inline shortVec<int> sign(const shortVec<T>& v){
    shortVec<int> vret(v.size());
    for(uint i=0; i<v.size(); i++)
        vret[i] = sign(v[i]);
    return vret;
}
    
/**
 * returns a vector containing the abs of each parameter
 */
template <typename T>
inline shortVec<T> vabs(const shortVec<T>& v){
    shortVec<T> vret(v.size());
    for(uint i=0; i<v.size(); i++)
        vret[i] = std::abs(v[i]);
    return vret;
}
    
template <typename T>
inline shortVec<T> operator-(const shortVec<T>& v1){
    shortVec<T> vret(v1.size());
    for(uint i=0; i<v1.size(); i++)
        vret[i] = -v1[i];
    return vret;
}

template <typename T>
inline shortVec<T> sqrt(const shortVec<T>& v1){
    shortVec<T> vret(v1.size());
    for(uint i=0; i<v1.size(); i++)
        vret[i] = std::sqrt(v1[i]);
    return vret;
}

template <typename T>
inline shortVec<T> vexp(const shortVec<T>& v1){
    shortVec<T> vret(v1.size());
    for(uint i=0; i<v1.size(); i++)
        vret[i] = std::exp(v1[i]);
    return vret;
}

template <typename T>
inline shortVec<T> verf(const shortVec<T>& v1){
    shortVec<T> vret(v1.size());
    for(uint i=0; i<v1.size(); i++)
        vret[i] = std::erf(v1[i]);
    return vret;
}

template <typename T>
inline shortVec<T> pow(const shortVec<T>& v1, Real p){
    shortVec<T> vret(v1.size());
    for(uint i=0; i<v1.size(); i++)
        vret[i] = std::pow(v1[i], p);
    return vret;
}



/********************************************
 *    Arithmetic operations with vector     *
 ********************************************/
template <typename T>
void checkSize(const shortVec<T>& v1, const shortVec<T>& v2){
    if(v1.size() != v2.size())
        throw std::runtime_error("Can't do arithmetic operations on vectors of different sizes");
}

// Operators with vectors
template <typename T>
inline shortVec<T> operator+(const shortVec<T>& v1, const shortVec<T>& v2){
    checkSize(v1, v2);
    shortVec<T> vret(v1.size());
    for(uint i=0; i<v1.size(); i++)
        vret[i] = v1[i] + v2[i];
    return vret;
}

template <typename T>
inline shortVec<T>& operator+=(shortVec<T>& v1, const shortVec<T>& v2){
    checkSize(v1, v2);
    for(uint i=0; i<v1.size(); i++)
        v1[i] += v2[i];
    return v1;
}

template <typename T>
inline shortVec<T> operator-(const shortVec<T>& v1, const shortVec<T>& v2){
    checkSize(v1, v2);
    shortVec<T> vret(v1.size());
    for(uint i=0; i<v1.size(); i++)
        vret[i] = v1[i] - v2[i];
    return vret;
}

template <typename T>
inline shortVec<T> operator*(const shortVec<T>& v1, const shortVec<T>& v2){
    checkSize(v1, v2);
    shortVec<T> vret(v1.size());
    for(uint i=0; i<v1.size(); i++)
        vret[i] = v1[i] * v2[i];
    return vret;
}
    
template <typename T, typename Y>
inline shortVec<T>& operator*=(shortVec<T>& v1, const Y& v2){
    for(uint i=0; i<v1.size(); i++)
        v1[i] *= v2;
    return v1;
}

template <typename T>
inline shortVec<T> operator/(const shortVec<T>& v1, const shortVec<T>& v2){
    checkSize(v1, v2);
    shortVec<T> vret(v1.size());
    for(uint i=0; i<v1.size(); i++)
        vret[i] = v1[i] / v2[i];
    return vret;
}

template <typename T, typename Y>
inline shortVec<T>& operator/=(shortVec<T>& v1, const Y& v2){
    for(uint i=0; i<v1.size(); i++)
        v1[i] /= v2;
    return v1;
}

//! Coefficient-wise minimum
template <typename T>
inline shortVec<T> c_min(const shortVec<T>& v1, const shortVec<T>& v2){
    checkSize(v1, v2);
    shortVec<T> vret(v1.size());
    for(uint i=0; i<v1.size(); i++)
        vret[i] = std::min(v1[i], v2[i]);
    return vret;
}

//! Coefficient-wise minimum
template <typename T>
inline shortVec<T> c_max(const shortVec<T>& v1, const shortVec<T>& v2){
    checkSize(v1, v2);
    shortVec<T> vret(v1.size());
    for(uint i=0; i<v1.size(); i++)
        vret[i] = std::max(v1[i], v2[i]);
    return vret;
}

// Operators with a vector and a scalar
template <typename T, typename S>
inline shortVec<T> operator+(const shortVec<T>& v1, S v2){
    shortVec<T> vret(v1.size());
    for(uint i=0; i<v1.size(); i++)
        vret[i] = v1[i] + (T)v2;
    return vret;
}

template <typename T, typename S>
inline shortVec<T> operator-(const shortVec<T>& v1, S v2){
    shortVec<T> vret(v1.size());
    for(uint i=0; i<v1.size(); i++)
        vret[i] = v1[i] - (T)v2;
    return vret;
}

template <typename T, typename S>
inline shortVec<T> operator*(const shortVec<T>& v1, S v2){
    shortVec<T> vret(v1.size());
    for(uint i=0; i<v1.size(); i++)
        vret[i] = v1[i] * (T)v2;
    return vret;
}

template <typename T, typename S>
inline shortVec<T> operator/(const shortVec<T>& v1, S v2){
    vec vret(v1.size());
    for(uint i=0; i<v1.size(); i++)
        vret[i] = v1[i] / (T)v2;
    return vret;
}
    
//! Coefficient-wise minimum
template <typename T, typename S>
inline shortVec<T> c_min(const shortVec<T>& v1, S v2){
    vec vret(v1.size());
    for(uint i=0; i<v1.size(); i++)
        vret[i] = std::min(v1[i], (T)v2);
    return vret;
}
    
//! Coefficient-wise maximum
template <typename T, typename S>
inline shortVec<T> c_max(const shortVec<T>& v1, S v2){
    vec vret(v1.size());
    for(uint i=0; i<v1.size(); i++)
        vret[i] = std::max(v1[i], (T)v2);
    return vret;
}


// Operators with a scalar and a vector
template <typename T, typename S>
inline shortVec<T> operator+(S v1, const shortVec<T>& v2){
    shortVec<T> vret(v2.size());
    for(uint i=0; i<v2.size(); i++)
        vret[i] = (T)v1 + v2[i];
    return vret;
}

template <typename T, typename S>
inline shortVec<T> operator-(S v1, const shortVec<T>& v2){
    shortVec<T> vret(v2.size());
    for(uint i=0; i<v2.size(); i++)
        vret[i] = (T)v1 - v2[i];
    return vret;
}

template <typename T, typename S>
inline shortVec<T> operator*(S v1, const shortVec<T>& v2){
    shortVec<T> vret(v2.size());
    for(uint i=0; i<v2.size(); i++)
        vret[i] = (T)v1 * v2[i];
    return vret;
}

template <typename T, typename S>
inline shortVec<T> operator/(S v1, const shortVec<T>& v2){
    shortVec<T> vret(v2.size());
    for(uint i=0; i<v2.size(); i++)
        vret[i] = (T)v1 / v2[i];
    return vret;
}

//! Coefficient-wise minimum
template <typename T, typename S>
inline shortVec<T> c_min(S v1, const shortVec<T>& v2){
    shortVec<T> vret(v2.size());
    for(uint i=0; i<v2.size(); i++)
        vret[i] = std::min((T)v1, v2[i]);
    return vret;
}

//! Coefficient-wise maximum
template <typename T, typename S>
inline shortVec<T> c_max(S v1, const shortVec<T>& v2){
    shortVec<T> vret(v2.size());
    for(uint i=0; i<v2.size(); i++)
        vret[i] = std::max((T)v1, v2[i]);
    return vret;
}

/*********************************************
 *     Comparison Operators for vectors      *
 *********************************************/
template <typename T>
inline bool feq(const shortVec<T>& v1, const shortVec<T>& v2){
    if(v1.size() != v2.size())
        return false;

    for(uint i=0;  i<v1.size(); i++){
        if(!feq(v1[i], v2[i]))
            return false;
    }

    return true;
}

/**
 * Sort the first four elements of the array using a fixed number of steps (for speed)
 * @tparam T The data type
 * @tparam F The comparator Type
 * @param array The array (of size 4) to be sorted
 * @param comp The comparater used to sort
 */
template< typename T, typename F>
inline void fastSort4(std::array<T, 4>& array, F comp) {
    T low1, high1, low2, high2, lowest, middle1, middle2, highest;
    auto a = array[0];
    auto b = array[1];
    auto c = array[2];
    auto d = array[3];


    if (comp(a, b)) {
        low1 = a;
        high1 = b;
    } else {
        low1 = b;
        high1 = a;
    }

    if (comp(c, d)) {
        low2 = c;
        high2 = d;
    } else {
        low2 = d;
        high2 = c;
    }

    if (comp(low1, low2)) {
        lowest = low1;
        middle1 = low2;
    } else {
        lowest = low2;
        middle1 = low1;
    }

    if (comp(high2, high1)) {
        highest = high1;
        middle2 = high2;
    } else {
        highest = high2;
        middle2 = high1;
    }

    if (comp(middle1, middle2))
        array = {lowest, middle1, middle2, highest};
    else
        array = {lowest, middle2, middle1, highest};
}

template<typename F>
inline void fastSort4(ElementBase<4>& element, F comparator){
    fastSort4<int, F>(element.storage(), comparator);
}



}

#endif

