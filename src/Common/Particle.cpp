#include "Particle.h"
#include "Grid.h"
#include "Interpolator.h"
#include "StringUtils.h"
#include "Threads.h"

using namespace picard;
using namespace std;

size_t Species::highestIndex = 0;

SpeciesIndex picard::getSpeciesMap(const Particles& particles){
    std::map<size_t, SpeciesPtr> speciesMap;
    for(auto& entry : particles)
        speciesMap[entry.first->id] = entry.first;
    return speciesMap;
};

Real Particle::kineticEnergy() const{
    return 0.5*m()*dot(v,v);
}

Real Particle::potentialEnergy(const Grid& grid, const Interpolator& interp) const{
    Real pot = interp.scalarFieldValue(r, grid.potential, grid);
    return q()*pot;
}

Real Particle::totalEnergy(const Grid& grid, const Interpolator& interp) const {
    return kineticEnergy() + potentialEnergy(grid, interp);
}

bool Particle::operator<(const Particle& other) const {
    return (m() < other.m()) || (m() == other.m() && q() < other.q());
}

std::string Particle::name() const {
    return (_traits) ? _traits->name : "unknown";
}

SpeciesPtr Particle::species_ptr() const {
    return _traits->c_ptr();
}

size_t Particle::speciesId() const {
    return _traits->id;
}

Real Particle::m() const {
    return _traits->m;
}

Real Particle::q() const {
    return _traits->q;
}

std::ostream& picard::operator<<(std::ostream& os, const ParticleStatus& ps){
    switch(ps){
        case REMOVE_PARTICLE :
            os << "Remove Particle";
            break;
        case PARTICLE_UNCHANGED :
            os << "Particle Unchanged";
            break;
        case PARTICLE_UPDATED :
            os << "Particle Updated";
            break;
        case REAPPLY :
            os << "Reapply Interaction";
            break;
        default:
            os << "Undefined Particle Status";
    }
    return os;
}

std::ostream& picard::operator<<(std::ostream& os, const InteractionResults& ps){
    os << "Interaction Result with status \"" << ps.status << "\" and adding: " << ps.particlesToAdd.size() << " new particles";
    return os;
}

Particle Species::generate(const vec& r) const {
    return Particle(r, velocity_distribution(), this);
}

std::vector<std::string> Species::headers() {
    //TODO: write out init characteristics
    return {"id", "name", "mass", "charge"};
}

std::ostream& picard::operator<<(std::ostream& os, const Species& st) {
    // TODO write out init characteristics and reference mass/charge
    os << st.id << ", \"" << st.name << "\", " << st.m << ", " << st.q;
    return os;
}

vec Species::temperature(const vec& velocity_variance) const {
    return velocity_variance*m / reference->simKb(); // sigma^2 = kT/m
}

Real Species::reducedMass(Real m1, Real m2) {
    return m1*m2/(m1 + m2);
}

void Species::updateMassCharge() {
    if(!reference)
        throw runtime_error("Species (updateMassCharge) Error: " + name + " does not have reference parameters");
    q = q_qref*reference->simCharge();
    m = m_mref*reference->simMass();
}

std::list<Particle> PopulationGenerator::generate_spatially_uniform_particles(SpeciesPtr species, const Grid &grid,
                                                                              size_t count) {
    list<Particle> particles;
    count = getThreadLoad(count);
    for (uint i=0; i<count; i++)
        particles.push_back(species->generate(grid.randomPermittedLocation()));

    return particles;
}

std::list<Particle>
PopulationGenerator::generate_spatially_cartesian_particles(SpeciesPtr species, const Grid& grid, ivec spatSamples)
{

    // Fill all available cells with a cartesian grid of particles
    list<Particle> particles;
    auto nDim = grid.nDim();
    auto samplesPerCell = product(spatSamples);
    if (samplesPerCell < numThreads())
        throw runtime_error(
                "Options (fillAvailableCells) Error: Total spatial samples per cell is less than total threads.");

    // Loop through all of the cells
    for (int i = 0; i < grid.totalPoints(); i++) {
        auto nodePos = grid.position(i);
        // Check if this is a valid cell to populate with particles
        if (grid.isPermitted(nodePos + 0.5 * grid.spacing())) {
            for (int sample = 0; sample < samplesPerCell; sample++) {
                if (rank() == sample % numThreads()) {
                    vec r(nDim);
                    auto xsamps = ijk(sample, spatSamples, nDim);
                    for (int n = 0; n < nDim; n++)
                        r[n] = nodePos[n] + (Real(xsamps[n]) + 0.5) * grid.spacing(n) / Real(spatSamples[n]);
                    particles.push_back(species->generate(r));
                }
            }
        }
    }
    return particles;
}

std::list<Particle> PopulationGenerator::generate_quiet_start_particles(SpeciesPtr species, const Grid &grid,
                                                                        const ivec& spatSamples, const ivec& velSamples)
{

    // Check for normal distribution for quiet start (otherwise we can't do it)
    for (auto& dist : species->velocity_distribution.getDistributions())
        to_maxwellian(dist);

    // Fill all available cells with a cartesian grid of particles
    list<Particle> particles;
    auto nDim = grid.nDim();
    auto samplesPerCell = product(spatSamples);
    if (samplesPerCell < numThreads())
        throw runtime_error("Options (fillAvailableCells) Error: Total spatial samples per cell is less"
                                    "than total threads.");

    // Loop through all of the cells
    for (int i = 0; i < grid.totalPoints(); i++) {
        auto nodePos = grid.position(i);
        // Check if this is a valid cell to populate with particles
        if (grid.isPermitted(nodePos + 0.5 * grid.spacing())) {
            for (int sample = 0; sample < samplesPerCell; sample++) {
                if (rank() == sample % numThreads()) {
                    vec r(nDim);
                    auto xsamps = ijk(sample, spatSamples, nDim);
                    for (int n = 0; n < nDim; n++)
                        r[n] = nodePos[n] + (Real(xsamps[n]) + 0.5) * grid.spacing(n) / Real(spatSamples[n]);
                    fillCDFVelSamples(species, r, particles, grid, velSamples);
                }
            }
        }
    }
    return particles;
}

void PopulationGenerator::perturb_position(const Grid &grid, std::list<Particle> &particles,
                                           std::vector<multi_function> spatial_perturbations) {
    auto L = grid.size();
    for(auto& p : particles) {
        vec total_pert = vec(p.r.size(), 0);
        for (auto pert : spatial_perturbations)
            total_pert += pert(p.r / L); // perturbation normalized to grid scale
        p.r += total_pert;

        //TODO - periodic perturbation (This should be moved elsewhere for non-periodic cases
        for(int n=0; n<grid.nDim(); n++) {
            if (p.r[n] < grid.min(n))
                p.r[n] += L[n];
            if (p.r[n] > grid.max(n))
                p.r[n] -= L[n];
        }
    }
}

void PopulationGenerator::fillCDFVelSamples(SpeciesPtr species, const vec& r, std::list<Particle>& particles,
                                            const Grid& grid, const ivec& velSamples) {
    // TODO: Convert this to the new "maxwellian" distribution functionality
    auto nDim = grid.nDim();
    auto& velDist = species->velocity_distribution;

    // Fill velocity distribution characteristics
    vec mean(nDim), stddev(nDim), dspeed(nDim);
    for (int i = 0; i < nDim; i++) {
        mean[i] = velDist[i]->params()[0];
        stddev[i] = velDist[i]->params()[1];
        dspeed[i] = 0.999 / (velSamples[i] - 1.);
    }

    // Create a new particle for each desired sample
    for (int sample = 0; sample < product(velSamples); sample++) {
        vec v(nDim);
        auto vsamps = ijk(sample, velSamples, nDim);
        for (int i = 0; i < nDim; i++) {
            if (velSamples[i] != 1)
                v[i] = normal_cdf_sample(mean[i], stddev[i], dspeed[i] * vsamps[i]);
            else
                v[i] = mean[i];
        }
        particles.emplace_back(Particle(r, v, species.get()));
    }
}

std::list<Particle> PopulationGenerator::generate_gaussian_density(SpeciesPtr species, const Grid& grid, size_t count) {
    list<Particle> particles;

    auto dist = uniform_multi_distribution(grid.min(),grid.size() / 5.0);

    count = getThreadLoad(count);
    while(particles.size() < count){
        auto r = vabs(dist());
        if(grid.isInside(r))
            particles.push_back(species->generate(r));
    }

    return particles;
}


std::ostream& picard::operator<<(std::ostream& os, const Particle& p) {
    os << "Particle info: " << " M: " << p.m() << " Q: " << p.q();
    os << " r: (" << p.r << ")";
    os << " v: (" << p.v << ")";
    os << " r_last: (" << p.r_last << ")";
    return os;
}

