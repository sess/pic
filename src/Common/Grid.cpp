#include <numeric>
#include <random>
#include <chrono>
#include "Grid.h"
#include "Label.h"
#include "CastUtils.h"
#include "Distributions.h"

using namespace picard;
using namespace std;

Grid::Grid(Domain d, const ivec& numPts) : Domain(d), numGridPtsXYZ(numPts) {
    refresh();
}

void Grid::setNumPoints(ivec pts) {
    numGridPtsXYZ = pts;
    refresh();
}

void Grid::refresh() {
    Domain::refresh();

    // Correct for dimension
    numGridPtsXYZ.resize(_nDim,2);

    // Compute useful quantities
    axisRes = size() / (vcast<Real>(numGridPtsXYZ) - 1.);
    nGTotal = product(numGridPtsXYZ);
    _cellVolume = product(axisRes);
    fillNodeLocations();
    fieldLabels.resize(nGTotal);
}

void Grid::allocateGridData(int nSpecies) {
    // Initialize all vectors to be zero or NULL
    eField = vector2d<Real>::Constant(nGTotal, _nDim, 0.0);

//    potential = samples<Real>(nGTotal, std::uniform_real_distribution<Real>()); // Random init helps with 1st iterative solution
    potential = vector<Real>(nGTotal, 0.0);
    chargeDensity = vector2d<Real>::Constant(nGTotal, nSpecies, 0.0);
    numberDensity = vector2d<Real>::Constant(nGTotal, nSpecies, 0.0);
    _temperature.resize(nSpecies, vector2d<Real>::Constant(nGTotal, _nDim, 0));
    _averageVelocity.resize(nSpecies, vector2d<Real>::Constant(nGTotal, _nDim, 0));
    _averageSqVelocity.resize(nSpecies, vector2d<Real>::Constant(nGTotal, _nDim, 0));
    _velocityVariance.resize(nSpecies, vector2d<Real>::Constant(nGTotal, _nDim, 0));

    // Prepare the surface charge containers
    surfaceChargeMap.clear();
    set<int> bnodes;
    for (auto& el : particleBoundaryLabels)
        if (el.second->type == ParticleBoundaryLabel::ABSORBING || el.second->type == ParticleBoundaryLabel::SOLID)
            for (auto i : el.first)
                bnodes.insert(i);
    if (bnodes.size()) {
        surfaceChargeGlobal.assign(bnodes.size(), 0.0);
        surfaceChargeLocal.assign(bnodes.size(), 0.0);
        int index = 0;
        for (auto bnode : bnodes)
            surfaceChargeMap.insert({bnode, index++});
    }
}

int Grid::index(const ivec& ijk) const {
    int index = 0;
    int multiplier = 1;
    for (int i = 0; i < _nDim - 1; i++) {
        index += ijk[i] * multiplier;
        multiplier *= numPoints(i);
    }
    return index + ijk[_nDim - 1] * multiplier;
}

ivec Grid::ijk(const vec& gridPosition) const {
    vector<int> ijk(_nDim);
    for (int i = 0; i < _nDim; i++) {
        double dindex = (gridPosition[i] - min(i)) / spacing(i);
        double index;
        double f_part = modf(dindex, &index);
        ijk[i] = (int) index;
        if (feq(f_part, 1.0))
            ijk[i]++;
    }
    return ijk;
}

Real Grid::getSurfaceCharge(int node) const {
    auto node_it = surfaceChargeMap.find(node);
    return (node_it == surfaceChargeMap.end()) ? 0.0 : surfaceChargeGlobal[node_it->second];
}

void Grid::contributeSurfaceCharge(int node, Real charge_contribution) {
    auto node_it = surfaceChargeMap.find(node);
    if(node_it != surfaceChargeMap.end())
        surfaceChargeGlobal[node_it->second] += charge_contribution;
}

Real Grid::activeVolume() const {
    auto totalVol = 0;
    for (auto& sd : subDomains) {
        if (sd.particlesPermitted())
            totalVol += sd.volume();
    }
    return totalVol;
}

bool Grid::isPermitted(const vec& r) const {
    if (isOutside(r))
        return false;

    for (auto& d : subDomains)
        if (!d.particlesPermitted() && !d.isOutside(r))
            return false;

    return true;
}

GridIntersection Grid::intersectsBoundary(const std::vector<Real>& ray) const {
    // Loop through each subdomain and see if that ray intersects any boundaries
    for (uint d = 0; d < subDomains.size(); d++) {
        auto& domain = subDomains[d];

        GridIntersection intersection = domain.intersects(ray);
        if (intersection) {
            intersection.domainIndex = d;
            int nodeIndex = index(intersection.point);
            auto element = getBoundaryElement(nodeIndex, intersection.dim);
            intersection.label = particleBoundaryLabels.at(element);
            return intersection;
        }
    }
    return Intersection();
}

ivec Grid::getAdjacentNode(const ivec& n1, const ivec& disp) const {
    ivec n2(_nDim);
    for(int i=0; i<_nDim; i++){
        auto n = n1[i] + disp[i];
        if(n < 0 || n >= numPoints(i))
            n = n1[i] - disp[i];
        n2[i] = n;
    }
    return n2;
}

Element Grid::getBoundaryElement(int nodeIndex, int dim) const {
    Element e(_nDim - 1);
    auto loc = ijk(nodeIndex);

    auto lowerCorner = ijk(nodeIndex);
    int in = 0;
    for (uint i = 0; i < std::pow(2,_nDim); i++) {
        auto disp = displacement(i);
        if (disp[dim] == 0)
            e[in++] = index(getAdjacentNode(lowerCorner, disp));
    }
    fastSort4(e, std::greater<int>());
    return e;
}

Domain Grid::makeDomain(const Element& e) const{
    return Domain(_nDim, position(e.back()), position(e.front()));
}

Cell Grid::getEnclosingCell(const vec& position) const {
    if(isOutside(position))
        return {};

    Cell c(_nDim);

    auto lowerCorner = ijk(position);
    for(uint i = 0; i<c.size(); i++){
        c[i] = index(getAdjacentNode(lowerCorner, displacement(i)));
    }

    return c;
}


std::set<Element> Grid::boundaryElements() const {
    set<Element> elements;

    for (int nodeIndex = 0; nodeIndex < totalPoints(); nodeIndex++) {
        auto id_list = boundaryIds(nodeIndex);
        for (auto id : id_list)
            elements.insert(getBoundaryElement(nodeIndex, id / 2));
    }

    return elements;
}

void Grid::fillNodeLocations() {
    nodePositions.resize(totalPoints(), nDim());
    for (auto n = 0; n < totalPoints(); n++) {
        auto loc = ijk(n);
        for (auto i = 0; i < nDim(); i++) {
            nodePositions(n, i) = min(i) + spacing(i) * loc[i];
        }
    }
}

vector<BoundaryId> Grid::boundaryIds(int nodeIndex) const {
    vector<BoundaryId> ids;
    if (nodeIndex < 0 || nodeIndex >= totalPoints())
        return ids;

    auto loc = ijk(nodeIndex);
    for (int i = 0; i < 2 * _nDim; i++) {
        int dim = i / 2;
        int boundaryLoc = (i % 2 == 0) ? 0 : numPoints(i / 2) - 1;
        if (loc[dim] == boundaryLoc)
            ids.push_back((BoundaryId)i);
    }
    return ids;
}

BoundaryId Grid::boundaryId(Element element) const {
    for (int i = 0; i < 2 * _nDim; i++) {
        int dim = i / 2;
        int boundaryLoc = (i % 2 == 0) ? 0 : numPoints(dim) - 1;
        bool all_passed = true;
        for (auto n : element) {
            if (n >= 0) {
                auto loc = ijk(n);
                if (loc[dim] != boundaryLoc) {
                    all_passed = false;
                    break;
                }
            }
        }
        if (all_passed)
            return (BoundaryId)i;
    }
    return NONE; // Could not find an associated boundary
}


vec Grid::randomPermittedLocation() const {
    std::map<Real, int> cdf;
    auto vol = activeVolume();
    Real total = 0;
    for (uint i = 0; i < subDomains.size(); i++) {
        auto& sd = subDomains[i];
        if (sd.particlesPermitted()) {
            total += sd.volume() / vol;
            cdf.insert(make_pair(total, i));
        }
    }

    for (auto& entry : cdf) {
        if (uniform() < entry.first)
            return subDomains[entry.second].randomPositionInside();
    }
    throw runtime_error("Grid (getRandomPermittedLocation) Error : Didn't find valid location for random pos");
}

void NeutralizingGrid::allocateGridData(int nSpecies) {
    Grid::allocateGridData(nSpecies);
    neutralizingBackground.assign(nGTotal, 0.0);
}

Real NeutralizingGrid::getChargeDensity(int iNode) const {
    return Grid::getChargeDensity(iNode) + neutralizingBackground[iNode];
}

void NeutralizingGrid::setupChargeDensity() {
    for (uint i = 0; i < neutralizingBackground.size(); i++)
        neutralizingBackground[i] = -1.0 * chargeDensity.row(i).sum();
}

std::shared_ptr<NeutralizingGrid> NeutralizingGrid::Periodic(const Domain& d, const ivec& numPoints) {
    return make_shared<NeutralizingGrid>(*Grid::Periodic(d, numPoints));
}

std::shared_ptr<NeutralizingGrid> NeutralizingGrid::SingleDomain(const Domain& d, const ivec& numPoints,
                                                 std::map<int, std::shared_ptr<FieldLabel>> fieldLabels,
                                                 std::map<int, std::shared_ptr<ParticleBoundaryLabel>> particleLabels) {
    return make_shared<NeutralizingGrid>(*Grid::SingleDomain(d, numPoints, fieldLabels, particleLabels));
}

std::ostream& picard::operator<<(std::ostream& os, const Grid& g) {
    os << (Domain&) g << " pts: (" << g.numPoints() << ")";
    return os;
}

void Grid::reorderLabelPriority(void) {
    int Ngx = numGridPtsXYZ[0];

    if (_nDim == 1) { //1-D grid, only alter priority of first and last points
        fieldLabels[0]->priority = fieldLabels[1]->priority - 1;
        fieldLabels[Ngx - 1]->priority = fieldLabels[Ngx - 2]->priority - 1;
    }

    if (_nDim == 2) { //2-D grid, skip the corner points

        int Ngy = numGridPtsXYZ[1];

        for (int iNode = 1; iNode < Ngx - 1; iNode++) {
            fieldLabels[iNode]->priority = fieldLabels[iNode + Ngx]->priority - 1; //bottom
            fieldLabels[iNode + Ngx * (Ngy - 1)]->priority =
                    fieldLabels[iNode + Ngx * (Ngy - 1) - Ngx]->priority - 1; //top
        }

        for (int iNode = 1; iNode < Ngy - 1; iNode++) {
            fieldLabels[Ngx * iNode]->priority = fieldLabels[1 + Ngx * iNode]->priority - 1; //left
            fieldLabels[Ngx - 1 + Ngx * iNode]->priority = fieldLabels[Ngx - 2 + Ngx * iNode]->priority - 1; //right
        }

    }

    if (_nDim == 3) { //3-D grid

        int Ngy = numGridPtsXYZ[1];
        int Ngz = numGridPtsXYZ[2];

        for (int i = 1; i < Ngx - 1; i++) //XY planes
            for (int j = 1; j < Ngy - 1; j++) {
                int nodeIndex = i + Ngx * j;

                fieldLabels[nodeIndex]->priority = fieldLabels[nodeIndex + Ngx * Ngy]->priority - 1; //z=zmin plane
                fieldLabels[nodeIndex + Ngx * Ngy * (Ngz - 1)]->priority =
                        fieldLabels[nodeIndex + Ngx * Ngy * (Ngz - 1) - Ngx * Ngy]->priority - 1;//z=zmax plane

            }
        for (int j = 1; j < Ngy - 1; j++) //YZ planes
            for (int k = 1; k < Ngz - 1; k++) {
                int nodeIndex = Ngx * j + Ngx * Ngy * k;

                fieldLabels[nodeIndex]->priority = fieldLabels[nodeIndex + 1]->priority - 1;//x=xmin plane
                fieldLabels[nodeIndex + Ngx - 1]->priority =
                        fieldLabels[nodeIndex + Ngx - 2]->priority - 1;//x=xmax plane

            }
        for (int i = 1; i < Ngx - 1; i++) //XZ planes
            for (int k = 1; k < Ngz - 1; k++) {
                int nodeIndex = i + Ngx * Ngy * k;

                fieldLabels[nodeIndex]->priority = fieldLabels[nodeIndex + Ngx]->priority - 1; //y=ymin plane
                fieldLabels[nodeIndex + Ngx * (Ngy - 1)]->priority =
                        fieldLabels[nodeIndex + Ngx * (Ngy - 1) - Ngx]->priority - 1; //y=ymax plane

            }
    }
}

std::shared_ptr<Grid> Grid::Periodic(const Domain& d, const ivec& numPoints) {
    std::map<int, shared_ptr<FieldLabel>> fieldLabels;
    std::map<int, shared_ptr<ParticleBoundaryLabel>> particleLabels;

    int priority = 0;
    fieldLabels[6] = makeInterior(to_string(6), priority++, 1, true);
    for (int i = 0; i < d.nDim() * 2; i++) {
        BoundaryId b_id = (BoundaryId)i;
        fieldLabels[i] = makePeriodicField(toString(b_id), priority, d.normal(b_id));
        particleLabels[i] = makePeriodicParticle(toString(b_id), priority++, d.normal(b_id));
    }

    return SingleDomain(d, numPoints, fieldLabels, particleLabels);
}

std::shared_ptr<Grid> Grid::SingleDomain(const Domain& d, const ivec& numPoints,
                                         std::map<int, std::shared_ptr<FieldLabel>> fieldLabels,
                                         std::map<int, std::shared_ptr<ParticleBoundaryLabel>> particleLabels) {
    auto grid = make_shared<Grid>(d, numPoints);
    grid->subDomains.push_back(d);
    grid->fillNodeLocations();

    if(fieldLabels.size()) {
        for (int n = 0; n < grid->totalPoints(); n++) {
            // Get all of the ids of this node and then assign the field label that has the highest
            auto ids = grid->boundaryIds(n);
            ids.push_back(INTERIOR);
            auto label = fieldLabels.at(ids[0]);
            for (auto id : ids) {
                try{
                    auto candidate = fieldLabels.at(id);
                    if (candidate->priority > label->priority)
                        label = candidate;
                } catch (...){
                    throw std::runtime_error("Grid (SingleDomain) Error: Couldn't find field label for id: " + toString(id));
                }
            }
            grid->fieldLabels[n] = label;
        }
        grid->addDirichletLabelIfNeeded();
    }

    if(particleLabels.size()) {
        for (auto& e : grid->boundaryElements()) {
            auto id = grid->boundaryId(e);
            try{
                grid->particleBoundaryLabels[e] = particleLabels.at(id);
            } catch (...){
                throw std::runtime_error("Grid (SingleDomain) Error: Couldn't find particle BC label for id: " + toString(id));
            }
        }
    }

    return grid;
}

void Grid::zeroOutFields() {
    chargeDensity *= 0;
    numberDensity *= 0;

    for(auto& species : _averageVelocity)
        species*=0;

    for(auto& species : _averageSqVelocity)
        species*=0;

    for(auto& species : _velocityVariance)
        species*=0;
}

void Grid::addDirichletLabelIfNeeded() {
    if(fieldLabels.empty())
        return;

    for(auto& entry : fieldLabels){
        if(entry->type == FieldLabel::DIRICHLET || entry->type == FieldLabel::TIME_DEPENDENT_DIRICHLET)
            return;
    }
    fieldLabels[0] = makeDirichlet("reference", 0, vec(_nDim, 0), 0);
}


std::ostream& picard::operator<<(std::ostream& os, const GridIntersection& i){
    os << "Grid Intersection on \"" << i.label->name << "\" at (" << i.point << "). Type: " << i.label->type
       << ". Domain: " << i.domainIndex << " at boundary: " << toString(i.id) << " with count: " << i.count;
    return os;
}
