#ifndef SESS_PIC_CAST_UTILS_H
#define SESS_PIC_CAST_UTILS_H

#include "Definitions.h"
#include <iomanip>
#include <iostream>
#include <memory>

namespace picard {

// Convert a pointer to a reference but throw an exception if the pointer is a nullptr
template <typename T>
T& ptr_to_ref(std::shared_ptr<T> ptr, std::string ptr_name = "pointer"){
    if(!ptr)
        throw std::runtime_error("Error!" + ptr_name + " was empty");
    return *ptr;
}

//! Convert a pointer to a const reference but throw an exception if the pointer is a nullptr
template <typename T>
const T& ptr_to_cref(std::shared_ptr<T> ptr, std::string ptr_name = "pointer"){
    return ptr_to_ref<const T>(ptr, ptr_name);
}

/**
 * To string using the stream function
 * @tparam T The type to be converted to a string
 * @param t The object to be converted to a string
 * @return A String that comes from streaming the object to a stringstream
 */
template <typename T>
std::string tsfs(const T& t){
    std::stringstream s;
    s << t;
    return s.str();
}

// Catch all cast that performs a static cast
template<typename T, typename Y, typename ... Args>
struct cast_impl {
    static T cast(const Y& val) {
        return static_cast<T>(val);
    }
};

// generic rule for casting to string us tsfs
template<typename T, typename...Args>
typename std::enable_if<!std::is_integral<T>::value && !std::is_floating_point<T>::value, std::string>::type
cast_to_string(const T& t, Args...args){
    return tsfs(t);
}

// rule for casting integer type to string
template<typename T, typename... Args>
typename std::enable_if<std::is_integral<T>::value, std::string>::type cast_to_string(const T& t, Args...args){
    return std::to_string(t);
};

// rule for casting floating point type to string
template<typename T>
typename std::enable_if<std::is_floating_point<T>::value, std::string>::type cast_to_string(
        const T& t, int precision = std::numeric_limits<T>::digits10 + 10){
    std::stringstream s;
    s << std::fixed << std::setprecision(precision) << std::scientific << t;
    return s.str();
};

// rule for casting string to integer type
template<typename T>
typename std::enable_if<std::is_integral<T>::value, T>::type cast_from_string(const std::string& val){
    return static_cast<T>(std::stoi(val));
};

// rule for casting string to floating point type
template<typename T>
typename std::enable_if<std::is_floating_point<T>::value, T>::type cast_from_string(const std::string& val){
    return static_cast<T>(std::stod(val));
};

// Cast from string to another type
template<typename T>
struct cast_impl<T, std::string> {
    static T cast(const std::string& val) {
        return cast_from_string<T>(val);
    }
};

// Cast from string to bool
template <>
struct cast_impl<bool, std::string> {
    static bool cast(const std::string& val) {
        bool b;
        std::string s(val);
        std::transform(s.begin(), s.end(), s.begin(), ::tolower);
        std::istringstream(s) >> std::boolalpha >> b;
        return b;
    }
};

// Cast from a type to a string
template<typename T, typename ...Args>
struct cast_impl<std::string, T, Args...> {
    static std::string cast(const T& val, Args...args) {
        return cast_to_string(val, args...);
    }
};

// Disambiguation for the string/string cast
template <>
struct cast_impl<std::string, std::string> {
    static std::string cast(const std::string& val) {
        return val;
    }
};

// Vector to Vector conversion
template<typename T, typename Y, template<class, class...> class VEC, class... VEC_IN_ARGS, class... VEC_OUT_ARGS, typename ...Args>
struct cast_impl<VEC<T, VEC_OUT_ARGS...>, VEC<Y, VEC_IN_ARGS...>, Args...> {
    static VEC<T, VEC_OUT_ARGS...> cast(const VEC<Y, VEC_IN_ARGS...>& v, Args...args) {
        VEC<T, VEC_OUT_ARGS...> vret(v.size());
        for(uint i=0; i<v.size(); i++){
            vret[i] = cast_impl<T, Y, Args...>::cast(v[i], args...);
        }
        return vret;
    }
};

// Convert scalar to vector type
template<typename T, typename Y, template<class, class...> class VEC, class... VEC_ARGS, typename ...Args>
struct cast_impl<VEC<T, VEC_ARGS...>, Y, Args...> {
    static VEC<T, VEC_ARGS...> cast(const Y& v, Args...args) {
        VEC<T, VEC_ARGS...> vec = {cast_impl<T,Y, Args...>::cast(v, args...)};
        return vec;
    }
};

// Catch-All cast function
template<typename T, typename Y, typename ...Args>
T cast(const Y& v, Args...args) {
    return cast_impl<T, Y, Args...>::cast(v, args...);
}

// Casting of vectors using only the internal type
template <typename T, typename Y, template<class, class...> class VEC, class... VEC_ARGS, typename...Args>
VEC<T> vcast(const VEC<Y, VEC_ARGS...>& vec, Args...args){
    VEC<T> ret_vec(vec.size());
    for(uint i=0; i< vec.size(); i++){
        ret_vec[i] = cast_impl<T, Y, Args...>::cast(vec[i], args...);
    }
    return ret_vec;
};


}

#endif /* CastUtils_h */
