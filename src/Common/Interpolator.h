#ifndef SESS_PIC_INTERPOLATOR_H
#define SESS_PIC_INTERPOLATOR_H

#include "Definitions.h"
#include "VectorUtils.h"

namespace picard {

class ParticleMover;
class Particle;
class Grid;
class Label;

class Interpolator {
public:
    
    /**
     * Interpolates the charge density of the particle to the grid
     * @param p The particle whose charge is to be interpolated onto the grid
     * @param iSpecies The particle species index to know where which entry to add the charge density to
     * @param grid The grid for the particle to be interpolated onto
     * @param numParticlesPerNode A list of the total weights of particles contributing to each grid node. If not null, the list is updated with this particle
     */
    void particleToGrid(Particle& p, int iSpecies, Grid& grid, vector2d<Real>* sumOfWeights = nullptr) const;


    /**
     * Interpolates the electric field (and magnetic field if specified) onto a particle
     * @param The particle who will have the fields interpolated onto it
     * @param The grid which contains the electric and magnetic fields to be interpolated from
     * @param mover The ParticleMover which has electric and magnetic field containers for the current particle
     * @param bFieldFlag Whether or not to apply the magnetic field to the particle
     **/
    void gridToParticle(Particle& p, const Grid& grid, ParticleMover& mover, bool bFieldFlag) const;
    
    /**
     * Interpolates a scalar field onto the point specified. The field should be one-to-one mapped to the grid points such that the ith entry in the field corredsponds to the ith grid node.
     * @param position The position of the point to get the interpolated field at
     * @param field The field vector that is one-to-one mapped to the grid points
     * @param grid The grid to use for the interpolation
     */
    Real scalarFieldValue(const vec& position, const std::vector<Real>& field, const Grid& grid) const;

    /**
     * Weighting function for cartesian cell interpolation
     * @param nodePosition The position of the node in the cell
     * @param particlePosition The position of the particle (or interpolation point)
     * @param spacing The spacing in each dimension of the grid cell
     * @return A weighting value from 0-1
     */
    double W(const vec& nodePosition, const vec& particlePosition, const vec& spacing) const;
};
}


#endif //SESS_PIC_INTERPOLATOR_H
