#ifndef ELEMENT_H
#define ELEMENT_H

namespace picard{

/**
 * The class for storing elements (groups of indices) with a fixed-size stack storage
 * @tparam N The maximum number of entries in the element
 */
template <size_t N>
class ElementBase {
public:
    ElementBase(){
        for (uint index = 0; index < N; index++)
            _storage[index] = -1;
    }
    /**
     * Create an element of the specified dimension
     * @param dimension
     */
    ElementBase(size_t dimension) : _size(std::pow(2, dimension)) {
        for (uint index = 0; index < N; index++)
            _storage[index] = -1;
    }
    ElementBase(std::initializer_list<int> il) {
        unsigned int index = 0;
        for (auto i : il)
            _storage[index++] = i;
        for (; index < N; index++)
            _storage[index] = -1;
        _size = (short unsigned int) il.size();
    }

    int& operator[](size_t index) { return _storage[index]; }
    const int& operator[](size_t index) const { return _storage[index]; }

    int& front() { return _storage[0]; }
    const int& front() const { return _storage[0]; }
    int& back() { return _storage[_size - 1]; }
    const int& back() const { return _storage[_size - 1]; }

    bool operator<(const ElementBase<N>& other) const { return _storage < other._storage; }
    bool operator==(const ElementBase<N>& other) const { return _storage == other._storage; }

    typename std::array<int, N>::iterator begin() { return _storage.begin(); };
    typename std::array<int, N>::const_iterator cbegin() const { return _storage.cbegin(); };
    typename std::array<int, N>::const_iterator begin() const { return _storage.begin(); };
    typename std::array<int, N>::iterator end() { return _storage.begin() + _size; };
    typename std::array<int, N>::const_iterator cend() const { return _storage.cbegin() + _size; };
    typename std::array<int, N>::const_iterator end() const { return _storage.begin() + _size; };

    uint size() const { return _size; } //!< The number of indices in the element
    void setSize(short unsigned int sz){ _size = sz; } //!< Set the size of the element
    uint dimension() const { return (uint) log2(_size); } //!< The dimension of the element (0-3)
    std::array<int, N>& storage() { return _storage; };

private:
    std::array<int, N> _storage; //!< The stack-allocated storage of the element
    short unsigned int _size{0}; //!< The size of the used spots in storage
};

typedef ElementBase<4> Element; //!< Element on a boundary
typedef ElementBase<8> Cell; //! Element in the interior

template <size_t numEntries>
inline std::ostream& operator<<(std::ostream& os, const ElementBase<numEntries>& e){
    os << "[";
    for(uint i=0; i<numEntries-1; i++)
        os << e[i] << ", ";
    os << e[numEntries-1] << "]";
    return os;
}

}


#endif