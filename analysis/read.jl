include("parsing.jl")

# Read a subdirectory and store file path and file name
function addFilesFromSubDir(dir, subDir, files)
    contents = readdir(string(dir, subDir))
    for f in contents
        push!(files, FileStruct(f, string(subDir, f)))
    end
end

# Store the original directory so we can navigate back to it at the end
orig_dir = pwd()

# Read inputs (First argument is output directory and the second is the simulation timestep)
output_directory = ARGS[1]
dt = parse(Float64, ARGS[2])


# Setup data structures to store all of the data
plotData = Dict{String, Array{Float64,2}}() # Data for a single plot
plotData_Headers = Dict{String, Array{String,1}}() # headers for the data plot
species  = Dict{Int, Species}() # Species
scalings = Dict{String, Scale}() # Scaling parameters
vid_names = Set{String}() # List of names of scaling parameters
grid = Grid()

println("Reading files...")

# Parse the files and store the data
files = Array{FileStruct,1}(0)
addFilesFromSubDir(output_directory, "info/", files)
addFilesFromSubDir(output_directory, "data/", files)

cd(output_directory)
try mkdir("figures") end
for file in files
    # skip over non-data-containing files
    if filesize(file.path) == 0 || !isCSV(file.name) || file.name == "profile_results.csv"
        println(string("skipping: ", file.path))
        continue
    end



    # Parse out all needed information
    if file.name == "Species_Traits.csv"
        species = parseSpecies(file.path)
    elseif file.name == "Scaling.csv"
        if useScale
             scalings = parseScaling(file.path)
        end
    elseif file.name == "default_scaling.csv"
        if !useScale
            scalings = parseScaling(file.path)
        end
    elseif file.name == "grid.csv"
        grid = parseGrid(file.path)
    elseif isTimeSeries(file.path)
        name, time = parseTimeSeries(file.name, dt)
        push!(vid_names, name)
    else
        println(string("parsing ", file.name))
        data, headers = readdlm(file.path, ',', header=true)
        name = parseName(file.name)
        plotData[name] = data
        plotData_Headers[name] = vec(headers)
    end
end

cd(orig_dir)

