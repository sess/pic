
scale_aliases = Dict{String,String}(
"Velocity Average"=>"Velocity",
"Velocity Variance"=>"Velocity Sq",
"Position" => "Length",
"Species Temperature" => "Temperature",
"Species Velocity" => "Velocity",
"Temp" => "Temperature"
)

function getScale(scalings, name)
    if haskey(scalings, name)
        return scalings[name]
    elseif haskey(scalings, get(scale_aliases, name, name))
        return scalings[get(scale_aliases, name, name)]
    elseif contains(lowercase(name), "frequency")
        return scalings["Frequency"]
    else
        return Scale(1.0, "")
    end
end
