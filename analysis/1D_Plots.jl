# TODO Eventually support the log scaling option

function make1DPlot(dataset_name, data, grid, scale, xscale ,tscale)
    xmin, xmax = grid.min[1]*xscale.value, grid.max[1]*xscale.value;
    x = linspace(xmin, xmax, grid.pts[1])
    ymin, ymax = minMax(data, scale)

    anim = Animation()
    plots = Array{Any,1}()
    p_init = plot();
    rows, cols, indices = getSubplotMachinery(data);
    index = 1;
    for key in sort(collect(keys(data)))
        t= key*tscale.value
        p = plot();
        labels = keys(data[key].series)
        for s in labels
            plot!(p, x, data[key].series[s]*scale.value, label=s, size=frame_size)
        end
        plot!(p, yformatter = formatter(ymax), xformatter = formatter(xmax))
        title!(p, string(dataset_name, ", t= ", @sprintf("%.2e", t)))
        xlabel!(p, unitStr("X", xscale))
        ylabel!(p, unitStr(dataset_name, scale))
        ylims!(p, (ymin, ymax))

        # Save the initial state of the plot before min/max scaling
        if isempty(plots) saveFig(p, string(dataset_name, "_initial")) end

        frame(anim, p)
        if index in indices
            push!(plots, p)
        end
        index = index + 1;
    end
    subplots = plot(plots..., layout = (rows,cols), size=(frame_width*cols, frame_height*rows))
    saveFig(subplots, string(dataset_name, "_grid"))
    saveGif(anim, string(dataset_name, "_animation"),  FPS)
end

function make1DHistogram(dataset_name, data, grid, scale, tscale)
    ymin, ymax = minMax(data, scale)

    anim = Animation()
    plots = Array{Any,1}()
    rows, cols, indices = getSubplotMachinery(data);
    index = 1;
    for key in sort(collect(keys(data)))
        t = key*tscale.value;
        println(string("plotting ", dataset_name, " @ t=",t))
        p = plot();
        labels = keys(data[key].series)
        for s in labels
            d = data[key].series[s]*scale.value
            histogram!(p, d, normed=true, label=string(s, "  ", L"  \sigma^2=", var(d)), fillalpha=0.4, size = frame_size)
        end
        plot!(p, xformatter = formatter(ymax))
        title!(p, string(dataset_name, ", t= ", @sprintf("%.2e", t)))
        xlabel!(p, unitStr(dataset_name, scale))
        ylabel!(p, "Count")
        # Save the initial state of the plot before min/max scaling
        if isempty(plots) saveFig(p, string(dataset_name, "_initial")); end
        xlims!((ymin, ymax))

        frame(anim)
        if index in indices
            push!(plots, p)
        end
        index = index + 1;
    end
    subplots = plot(plots..., layout = (rows,cols), size=(frame_width*cols, frame_height*rows))
    saveFig(subplots, string(dataset_name, "_grid"))
    saveGif(anim, string(dataset_name, "_animation"),  FPS)
end

function makePhaseSpacePlot(positionFrames, velocityFrames, grid, xscale, vscale, tscale)
    ymin, ymax = minMax(velocityFrames, vscale)
    xmin, xmax = (grid.min[1]*xscale.value, grid.max[1]*xscale.value)

    anim = Animation()
    plots = Array{Any,1}()
    rows, cols, indices = getSubplotMachinery(positionFrames);
    index = 1;
    for key in sort(collect(keys(positionFrames)))
        t = key*tscale.value;
        p = plot();
        labels = keys(positionFrames[key].series)
        for s in labels
            scatter!(p, positionFrames[key].series[s]*xscale.value, velocityFrames[key].series[s]*vscale.value,label=s, size = frame_size)
        end
        plot!(p, yformatter = formatter(ymax), xformatter = formatter(xmax))
        title!(p, string("Phase Space (Velocity vs. Position)", ", t= ", @sprintf("%.2e", t)))
        xlabel!(p, unitStr("Position", xscale))
        ylabel!(p, unitStr("Velocity", vscale))
        xlims!(p, (xmin, xmax))

        # Save the initial state of the plot before min/max scaling
        if isempty(plots) saveFig(p, "phase_space_initial"); end
        ylims!((ymin, ymax))

        frame(anim)
        if index in indices
            push!(plots, p)
        end
        index = index + 1;
    end
    subplots = plot(plots..., layout = (rows,cols), size=(frame_width*cols, frame_height*rows))
    saveFig(subplots, "phase_space_grid")
    saveGif(anim, "phase_space_animation",  FPS)
end
