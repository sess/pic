using LaTeXStrings
using Plots
using Plots.PlotMeasures
pyplot()

function formatter(maxval)
    if maxval < 0.01 || maxval > 1000
        return x -> @sprintf("%.2e", x)
    else
        return x -> @sprintf("%.2f", x)
    end
end

function saveGif(anim, name, FPS)
    name = replace(name, " ", "_");
    print(string("Saving gif: ", name,"..."))
    cd("figures")
    gif(anim, string(name, ".gif"), fps = FPS)
    cd("..")
    println("done!")
end

function saveFig(p, dataset_name)
    dataset_name = replace(dataset_name, " ", "_");
    print(string("Saving figure: ", dataset_name,"..."))
    cd("figures")
    savefig(p, dataset_name)
    cd("..")
    println("done!")
end

function getSubplotMachinery(data)
    numFrames = length(keys(data));
    if numFrames == 1
        return 1,1,1:1:1
    end
    numPlots = min(12, numFrames)
    rows::Int64  = 2
    cols::Int64 = floor(numPlots/rows)
    stride::Int = floor(numFrames / numPlots);
    if length(range(1,stride,numPlots)) > rows*cols
        numPlots -= 1
    end
    return rows, cols, range(1, stride, numPlots)
end

function minMax(data, scale)
    ymin_g = Inf
    ymax_g = -Inf

    for key in sort(collect(keys(data)))
        for s in collect(keys(data[key].series))
            ymin, ymax = extrema(data[key].series[s])
            ymin_g = min(ymin, ymin_g);
            ymax_g = max(ymax, ymax_g);
        end
    end
    return ymin_g*scale.value, ymax_g*scale.value
end

function minMax(data, scale, series, uselog = false, ref=nothing)
    ymin_g = Inf
    ymax_g = -Inf

    for key in sort(collect(keys(data)))
        ymin, ymax = Inf, -Inf;
        if uselog
            sz = size(data[key].series[series]);
            ymin, ymax = extrema(log10.(1.0 + data[key].series[series]./reshape(ref,sz)))
        else
            ymin, ymax = extrema(data[key].series[series])
        end
        ymin_g = min(ymin, ymin_g);
        ymax_g = max(ymax, ymax_g);
    end
    return ymin_g*scale.value, ymax_g*scale.value
end

include("1D_Plots.jl")
include("2D_Plots.jl")
include("3D_Plots.jl")

function makeVideoAndFrames(name, frames, grid, scale, xscale, tscale)
    if grid.nDim == 1
        if name in ["Position" "Velocity" "Energy"]
            make1DHistogram(name, frames, grid, scale, tscale)
        else
            make1DPlot(name, frames, grid, scale, xscale, tscale)
        end
    end

    if grid.nDim == 2
        println(string("making video for ", name))
        data_dim = size(first(values(first(frames).second.series)), 2);
        if name in ["Position" "Velocity"]
            println(string("don't handle particle quantities yet. skipping ", name))
        elseif name in ["Energy"]
            make1DHistogram(name, frames, grid, scale, tscale)
        elseif data_dim == 2 # Vector field
            println(string("don't handle  vector quantities yet. skipping ", name))
        elseif data_dim == 1 # Scalar field
            make2DContourPlot(name, frames, grid, scale, xscale, tscale)
        else
            println(string("Don't handle data of ", data_dim, " dims. skipping: ", name))
        end
    end
end

function makePlot(name, data, headers, scale, tscale)
    println(string("making plot for: ", name))
    p = plot(
            title=string(name, " vs. Time"),
            xlabel = unitStr("Time",tscale),
            ylabel = unitStr(name,scale))
    ymin, ymax = extrema(data[:,2:end])

    if ymax == ymin
        ymin = 0;
    end

    for i = 2:size(data,2)
        plot!(p, data[:, 1], data[:, i], label = headers[i])
        ylims!(p, (ymin*0.9, ymax*1.1))
    end
    plot!(p, yformatter = formatter(ymax), xformatter = formatter(maximum(data[:, 1])))
    saveFig(p, name)
end


