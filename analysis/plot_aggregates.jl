include("plotting.jl")

cd(output_directory)

println("Making single plots...")
# Loop through data to make plots
for key in keys(plotData)
    makePlot(key, plotData[key], plotData_Headers[key], getScale(scalings, key), getScale(scalings, "Time"))
end

cd(orig_dir)


