% MATLAB script used for generating time changing plasma source activities
% for the PIC.
% Written by Glenn Sugar on 5/23/2016


% define the activity via an anonymous function
nRealToNSim = 1e4;
beta  = 1; % number of ionizations per collision
vel = 60000; % meteoroid velocity (m/s)
velDrift = [0 -abs(vel)]; % the speed of the meteoroid (m/s)
velTherm = [244 244];  % the thermal velocity of the neutrals
T = 1e-5;
dt = 1e-9; % the time step of the simulation (sec)
%sigma = 0.000017742; % the meteoroid cross section (m^2)
sigma = 1.21e-8; % the meteoroid cross section (m^2)
filenameSources = '../plasma_sources/test.txt';
% Make the neutral collisions
filename = '../neutral_densities/test.txt';
makeNeutralCollisionFile

sourceActivity = neutralDensities*sigma*vel*dt*beta/nRealToNSim;

if exist(filenameSources, 'file') == 2
    disp(['Note that the file ', filenameSources, ' already exists!'])
    userInput = input('Do you want to overwrite? 1: yes, 0: no \n');
else
    userInput = 1;
end

if userInput == 1
    writeArray = sourceActivity';
    disp(['Writing the plasma source activity file in ', filenameSources])
    dlmwrite(filenameSources, writeArray, 'delimiter', ' ')
    disp('Done!')
else
    disp('The file was not overwritten, please use a different filename.')
end