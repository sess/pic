directory = '../../../meteor_1D/8624087/';
dt = 1e-13;
filenames_species0 = dir([directory 'rho_species_0_*.dat']);
filenames_species1 = dir([directory 'rho_species_1_*.dat']);
plot_movie = 1;
%filenames = dir('../test_output3/potential*.dat');

if plot_movie > 0
    for i = 1:numel(filenames_species0)
        filename_species0 = [directory filenames_species0(i).name];
        filename_species1 = [directory filenames_species1(i).name];
        underscoreIndex = find(filename_species0 == '_');
        underscoreIndex = underscoreIndex(end);
        lastDotIndex = find(filename_species0 == '.');
        lastDotIndex = lastDotIndex(end);
        filename = filename_species0;
        disp(['Reading in ', filename])
        [x0,n0] = readChargeDensity1D(filename);
        disp('Done reading the file.')
        underscoreIndex = find(filename_species0 == '_');
        underscoreIndex = underscoreIndex(end);
        lastDotIndex = find(filename_species0 == '.');
        lastDotIndex = lastDotIndex(end);

        underscoreIndex = find(filename_species1 == '_');
        underscoreIndex = underscoreIndex(end);
        lastDotIndex = find(filename_species1 == '.');
        lastDotIndex = lastDotIndex(end);
        filename = filename_species1;
        disp(['Reading in ', filename])
        [x1,n1] = readChargeDensity1D(filename);
        disp('Done reading the file.')
        underscoreIndex = find(filename_species1 == '_');
        underscoreIndex = underscoreIndex(end);
        lastDotIndex = find(filename_species1 == '.');
        lastDotIndex = lastDotIndex(end);

        n = n1+n0;

        %plot(x0,n)
        x0 = x0-0.1;
        plot(x0,n)
        xlim([-0.01 0.1])
        ylim([-2e-10 2e-10])
        xlabel('Distance from Meteoroid (m)')
        ylabel('Charge Density (c/m^3)')
        title(['Charge Density Time: ' num2str(dt*str2num(filename_species0(underscoreIndex+1:lastDotIndex-1))) ' sec'])
        pause(0.01)
        M(i) = getframe(gcf);
    end
    movie2avi(M, [directory 'Movie_ChargeDensity.avi'])

else
    i = numel(filenames_species0);
            filename_species0 = [directory filenames_species0(i).name];
        filename_species1 = [directory filenames_species1(i).name];
        underscoreIndex = find(filename_species0 == '_');
        underscoreIndex = underscoreIndex(end);
        lastDotIndex = find(filename_species0 == '.');
        lastDotIndex = lastDotIndex(end);
        filename = filename_species0;
        disp(['Reading in ', filename])
        [x0,n0] = readChargeDensity1D(filename);
        disp('Done reading the file.')
        underscoreIndex = find(filename_species0 == '_');
        underscoreIndex = underscoreIndex(end);
        lastDotIndex = find(filename_species0 == '.');
        lastDotIndex = lastDotIndex(end);

        underscoreIndex = find(filename_species1 == '_');
        underscoreIndex = underscoreIndex(end);
        lastDotIndex = find(filename_species1 == '.');
        lastDotIndex = lastDotIndex(end);
        filename = filename_species1;
        disp(['Reading in ', filename])
        [x1,n1] = readChargeDensity1D(filename);
        disp('Done reading the file.')
        underscoreIndex = find(filename_species1 == '_');
        underscoreIndex = underscoreIndex(end);
        lastDotIndex = find(filename_species1 == '.');
        lastDotIndex = lastDotIndex(end);

        n = n1+n0;

        plot(x0-0.1,n)
        ylim([-2e-10 2e-10]);
        xlim([-0.1 0.2])
        xlabel('Distance (m)')
        ylabel('Charge Density (C/m^3)')
        title('Charge Density')
        figure()
        plot(x0-0.1,-n0/(1.6e-19))
        hold on
        plot(x0-0.1,n1/(1.6e-19))
        hold off
        figure()
        semilogy(x0-0.1,-n0/(1.6e-19))
        ylim([1e5, 1e9])
end