%directory = '../../meteor_runs/';
directory = '../../../meteor_1D/8575191/';
filenames = dir([directory 'efield_*.dat']);

for i = 1:numel(filenames)
    filename = [directory filenames(i).name];
    disp(['Reading in ', filename])
    underscoreIndex = find(filename == '_');
    underscoreIndex = underscoreIndex(end);
    lastDotIndex = find(filename == '.');
    lastDotIndex = lastDotIndex(end);
    [x,e] = readEField1D(filename);
    disp('Done reading the file.')
    
    figure(1)
	plot(x,e)
    title(['EField Time: ' filename(underscoreIndex+1:lastDotIndex-1)])
    pause(0.01)
    %M(i) = getframe(gcf);
end
%movie2avi(M, 'Movie_Efield.avi')
