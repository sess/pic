% This script will read in and plot the number of particles
%filename = '../../test_output/_npart.dat';
filename = '../../../meteor_1D/1e-11_sourceside/_npart.dat';
[t,n0,n1] = readNumParticles(filename);

figure()
plot(t,n0,'b','linewidth',2)
hold on
plot(t,n1,'r','linewidth',2)
hold off
legend('Electrons', 'Ions')
title('Number of Particles vs Time')
xlabel('Time')
ylabel('Number of Particles')

figure()
plot(t,n1-n0)
title('Ions minus Electrons vs Time')
xlabel('Time')
ylabel('Number of Particles')
