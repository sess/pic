%% This file generates a text file with voltage as a function of time for any boundaries marked "pulse"
% pulse is essentially a time dependent Dirichlet condition

% written by SK on 3rd May 2016


%% Square
outputFilename='pulse_500_0.txt';

pulseStartTime=5;
pulseEndTime=1005.1;
simulationDt=0.1;
simulationEndT=155.1;
baseVoltage=0;
pulseVoltage=-500;

t=0:simulationDt:simulationEndT;

V=zeros(length(t));
i=1:length(t);

if (pulseStartTime<0 || pulseEndTime>simulationEndT)
    error('Incorrect pulse properties!');
end

V= baseVoltage*(t<pulseStartTime)+pulseVoltage*(t>=pulseStartTime & t<=pulseEndTime)+baseVoltage * (t>pulseEndTime);
dlmwrite(outputFilename,V,'delimiter','\n','precision',16);

%% Linear Ramp

outputFilename='pulse_500_long_1000.txt';

pulseStartTime=5;
pulseEndTime=1005.1;
simulationDt=0.1;
simulationEndT=1005.1;

baseVoltage=0;
pulseUpRampTime=10;
pulseMaxVoltage=-500;
pulseDownRampTime=0;
t=0:simulationDt:simulationEndT;

V=zeros(length(t));
i=1:length(t);

if (pulseStartTime<0 || pulseEndTime>simulationEndT)
    error('Incorrect pulse properties!');
end
   
if (pulseUpRampTime<pulseStartTime || pulseDownRampTime<0)
    error('Incorrect pulse properties!');
end 

if (pulseUpRampTime+pulseDownRampTime>pulseEndTime-pulseStartTime)
    error('Incorrect pulse properties!');
end 

pulseUpSlope = (pulseMaxVoltage-baseVoltage)/pulseUpRampTime;
pulseDownSlope= (baseVoltage-pulseMaxVoltage)/pulseDownRampTime;
pulseDownSlope=0;
V= baseVoltage*(t<pulseStartTime)...
    +(baseVoltage+pulseUpSlope*(t-pulseStartTime)).*(t>=pulseStartTime & t<pulseStartTime+pulseUpRampTime)...
    +pulseMaxVoltage*(t>=pulseStartTime+pulseUpRampTime & t<pulseEndTime-pulseDownRampTime)...
    +(pulseMaxVoltage+pulseDownSlope*(t-(pulseEndTime-pulseDownRampTime))).*(t>=pulseEndTime-pulseDownRampTime & t<=pulseEndTime)...
    +baseVoltage * (t>pulseEndTime);

dlmwrite(outputFilename,V,'delimiter','\n','precision',16);
