% MATLAB script used for generating time changing neutral density
% collisions for PIC.
% Written by Glenn Sugar on 5/20/2016

% define the neutral density via an anonymous function
%a = 2.37e20;
a = 1e19;
tau = 0.15056;
if ~exist('velDrift', 'var')
    velDrift = [0 -60000]; % the speed of the meteoroid (m/s)
end
if ~exist('velTherm', 'var')
    velTherm = [244 244];  % the thermal velocity of the neutrals
end
%getNeutralDensity = @(t) a*exp(t./tau);
getNeutralDensity = @(t) ones(size(t))*a;
getDriftVelocity = @(t) repmat(velDrift, [numel(t), 1]);
getThermVelocity = @(t) repmat(velTherm, [numel(t), 1]);
if ~exist('nRealToNSim', 'var')
    nRealToNSim = 1e13;
end
if ~exist('T', 'var')
    T = 1;
end
if ~exist('dt', 'var')
    dt = 0.001;
end
% define the filename
if ~exist('filename', 'var')
    filename = '../neutral_densities/test.txt';
end

% DON'T CHANGE BELOW
timeArray = 0:dt:(T+dt);
neutralDensities = getNeutralDensity(timeArray);
velocitiesDrift = getDriftVelocity(timeArray);
velocitiesTherm = getThermVelocity(timeArray);
if exist(filename, 'file') == 2
    disp(['Note that the file ', filename, ' already exists!'])
    userInput = input('Do you want to overwrite? 1: yes, 0: no \n');
else
    userInput = 1;
end

if userInput == 1
    writeArray = [neutralDensities' velocitiesTherm velocitiesDrift]; 
    disp(['Writing the neutral collisions file in ', filename])
    dlmwrite(filename, writeArray, 'delimiter', ' ')
    disp('Done!')
else
    disp('The file was not overwritten, please use a different filename.')
end
