% Test file for charge to grid interpolation
% Written by Anthony Corso 08/12/2015

% Import file using the Import functionality of MATLAB
% No need to reshape for use with scatter3

figure(1); clf
scatter3(X,Y, rho,10,rho); hold on;
dx = X(2) - X(1);
dy = Y(11) - Y(1);
az = 0; % azimuth for camera
el = 90; % angle for camera
view(az, el); % change view to look from above

% This is the actual location of the particle
q = 1;
px = 0;
py = 0;
lx = 3*dx; ly = 8*dy;
ux = 4*dx; uy = 9*dy;

x = lx; y = uy;
W = (dx - abs(x-px))*(dy - abs(y-py))/(dx*dy);
top_left = W*q/(dx*dy)

x = ux; y = uy;
W = (dx - abs(x-px))*(dy - abs(y-py))/(dx*dy);
top_right = W*q/(dx*dy)

x = lx; y = ly;
W = (dx - abs(x-px))*(dy - abs(y-py))/(dx*dy);
bottom_left = W*q/(dx*dy)

x = ux; y = ly;
W = (dx - abs(x-px))*(dy - abs(y-py))/(dx*dy);
bottom_right = W*q/(dx*dy)

colorbar
