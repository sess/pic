%directory = '../../meteor_runs/';
directory = '../../meteor_2d/8600612/';
dt = 1e-9;
filenames = dir([directory 'efield_*.dat']);
fig1 = figure(2);

for i = 1:numel(filenames)
    filename = [directory filenames(i).name];
    disp(['Reading in ', filename])
    underscoreIndex = find(filename == '_');
    underscoreIndex = underscoreIndex(end);
    lastDotIndex = find(filename == '.');
    lastDotIndex = lastDotIndex(end);
    readEField
    disp('Done reading the file.')
    x = reshape(x,sqrt(numel(x)),sqrt(numel(x)));
    y = reshape(y,sqrt(numel(x)),sqrt(numel(x)));
    ex = reshape(ex,sqrt(numel(x)),sqrt(numel(x)));
    ey = reshape(ey,sqrt(numel(x)),sqrt(numel(x)));

    
    
    figure(fig1)
    subplot(1,2,1)
    imagesc(x(:,1),y(1,:),ex);
    title(['EField X Time: ' num2str(dt*str2num(filename(underscoreIndex+1:lastDotIndex-1))) ' sec'])
    colorbar
    caxis([-1e-7 1e-7])
    
    subplot(1,2,2)
    imagesc(x(:,1),y(1,:),ey);
    title(['EField Y Time: ' num2str(dt*str2num(filename(underscoreIndex+1:lastDotIndex-1))) ' sec'])
    colorbar
    caxis([-1e-7 1e-7])
    
    pause(0.01)
    M(i) = getframe(fig1);
end
movie2avi(M, 'Movie_Efield.avi')
