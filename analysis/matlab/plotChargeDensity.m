dt = 1e-9;
directory = '../../meteor_2d/8600612/';
filenames_species0 = dir([directory 'rho_species_0_*.dat']);
filenames_species1 = dir([directory 'rho_species_1_*.dat']);
%filenames = dir('../test_output3/potential*.dat');
plot_movie = 1;
plot_charge = 0;
if plot_movie > 0
    fig1 = figure();
    fig2 = figure();
    for i = 1:numel(filenames_species0)
        filename_species0 = [directory filenames_species0(i).name];
        filename_species1 = [directory filenames_species1(i).name];
        underscoreIndex = find(filename_species0 == '_');
        underscoreIndex = underscoreIndex(end);
        lastDotIndex = find(filename_species0 == '.');
        lastDotIndex = lastDotIndex(end);
        filename = filename_species0;
        disp(['Reading in ', filename])
        chargeDensityReader
        disp('Done reading the file.')
        underscoreIndex = find(filename_species0 == '_');
        underscoreIndex = underscoreIndex(end);
        lastDotIndex = find(filename_species0 == '.');
        lastDotIndex = lastDotIndex(end);
        x0 = reshape(x,sqrt(numel(x)),sqrt(numel(x)));
        y0 = reshape(y,sqrt(numel(x)),sqrt(numel(x)));
        n0 = reshape(n,sqrt(numel(x)),sqrt(numel(x)));

        underscoreIndex = find(filename_species1 == '_');
        underscoreIndex = underscoreIndex(end);
        lastDotIndex = find(filename_species1 == '.');
        lastDotIndex = lastDotIndex(end);
        filename = filename_species1;
        disp(['Reading in ', filename])
        chargeDensityReader
        disp('Done reading the file.')
        underscoreIndex = find(filename_species1 == '_');
        underscoreIndex = underscoreIndex(end);
        lastDotIndex = find(filename_species1 == '.');
        lastDotIndex = lastDotIndex(end);
        x1 = reshape(x,sqrt(numel(x)),sqrt(numel(x))) - 1.5;
        y1 = reshape(y,sqrt(numel(x)),sqrt(numel(x))) - 1.5;
        n1 = reshape(n,sqrt(numel(x)),sqrt(numel(x)));

        n = n1+n0;

        %contourf(x0,y0,n);
        figure(fig1)
        if plot_charge
            figure(fig1)
            imagesc(x1(:,1),y1(1,:),n);
            xlabel('Distance from Meteoroid (m)')
            ylabel('Distance from Meteoroid (m)')
            title(['Charge Density Time: ' num2str(dt*str2num(filename_species0(underscoreIndex+1:lastDotIndex-1))) ' sec'])
            colorbar
            %caxis([-7e5,7e5])
            caxis([-7e-9,7e-9])
            %ylim([0.9e-7 1.1e-7])
            %caxis([-2,2])
            figure(fig2)
            imagesc(x1(:,1),y1(1,:),n);
            title(['Charge Density Time: ' num2str(dt*str2num(filename_species0(underscoreIndex+1:lastDotIndex-1))) ' sec'])
            xlabel('Distance from Meteoroid (m)')
            ylabel('Distance from Meteoroid (m)')
            colorbar
            %caxis([-7e5,7e5])
            caxis([-1e-7,1e-7])
        else
            figure(fig1)
            imagesc(x1(:,1),y1(1,:),log10(-n0/(1.6e-19)));
            caxis([10 13])
            cbar = get(colorbar,'YTick');
            xlabel('Distance from Meteoroid (m)')
            ylabel('Distance from Meteoroid (m)')
            title(['Electron Density Time: ' num2str(dt*str2num(filename_species0(underscoreIndex+1:lastDotIndex-1))) ' sec'])
            colorbar('YTickLabel',10.^cbar);
            figure(fig2)
            imagesc(x1(:,1),y1(1,:),log10(n1/(1.6e-19)));
            caxis([10 14])
            cbar = get(colorbar,'YTick');
            xlabel('Distance from Meteoroid (m)')
            ylabel('Distance from Meteoroid (m)')
            title(['Ion Density Time: ' num2str(dt*str2num(filename_species0(underscoreIndex+1:lastDotIndex-1))) ' sec'])
            colorbar('YTickLabel',10.^cbar);
        end

            
        pause(0.01)
        M1(i) = getframe(fig1);
        M2(i) = getframe(fig2);
    end
    if plot_charge
        movie2avi(M1, [directory 'Movie_ChargeDensity.avi'])
        movie2avi(M2, [directory 'Movie_ChargeDensity2.avi'])
    else
        movie2avi(M1, [directory 'Movie_ElectronDensity.avi'])
        movie2avi(M2, [directory 'Movie_IonDensity2.avi'])
    end
        
else
    i = numel(filenames_species0)-1;
    filename_species0 = [directory filenames_species0(i).name];
    filename_species1 = [directory filenames_species1(i).name];
    underscoreIndex = find(filename_species0 == '_');
    underscoreIndex = underscoreIndex(end);
    lastDotIndex = find(filename_species0 == '.');
    lastDotIndex = lastDotIndex(end);
    filename = filename_species0;
    disp(['Reading in ', filename])
    chargeDensityReader
    disp('Done reading the file.')
    underscoreIndex = find(filename_species0 == '_');
    underscoreIndex = underscoreIndex(end);
    lastDotIndex = find(filename_species0 == '.');
    lastDotIndex = lastDotIndex(end);
    x0 = reshape(x,sqrt(numel(x)),sqrt(numel(x)));
    y0 = reshape(y,sqrt(numel(x)),sqrt(numel(x)));
    n0 = reshape(n,sqrt(numel(x)),sqrt(numel(x)));
    
    underscoreIndex = find(filename_species1 == '_');
    underscoreIndex = underscoreIndex(end);
    lastDotIndex = find(filename_species1 == '.');
    lastDotIndex = lastDotIndex(end);
    filename = filename_species1;
    disp(['Reading in ', filename])
    chargeDensityReader
    disp('Done reading the file.')
    underscoreIndex = find(filename_species1 == '_');
    underscoreIndex = underscoreIndex(end);
    lastDotIndex = find(filename_species1 == '.');
    lastDotIndex = lastDotIndex(end);
    x1 = reshape(x,sqrt(numel(x)),sqrt(numel(x)));
    y1 = reshape(y,sqrt(numel(x)),sqrt(numel(x)));
    n1 = reshape(n,sqrt(numel(x)),sqrt(numel(x)));
    
    n = n1+n0;
    
    %contourf(x0,y0,n);
    imagesc(x1(:,1),y1(1,:),n);
    title(['Charge Density Time: ' num2str(dt*str2num(filename_species0(underscoreIndex+1:lastDotIndex-1))) ' sec'])
    colorbar
    %caxis([-7e5,7e5])
    caxis([-7e-9,7e-9])
end

imagesc(x1(:,1),y1(1,:),log10(-n0/(1.6e-19)));
cbar = get(colorbar,'YTick');
colorbar('YTickLabel',10.^cbar);
title('Electron Density')

