% This script will read in and plot the number of particles
%filename = '../../test_output/_npart.dat';
filename = '../../meteor_small4/_npart.dat';
readNumParticles
numSims = 1;
numParticles = numParticles0+numParticles1;

zeroIndex = find(time==0);
zeroIndex = [1,zeroIndex;numel(time)];
colors = ['b','r','g','k','m','c',];
for i = 1:numSims
    color = colors(mod(i-1,numel(colors))+1);
    plot(time(zeroIndex(end-i):zeroIndex(end-i+1)-2),...
         numParticles(zeroIndex(end-i):zeroIndex(end-i+1)-2),color)
    hold on
end
hold off
title('Number of Particles vs Time')
xlabel('Time')
ylabel('Number of Particles')
