%directory = '../../meteor_runs/';
directory = '../../meteor_small_runs/time15/';
filenames = dir([directory 'phi_*.dat']);

for i = 1:numel(filenames)
    filename = [directory filenames(i).name];
    disp(['Reading in ', filename])
    underscoreIndex = find(filename == '_');
    underscoreIndex = underscoreIndex(end);
    lastDotIndex = find(filename == '.');
    lastDotIndex = lastDotIndex(end);
    readPotential
    disp('Done reading the file.')
    x = reshape(x,sqrt(numel(x)),sqrt(numel(x)));
    y = reshape(y,sqrt(numel(x)),sqrt(numel(x)));
    phi = reshape(phi,sqrt(numel(x)),sqrt(numel(x)));

    imagesc(x(:,1),y(1,:),phi);
    title(['Potential Time: ' filename(underscoreIndex+1:lastDotIndex-1)])
    colorbar
    
    pause(0.01)
    %M(i) = getframe(gcf);
end
%movie2avi(M, 'Movie_Efield.avi')
