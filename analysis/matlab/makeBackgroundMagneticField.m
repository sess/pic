% MATLAB script used for generating background B field for PIC
% Written by Glenn Sugar on 2/8/2016

% define the magnetic field function (use anonymous function)
%getBField = @(x,y,z) [0*ones(size(x)); 200*sin(linspace(pi/2-0.2,pi/2+0.2,numel(y))); 0*ones(size(z))];
%getBField = @(x,y,z) repmat([0; 0; 200], [1, numel(x)]);
%getBField = @(x,y,z) [0*ones(size(x)); 0*ones(size(y));abs(200*(z-mean(z))/max(z))];
m = [0; 0; 10.0];
getBField = @(x,y,z) ([3*dot([x-0.5; y-0.5; z-0.5], repmat(m, [1,size(x,2)])).*(x-0.5); ...
    3*dot([x-0.5; y-0.5; z-0.5], repmat(m, [1,size(x,2)])).*(y-0.5);
    3*dot([x-0.5; y-0.5; z-0.5], repmat(m, [1,size(x,2)])).*(z-0.5)]./ ...
    repmat(((x-0.5).^2+(y-0.5).^2+(z-0.5).^2).^(5./2), [3,1]) - ...
    [repmat(m(1), [1,size(x,2)]);
    repmat(m(2), [1,size(x,2)]);
    repmat(m(3), [1,size(x,2)])]./ ...
    repmat(((x-0.5).^2+(y-0.5).^2+(z-0.5).^2).^(3./2), [3,1]))*0.1;
%getBField = @(x,y,z) [-700*sqrt((x-0.5).^2+(y-0.5).^2).*sin(atan2(y-0.5,x-0.5));
%    700*sqrt((x-0.5).^2+(y-0.5).^2).*cos(atan2(y-0.5,x-0.5));
%    0*ones(size(z))];

% define the filename
filename = '../background_magnetic_fields/DipoleMagneticField.txt';

% define the mesh parameters
x0 = 0.0; xf = 1.0; nx = 10;
y0 = 0.0; yf = 1.0; ny = 10;
z0 = 0.0; zf = 1.0; nz = 10;
ng = nx*ny*nz;

xArray = linspace(x0,xf,nx);
yArray = linspace(y0,yf,ny);
zArray = linspace(z0,zf,nz);
xIndexArray = 1:nx;
yIndexArray = 1:ny;
zIndexArray = 1:nz;
xGridIndex = mod((0:ng-1),nx)+1;
yGridIndex = mod(floor((0:ng-1)/nx),ny)+1;
zGridIndex = floor((0:ng-1)/(nx*ny))+1;
xGridValue = (xGridIndex-1)*(xf-x0)/(nx-1)+x0;
yGridValue = (yGridIndex-1)*(yf-y0)/(ny-1)+y0;
if nz == 1
    zGridValue = zeros(size(xGridValue));
else
    zGridValue = (zGridIndex-1)*(zf-z0)/(nz-1)+z0;
end
bField = getBField(xGridValue, yGridValue, zGridValue);
%see if the file exists
if exist(filename, 'file') == 2
    disp(['Note that the file ', filename, ' already exists!'])
    userInput = input('Do you want to overwrite? 1: yes, 0: no \n');
else
    userInput = 1;
end

if userInput == 1
    disp(['Writing the magnetic field file in ', filename])
    dlmwrite(filename,bField','delimiter',' ')
    disp('Done!')
else
    disp('The file was not overwritten, please use a different filename.')
end
figure()
quiver3(xGridValue,yGridValue,zGridValue,bField(1,:),bField(2,:),bField(3,:))
%     %see how many points there are
%     totalPoints = nx*ny*nz;
%     %set variables for status bar
%     currentPoint = 0;
%     counter = 0.0;
%     strCR = -1;
%     strDotsMaximum = 25;
%     bFieldArray = getBField(xArray,yArray,zArray);
%     disp(['Writing the magnetic field file in ', filename])
%     %open the file
%     fileID = fopen(filename,'w');
%     for i = 1:totalPoints
%         fprintf(fileID,'%1.5f %1.5f %1.5f\n', bField(:,i));
%         c = floor(i/totalPoints*100);
%         percentageOut = [num2str(c) '%% '];
%         percentageOut = [percentageOut repmat(' ',1,5-length(percentageOut)-1)];
%         nDots = floor(c/100*strDotsMaximum);
%         dotOut = ['[' repmat(':',1,nDots) repmat('-',1,strDotsMaximum-nDots) ']'];
%         strOut = [percentageOut dotOut];
%         if strCR == -1
%             fprintf(strOut)
%         else
%             fprintf([strCR strOut])
%         end
%         strCR = repmat('\b',1,length(strOut)-1);
%     end
%
%     for zi = 1:nz
%         for yi = 1:ny
%             for xi = 1:nx
%                 %calculate B field
%                 bField = getBField(xArray(xi),yArray(yi),zArray(zi));
%                 fprintf(fileID,'%1.5f %1.5f %1.5f\n', bField);
%                 currentPoint = currentPoint+1;
%                 %if mod(currentPoint,totalPoints/100) == 0
%                     counter = counter+1;
%                     c = floor(counter/totalPoints*100);
%                     percentageOut = [num2str(c) '%% '];
%                     percentageOut = [percentageOut repmat(' ',1,5-length(percentageOut)-1)];
%                     nDots = floor(c/100*strDotsMaximum);
%                     dotOut = ['[' repmat(':',1,nDots) repmat('-',1,strDotsMaximum-nDots) ']'];
%                     strOut = [percentageOut dotOut];
%                     if strCR == -1
%                         fprintf(strOut)
%                     else
%                         fprintf([strCR strOut])
%                     end
%                     strCR = repmat('\b',1,length(strOut)-1);
%                 %end
%             end
%         end
%     end
%    %fprintf('\n')
%    %fclose(fileID);