% Written by: Anthony Corso - 07/08/2015
% reads in the position data output from the sess-pic
clear; 

posData = csvread('../pos.csv');
Nparticles = size(posData,2) / 2;
Nsteps = size(posData,1);

xdata = reshape(posData(logical(repmat(mod(1:2*Nparticles, 2), Nsteps, 1))), Nsteps, Nparticles);
ydata = reshape(posData(logical(repmat(mod(2:2*Nparticles+1, 2), Nsteps, 1))), Nsteps, Nparticles);

figure(1)
for i=1:Nsteps
    scatter(xdata(i,:), ydata(i,:), 'o');
    title(['Particle Positions at timestep n=' num2str(i-1)]);
    xlabel('X'); ylabel('Y');
    xlim([-5 5]);
    ylim([-5 5]);
    pause();
end

