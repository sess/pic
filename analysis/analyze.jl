tic()
include("scale.jl")
include("read.jl")
include("generate_new_aggregate_quantities.jl")
include("plot_aggregates.jl")
include("plot_timeseries.jl")

# Including additional julia files for analysis
if length(ARGS) > 2
    for fileName in ARGS[3:end]
        println(string("Including file ", fileName))
        include(fileName)
    end
end

toc()

