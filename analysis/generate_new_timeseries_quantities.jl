function gen_new_data(frames)
    ## Compute the energy distribution if particle velocities are present
    if haskey(frames, "Velocity")
        println("Calculating particle energies from velocity")
        frames["Energy"] = Frames()
        for t in keys(frames["Velocity"])
            frames["Energy"][t] = _Frame_()
            for s in keys(frames["Velocity"][t].series)
                frames["Energy"][t].series[s] = mapslices(v->0.5*getSpecies(species, s).mass*dot(v,v), frames["Velocity"][t].series[s],2)
            end
        end
    end

    if haskey(frames, "Temperature")
        println("Calculating average temperature per species")
        frames["Temp"] = Frames()
        for t in keys(frames["Temperature"])
            frames["Temp"][t] = _Frame_()
            for s in keys(frames["Temperature"][t].series)
                frames["Temp"][t].series[s] = mean(frames["Temperature"][t].series[s],2)
            end
        end
    end
end