using DataStructures

# Switches for changing behavior
useScale = false # if false, the nondimensional scale will be used (i.e. 1) if true then the contents of scaling.csv will be used
useCbarLims = true #if false, the colorbar limits will be changeable, if true the color will have a min max defined by the full series min max (over all timesteps)
useLogScale = true # if false a linear color scale is used, if true, a log10 scale is used


FPS = 10;
frame_width = 600;
frame_height = 400;
frame_size = (frame_width, frame_height);

# Structure for storing filename and full file path
struct FileStruct
    name::String
    path::String
end

# Grid structure for plotting
struct Grid
    nDim::Int
    min::Array{Float64, 1}
    max::Array{Float64, 1}
    pts::Array{Int, 1}
end
Grid() = Grid(0, [], [], [])

struct Species
    name::String
    mass::Float64
    charge::Float64
end

struct Scale
    value::Float64
    units::String
end

function unitStr(prefix, scale)
    if scale.units == ""
        return prefix
    else
        return latexstring("\\rm{", prefix, "}\\ [", scale.units, "]");
    end
end

struct _Frame_
    series::OrderedDict{String, Array{Float64,2}}
end

_Frame_() = _Frame_(OrderedDict())
_Frame_(arr::Array{Float64,2}) = _Frame_(OrderedDict("" => arr));

Frames = Dict{Float64, _Frame_} # Defines a list of frames sorted by time