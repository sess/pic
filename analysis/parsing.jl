include("definitions.jl")

function getSpecies(species, name)
    for s in values(species)
        if s.name == name
            return s
        end
    end
end

timeSeriesRegex = r"^(.*)\_(i|t)(\d*).csv" # Matches anything with time/iteration information in file name
rankRegex = r"^rank(\d*)_(.*)" # Matches anything starting with rankx

# Remove unneccesary characters from data set name
cleanName(name) = replace(name, r"_", " ")

# Check to see if a file is a csv file
isCSV(fileName) = ismatch(r"^.*.csv", fileName)

# Check to see if a file has time/iteration information in its name
isTimeSeries(fileName) = ismatch(timeSeriesRegex, fileName)

# Check to see if a file has rank as a prefix to its name
hasRank(fileName) = ismatch(rankRegex, fileName)

# Get the simulation time from the filename l_type is 't' or 'i' value is the corresponding time or iteration, and dt is used if the supplied value is an iteration
function parseTime(l_type, val, dt)
    if l_type == "i"
        return parse(Float64, val)*dt
    elseif l_type == "t"
        return parse(Float64, val)
    end
end

# Get the name and time of a file that represents one frame of data
function parseTimeSeries(fileName, dt)
    f = removeRank(fileName);
    m = match(timeSeriesRegex, f)
    return cleanName(m.captures[1]), parseTime(m.captures[2], m.captures[3], dt)
end

# Get the cleaned-up name of a csv file (without the extension)
parseName(fileName) = cleanName(match(r"^(.*).csv", fileName).captures[1])

# Remove the rank information from a file name in order to be further parsed
function removeRank(fileName)
    if hasRank(fileName)
        return match(rankRegex, fileName).captures[2]
    else
        return fileName
    end
end

# Parse a csv file containing grid information
function parseGrid(filePath)
    data, headers = readdlm(filePath, ',', header=true)
    ndim = data[findin(headers, ["ndim"])[1]]
    min, max, pts= Vector{Float64}(), Vector{Float64}(), Vector{Int}()
    push!(min, data[findin(headers, ["xmin"])[1]])
    push!(max, data[findin(headers, ["xmax"])[1]])
    push!(pts, data[findin(headers, ["nx"])[1]])
    if ndim > 1
        push!(min, data[findin(headers, ["ymin"])[1]])
        push!(max, data[findin(headers, ["ymax"])[1]])
        push!(pts, data[findin(headers, ["ny"])[1]])
    end
    if ndim > 2
        push!(min, data[findin(headers, ["zmin"])[1]])
        push!(max, data[findin(headers, ["zmax"])[1]])
        push!(pts, data[findin(headers, ["nz"])[1]])
    end
    return Grid(ndim, min, max, pts)
end

# Get the species names in the order that they were written
function parseSpecies(filePath)
    data, headers = readdlm(filePath, ',', header=true)
    headers = broadcast(lstrip, headers)
    species  = Dict{Int, Species}()
    name_col = findin(headers, ["name"])[1]
    id_col = findin(headers, ["id"])[1]
    m_col = findin(headers, ["mass"])[1]
    q_col = findin(headers, ["charge"])[1]
    for i = 1:size(data, 1)
        name = rstrip(lstrip(data[i, name_col], ['"', ' ']),'"')
        mass = data[i,m_col]
        charge = data[i, q_col]
        species[data[i,id_col]] = Species(name, mass, charge)
    end
    return species
end

# Parse scaling data
function parseScaling(filePath)
    data, headers = readdlm(filePath, ',', header=true)
    headers = broadcast(lstrip, headers)
    scalings  = Dict{String, Scale}()
    for i = 1:size(data, 1)
        scalings[data[i,1]] = Scale(data[i, 2], data[i,3])
    end
    return scalings
end

function prepareFrameData(frameData, name, time, hasRank)
    # If this is the first data with this name, add an empty frames to the dictionary
    if !haskey(frameData, name)
        frameData[name] = Frames();
    end

    # If there is already an entry under this time and the file is not one of several ranks
    # then skip this file and print an error
    if haskey(frameData[name], time) && !hasRank
        error(string("Warning! Found duplicate entry w/o rank information. Name: ", name, " time: ", time))
    end

    # If there is no entry at the specified time add an empty _Frame_
    if !haskey(frameData[name], time)
        frameData[name][time] = _Frame_();
    end
end

# Parse a file that has stored properties per particle (i.e. position and velocity)
function addFrameFromParticleQuantities(frameData, fileName, filePath, dt, species)
    name, time = parseTimeSeries(fileName, dt)
    prepareFrameData(frameData, name, time, hasRank(fileName))
    d, header = readdlm(filePath, ',', header = true)
    for s_id in unique(d[:,1])
        s_name = species[s_id].name
        frameData[name][time].series[s_name] = vcat(
             get(frameData[name][time].series, s_name, Array{Float64, 2}(0,size(d, 2)-1)),
             d[d[:,1] .== s_id, 2:end])
    end
end

function addFrameWithSpeciesVectorQuantities(frameData, fileName, filePath, dt, species)
    name, time = parseTimeSeries(fileName, dt)
    prepareFrameData(frameData, name, time, hasRank(fileName))
    d, header = readdlm(filePath, ',', header = true)
    for s_id in unique(d[:,1])
        s_name = species[s_id].name
        frameData[name][time].series[s_name] = vcat(
             get(frameData[name][time].series, s_name, Array{Float64, 2}(0,size(d, 2)-1)),
             d[d[:,1] .== s_id, 2:end])
    end
end

# Parse a file that has grid quantities stored for different species (i.e. charge density)
function addFrameWithMultiSeries(frameData, fileName, filePath, dt)
    name, time = parseTimeSeries(fileName, dt)
    prepareFrameData(frameData, name, time, hasRank(fileName))
    data, header = readdlm(filePath, ',', header = true)
    for h_id in 1:length(header)
        dcol = data[:, h_id];
        frameData[name][time].series[header[h_id]] = reshape(dcol, length(dcol), 1);
    end
end

# Parse a file that stores grid infomration (independent of species i.e. fields and potential)
function addFrameWithSingleSeries(frameData, fileName, filePath, dt)
    name, time = parseTimeSeries(fileName, dt)
    prepareFrameData(frameData, name, time, hasRank(fileName))
    data, header = readdlm(filePath, ',',  header = true)
    frameData[name][time].series[name] = data;
end

