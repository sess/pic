function getTitle(series, dataset_name, t)
    if series != dataset_name
        string(series, " ", dataset_name, ",\n  t= ", @sprintf("%.2e", t))
    else
        string(dataset_name,",\n  t= ", @sprintf("%.2e", t))
    end
end


function make2DContourPlot(dataset_name, data, grid, scale, xscale ,tscale)
    xmin, xmax = grid.min[1]*xscale.value, grid.max[1]*xscale.value;
    ymin, ymax = grid.min[2]*xscale.value, grid.max[2]*xscale.value;

    x = linspace(xmin, xmax, grid.pts[1])
    y = linspace(ymin, ymax, grid.pts[2])

    anim = Dict{String, Animation}()
    plots = Dict{String, Array{Any,1}}()

    rows, cols, indices = getSubplotMachinery(data);
    index = 1;
    first = true
    zmin, zmax = Dict{String, Float64}(), Dict{String, Float64}()
    firstframe = Dict{String, Array{Float64, 2}}();
    allowLogScale = Dict{String, Bool}();
    for key in sort(collect(keys(data)))
        t = key*tscale.value;
        println(string("plotting ", dataset_name, " @ t=",t))
        labels = keys(data[key].series)
        for s in labels
            z = scale.value*reshape(data[key].series[s][:, 1], grid.pts...)'
            if first
                zmin1, zmax1 = minMax(data, scale, s);
                # asllow the log if everything is negative or if everything is positive (i.,e. if min/max have same sign)
                allowLogScale[s] = (zmin1*zmax1 > 0)
                if allowLogScale[s] && useLogScale
                    zmin1, zmax1 = minMax(data, scale, s, true, z)
                end
                zmin[s] = zmin1;
                zmax[s] = zmax1;
                firstframe[s] = z;
            end

            useLog = allowLogScale[s] && useLogScale

            # If we are allowed to a do a log scale and we want to then use this log method
            if useLog
                z = log10.(1 + z ./ firstframe[s]);
            end

            # Plot the desired window size
            p = plot(size=(frame_width + 270,frame_height + 100), ticks=nothing, axis=nothing, foreground_color_border = nothing)

            # Plot the contour as an inset
            contour!(x, y, z, colorbar=true, fill_z = z, fill=true, inset = bbox(70px,50px, frame_width*px,frame_height*px,:left,:top), subplot=2, xformatter = formatter(xmax), yformatter = formatter(ymax), title=getTitle(s, dataset_name, t), xlabel=unitStr("X", xscale), ylabel=unitStr("Y", xscale), fillrange=-Inf)

            # Create a new empty plot to use ylabel as colorbar label
            ylab = L"\log_{10}(1+x/x_0)"
            if !useLog
                ylab = unitStr("", scale)
            end
            plot!(ylabel = ylab, inset = bbox(200px + frame_width*px ,50px, frame_width*px,frame_height*px,:left,:top), subplot=3 , ticks=nothing, axis=nothing, foreground_color_border = nothing )


            if first
                anim[s] = Animation()
                plots[s] = Array{Any, 1}()

                # Save all of the initial plots before min/max scaling
                name = string(dataset_name, "_", s, "_initial")
                if s == dataset_name
                    name = string(dataset_name, "_initial")
                end
                saveFig(p, name);
            end
            # If we want, set the colorbar lims
            if useCbarLims
                plot!(clims = (zmin[s], zmax[s]));
            end


            # Add frames and plots to dictionary
            frame(anim[s], p)
            if index in indices
                push!(plots[s], p)
            end
        end
        first = false
        index = index + 1
    end

    # Output for each series
    for s in keys(plots)
        name_base = string(dataset_name, "_", s)
        if s == dataset_name
            name_base = dataset_name;
        end
        sz = ((frame_width + 270)*cols,(frame_height + 100)*rows);
        subplots = plot(plots[s]..., layout = (rows,cols), size=sz)
        saveFig(subplots, string(name_base, "_grid"))
        saveGif(anim[s], string(name_base, "_animation"),  FPS)
    end
end