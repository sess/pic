include("generate_new_timeseries_quantities.jl")
load_p_and_v_together =  (grid.nDim == 1 && "Position" in vid_names && "Velocity" in vid_names)
println("Making videos...")
for cname in vid_names
    if cname == "Velocity" && load_p_and_v_together
        continue
    end
    frames = Dict{String, Frames}();# Frames for a movie

    cd(output_directory)
    for file in files
        if isTimeSeries(file.path)
            name, time = parseTimeSeries(file.name, dt)
            if !(cname == "Position" && name == "Velocity" && load_p_and_v_together) && name != cname continue end
            println(string("parsing ", name, " @ ", time))
            if name in ["Position", "Velocity"]
                addFrameFromParticleQuantities(frames, file.name, file.path, dt, species)
            elseif name in ["Electric Field", "Magnetic Field", "Potential"]
                addFrameWithSingleSeries(frames, file.name, file.path, dt)
            elseif name in ["Velocity Variance", "Velocity Average", "Temperature"]
                addFrameWithSpeciesVectorQuantities(frames, file.name, file.path, dt, species)
            else
                addFrameWithMultiSeries(frames, file.name, file.path, dt)
            end
        end
    end

    gen_new_data(frames);

    # Phase space plotting for 1D simulations
    if grid.nDim == 1 && haskey(frames, "Position") && haskey(frames, "Velocity")
        makePhaseSpacePlot(frames["Position"], frames["Velocity"], grid,
        getScale(scalings, "Position"), getScale(scalings, "Velocity"), getScale(scalings, "Time"))
    end

    # Loop through data to make videos
    for key in keys(frames)
        makeVideoAndFrames(key, frames[key], grid, getScale(scalings, key), getScale(scalings, "Length"), getScale(scalings, "Time"))
    end
    cd(orig_dir)
end
println("Done making videos")