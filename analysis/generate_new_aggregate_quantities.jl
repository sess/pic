# Combine the energies by species and all together
energyTypes = ["Kinetic Energy", "Potential Energy", "Total Energy"]
println("Generating new data for plotting different energy measures")
index = 2;
for etype in energyTypes
    if haskey(plotData, etype)
        pdata = plotData[etype]
        headers = plotData_Headers[etype]
        for i in 2:length(headers)
            species_energy_name = string(headers[i], " Energy");
            if !haskey(plotData_Headers, species_energy_name)
                plotData_Headers[species_energy_name] = ["Time"]
                # set the time to the "species energy"
                plotData[species_energy_name] = reshape(pdata[:,1], length(pdata[:,1]),1)
                scale_aliases[species_energy_name] = "Energy";
            end
            push!(plotData_Headers[species_energy_name], etype)

            # hcat the current etype to the "species energy" array
            plotData[species_energy_name] = hcat(plotData[species_energy_name], pdata[:,i])
        end
    end
    index
end